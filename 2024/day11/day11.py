#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from functools import cache


class Day11(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.stones = None
        self.parse_file()

    def parse_file(self):
        stones = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                stones += [int(x) for x in line]
        self.stones = stones

    def apply_blinks_to_stone_brute_force(self, stone, num_blinks):
        mapresult = {}
        overall_max = 0
        stones = [stone]
        for i in range(num_blinks):
            for s in range(len(stones)):
                str_rep = str(stones[s])
                if stones[s] == 0:
                    stones[s] = 1
                elif len(str_rep)%2 == 0:
                    # The description specifies where the new stones go, but I don't think
                    # it actually matters, since we only care about the number of stones
                    stones[s] = int(str_rep[:len(str_rep)//2])
                    stones.append(int(str_rep[len(str_rep)//2:]))
                else:
                    stones[s] = stones[s] * 2024
            logging.info(f"After blink {i}: {stones}")
            if self.debug:
                overall_max = max(overall_max, max(stones))
                logging.info(f"Overall max: {overall_max}")
                input("enter to continue")
            mapresult[i+1] = len(stones)
        return len(stones), mapresult

    def count_stones_brute_force(self, num_blinks):
        total_stones = 0
        for stone in self.stones:
            logging.info(f"Stone {stone}")
            total_stones += (self.apply_blinks_to_stone_brute_force(stone, num_blinks))[0]
        return total_stones

    @cache
    def apply_blinks_to_stone_clever(self, stone, num_blinks):
        logging.info(f"{num_blinks} blinks left, check {stone}")
        str_rep = str(stone)
        if num_blinks == 0:
            result = 1
        elif stone == 0:
            result = self.apply_blinks_to_stone_clever(1, num_blinks-1)
        elif len(str_rep)%2 == 0:
            result = self.apply_blinks_to_stone_clever(int(str_rep[:len(str_rep)//2]), num_blinks-1)
            result += self.apply_blinks_to_stone_clever(int(str_rep[len(str_rep)//2:]), num_blinks-1)
        else:
            result = self.apply_blinks_to_stone_clever(stone*2024, num_blinks-1)
        return result

    def count_stones_clever(self, num_blinks):
        total_length = 0
        for stone in self.stones:
            logging.info(f"Test stone {stone}")
            total_length += self.apply_blinks_to_stone_clever(stone, num_blinks)
        return total_length



if __name__ == '__main__':
    parser = getBasicArgparser('day 11')

    args = parser.parse_args()

    day11 = Day11(args.i, args.v)

    blinks = 25
    print(f"Part 1: After {blinks} blinks, there are this many stones: {day11.count_stones_brute_force(blinks)}")
    blinks = 75
    print(f"Part 2: After {blinks} blinks, there are this many stones: {day11.count_stones_clever(blinks)}")
