#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point


START = 0
END = 9

UP = Point(-1,0)
LEFT = Point(0,-1)
RIGHT = Point(0,1)
DOWN = Point(1,0)

class Day10(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.map = None
        self.start_positions = []
        self.end_positions = []
        self.map_width = None
        self.map_height = None
        self.parse_file()

    def print_map(self, map):
        for line in map:
            logging.info(''.join([str(x) for x in line]))

    def parse_file(self):
        self.map = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                row = []
                for x in line:
                    ix = int(x)
                    row.append(ix)
                    if ix == START:
                        self.start_positions.append(Point(row=len(self.map), col=len(row)-1))
                    elif ix == END:
                        self.end_positions.append(Point(row=len(self.map), col=len(row)-1))
                self.map.append([int(x) for x in line])
        self.map_height = len(self.map)
        self.map_width = len(self.map[0])
        self.print_map(self.map)
        logging.info(f"Start positions: {self.start_positions}")
        logging.info(f"End positions: {self.end_positions}")

    def get_val(self,pos):
        return self.map[pos.row][pos.col]

    def get_adj_pos(self, pos):
        return [
            pos+UP,
            pos+LEFT,
            pos+RIGHT,
            pos+DOWN
        ]

    def get_valid_endings(self, cur_pos, recurse_n=0):
        total = []
        cur_val = self.get_val(cur_pos)
        for adj_pos in self.get_adj_pos(cur_pos):
            if 0 <= adj_pos.row < self.map_height and 0 <= adj_pos.col < self.map_width:
                adj_val = self.get_val(adj_pos)
                if adj_val == cur_val + 1:
                    if adj_val == END:
                        logging.info(recurse_n*' ' + f"Step {cur_pos} to {adj_pos}: {adj_val}!")
                        total.append(adj_pos)
                    else:
                        logging.info(recurse_n*' ' + f"Step {cur_pos} to {adj_pos}: {adj_val}")
                        total += self.get_valid_endings(adj_pos, recurse_n+1)
        return total

    def find_all_valid_endings(self):
        total = 0
        for start in self.start_positions:
            total_trails = []
            logging.info(f"Start from {start}")
            total_trails += self.get_valid_endings(start)
            total_valid_endings = set(total_trails)
            logging.info(f"Valid ending positions: {total_trails} (set {total_valid_endings})")
            total += len(total_valid_endings)
        return total

    def find_all_trail_paths(self):
        total = 0
        for start in self.start_positions:
            total_trails = []
            logging.info(f"Start from {start}")
            total_trails += self.get_valid_endings(start)
            logging.info(f"Valid ending positions: {total_trails}")
            total += len(total_trails)
        return total


if __name__ == '__main__':
    parser = getBasicArgparser('day 10')

    args = parser.parse_args()

    day10 = Day10(args.i, args.v)

    print(f"Part 1: There are {day10.find_all_valid_endings()} trails")
    print(f"Part 2: There are {day10.find_all_trail_paths()} trails")
