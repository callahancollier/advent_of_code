#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, Directions, ShortestPathCardinalBase
import numpy as np

EMPTY = '.'
WALL = '#'
START = 'S'
END = 'E'


class MazeTraverse(ShortestPathCardinalBase):
    def __init__(self, debug, start, end, array):
        super().__init__(debug, start, end, array)

    def manhattan(self, point):
        return super().manhattan(point)

    def involves_turn(self, pointA, pointB, pointC):
        if pointA is None or pointB is None or pointC is None:
            return False
        if pointA.row == pointB.row == pointC.row:
            return False
        if pointA.col == pointB.col == pointC.col:
            return False
        return True

    def get_neighbours(self, node, camefrom):
        candidates = super().get_neighbours(node, camefrom)
        prev = None if node not in camefrom.keys() else camefrom[node]
        neighbours = []
        for c in candidates:
            if self.array[c.row, c.col] != WALL:
                if prev is None or prev != c:
                    neighbours.append(c)
        logging.info(f" Node {node} from {prev} has neighbours {neighbours}")
        return neighbours

    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        prev = None if current not in camefrom.keys() else camefrom[current]
        if current == self.start:
            prev = current + Directions.LEFT # start out by moving east
        turn_score = 0
        if self.involves_turn(prev, current, neighbour_node):
            turn_score = 1000
        next_g_score =  turn_score + current_g_score + 1
        logging.info(f"   Next g score for {current} from {prev} to {neighbour_node}: {next_g_score} (was {current_g_score})")
        return next_g_score

    def tally_path(self, current, camefrom):
        disp = self.array.copy()
        logging.info(f"Camefrom {camefrom}")
        logging.info(f"Tally path current {current}")
        pathtotal = 1
        prev = None
        prev_prev = None
        disp[current.row, current.col] = 'o'
        pathlength = 0
        while current in camefrom.keys():
            prev_prev = prev
            prev = current
            current = camefrom[current]
            disp[current.row, current.col] = 'o'
            pathtotal += 1
            logging.info(f"              compare {prev_prev} {prev} {current}")
            if self.involves_turn(prev_prev, prev, current):
                pathtotal += 1000
                logging.info(f"{current}  (turn)")
            else:
                logging.info(current)
            pathlength += 1
        # Don't count the start point of the path
        pathtotal -= 1
        # But do check if we did a turn right at the beginning
        if self.involves_turn(prev, current, self.start+Directions.LEFT):
            logging.info(f"plus one final turn")
            pathtotal += 1000
        for r in disp:
            logging.info(''.join(r))
        logging.info(f"Path length: {pathlength}")
        return pathtotal



class Day16(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.maze = None
        self.start = None
        self.end = None
        self.parse_file()

    def parse_file(self):
        maze = []
        start = None
        end = None
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if START in line:
                    start = Point(row=len(maze), col=line.index(START))
                if END in line:
                    end = Point(row=len(maze), col=line.index(END))
                maze.append([x for x in line])
        self.maze = np.array(maze)
        self.start = start
        self.end = end

    def get_best_path(self):
        calculator = MazeTraverse(self.debug, self.start, self.end, self.maze.copy())
        return calculator.get_shortest_path()


if __name__ == '__main__':
    parser = getBasicArgparser('day 16')

    args = parser.parse_args()

    day16 = Day16(args.i, args.v)

    print(f"Part 1: Best path costs {day16.get_best_path()}")

    #134596 is too high