#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, ShortestPathCardinalBase
import numpy as np
from tqdm import tqdm


class Memory(ShortestPathCardinalBase):
    def __init__(self, debug, area):
        super().__init__(debug, Point(0, 0), Point(len(area)-1, len(area[0])-1), area)

    def get_neighbours(self, node, camefrom):
        neighbours = super().get_neighbours(node, camefrom)
        actual_neighbours = []
        for n in neighbours:
            if self.array[n.row, n.col] != -1:
                actual_neighbours.append(n)
        return actual_neighbours
    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        return current_g_score + self.array[neighbour_node.row, neighbour_node.col]

    def tally_path(self, current, camefrom):
        pathtotal = self.array[current.row, current.col]
        while current in camefrom.keys():
            current = camefrom[current]
            pathtotal += self.array[current.row, current.col]
        # Don't count the start point of the path
        pathtotal -= self.array[0, 0]
        return pathtotal


class Day18(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.holes = None
        self.area = None
        self.parse_file()

    def parse_file(self):
        walls = []
        maxidx = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(',')
                walls.append(Point(col=int(line[0]), row=int(line[1])))
                maxidx = max(maxidx, walls[-1].row)
                maxidx = max(maxidx, walls[-1].col)
        maxidx += 1
        self.area = np.array(maxidx*[maxidx*[1]])
        self.holes = walls

    def first_n_fall(self, n):
        area = self.area.copy()
        for i in range(n):
            hole = self.holes[i]
            area[hole.row, hole.col] = -1
        logging.info(area)
        return area

    def fall_and_get_path(self, n):
        area = self.first_n_fall(n)
        mem = Memory(self.debug, area)
        return mem.get_shortest_path()

    def fall_more_and_get_nonpath(self, n):
        area = self.first_n_fall(n)
        for i in tqdm(range(n,len(self.holes))):
            next = self.holes[i]
            area[next.row, next.col] = -1
            mem = Memory(self.debug, area)
            try:
                mem.get_shortest_path()
            except RuntimeError:
                return f"{next.col},{next.row}"
        logging.error("Not found")
        sys.exit(1)

if __name__ == '__main__':
    parser = getBasicArgparser('day 18')

    args = parser.parse_args()

    day18 = Day18(args.i, args.v)
    n=1024
    print(f"Part 1: After {n} bytes have fallen, shortest path scores {day18.fall_and_get_path(n)}")
    print(f"Part 2: After {n} bytes have fallen, path blocked by {day18.fall_more_and_get_nonpath(n)}")
