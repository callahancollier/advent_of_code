#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from dataclasses import dataclass, field
from copy import deepcopy
import math


@dataclass
class Registers:
    A : int
    B : int
    C : int

    def __str__(self):
        return f"(a={self.A},b={self.B},c={self.C})"
    __repr__ = __str__

@dataclass
class Instruction:
    opcode : int # three bits
    operand : int # three bits

    def __str__(self):
        return f"(opcode={self.opcode}, operand={self.operand})"
    __repr__ = __str__

@dataclass
class Computer:
    registers: Registers
    program : list[Instruction]
    instruction_pointer : int = 0
    output : list[str] = field(default_factory=list)
    instr_pt_save : list[int] = field(default_factory=list)

    @staticmethod
    def combo_operand(combo, registers):
        result = -1
        if combo == 7:
            logging.error(f"Illegal combo operand {combo}")
        elif combo == 6:
            result = registers.C
        elif combo == 5:
            result = registers.B
        elif combo == 4:
            result = registers.A
        else:
            result = combo
        #logging.info(f" Combo op {combo} results in {result}")
        return result

    def adv(self, combo_op):
        denominator = 2 ** self.combo_operand(combo_op, self.registers)
        division = self.registers.A // denominator
        self.registers.A = division
        self.instruction_pointer += 1
        print('.')
        #logging.info(f"adv")

    def adv_backwards(self, combo_op):
        # Integer division == bit shifts
        if combo_op == 4: #won't work for this case
            logging.warning(f"Made a bad assumption")
        self.registers.A = self.registers.A << self.combo_operand(combo_op, self.registers)
        return self.combo_operand(combo_op, self.registers)

    def bdv(self, combo_op):
        denominator = 2**self.combo_operand(combo_op, self.registers)
        division = self.registers.A // denominator
        self.registers.B = division
        self.instruction_pointer += 1
        #logging.info(f"bdv")

    def bdv_backwards(self, combo_op):
        if combo_op == 5: #won't work for this case
            logging.warning(f"Made a bad assumption")
        self.registers.B = self.registers.A * (2**self.combo_operand(combo_op, self.registers))
        return self.combo_operand(combo_op, self.registers)

    def cdv(self, combo_op):
        denominator = 2**self.combo_operand(combo_op, self.registers)
        division = self.registers.A // denominator
        self.registers.C = division
        self.instruction_pointer += 1
        #logging.info(f"cdv")

    def cdv_backwards(self, combo_op):
        if combo_op == 6: #won't work for this case
            logging.warning(f"Made a bad assumption")
        self.registers.C = self.registers.A * (2**self.combo_operand(combo_op, self.registers))
        return self.combo_operand(combo_op, self.registers)

    def bxl(self, literal_op):
        result = self.registers.B ^ literal_op
        self.registers.B = result
        self.instruction_pointer += 1
        #logging.info(f"bxl")

    def bxl_backwards(self, literal_op):
        self.registers.B = self.registers.B ^ literal_op

    def bst(self, combo_op):
        self.registers.B = self.combo_operand(combo_op, self.registers) % 8
        self.instruction_pointer += 1
        #logging.info(f"bst")

    def bst_backwards(self, combo_op):
        if combo_op == 5:
            logging.warning(f"Made a bad assumption")
        # Not sure about this one, b can be any b*8*n for any value of n
        return #We'll just try a no-op for now

    def jnz(self, literal_op):
        if self.registers.A == 0:
            self.instruction_pointer += 1
            return
        self.instruction_pointer = literal_op
        #logging.info(f"jnz")

    def jnz_backwards(self, literal_op):
        return
        # This is not general but it'll work for the input in question
        if self.registers.A == 0:
            self.instruction_pointer = 2
            return
        self.instruction_pointer = 0

    def bxc(self, op):
        result = self.registers.B ^ self.registers.C
        self.registers.B = result
        self.instruction_pointer += 1
        #logging.info(f"bxc")

    def bxc_backwards(self, op):
        self.registers.B = self.registers.B ^ self.registers.C

    def out(self, combo_op):
        val = self.combo_operand(combo_op, self.registers) % 8
        self.output.append(val)
        self.instruction_pointer += 1
        #logging.info(f"out")

    def out_backwards(self, combo_op):
        val = self.output[-1]
        if combo_op == 6:
            self.registers.C = val
        elif combo_op == 5:
            self.registers.B = val
        elif combo_op == 4:
            self.registers.A = val
        self.output = self.output[:-1]

    def is_program_done(self):
        if self.instruction_pointer >= len(self.program):
            logging.info(f"Instruction pointer followed this path: {self.instr_pt_save}")
            logging.info(f"Final state: {self.registers}, output={self.output}")
            return True
        return False

    def get_instruction(self):
        return self.program[self.instruction_pointer]

    def lookup_instr_function(self, opcode):
        instr_lookup = {
            0 : self.adv,
            1 : self.bxl,
            2 : self.bst,
            3 : self.jnz,
            4 : self.bxc,
            5 : self.out,
            6 : self.bdv,
            7 : self.cdv
        }
        return instr_lookup[opcode]

    def run_one_instruction(self):
        self.instr_pt_save.append(self.instruction_pointer)
        instruction = self.get_instruction()
        self.lookup_instr_function(instruction.opcode)(instruction.operand)
        logging.info(f"{self.instr_pt_save[-1]}: {instruction} gives {self.registers}, output={self.output}, IP={self.instruction_pointer}")


    def get_output(self):
        return ','.join([str(x) for x in self.output])

    def lookup_backwards_instr_function(self, opcode):
        instr_lookup = {
            0 : self.adv_backwards,
            1 : self.bxl_backwards,
            2 : self.bst_backwards,
            3 : self.jnz_backwards,
            4 : self.bxc_backwards,
            5 : self.out_backwards,
            6 : self.bdv_backwards,
            7 : self.cdv_backwards
        }
        return instr_lookup[opcode]

    def run_one_instruction_backwards(self):
        self.instruction_pointer = self.instr_pt_save[-1]
        self.instr_pt_save = self.instr_pt_save[:-1]
        instruction = self.get_instruction()
        res = self.lookup_backwards_instr_function(instruction.opcode)(instruction.operand)
        logging.info(f"{self.instr_pt_save[-1]}: {instruction} gives {self.registers}, output={self.output}, IP={self.instruction_pointer}")
        return res



REG_A = 'Register A: '
REG_B = 'Register B: '
REG_C = 'Register C: '
PROG = 'Program: '


class Day17(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.computer = None
        self.program = None
        self.parse_file()

    def parse_file(self):
        a = None
        b = None
        c = None
        program = None
        instrs = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if line.startswith(REG_A):
                    if a is not None:
                        logging.error(f"Malformed input {line}")
                        sys.exit(1)
                    a = int(line[len(REG_A):])
                elif line.startswith(REG_B):
                    if b is not None:
                        logging.error(f"Malformed input {line}")
                        sys.exit(1)
                    b = int(line[len(REG_B):])
                elif line.startswith(REG_C):
                    if c is not None:
                        logging.error(f"Malformed input {line}")
                        sys.exit(1)
                    c = int(line[len(REG_C):])
                elif line.startswith(PROG):
                    opcode = None
                    vals = line[len(PROG):].split(',')
                    program = [int(val) for val in vals]
                    for val in program:
                        if opcode is None:
                            opcode = val
                        else:
                            operand = val
                            instrs.append(Instruction(opcode=opcode, operand=operand))
                            opcode = None
        self.computer = Computer(registers=Registers(a,b,c), program=instrs)
        self.program = program
        logging.info(f"Start with registers: {self.computer.registers}")
        logging.info(f"Program: {self.computer.program}")

    def get_program_output(self):
        computer = deepcopy(self.computer)
        while not computer.is_program_done():
            computer.run_one_instruction()
        return computer.get_output()

    def find_reg_A_for_self_output_bruteforce(self):
        result = ''
        A = 1 #A is not going to be 0 or 1 so we can safely skip those
        length = len(result)
        while result != self.program:
            if len(result) < len(self.program):
                A = A << 3
            else:
                A += 1
            computer = deepcopy(self.computer)
            computer.registers.A = A
            while not computer.is_program_done():
                computer.run_one_instruction()
            #result = computer.get_output()
            result = [int(x) for x in computer.output]
            logging.info(f"Result is {result}")
            if len(result) != length and A > 0:
                length = len(result)
                logging.info(f"New digit after {A} (2^{math.log2(A)})")
                if self.debug:
                    input('enter')
            logging.info(f"Goal is {self.program}")
        return A

    def find_reg_A_for_self_output(self):
        # Go to the end
        computer = deepcopy(self.computer)
        computer.registers.A = 117440
        while not computer.is_program_done():
            computer.run_one_instruction()
        computer.instruction_pointer -= 1
        logging.info(f"IP start at {computer.instruction_pointer}")
        correct_output = self.program
        logging.info(f"Goal is {correct_output}")
        # Now start working backwards until we hit a shift of A
        n_correct_outputs = 1
        while n_correct_outputs < len(correct_output):
            print(f"--> Working on output {n_correct_outputs}/{len(correct_output)}")
            while computer.get_instruction().opcode != 0: #adv instruction
                n_bits = computer.run_one_instruction_backwards()
            logging.info(f"--> Check {n_bits} of A={computer.registers.A}")
            save = deepcopy(computer)
            for i in range(2**n_bits):
                computer = deepcopy(save)
                A = computer.registers.A + i
                computer.registers.A = A
                computer.run_one_instruction_backwards()
                while computer.get_instruction().opcode != 0:  # adv instruction
                    computer.run_one_instruction_backwards()
                while not computer.is_program_done():
                    computer.run_one_instruction()
                logging.info(f"--> Checked A={A}: output {computer.output} matches {correct_output[-1*n_correct_outputs-1:]}")
                if computer.output[-1*n_correct_outputs-1:] == correct_output[-1*n_correct_outputs-1:]:
                    save.registers.A = A
                    logging.info(f"--> A={A}")
                    n_correct_outputs += 1
                    break
            computer = save
            computer.run_one_instruction_backwards()
        return computer.registers.A

    def find_reg_A_for_self_output_smart_bruteforce(self):
        # Each time we lose bits of A (ie adv), save the state and number of unknown bits
        # Assume B and C don't depend on A
        result = []
        save = deepcopy(self.computer)
        while result != self.program:
            computer = deepcopy(save)
            computer.run_one_instruction()
            old_A = save.registers.A
            new_A = computer.registers.A
            n_shifts = 0
            while old_A != new_A:
                old_A = old_A >> 1
                n_shifts += 1
            logging.info(f"Reg A shifted by {n_shifts}")
            sys.exit(1)

    # For part 2, work backwards from the end until you do a shift on A
    # Then for the unknown bits on A (add 2^N_shift), see which one gets the right output
    # Pick that value for A at that point and keep working backwards

    # TODO for part 2: get the answer lol
    # Restructure the Computer class so instead of run_program it's run_instruction() and returns whether you're done
    # Then we can more easily go backwards then forwards then backwards then forwards etc


4
if __name__ == '__main__':
    parser = getBasicArgparser('day 17')

    args = parser.parse_args()

    day17 = Day17(args.i, args.v)

    print(f"Part 1: Program outputs {day17.get_program_output()}")
    print(f"Part 2: Program outputs itself when register A is {day17.find_reg_A_for_self_output()}")