#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, get_num_points_enclosed_by_loop
import numpy as np
import copy

UP = Point(-1, 0)
LEFT = Point(0, -1)
RIGHT = Point(0, 1)
DOWN = Point(1, 0)
DIRECTIONS = [
    UP, DOWN, LEFT, RIGHT
]


class Day12(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.plots = None
        self.garden = None
        self.garden_height = None
        self.garden_width = None
        self.parse_file()

    def parse_file(self):
        plots = {}
        garden = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                row = []
                for char in line:
                    if char not in plots.keys():
                        plots[char] = []
                    pt = Point(row=len(garden), col=len(row))
                    found = []
                    for section in range(len(plots[char])):
                        logging.info(f"{char}: Is {pt} contiguous to {plots[char][section]}?")
                        if self.is_contiguous(plots[char][section], pt):
                            found.append(section)
                            logging.info("yes")
                        else:
                            logging.info("no")
                    if len(found) == 0:
                        logging.info(f"Not found, {plots[char]} becomes")
                        plots[char].append([pt])
                        logging.info(f"   {plots[char]}")
                    elif len(found) == 1:
                        logging.info(f"One found, {plots[char]} becomes")
                        plots[char][found[0]].append(pt)
                        logging.info(f"   {plots[char]}")
                    else:
                        logging.info(f"Multiple found, {plots[char]} becomes")
                        #Multiple contiguous spaces, combine them together
                        new = []
                        comb = []
                        for i in range(len(plots[char])):
                            if i in found:
                                comb += plots[char][i]
                            else:
                                new.append(plots[char][i])
                        comb.append(pt)
                        new.append(comb)
                        plots[char] = new
                        logging.info(f"   {plots[char]}")
                    row.append(char)
                garden.append(row)
        self.plots = plots
        self.garden = garden
        self.garden_height = len(self.garden)
        self.garden_width = len(self.garden[0])
        logging.info(np.array(self.garden))
        logging.info(self.plots)

    def is_contiguous(self, lst, new_pt):
        for pt in lst:
            if self.is_adjacent(pt, new_pt):
                return True
        return False

    def is_adjacent(self, pt1, pt2):
        if abs(pt1.row - pt2.row) <= 1 and pt1.col == pt2.col:
            return True
        if abs(pt1.col - pt2.col) <= 1 and pt1.row == pt2.row:
            return True
        return False

    def display_region(self, region):
        disp = copy.deepcopy(self.garden)
        for r in range(self.garden_height):
            for c in range(self.garden_width):
                if Point(r,c) not in region:
                    disp[r][c] = '.'
        logging.info(np.array(disp))

    def get_num_edges(self, pt, garden):
        edges = 0
        for dir in DIRECTIONS:
            adj = pt+dir
            if adj.row < 0 or adj.row >= self.garden_height:
                #logging.info(f"  adj {adj} off row edge")
                edges += 1
            elif adj.col < 0 or adj.col >= self.garden_width:
                #logging.info(f"  adj {adj} off col edge")
                edges += 1
            elif garden[adj.row][adj.col] != self.garden[pt.row][pt.col]:
                #logging.info(f"  comp {garden[pt.row][pt.col]}{pt} to {garden[adj.row][adj.col]}{adj}")
                edges += 1
        logging.info(f"Check {garden[pt.row][pt.col]}{pt}: edges={edges}")
        return edges

    def get_perimeter(self, region):
        perim = 0
        for pt in region:
            perim += self.get_num_edges(pt, self.garden)
        return perim

    def garden_at(self, pt):
        if 0 <= pt.row < self.garden_height:
            if 0 <= pt.col < self.garden_width:
                return self.garden[int(pt.row)][int(pt.col)]
        raise ValueError

    def get_num_sides(self, name, region):
        # Super helpful for visualizing this:
        # https://www.reddit.com/r/adventofcode/comments/1hcxmpp/2024_day_12_part_2_visualisation_of_my_first/
        sides = 0
        ups = {}
        downs = {}
        lefts = {}
        rights = {}
        for row in range(self.garden_height):
            last_found_up = False
            last_found_down = False
            for col in range(self.garden_width):
                pt = Point(row,col)
                if self.garden[row][col] == name and pt in region:
                    #logging.info(f"  Check {pt}")
                    if pt.row == 0 or self.garden_at(pt+UP) != name:
                        if not last_found_up:
                            #logging.info("    UP")
                            if row not in ups.keys():
                                ups[row] = 0
                            ups[row] += 1
                            last_found_up = True
                    else:
                        last_found_up = False

                    if pt.row == self.garden_height-1 or self.garden_at(pt+DOWN) != name:
                        if not last_found_down:
                            #logging.info("    DOWN")
                            if row not in downs.keys():
                                downs[row] = 0
                            downs[row] += 1
                            last_found_down = True
                    else:
                        last_found_down = False
                else:
                    last_found_up = False
                    last_found_down = False

        for col in range(self.garden_width):
            last_found_left = False
            last_found_right = False
            for row in range(self.garden_height):
                pt = Point(row,col)
                if self.garden[row][col] == name and pt in region:
                    logging.info(f"  Check {pt}")
                    if pt.col == 0 or self.garden_at(pt+LEFT) != name:
                        if not last_found_left:
                            if col not in lefts.keys():
                                lefts[col] = 0
                            lefts[col] += 1
                            last_found_left = True
                    else:
                        last_found_left = False

                    if pt.col == self.garden_width-1 or self.garden_at(pt+RIGHT) != name:
                        if not last_found_right:
                            logging.info("    RIGHT")
                            if col not in rights.keys():
                                rights[col] = 0
                            rights[col] += 1
                            last_found_right = True
                    else:
                        last_found_right = False
                else:
                    last_found_left = False
                    last_found_right = False

        logging.info(f"U: {ups}")
        logging.info(f"D: {downs}")
        logging.info(f"L: {lefts}")
        logging.info(f"R: {rights}")
        for x in ups.values():
            sides += x
        for x in downs.values():
            sides += x
        for x in lefts.values():
            sides += x
        for x in rights.values():
            sides += x
        return sides

    def get_total_price_of_fence(self):
        price = 0
        for name in self.plots.keys():
            for region in self.plots[name]:
                self.display_region(region)
                area = len(region)
                perim = self.get_perimeter(region)
                logging.info(f"region {name}: {area}*{perim}")
                price += area*perim
        return price

    def get_total_price_of_fence_bulk(self):
        price = 0
        for name in self.plots.keys():
            for region in self.plots[name]:
                self.display_region(region)
                area = len(region)
                sides = self.get_num_sides(name, region)
                logging.info(f"region {name}: {area}*{sides}")
                price += area*sides
        return price


if __name__ == '__main__':
    parser = getBasicArgparser('day 12')

    args = parser.parse_args()

    day12 = Day12(args.i, args.v)

    print(f"Part 1: Total price of fence is {day12.get_total_price_of_fence()}")
    print(f"Part 2: Total price of fence is {day12.get_total_price_of_fence_bulk()}")