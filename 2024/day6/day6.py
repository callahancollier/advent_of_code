#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
from dataclasses import dataclass
from enum import Enum
import copy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point

class LoopException(Exception):
    pass


DIRS = [ '^', '>', 'v', '<']
N_DIRS = 4

MOVEMENT = {
    '^' : Point(-1, 0),
    '>' : Point(0,1),
    'v' : Point(1, 0),
    '<' : Point(0, -1)
}
EMPTY = '.'
OBSTRUCTION = '#'
VISITED = 'X'

@dataclass
class Lab:
    loc : np.array
    guard : Point
    guard_dir : str

    def get(self, point: Point):
        if point.row < 0 or point.col < 0:
            raise IndexError
        return self.loc[point.row, point.col]

    def set(self, point: Point, val):
        self.loc[point.row, point.col] = val

    def __str__(self):
        return '\n'.join([''.join(x) for x in self.loc])
    __repr__ = __str__


class Day6(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.lab = None
        self.parse_file()
        self.height = len(self.lab.loc)
        self.width = len(self.lab.loc[0])

        self.original_lab = copy.deepcopy(self.lab)

        if self.lab.guard == Point(-1,-1):
            logging.error("Guard position not found!")
            sys.exit(1)

    def reset(self):
        self.lab = copy.deepcopy(self.original_lab)

    def parse_file(self):
        lab = []
        guard = Point(-1,-1)
        guard_dir = '^'
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                lab.append([x for x in line])
                for dir in DIRS:
                    try:
                        idx = lab[-1].index(dir)
                        if guard != Point(-1, -1):
                            logging.error("Guard found twice")
                            sys.exit(1)
                        guard = Point(len(lab)-1, idx)
                        guard_dir = lab[-1][idx]
                    except ValueError:
                        pass
        self.lab = Lab(loc=np.array(lab), guard=guard, guard_dir=guard_dir)

    def rotate_right(self, current_dir):
        cur_idx = DIRS.index(current_dir)
        next_idx = (cur_idx+1) % N_DIRS
        return DIRS[next_idx]

    def traverse(self, lab):
        corners = []
        logging.info(f"Guard {lab.guard_dir} starts at {lab.guard}")
        while 0 <= lab.guard.row < self.height and 0 <= lab.guard.col < self.width:
            turned = False
            lab.set(lab.guard, lab.guard_dir)
            if lab.get(lab.guard + MOVEMENT[lab.guard_dir]) != OBSTRUCTION:
                lab.guard = lab.guard + MOVEMENT[lab.guard_dir]
            else:
                logging.info("Skip step")
            logging.info(f"Guard loc: {lab.get(lab.guard)}")
            logging.info(f"Guard {lab.guard_dir} moves to {lab.guard}")
            try:
                if lab.get(lab.guard+MOVEMENT[lab.guard_dir]) == OBSTRUCTION:
                    if (lab.guard,lab.guard_dir) in corners:
                        raise LoopException('Found loop!')
                    corners.append((lab.guard,lab.guard_dir))
                    lab.guard_dir = self.rotate_right(lab.guard_dir)
                    logging.info(f"Guard turns {lab.guard_dir}")
                    turned = True
            except IndexError:
                break
            lab.set(lab.guard, lab.guard_dir)
            logging.info(lab)
            if turned and self.debug:
                input('enter to continue')
        lab.set(lab.guard, lab.guard_dir)
        logging.info(f"Final state:\n{lab}")
        return np.sum([np.count_nonzero(lab.loc == x) for x in DIRS])


    def traverse_for_part_2(self, lab, do_part_2=False):
        corners = []
        loop_obstructions = []
        logging.info(f"Guard {lab.guard_dir} starts at {lab.guard}")
        start = True
        while 0 <= lab.guard.row < self.height and 0 <= lab.guard.col < self.width:
            turned = False
            lab.set(lab.guard, lab.guard_dir)
            if lab.get(lab.guard + MOVEMENT[lab.guard_dir]) != OBSTRUCTION:
                if not start:
                    lab.guard = lab.guard + MOVEMENT[lab.guard_dir]
                else:
                    start = False
            else:
                logging.info("Skip step")
            logging.info(f"Guard loc: {lab.get(lab.guard)} at {lab.guard}")
            if do_part_2:
                potential_obstruction = self.check_for_loop(lab)
                if potential_obstruction != Point(-1,-1):
                    if lab.get(potential_obstruction) == EMPTY:
                        new_lab = copy.deepcopy(lab)
                        new_lab.set(potential_obstruction, OBSTRUCTION)
                        try:
                            logging.info(f"Recurse with guard at {new_lab.guard_dir} {new_lab.guard}:\n{new_lab}")
                            if self.debug:
                                input("enter to continue.")
                            save = self.debug
                            #self.debug = False
                            self.traverse(new_lab)
                            self.debug = save
                            logging.info("DONE RECURSE")
                        except LoopException:
                            logging.info("FOUND LOOP!!!")
                            loop_obstructions.append(potential_obstruction)
            logging.info(f"Guard {lab.guard_dir} moves to {lab.guard}")
            try:
                if lab.get(lab.guard+MOVEMENT[lab.guard_dir]) == OBSTRUCTION:
                    if (lab.guard,lab.guard_dir) in corners:
                        raise LoopException('Found loop!')
                    corners.append((lab.guard,lab.guard_dir))
                    lab.guard_dir = self.rotate_right(lab.guard_dir)
                    logging.info(f"Guard turns {lab.guard_dir}")

                    if do_part_2:
                        potential_obstruction = self.check_for_loop(lab)
                        if potential_obstruction != Point(-1, -1):
                            if lab.get(potential_obstruction) == EMPTY:
                                new_lab = copy.deepcopy(lab)
                                new_lab.set(potential_obstruction, OBSTRUCTION)
                                try:
                                    logging.info(
                                        f"Recurse with guard at {new_lab.guard_dir} {new_lab.guard}:\n{new_lab}")
                                    if self.debug:
                                        input("enter to continue.")
                                    save = self.debug
                                    # self.debug = False
                                    self.traverse(new_lab)
                                    self.debug = save
                                    logging.info("DONE RECURSE")
                                except LoopException:
                                    logging.info("FOUND LOOP!!!")
                                    loop_obstructions.append(potential_obstruction)
                    turned = True
            except IndexError:
                break
            lab.set(lab.guard, lab.guard_dir)
            logging.info(lab)
            if turned and self.debug:
                input('enter to continue')
        lab.set(lab.guard, lab.guard_dir)
        logging.info(f"Final state:\n{lab}")
        if do_part_2:
            logging.info(f"Loops found at {loop_obstructions}")
            return len(loop_obstructions)
        return np.sum([np.count_nonzero(lab.loc == x) for x in DIRS])

    def check_for_loop(self, lab):
        if lab.guard.row >= self.height or lab.guard.col >= self.width:
            return Point(-1, -1)
        tmp = self.rotate_right(lab.guard_dir)
        if tmp == '>':
            row = lab.loc[lab.guard.row]
            try:
                idx = row.tolist().index(OBSTRUCTION, lab.guard.col)
                if idx >= 0:
                    if lab.get(lab.guard + MOVEMENT[lab.guard_dir]) != OBSTRUCTION:
                        logging.info(f"Potential loop to the {tmp}, obstruct at {lab.guard + MOVEMENT[lab.guard_dir]}")
                        return lab.guard + MOVEMENT[lab.guard_dir]
            except (ValueError,IndexError):
                pass
        elif tmp == '<':
            row = lab.loc[lab.guard.row]
            try:
                idx = row.tolist().index(OBSTRUCTION, 0, lab.guard.col)
                if idx >= 0:
                    if lab.get(lab.guard + MOVEMENT[lab.guard_dir]) != OBSTRUCTION:
                        logging.info(f"Potential loop to the {tmp}, obstruct at {lab.guard + MOVEMENT[lab.guard_dir]}")
                        return lab.guard + MOVEMENT[lab.guard_dir]
            except (ValueError,IndexError):
                pass
        elif tmp == '^':
            col = lab.loc[:, lab.guard.col]
            try:
                idx = col.tolist().index(OBSTRUCTION, 0, lab.guard.row)
                if idx >= 0:
                    if lab.get(lab.guard + MOVEMENT[lab.guard_dir]) != OBSTRUCTION:
                        logging.info(f"Potential loop to the {tmp}, obstruct at {lab.guard + MOVEMENT[lab.guard_dir]}")
                        return lab.guard + MOVEMENT[lab.guard_dir]
            except (ValueError,IndexError):
                pass
        elif tmp == 'v':
            col = lab.loc[:, lab.guard.col]
            try:
                idx = col.tolist().index(OBSTRUCTION, lab.guard.row)
                if idx >= 0:
                    if lab.get(lab.guard + MOVEMENT[lab.guard_dir]) != OBSTRUCTION:
                        logging.info(f"Potential loop to the {tmp}, obstruct at {lab.guard + MOVEMENT[lab.guard_dir]}")
                        return lab.guard + MOVEMENT[lab.guard_dir]
            except (ValueError,IndexError):
                pass
        return Point(-1, -1)

    #def traverse_and_cause_loops(self):
    #    corners = [self.lab.guard]
    #    loop_causers = []
    #    logging.info(f"Guard {self.lab.guard_dir} starts at {self.lab.guard}")
    #    while 0 <= self.lab.guard.row < self.height and 0 <= self.lab.guard.col < self.width:
    #        turned = False
    #        self.lab.set(self.lab.guard, self.lab.guard_dir)
    #        self.lab.guard = self.lab.guard + MOVEMENT[self.lab.guard_dir]
    #        logging.info(f"Guard loc: {self.lab.get(self.lab.guard)}")

    #        if self.check_for_loop():
    #            loop_causers.append(self.lab.guard + MOVEMENT[self.lab.guard_dir])
    #            logging.info(f"Potential loop to the {tmp}, obstruct at {loop_causers[-1]}")
    #            input("enter to continue...")
    #        #tmp = self.rotate_right(self.lab.guard_dir)
    #        #if tmp == '>':
    #        #    row = self.lab.loc[self.lab.guard.row]
    #        #    try:
    #        #        idx = row.tolist().index(OBSTRUCTION, self.lab.guard.col)
    #        #        if idx >= 0:
    #        #            if self.lab.get(self.lab.guard + MOVEMENT[self.lab.guard_dir]) != OBSTRUCTION:
    #        #                loop_causers.append(self.lab.guard + MOVEMENT[self.lab.guard_dir])
    #        #                logging.info(f"Potential loop to the {tmp}, obstruct at {loop_causers[-1]}")
    #        #                input("enter to continue...")
    #        #    except ValueError:
    #        #        pass
    #        #elif tmp == '<':
    #        #    row = self.lab.loc[self.lab.guard.row]
    #        #    try:
    #        #        idx = row.tolist().index(OBSTRUCTION, 0, self.lab.guard.col)
    #        #        if idx >= 0:
    #        #            if self.lab.get(self.lab.guard + MOVEMENT[self.lab.guard_dir]) != OBSTRUCTION:
    #        #                loop_causers.append(self.lab.guard + MOVEMENT[self.lab.guard_dir])
    #        #                logging.info(f"Potential loop to the {tmp}, obstruct at {loop_causers[-1]}")
    #        #                input("enter to continue...")
    #        #    except ValueError:
    #        #        pass
    #        #elif tmp == '^':
    #        #    col = self.lab.loc[:, self.lab.guard.col]
    #        #    try:
    #        #        idx = col.tolist().index(OBSTRUCTION, 0, self.lab.guard.row)
    #        #        if idx >= 0:
    #        #            if self.lab.get(self.lab.guard + MOVEMENT[self.lab.guard_dir]) != OBSTRUCTION:
    #        #                loop_causers.append(self.lab.guard + MOVEMENT[self.lab.guard_dir])
    #        #                logging.info(f"Potential loop to the {tmp}, obstruct at {loop_causers[-1]}")
    #        #                input("enter to continue...")
    #        #    except ValueError:
    #        #        pass
    #        #elif tmp == 'v':
    #        #    col = self.lab.loc[:, self.lab.guard.col]
    #        #    try:
    #        #        idx = col.tolist().index(OBSTRUCTION, self.lab.guard.row)
    #        #        if idx >= 0:
    #        #            if self.lab.get(self.lab.guard + MOVEMENT[self.lab.guard_dir]) != OBSTRUCTION:
    #        #                loop_causers.append(self.lab.guard + MOVEMENT[self.lab.guard_dir])
    #        #                logging.info(f"Potential loop to the {tmp}, obstruct at {loop_causers[-1]}")
    #        #                input("enter to continue...")
    #        #    except ValueError:
    #        #        pass

    #        #if self.lab.get(self.lab.guard) in DIRS:
    #        #    if self.lab.get(self.lab.guard) == self.rotate_right(self.lab.guard_dir):
    #        #        loop_causers.append(self.lab.guard + MOVEMENT[self.lab.guard_dir])
    #        #        logging.info(f"Potential loop! {loop_causers[-1]}")
    #        #        if self.debug:
    #        #            input("enter to continue...")
    #        logging.info(f"Guard {self.lab.guard_dir} moves to {self.lab.guard}")
    #        try:
    #            if self.lab.get(self.lab.guard+MOVEMENT[self.lab.guard_dir]) == OBSTRUCTION:
    #                self.lab.guard_dir = self.rotate_right(self.lab.guard_dir)
    #                corners.append(self.lab.guard)
    #                logging.info(f"Guard turns {self.lab.guard_dir}, corner at {corners[-1]}")
    #                turned = True
    #        except IndexError:
    #            break
    #        self.lab.set(self.lab.guard, self.lab.guard_dir)
    #        logging.info(self.lab)
    #        if turned and self.debug:
    #            input('enter to continue')
    #    self.lab.set(self.lab.guard, self.lab.guard_dir)
    #    logging.info(f"Final state:\n{self.lab}")
    #    return len(loop_causers)


if __name__ == '__main__':
    parser = getBasicArgparser('day 6')

    args = parser.parse_args()

    day6 = Day6(args.i, args.v)

    print(f"Part 1: Guard visits {day6.traverse(copy.deepcopy(day6.original_lab))} distinct positions")
    print(f"Part 2: Places to cause loops: {day6.traverse_for_part_2(day6.lab, True)}")

#2479 too high
#2201 too low
#2463 too high
#2261 is wrong