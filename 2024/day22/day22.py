#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
import numpy as np
from tqdm import tqdm
from collections import Counter


class Day22(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.secret_numbers = None
        self.parse_file()
        self.sequence_length = 4

    def parse_file(self):
        self.secret_numbers = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.secret_numbers.append(int(line))

    def mix(self, val1, val2):
        return val1 ^ val2

    def prune(self, val):
        return val % 16777216

    def get_next_number(self, current_number):
        # 64 == 2**6
        # 32 == 2**5
        # 16777216 == 2**24
        # 2048 == 2**11
        result = self.prune(self.mix(current_number << 6, current_number))
        result = self.prune(self.mix(result >> 5, result))
        result = self.prune(self.mix(result << 11, result))
        return result

    def generate_n_secret_numbers(self, n_iterations):
        zeros = np.zeros((n_iterations, len(self.secret_numbers)),dtype=int)
        secrets = np.vstack([np.array(self.secret_numbers), zeros]) #secret is column, iteration is row
        changes = np.zeros((n_iterations, len(self.secret_numbers)), dtype=int)
        self.bananalookup = [] #len(self.secret_numbers)*[{}]
        for i in range(len(self.secret_numbers)):
            self.bananalookup.append({})
        for i in range(n_iterations):
            for j in range(len(self.secret_numbers)):
                secrets[i+1,j] = self.get_next_number(secrets[i,j])
                changes[i,j] = secrets[i+1,j]%10 - secrets[i,j]%10
                sequence = changes[i+1-self.sequence_length:i+1,j]
                if len(sequence) == self.sequence_length:
                    seqstr = ','.join([str(x) for x in sequence])
                    #print(f"Monkey {j}: sequence is {seqstr}, i is {i}, range is [{i+1-self.sequence_length}:{i+1}]")
                    if seqstr not in self.bananalookup[j].keys():
                        self.bananalookup[j][seqstr] = secrets[i+1,j]%10
                logging.info(f"  After {i}, secret[{j}] becomes {secrets[i+1,j]}")
        for j in range(len(self.secret_numbers)):
            logging.info(f"After {n_iterations}, {self.secret_numbers[j]} becomes {secrets[-1,j]}")
        #print(self.bananalookup)
        return secrets, changes

    def get_sum_of_secrets_after_n(self, n_iterations):
        secrets, _ = self.generate_n_secret_numbers(n_iterations)
        return sum(secrets[-1,:])

    def sell_bananas_for_sequence(self, sequence, secrets, changes):
        bananas = 0
        length = len(sequence)
        if length != self.sequence_length:
            logging.error(f"something has gone wrong, {sequence}")
            sys.exit(1)
        for monkey in range(len(secrets[0])):
            logging.info(f"  Monkey {monkey}: seq {sequence} in {changes[:,monkey]}")
            strseq = ','.join([str(x) for x in sequence])
            if strseq in self.bananalookup[monkey].keys():
                bananas += self.bananalookup[monkey][strseq]
                #print(f"   Monkey {monkey} sells you {self.bananalookup[monkey][strseq]} for {strseq}")
            #print(f"   Monkey {monkey} sells you 0 for {strseq}, lookup {self.bananalookup[monkey]}")
            #try:
            #    idx = changes[:,monkey].tolist().index(sequence[-1])
            #    logging.info(f"  Try index {idx}: {changes[idx+1-len(sequence):idx+1,monkey] == sequence}")
            #    while idx < len(changes[:,monkey])-1 and not all(changes[idx+1-len(sequence):idx+1,monkey] == sequence):
            #        idx = changes[:, monkey].tolist().index(sequence[-1], idx+1)
            #        logging.info(f"  Try index {idx}: {changes[idx + 1 - len(sequence):idx + 1, monkey] == sequence}")
            #except ValueError:
            #    logging.info(f"  Nope")
            #    idx = len(changes[:,0])
            #logging.info(f"  Index {idx}")
            #if idx < len(changes[:,monkey])-1:
            #    logging.info(f"  Monkey {monkey} sells you {secrets[idx+1,monkey]%10} bananas at index {idx}")
            #    bananas += secrets[idx+1,monkey]%10
        return bananas

    def get_best_sequence(self, n_iterations):
        max_bananas = 0
        secrets, changes = self.generate_n_secret_numbers(n_iterations)
        checked_sequences = []
        for monkey in tqdm(range(len(self.secret_numbers))):
            for max_idx in range(self.sequence_length, n_iterations):
                logging.info(f"Changes {changes[:,monkey]} at index {max_idx} for monkey {monkey}")
                sequence = changes[max_idx-4:max_idx,monkey]
                strseq = ','.join([str(x) for x in sequence])
                if strseq in checked_sequences:
                    continue
                checked_sequences.append(strseq)
                bananas = self.sell_bananas_for_sequence(sequence, secrets, changes)
                logging.info(f"Sequence {sequence} for monkey {monkey} at index {max_idx}, sells for {bananas} bananas")
                logging.info(f"Max from {max_bananas}")
                max_bananas = max(max_bananas, bananas)
                logging.info(f"Max to {max_bananas}")
        return max_bananas

    def get_best_sequence_improved(self, n_iterations):
        _, _ = self.generate_n_secret_numbers(n_iterations)
        # combine the dictionaries
        combined_max = 0
        print("Combining dictionaries...")
        combined_bananalookup = {}
        for monkey in range(len(self.secret_numbers)):
            for banana_key in self.bananalookup[monkey].keys():
                if banana_key.count(',') != self.sequence_length-1:
                    print("Something has gone wrong!")
                    continue
                if banana_key not in combined_bananalookup:
                    combined_bananalookup[banana_key] = self.bananalookup[monkey][banana_key]
                else:
                    combined_bananalookup[banana_key] += self.bananalookup[monkey][banana_key]
                combined_max = max(combined_max, combined_bananalookup[banana_key])
        print(f"Combined dictionaries. Max is {combined_max}")
        return combined_max


if __name__ == '__main__':
    parser = getBasicArgparser('day 22')

    args = parser.parse_args()

    day22 = Day22(args.i, args.v)

    n = 2000
    print(f"Part 1: Adding the {n}th secrets together gives {day22.get_sum_of_secrets_after_n(n)}")
    print(f"Part 2: Bananas sold for {day22.get_best_sequence_improved(n)}")

    #1713 too low