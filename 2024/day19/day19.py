#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from functools import cache


class Day19(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.patterns = None
        self.towel_options = None
        self.parse_file()

    def parse_file(self):
        patterns = []
        towel_options = {}
        do_patterns = False
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    do_patterns = True
                    continue
                line = line[0]
                if do_patterns:
                    patterns.append(line)
                else:
                    line = line.split(', ')
                    for x in line:
                        if x[0] not in towel_options.keys():
                            towel_options[x[0]] = []
                        towel_options[x[0]].append(x)
        self.patterns = patterns
        self.towel_options = towel_options
        logging.info(f"Patterns: {self.patterns}")
        logging.info(f"Options: {self.towel_options}")

    def does_towel_work(self, towel, remaining_pattern):
        if towel == remaining_pattern[:len(towel)]:
            return True

    @cache
    def is_pattern_possible(self, pattern, recursion=1):
        if len(pattern) == 0:
            logging.info(recursion*' ' + f"All done!")
            return True
        if pattern[0] not in self.towel_options.keys():
            logging.info(recursion*' ' + f"All done.")
            return False
        for option in self.towel_options[pattern[0]]:
            logging.info(recursion*' ' + f"Does {option} work for {pattern}?")
            if self.does_towel_work(option, pattern):
                new_pattern = pattern[len(option):]
                logging.info(recursion*' ' + f"Yes! Pattern becomes {new_pattern}")
                if self.is_pattern_possible(new_pattern, recursion+1):
                    return True
        logging.info(recursion*' ' + f"All done")
        return False

    @cache
    def get_num_possible_arrangements(self, pattern, recursion=1):
        possible = 0
        if len(pattern) == 0:
            logging.info(recursion*' ' + f"All done!")
            return 1
        if pattern[0] not in self.towel_options.keys():
            logging.info(recursion*' ' + f"All done.")
            return possible
        logging.info(recursion*' ' + f"All options: {self.towel_options[pattern[0]]}")
        for option in self.towel_options[pattern[0]]:
            logging.info(recursion*' ' + f"Does {option} work for {pattern}?")
            if self.does_towel_work(option, pattern):
                new_pattern = pattern[len(option):]
                logging.info(recursion*' ' + f"Yes! Pattern becomes {new_pattern}")
                possible += self.get_num_possible_arrangements(new_pattern, recursion+1)
        logging.info(recursion*' ' + f"All done, {possible} possible")
        return possible

    def count_possible_towel_patterns(self):
        possible = 0
        for pattern in self.patterns:
            logging.info(f"Check {pattern}")
            if self.is_pattern_possible(pattern):
                possible += 1
        return possible

    def count_all_possible_towel_patterns(self):
        possible = 0
        for pattern in self.patterns:
            logging.info(f"Check {pattern}")
            new = self.get_num_possible_arrangements(pattern)
            logging.info(f"{new} options")
            possible += new
        return possible


if __name__ == '__main__':
    parser = getBasicArgparser('day 19')

    args = parser.parse_args()

    day19 = Day19(args.i, args.v)

    print(f"Part 1: Possible patterns: {day19.count_possible_towel_patterns()}")
    print(f"Part 2: Possible patterns: {day19.count_all_possible_towel_patterns()}")
