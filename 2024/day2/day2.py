#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day2(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.data = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                self.data.append([int(x) for x in line])

    def all_increasing_or_decreasing(self, slopes):
        all_increasing = True
        all_decreasing = True
        for slope in slopes:
            if slope > 0:
                logging.info(f"     {slope} ./")
                all_increasing = False
            if slope < 0:
                logging.info(f"     {slope} \.")
                all_decreasing = False
            if slope == 0:
                logging.info(f"     {slope} --")
                all_increasing = False
                all_decreasing = False
        logging.info(f"  {slopes}: increasing {all_increasing} decreasing {all_decreasing}")
        if all_increasing or all_decreasing:
            return True
        return False

    def all_differences_within(self, slopes, lower_range, upper_range):
        all_within = True
        for slope in slopes:
            if abs(slope) < lower_range or abs(slope) > upper_range:
                all_within = False
        return all_within
    def is_safe(self, row):
        derivatives = []
        for i in range(len(row)-1):
            derivatives.append(row[i+1] - row[i])
        if self.all_increasing_or_decreasing(derivatives):
            if self.all_differences_within(derivatives, 1, 3):
                return True
        return False

    def get_num_safe(self):
        safe = 0
        for row in self.data:
            pstr = "is not "
            if self.is_safe(row):
                safe += 1
                pstr = "is"
                logging.info(f"row {row} {pstr} safe")
        return safe

    def get_num_safe_with_one_removal(self):
        safe = 0
        for row in self.data:
            if self.is_safe(row):
                safe += 1
                continue
            for idx in range(len(row)):
                adjusted_row = row.copy()
                del adjusted_row[idx]
                if self.is_safe(adjusted_row):
                    safe += 1
                    break
        return safe


if __name__ == '__main__':
    parser = getBasicArgparser('day 2')

    args = parser.parse_args()

    day2 = Day2(args.i, args.v)
    print(f"Part 1: There are {day2.get_num_safe()} safe reports")
    print(f"Part 2: There are {day2.get_num_safe_with_one_removal()} safe reports")
