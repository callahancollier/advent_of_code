#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, Directions
from functools import cache
from tqdm import tqdm

FORBIDDEN = 'FORBIDDEN'
START = 'A'
BUTTON_PRESS = 'A'

NUMPAD = {
    '7': Point(0, 0),
    '8': Point(0, 1),
    '9': Point(0, 2),
    '4': Point(1, 0),
    '5': Point(1, 1),
    '6': Point(1, 2),
    '1': Point(2, 0),
    '2': Point(2, 1),
    '3': Point(2, 2),
    '0': Point(3, 1),
    'A': Point(3, 2),
    FORBIDDEN : Point(3,0)
}

DIRPAD = {
    FORBIDDEN: Point(0, 0),
    '^': Point(0, 1),
    'A': Point(0, 2),
    '<': Point(1, 0),
    'v': Point(1, 1),
    '>': Point(1, 2)
}

class Day21(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.codes = []
        self.cache = {}
        self.parse_file()

    def parse_file(self):
        codes = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                codes.append([x for x in line])
        self.codes = codes

    @cache
    def DEPR_get_pattern(self, remaining_code, moves, current_pos, is_dirpad, n_recurse=0):
        if is_dirpad:
            buttonlookup = DIRPAD
        else:
            buttonlookup = NUMPAD
        next_pos = buttonlookup[remaining_code[0]]
        move_down = next_pos.row - current_pos.row
        move_right = next_pos.col - current_pos.col
        updown_moves = move_down*'v' if move_down >= 0 else (-1*move_down)*'^'
        leftright_moves = move_right*'>' if move_right >= 0 else (-1*move_right)*'<'
        # < is farthest from A on the numeric keypad, so try to avoid <A if possible
        # ie, move right second if possible, move up second if possible
        # from best to worst, the best combinations are up+right, down+right, left+up, left+down (^>, v>, <^, <v)
        # < before v before ^ before >
        # Some observations:
        # You can't move right into the forbidden spaces
        # You can only move up into the forbidden col if it's the only thing above you
        #logging.info(n_recurse*' ' + f"right {move_right} down {move_down}")
        if move_right > 0 and move_down < 0: # ^>
            if current_pos.col != buttonlookup[FORBIDDEN].col or current_pos.row != buttonlookup[FORBIDDEN].row+1:
                moves += updown_moves + leftright_moves
            else:
                moves += leftright_moves + updown_moves
        elif move_right > 0 and move_down > 0: # v>
            if current_pos.col != buttonlookup[FORBIDDEN].col or abs(current_pos.row - buttonlookup[FORBIDDEN].row) > abs(move_down):
                moves += updown_moves + leftright_moves
            else:
                moves += leftright_moves + updown_moves
        elif move_right < 0 and move_down < 0: # <^
            #logging.info(n_recurse*' ' + f"abs {current_pos.col - buttonlookup[FORBIDDEN].col} > {move_right}")
            if current_pos.row != buttonlookup[FORBIDDEN].row or abs(current_pos.col - buttonlookup[FORBIDDEN].col) > abs(move_right):
                moves += leftright_moves + updown_moves
            else:
                moves += updown_moves + leftright_moves
        else: # <v
            if current_pos.row != buttonlookup[FORBIDDEN].row or abs(current_pos.col - buttonlookup[FORBIDDEN].col) > abs(move_right):
                moves += leftright_moves + updown_moves
            else:
                moves += updown_moves + leftright_moves
        moves += BUTTON_PRESS
        #logging.info(n_recurse*' ' + f"{remaining_code[0]}: Move from {current_pos} to {next_pos}, moves {moves} remaining {remaining_code[1:]}")
        if len(remaining_code) > 1:
            return self.get_pattern(remaining_code[1:], moves, next_pos, is_dirpad, n_recurse+1)
        return moves

    @cache
    def get_pattern(self, remaining_code, moves, current_pos, is_dirpad, n_recurse=0):
        next_button_press = remaining_code.index(BUTTON_PRESS)
        chunk = remaining_code[:next_button_press+1]
        remaining_segments = remaining_code[next_button_press+1:]
        while chunk in self.cache.keys() and len(remaining_segments) > 0:
            moves += self.cache[chunk]
            next_button_press = remaining_segments.index(BUTTON_PRESS)
            chunk = remaining_segments[:next_button_press+1]
            remaining_segments = remaining_segments[next_button_press+1:]
        new_moves = self.get_pattern_no_recurse(chunk, '', current_pos, is_dirpad, n_recurse+1)
        self.cache[chunk] = new_moves
        #logging.info(f"Update cache: {self.cache}")
        moves += new_moves
        #logging.info(n_recurse*' ' + f"{chunk}: Move from {current_pos}, moves {moves} remaining {remaining_segments}")
        if len(remaining_segments) > 1:
            return self.get_pattern(remaining_segments, moves, current_pos, is_dirpad, n_recurse+1)
        return moves

    def get_pattern_no_recurse(self, remaining_code, moves, current_pos, is_dirpad, n_recurse=0):
        if is_dirpad:
            buttonlookup = DIRPAD
        else:
            buttonlookup = NUMPAD
        for code in remaining_code:
            next_pos = buttonlookup[code]
            move_down = next_pos.row - current_pos.row
            move_right = next_pos.col - current_pos.col
            updown_moves = move_down*'v' if move_down >= 0 else (-1*move_down)*'^'
            leftright_moves = move_right*'>' if move_right >= 0 else (-1*move_right)*'<'
            # < is farthest from A on the numeric keypad, so try to avoid <A if possible
            # ie, move right second if possible, move up second if possible
            # from best to worst, the best combinations are up+right, down+right, left+up, left+down (^>, v>, <^, <v)
            # Some observations:
            # You can't move right into the forbidden spaces
            # You can only move up into the forbidden col if it's the only thing above you
            #logging.info(n_recurse*' ' + f"right {move_right} down {move_down}")
            if move_right > 0 and move_down < 0: # ^>
                if current_pos.col != buttonlookup[FORBIDDEN].col or current_pos.row != buttonlookup[FORBIDDEN].row+1:
                    moves += updown_moves + leftright_moves
                else:
                    moves += leftright_moves + updown_moves
            elif move_right > 0 and move_down > 0: # v>
                if current_pos.col != buttonlookup[FORBIDDEN].col or abs(current_pos.row - buttonlookup[FORBIDDEN].row) > abs(move_down):
                    moves += updown_moves + leftright_moves
                else:
                    moves += leftright_moves + updown_moves
            elif move_right < 0 and move_down < 0: # <^
                #logging.info(n_recurse*' ' + f"abs {current_pos.col - buttonlookup[FORBIDDEN].col} > {move_right}")
                if current_pos.row != buttonlookup[FORBIDDEN].row or abs(current_pos.col - buttonlookup[FORBIDDEN].col) > abs(move_right):
                    moves += leftright_moves + updown_moves
                else:
                    moves += updown_moves + leftright_moves
            else: # <v
                if current_pos.row != buttonlookup[FORBIDDEN].row or abs(current_pos.col - buttonlookup[FORBIDDEN].col) > abs(move_right):
                    moves += leftright_moves + updown_moves
                else:
                    moves += updown_moves + leftright_moves
            moves += BUTTON_PRESS
            #logging.info(n_recurse*' ' + f"{remaining_code[0]}: Move from {current_pos} to {next_pos}, moves {moves} remaining {remaining_code[1:]}")
            current_pos = next_pos
        return moves

    def punch_code_big(self, n_nested):
        # build the cache
        self.punch_code(5)
        total_sum = 0
        for c in tqdm(self.codes):
            code = ''.join(c)
            pattern = self.get_pattern_no_recurse(code, '', NUMPAD[START], False)

            print(self.cache)
            remaining = pattern
            lookup = {}
            while len(remaining) > 0:
                idx = remaining.index(BUTTON_PRESS)
                this_seg = remaining[:idx+1]
                remaining = remaining[idx+1:]
                if this_seg not in self.cache.keys():
                    logging.error(f"Bad cache missing {this_seg}")
                    sys.exit(1)
                res = self.cache[this_seg]
                while len(res) > 0:
                    nxt = res.index(BUTTON_PRESS)
                    if res[:nxt+1] not in lookup.keys():
                        lookup[res[:nxt+1]] = 0
                    lookup[res[:nxt+1]] += 1
                    res = res[nxt+1:]
            print(f"after first:\n{lookup}")

            for i in range(n_nested-1):
                lookup = self.do_loop(lookup)
                print(f"after {i}:\n{lookup}")

            for k in lookup.keys():
                total_sum += (len(k)*lookup[k])*self.get_numeric_part(c)

        return total_sum

    def do_loop(self, lookup):
        new_lookup = {}
        for seg in lookup.keys():
            if seg not in self.cache.keys():
                logging.error(f"Bad cache missing {seg}!")
                sys.exit(1)

            res = self.cache[seg]
            while len(res) > 0:
                nxt = res.index(BUTTON_PRESS)
                if res[:nxt + 1] not in new_lookup.keys():
                    new_lookup[res[:nxt + 1]] = 0
                new_lookup[res[:nxt + 1]] += lookup[seg]
                res = res[nxt + 1:]

        return new_lookup

    def punch_code(self, n_nested):
        complexity_sum = 0
        for code in tqdm(self.codes):
            to_disp = [''.join(code)]
            logging.info(f"Check {''.join(code)}")
            pattern = self.get_pattern(''.join(code), '', NUMPAD[START], False)
            logging.info(f"Numpad presses are {pattern}")
            to_disp.append(pattern)
            for i in range(n_nested-1):
                pattern = self.get_pattern(pattern, '', DIRPAD[START], True)
                logging.info(f"Un-nest {i}: {pattern}")
                to_disp.append(pattern)
            complexity_sum += self.get_complexity(code, pattern)
            self.disp_nice(to_disp)
        return complexity_sum

    def disp_nice(self, to_disp):
        new_disp = []
        for i in reversed(range(len(to_disp))):
            if i == len(to_disp)-1:
                new_disp.append(to_disp[i])
            else:
                string = ''
                idx = 0
                for j in range(len(new_disp[-1])):
                    if new_disp[-1][j] != BUTTON_PRESS:
                        string += ' '
                    else:
                        string += to_disp[i][idx]
                        idx += 1
                new_disp.append(string)
        for s in new_disp:
            logging.info(s)

    def get_numeric_part(self, code):
        str_num = ''
        for v in code:
            if v.isnumeric():
                str_num += v
        return int(str_num)

    def get_complexity(self, code, sequence):
        numeric = self.get_numeric_part(code)
        complexity = len(sequence) * numeric
        logging.info(f"Complexity score of {code} ({numeric}) and {len(sequence)}: {complexity}")
        return complexity


if __name__ == '__main__':
    parser = getBasicArgparser('day 21')

    args = parser.parse_args()

    day21 = Day21(args.i, args.v)

    print(f"Part 1: Sum of complexities is {day21.punch_code_big(2)}")
    print(f"Part 2: Sum of complexities is {day21.punch_code_big(25)}")

    #214454 too high
    #205950 too high
    #62737 too low
    #205658 is wrong
    #202450 is wrong
    #203814


    #example2.txt should give 76 for part 1
    #example3.txt should give 151826 for part 1
    #example.txt should give 154115708116294 for part 2

    #282720740452712 too high