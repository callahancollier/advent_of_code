#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point
import numpy as np
from dataclasses import dataclass
import copy
import itertools


EMPTY = '.'
ANTINODE = '#'

@dataclass(frozen=True)
class Antenna(Point):
    id : str

    def __str__(self):
        return "{}:({},{})".format(self.id, self.row, self.col)
    __repr__ = __str__

    def __eq__(self, other):
        if self.id != other.id:
            return False
        return super().__eq__(other)

    def __add__(self, other):
        new_row = self.row + other.row
        new_col = self.col + other.col
        return Point(row=new_row, col=new_col)


def reformat_for_print(city):
    return '\n'.join([''.join(x) for x in city])


class Day8(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.city = None
        self.antennas = None
        self.city_height = -1
        self.city_width = -1
        self.parse_file()

    def parse_file(self):
        city = []
        antennas = {}
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                city.append([x for x in line])
                for i in range(len(city[-1])):
                    loc = city[-1][i]
                    if loc != EMPTY:
                        if loc not in antennas:
                            antennas[loc] = []
                        antennas[loc].append(Point(row=(len(city)-1), col=i))
                    elif loc == ANTINODE:
                        city[-1][i] = EMPTY
        self.city = np.array(city)
        self.antennas = antennas
        self.city_height = len(self.city)
        self.city_width = len(self.city[0])
        logging.info(f"Antennas: {self.antennas}")
        logging.info(f"City: width {self.city_width} height {self.city_height}\n{reformat_for_print(self.city)}")

    def get_antinode1(self, higher, lower):
        newrow1 = higher.row - abs(higher.row - lower.row)
        newcol1 = higher.col + (higher.col - lower.col)
        return Point(row=newrow1, col=newcol1)

    def get_antinode2(self, higher, lower):
        newrow2 = lower.row + abs(higher.row - lower.row)
        newcol2 = lower.col - (higher.col - lower.col)
        return Point(row=newrow2, col=newcol2)

    def is_within_city(self, node):
        return 0 <= node.row < self.city_height and 0 <= node.col < self.city_width

    def find_antinodes(self, extended=False):
        antinodes = set()
        for a in self.antennas.keys():
            locations = self.antennas[a]
            logging.info(f"Check {a} antennas: {locations}")
            if len(locations) < 2:
                continue
            for comb in itertools.combinations(locations, 2):
                new_antinodes = []

                #(This does still work if they're horizontal)
                higher = comb[0]
                lower = comb[1]
                if comb[1].row < comb[0].row:
                    higher = comb[1]
                    lower = comb[0]

                newnode1 = self.get_antinode1(higher, lower)
                newnode2 = self.get_antinode2(higher, lower)
                new_antinodes.append(self.get_antinode1(higher, lower))
                new_antinodes.append(self.get_antinode2(higher, lower))

                if extended:
                    while self.is_within_city(newnode1):
                        logging.info(f"1 From {newnode1}")
                        new_newnode1 = self.get_antinode1(newnode1, higher)
                        logging.info(f"1 To {new_newnode1}")
                        new_antinodes.append(new_newnode1)
                        higher = newnode1
                        newnode1 = new_newnode1
                    while self.is_within_city(newnode2):
                        logging.info(f"2 From {newnode2} (lower {lower})")
                        new_newnode2 = self.get_antinode2(lower, newnode2)
                        logging.info(f"2 To {new_newnode2}")
                        new_antinodes.append(new_newnode2)
                        lower = newnode2
                        newnode2 = new_newnode2

                logging.info(f"Antennae {comb} give antinodes {new_antinodes}")
                for an in new_antinodes:
                    if self.is_within_city(an):
                        antinodes.add(an)

                if extended:
                    for antenna in self.antennas.keys():
                        for loc in self.antennas[antenna]:
                            antinodes.add(loc)
        if self.debug:
            disp = copy.deepcopy(self.city)
            for anode in antinodes:
                disp[anode.row, anode.col] = ANTINODE
            logging.info(reformat_for_print(disp))
        return len(antinodes)


if __name__ == '__main__':
    parser = getBasicArgparser('day 8')

    args = parser.parse_args()

    day8 = Day8(args.i, args.v)

    print(f"Part 1: Number of antinode locations is {day8.find_antinodes()}")
    print(f"Part 2: Number of antinode locations is {day8.find_antinodes(True)}")
