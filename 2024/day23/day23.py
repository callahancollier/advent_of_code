#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from dataclasses import dataclass
import itertools


@dataclass
class Connection:
    compA : str
    compB : str

    def __str__(self):
        return f"({self.compA}-{self.compB})"
    __repr__ = __str__


class Day23(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.lan = None
        self.connections = None
        self.parse_file()

    def parse_file(self):
        connections = []
        lan = {}
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split('-')
                connections.append(Connection(line[0], line[1]))
                if line[0] not in lan:
                    lan[line[0]] = [connections[-1]]
                else:
                    lan[line[0]].append(connections[-1])
                connections.append(Connection(line[1], line[0]))
                if line[1] not in lan:
                    lan[line[1]] = [connections[-1]]
                else:
                    lan[line[1]].append(connections[-1])
        self.lan = lan
        self.connections = connections
        logging.info(self.lan)

    def DEPR_get_sets_of_three_with_t(self):
        cmp_set = set()
        for group in self.lan.values():
            if len(group) >= 3:
                for computer in group:
                    if computer.find('t') > 0:
                        cmp_set.add(group.sort()) #Doesn't matter how it sorts as long as it's consistent
                        break
        return len(cmp_set)

    def get_sets_of_three_with_t(self):
        cmp_set = set()
        for tag in self.lan.keys():
            for conn1 in self.lan[tag]:
                for conn2 in self.lan[conn1.compB]:
                    for conn3 in self.lan[conn2.compB]:
                        logging.info(f"Check {conn1} -> {conn2} -> {conn3}")
                        if conn3.compB == tag:
                            lst = [conn1.compA, conn2.compA, conn3.compA]
                            lst.sort() # Don't care how it sorts as long as its consistent
                            logging.info(f"Yes! {lst}")
                            for name in lst:
                                test = name.find('t')
                                logging.info(f"Test {name}: {test}")
                                if test == 0:
                                    logging.info(f"Computer involving t found!")
                                    cmp_set.add(','.join([str(x) for x in lst]))

        logging.info(cmp_set)
        return len(cmp_set)

    def get_set_length(self, start, curr, lst):
        for conn in self.lan[curr]:
            logging.info(f"Start {start} curr {curr} check {conn}, {lst}")
            if conn.compB == start and len(lst) > 1:
                copy = lst.copy()
                copy.append(conn.compA)
                self.completed_lists.append(copy)
                logging.info(f"All done, final list is {copy}")
                sys.exit(1)
            if len(lst) > 0 and conn.compB in lst:
                continue
            copy = lst.copy()
            copy.append(conn.compA)
            self.get_set_length(start, conn.compB, copy)

    def get_longest_set_prev(self):
        self.completed_lists = []
        longest_set = []
        longest = 0
        for tag in self.lan.keys():
            self.get_set_length(tag, tag, [])
        group = self.completed_lists[-1]
        if len(group) > longest:
            group.sort()
            longest_set = group
            longest = len(group)
        return ','.join([str(x) for x in longest_set])

    def re_organize_dict(self):
        lan = {}
        for key in self.lan.keys():
            lan[key] = [x.compB for x in self.lan[key]]
        return lan

    def get_longest_set(self):
        winner = []
        lan = self.re_organize_dict()
        for computer in lan.keys():
            candidates = lan[computer]
            reachables = []
            logging.info(f"Candidates: {candidates}")
            #for each candidate, look at each other candidate and see if you can get to them
            for candidateA in candidates:
                can_get_to = [computer, candidateA]
                for candidateB in candidates:
                    if candidateB == candidateA:
                        continue
                    logging.info(f"Can reach {candidateB} from {candidateA}?")
                    if candidateB in lan[candidateA]:
                        can_get_to.append(candidateB)
                logging.info(f"{candidateA} can reach {can_get_to}")
                can_get_to.sort()
                reachables.append(','.join([str(x) for x in can_get_to]))
                counts = {}
                for item in reachables:
                    if item not in counts.keys():
                        counts[item] = 0
                    counts[item] += 1
                valids = []
                logging.info(f"Counts: {counts}")
                for c in counts.keys():
                    if counts[c] == c.count(','):
                        valids.append(c)
            logging.info(f"Group: {valids}")
            for val in valids:
                if len(val) > len(winner):
                    winner = val
            logging.info(f"Current longest group is {winner}")
        return winner


if __name__ == '__main__':
    parser = getBasicArgparser('day 23')

    args = parser.parse_args()

    day23 = Day23(args.i, args.v)

    #print(f"Part 1: Number of computer groups of 3 including a t is {day23.get_sets_of_three_with_t()}")
    print(f"Part 2: Longest group is {day23.get_longest_set()}")

    #bd,en,ev,gq,ht,hu,kl,ll,os,pa,pn,qn,ub is wrong
    #au,db,fx,gz,jm,kh,ky,mo,ra,sg,yv,za,ze is also wrong