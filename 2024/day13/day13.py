#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point
from dataclasses import dataclass
import numpy as np
import math


@dataclass
class Machine:
    button_a : Point
    button_b : Point
    prize : Point

    def get_fewest_cost_to_win(self, max_presses, cost_for_a, cost_for_b, prize_add=1):
        # N_a * a_x + N_b * b_x = prize_x
        # N_a * a_y + N_b * b_y = prize_y
        # x = row, y = col
        prize = Point(prize_add+self.prize.row, prize_add+self.prize.col)
        logging.info(f"Test A={self.button_a} B={self.button_b} prize={prize}")
        cost = sys.maxsize
        for a in range(max_presses):
            b = (prize.row - a*self.button_a.row) / self.button_b.row
            logging.info(f" Check a={a}, b={b}")
            if 0 <= b <= max_presses and b.is_integer():
                logging.info(f"Found integer b")
                b = int(b)
                if a*self.button_a.col + b*self.button_b.col == prize.col:
                    new_cost = cost_for_a * a + cost_for_b * b
                    logging.info(f"a={a} b={b} is an option, costs {new_cost}")
                    # From observation on part 1, the inputs are nice and will only ever have one solution
                    return new_cost
                    #cost = min(cost, new_cost)
        if cost == sys.maxsize:
            return 0
        return cost

    def get_fewest_cost_to_win_linalg(self, cost_for_a, cost_for_b, prize_add=0):
        # It's a system of two linear equations:
        # N_a * a_x + N_b * b_x = prize_x
        # N_a * a_y + N_b * b_y = prize_y
        logging.info(f"Test A={self.button_a} B={self.button_b} prize={self.prize}")
        coeffs = np.array([
            [self.button_a.row, self.button_b.row],
            [self.button_a.col, self.button_b.col]
        ])
        res = np.array([self.prize.row, self.prize.col]) + prize_add
        answer = np.linalg.solve(coeffs, res)
        logging.info(answer)
        if answer[0] > 0 and abs(round(answer[0]) - answer[0]) < 0.0001:
            logging.info(f"a Integer {answer[0]}")
            if answer[1] > 0 and abs(round(answer[1]) - answer[1]) < 0.0001:
                logging.info(f"b Integer {answer[1]}")
                result = cost_for_a*int(round(answer[0])) + cost_for_b*int(round(answer[1]))
                logging.info(f"Result {result}")
                return result
            else:
                logging.info(answer[1])
        return 0


class Day13(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.machines = None
        self.cost_to_press_a = 3
        self.cost_to_press_b = 1
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            machines = []
            reader = csv.reader(csvfile, delimiter='\n')
            a = None
            b = None
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0].split(':')
                if line[0] == 'Button A':
                    data = line[1].split(',')
                    if not data[0].startswith(' X+') or not data[1].startswith(' Y+'):
                        logging.error(f"Malformed input: {data}")
                        sys.exit(1)
                    a = Point(int(data[0][2:]), int(data[1][2:]))
                    #logging.info(f"line {line} read a {a}")
                elif line[0] == 'Button B':
                    data = line[1].split(',')
                    if not data[0].startswith(' X+') or not data[1].startswith(' Y+'):
                        logging.error(f"Malformed input: {data}")
                        sys.exit(1)
                    b = Point(int(data[0][2:]), int(data[1][2:]))
                    #logging.info(f"line {line} read b {b}")
                elif line[0] == 'Prize':
                    data = line[1].split(',')
                    if not data[0].startswith(' X=') or not data[1].startswith(' Y='):
                        logging.error(f"Malformed input: {data}")
                        sys.exit(1)
                    p = Point(int(data[0][3:]), int(data[1][3:]))
                    machines.append(Machine(button_a=a, button_b=b, prize=p))
                    #logging.info(f"Read A={machines[-1].button_a} B={machines[-1].button_b} prize={machines[-1].prize}")
                    a = None
                    b = None
        self.machines = machines

    def get_cheapest_way_to_win(self, max_presses, add=0):
        spent = 0
        for machine in self.machines:
            #costb = machine.get_fewest_cost_to_win(max_presses, self.cost_to_press_a, self.cost_to_press_b, add)
            cost = machine.get_fewest_cost_to_win_linalg(self.cost_to_press_a, self.cost_to_press_b, add)
            logging.info(f"Cost: {cost}")
            spent += cost
        return spent


if __name__ == '__main__':
    parser = getBasicArgparser('day 13')

    args = parser.parse_args()

    day13 = Day13(args.i, args.v)

    print(f"Part 1: Cheapest cost for as many prizes as possible is {day13.get_cheapest_way_to_win(100)}")
    print(f"Part 2: Cheapest cost for as many prizes as possible is {day13.get_cheapest_way_to_win(10000000, 10000000000000)}")