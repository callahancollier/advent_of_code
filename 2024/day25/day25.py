#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
import numpy as np


class Day25(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.locks = None
        self.keys = None
        self.parse_file()

    def parse_file(self):
        locks = []
        keys = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            lock = []
            key = []
            for line in reader:
                if len(line) == 0:
                    if len(lock) != 0:
                        locks.append(np.array(lock))
                    elif len(key) != 0:
                        keys.append(np.array(key))
                    lock = []
                    key = []
                    continue
                line = line[0]
                if len(lock) != 0:
                    lock.append([int(x=='#') for x in line]) #todo
                elif len(key) != 0:
                    key.append([int(x=='#') for x in line]) #todo
                else:
                    if line[0] == '#':
                        lock.append([int(x=='#') for x in line])  # todo
                    else:
                        key.append([int(x=='#') for x in line])  # todo
        if len(lock) != 0:
            locks.append(np.array(lock))
        elif len(key) != 0:
            keys.append(np.array(key))

        self.locks = locks
        self.keys = keys
        logging.info(self.locks)

    def get_n_non_overlapping_keys(self):
        num_not_overlapping = 0
        for key in self.keys:
            for lock in self.locks:
                logging.info(f"Key:\n {key}\nLock:\n{lock}")
                combined = key + lock
                logging.info(f"Combined:\n{combined}")
                if (combined == 2).sum() == 0:
                    logging.info("No overlaps!")
                    num_not_overlapping += 1
        return num_not_overlapping


if __name__ == '__main__':
    parser = getBasicArgparser('day 25')

    args = parser.parse_args()

    day25 = Day25(args.i, args.v)

    print(f"Part 1: Number of keys that don't overlap with locks are {day25.get_n_non_overlapping_keys()}")