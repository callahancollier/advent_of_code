#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
import operator
import itertools

def concat_operator(a, b):
    return int(str(a) + str(b))

TWO_OPS = [ operator.mul, operator.add ]
THREE_OPS = [ operator.mul, operator.add, concat_operator]


@dataclass
class Equation:
    result : int
    operands : list

    def validate(self, operator_list):
        number_of_operators = len(self.operands)-1
        logging.info(f"Test {self.result}: {self.operands}")
        comb_list = ''.join([str(x) for x in range(len(operator_list))])
        for binary_rep in [''.join(x) for x in itertools.product(comb_list, repeat=number_of_operators)]:
            result = self.operands[0]
            op = 1
            for bit in binary_rep:
                logging.info(f"bit {bit}")
                result = operator_list[int(bit)](result, self.operands[op])
                op += 1
                if result > result:
                    break
            logging.info(f"{binary_rep} result {result}")
            if result == self.result:
                logging.info("Valid!")
                return True
        return False


class Day7(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.equations = None
        self.parse_file()

    def parse_file(self):
        equations = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                line = line.split(':')
                equations.append(Equation(int(line[0]), [int(x) for x in line[1].strip().split(' ')]))
        self.equations = equations

    def validate_equations(self, op_list):
        total = 0
        for equ in self.equations:
            if equ.validate(op_list):
                total += equ.result
        return total


if __name__ == '__main__':
    parser = getBasicArgparser('day 7')

    args = parser.parse_args()

    day7 = Day7(args.i, args.v)

    print(f"Part 1: sum of valid equations is {day7.validate_equations(TWO_OPS)}")
    print(f"Part 2: sum of valid equations is {day7.validate_equations(THREE_OPS)}")
