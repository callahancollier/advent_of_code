#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, Directions
import numpy as np

MOVEMENT = {
    '^' : Directions.UP,
    'v' : Directions.DOWN,
    '<' : Directions.LEFT,
    '>' : Directions.RIGHT
}

UPDOWN = ['^', 'v']

ROBOT = '@'
EMPTY = '.'
WALL = '#'
BOX = 'O'
WIDE_BOX_L = '['
WIDE_BOX_R = ']'
WIDE_BOX = '[]'


class Day15(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.location = None
        self.moves = None
        self.robot = None
        self.parse_file()

    def parse_file(self):
        space = []
        moves = []
        robot = None
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            stage = 'space'
            for line in reader:
                if len(line) == 0:
                    stage = 'moves'
                    continue
                line = line[0]
                if stage == 'space':
                    if ROBOT in line:
                        robot = Point(row=len(space), col=line.index(ROBOT))
                    space.append([x for x in line])
                else:
                    moves += [x for x in line]
        self.location = np.array(space)
        self.moves = moves
        self.robot = robot
        self.disp(self.location)
        logging.info(f"Robot starts at {self.robot}")

    def get_pos(self, location, pt):
        return location[pt.row][pt.col]

    def set_pos(self, location, pt, val):
        location[pt.row][pt.col] = val
        return location

    def disp(self, location, prepend=0):
        for r in location:
            logging.info(prepend*' ' + ''.join(r))

    def move_robot(self, location, cur_robot):
        for move in self.moves:
            logging.info(f"Move {move} (robot at {cur_robot}):")
            #if self.debug:
            #    input('enter to continue')
            location, robot_moved = self.move_one(location, cur_robot, move)
            if robot_moved:
                cur_robot = cur_robot + MOVEMENT[move]
            logging.info(f"Moved {move} (robot at {cur_robot}):")
            self.disp(location)
        return location


    def move_one(self, location, cur, dir, robot_moved=False, recursion=1, do_left_right=True):
        next = cur + MOVEMENT[dir]
        cur_val = self.get_pos(location, cur)
        next_val = self.get_pos(location, next)

        if next_val == WALL:
            logging.info(recursion*' ' + f"Hit wall")
            self.disp(location, recursion)
            return location, robot_moved
        elif next_val != EMPTY:
            # Try to move the next one first
            if next_val == WIDE_BOX_L and dir in UPDOWN and do_left_right:
                location = self.move_two(location.copy(), next, dir, recursion+1)
            elif next_val == WIDE_BOX_R and dir in UPDOWN and do_left_right:
                logging.info(recursion*' ' + f"conv right {next} to {next+Directions.LEFT}")
                location = self.move_two(location.copy(), next+Directions.LEFT, dir, recursion+1)
            else:
                location, robot_moved = self.move_one(location, next, dir, robot_moved, recursion+1)
            next_val = self.get_pos(location, next)

        if next_val == EMPTY:
                # Now do the move if we can
                if next_val == WIDE_BOX_L and dir in UPDOWN and do_left_right:
                    location = self.move_two(location.copy(), next, dir, recursion + 1)
                elif next_val == WIDE_BOX_R and dir in UPDOWN and do_left_right:
                    location = self.move_two(location.copy(), next + Directions.LEFT, dir, recursion + 1)
                else:
                    location = self.set_pos(location, next, cur_val)
                    location = self.set_pos(location, cur, next_val)

                moved = True if cur_val == ROBOT else robot_moved
                logging.info(recursion*' ' + f"Moved {cur_val} into {next_val} at {next}:")
                self.disp(location, recursion)
                return location, moved

        return location, robot_moved

    def move_two(self, location, cur_left, dir, recursion=1):
        logging.info(recursion*' ' + f"Need to check box with left side at {cur_left}")
        next_left = cur_left + MOVEMENT[dir]
        cur_val_left = self.get_pos(location, cur_left)
        next_val_left = self.get_pos(location, next_left)
        new_loc, _ = self.move_one(location.copy(), cur_left, dir, False, recursion)

        if np.any(new_loc != location):
            cur_right = cur_left + Directions.RIGHT
            next_right = cur_right + MOVEMENT[dir]
            cur_val_right = self.get_pos(location, cur_right)
            next_val_right = self.get_pos(location, next_right)
            newest_loc, _ = self.move_one(new_loc.copy(), cur_right, dir, False, recursion)
            if np.any(newest_loc != new_loc):
                # Only return if you can move both the left and right sides
                logging.info(recursion*' ' + f"Moved both!")
                return newest_loc

        logging.info(recursion*' ' + "Bail out")
        return location

    def DEPR_move_one(self, location, cur, dir, robot_moved=False, recursion=1, do_side_check=True):
        next = cur + MOVEMENT[dir]
        cur_val = self.get_pos(location, cur)
        next_val = self.get_pos(location, next)
        #next_val_beside = self.get_pos(location, next+Directions.LEFT) if cur_val == WIDE_BOX_R and dir in [Directions.UP, Directions.DOWN] else None
        #next_val_beside = self.get_pos(location, next+Directions.RIGHT) if cur_val == WIDE_BOX_L and dir in [Directions.UP, Directions.DOWN] else next_val_beside
        #cur_val_beside = self.get_pos(location, cur+Directions.LEFT) if cur_val == WIDE_BOX_R and dir in [Directions.UP, Directions.DOWN] else None
        #cur_val_beside = self.get_pos(location, cur+Directions.RIGHT) if cur_val == WIDE_BOX_L and dir in [Directions.UP, Directions.DOWN] else cur_val_beside

        next_beside = None
        cur_beside = None
        next_val_beside = None
        cur_val_beside = None
        if do_side_check:
            if cur_val == WIDE_BOX_R and dir in UPDOWN:
                logging.info(recursion*' ' + f"Need to account for wide box to the left")
                next_beside = next + Directions.LEFT
                cur_beside = cur + Directions.LEFT
            elif cur_val == WIDE_BOX_L and dir in UPDOWN:
                logging.info(recursion*' ' + f"Need to account for wide box to the right")
                next_beside = next + Directions.RIGHT
                cur_beside = cur + Directions.RIGHT
            if next_beside is not None and cur_beside is not None:
                next_val_beside = self.get_pos(location, next_beside)
                cur_val_beside = self.get_pos(location, cur_beside)

        location_save = location.copy()
        if next_val == WALL:
            logging.info(recursion*' ' + f"Hit wall")
            self.disp(location, recursion)
            return location, robot_moved
        elif next_val != EMPTY:
            # Try to move the next one first
            if next_val_beside is not None and cur_val_beside is not None:
                logging.info(recursion*' ' + f"Recurse beside")
                new_loc, _ = self.move_one(location.copy(), next_beside, dir, robot_moved, recursion+1)
                if np.any(new_loc != location):
                    location_save = location.copy()
                    location, robot_moved = self.move_one(new_loc, next, dir, robot_moved, recursion + 1)
                    next_val = self.get_pos(location, next)
                    next_val_beside = self.get_pos(location, next_beside)
                    logging.info(recursion*' ' + "Moved beside")
            else:
                location, robot_moved = self.move_one(location, next, dir, robot_moved, recursion+1)
                next_val = self.get_pos(location, next)

        if next_val == EMPTY:
            if next_val_beside is None or next_val_beside == EMPTY:
                # Now do the move if we can
                if next_val_beside is not None and cur_val_beside is not None:
                    logging.info(recursion*' ' + f"Need to check beside:")
                    new_loc, _ = self.move_one(location.copy(), cur_beside, dir, robot_moved, recursion+1, False)
                    # TODO I think we do just compare the places in new_loc/location we care about instead of entire array
                    if np.any(new_loc != location): # successfully moved
                        location = self.set_pos(new_loc, next, cur_val)
                        location = self.set_pos(location, cur, next_val)
                        logging.info(recursion*' ' + f"Moved adjacent:")
                        self.disp(location, recursion)
                    else:
                        logging.info("Bailed out")
                        location = location_save
                else:
                    location = self.set_pos(location, next, cur_val)
                    location = self.set_pos(location, cur, next_val)

                moved = True if cur_val == ROBOT else robot_moved
                logging.info(recursion*' ' + f"Moved {cur_val} into {next_val} at {next}:")
                self.disp(location, recursion)
                return location, moved
        else:
            location = location_save
            logging.info(recursion*' ' + "bailed out")

        return location, robot_moved

    def move_robot_calculate_gps_sum_narrow(self):
        location = self.location.copy()
        robot = self.robot
        return self.move_robot_calculate_gps_sum(location, BOX, robot)

    def move_robot_calculate_gps_sum(self, start_locations, box, robot):
        sum = 0
        final_locations = self.move_robot(start_locations, robot)
        height = len(final_locations)
        width = len(final_locations[0])
        for r in range(height):
            for c in range(width):
                pt = Point(r,c)
                if self.get_pos(final_locations, pt) == box:
                    val = 100*r + c
                    logging.info(f"Box at {pt} has GPS coord {val}")
                    sum += val
        return sum

    def widen(self, location, robot):
        loc = []
        for r in location:
            new_r = []
            for t in r:
                if t == '#': new_r += [ '#', '#']
                elif t == 'O': new_r += ['[', ']']
                elif t == '.': new_r += ['.', '.']
                elif t == '@': new_r += ['@','.']
            loc.append(new_r)
        self.disp(loc)
        return np.array(loc), Point(row=robot.row, col=(2*robot.col))

    def move_robot_calculate_gps_sum_wide(self):
        location, robot = self.widen(self.location, self.robot)
        return self.move_robot_calculate_gps_sum(location, WIDE_BOX_L, robot)


if __name__ == '__main__':
    parser = getBasicArgparser('day 15')

    args = parser.parse_args()

    day15 = Day15(args.i, args.v)

    print(f"Part 1: Sum of all GPS coordinates is {day15.move_robot_calculate_gps_sum_narrow()}")
    print(f"Part 2: Sum of all GPS coordinates is {day15.move_robot_calculate_gps_sum_wide()}")
