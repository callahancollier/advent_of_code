#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day1(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.left = []
        self.right = []

        self.parse_file()

    def parse_file(self):
        self.left = []
        self.right = []
        separator = '   ' #Three spaces for some reason?
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(separator)
                if len(line) != 2:
                    logging.error('Malformed input!')
                    sys.exit(1)
                self.left.append(int(line[0]))
                self.right.append(int(line[1]))

    def calculate_total_distance(self):
        sorted_left = self.left.copy()
        sorted_left.sort()
        sorted_right = self.right.copy()
        sorted_right.sort()
        total_distance = 0
        for i in range(len(self.left)):
            left = sorted_left[i]
            right = sorted_right[i]
            diff = abs(right - left)
            total_distance += diff
            logging.info(f"  Compare {left} and {right}: {diff}")
        return total_distance

    def calculate_similarity_score(self):
        similarity = 0
        for left in self.left:
            count = self.right.count(left)
            logging.info(f"Left value {left} appears {count} times in right")
            similarity += left*count
        return similarity


if __name__ == '__main__':
    parser = getBasicArgparser('day 1')

    args = parser.parse_args()

    day1 = Day1(args.i, args.v)
    print(f"Part 1: Total distance is {day1.calculate_total_distance()}")
    print(f"Part 2: Similarity score is {day1.calculate_similarity_score()}")
