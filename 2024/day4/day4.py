#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day4(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.wordsearch = self.parse_file()
        self.word = 'XMAS'
        self.pattern1 = np.array(
            [
                ['M', '.', 'S'],
                ['.', 'A', '.'],
                ['M', '.', 'S'],
            ]
        )
        self.pattern2 = np.array(
            [
                ['M', '.', 'M'],
                ['.', 'A', '.'],
                ['S', '.', 'S'],
            ]
        )
        self.dontcares = [(0,1),(1,0),(1,2),(2,1)]
        logging.info(self.wordsearch)
        if len(self.wordsearch) != len(self.wordsearch[0]):
            logging.error("Bad assumption that wordsearch will be square!")
            sys.exit(1)


    def parse_file(self):
        rows = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                rows.append( [x for x in line])
        return np.array(rows)

    def search(self, data):
        found = 0
        offset = 0
        while offset < len(data):
            logging.info(f"  Test from offset {offset}: {data[offset:]}")
            idx = data.find(self.word, offset)
            if idx >= 0:
                logging.info(f"      Found {self.word} at index {idx}")
                found += 1
                offset = idx+1
            else:
                break
        return found


    def search_rows(self):
        found = 0
        for r in range(len(self.wordsearch)):
            #forwards
            row = ''.join(self.wordsearch[r])
            logging.info(f"Row {r}: {row}")
            found += self.search(row)
            #backwards
            row = ''.join(np.flip(self.wordsearch[r]))
            logging.info(f"Row backwards {r}: {row}")
            found += self.search(row)
        return found

    def search_cols(self):
        found = 0
        for c in range(len(self.wordsearch[0])):
            #forwards
            col = ''.join(self.wordsearch[:, c])
            logging.info(f"Col {c}: {col}")
            found += self.search(col)
            #backwards
            col = ''.join(np.flip(self.wordsearch[:, c]))
            logging.info(f"Col backwards {c}: {col}")
            found += self.search(col)
        return found

    def search_diagonals(self):
        found = 0
        for d in range(-1*len(self.wordsearch)+1, len(self.wordsearch)):
            #forwards
            diag = ''.join(np.diag(self.wordsearch, d))
            logging.info(f"Diag {d}: {diag}")
            if len(diag) < len(self.word):
                logging.info("skip")
                continue
            found += self.search(diag)
            #backwards
            diag = ''.join(np.flip(np.diag(self.wordsearch, d)))
            logging.info(f"diag backwards {d}: {diag}")
            found += self.search(diag)
        return found

    def find_matches(self):
        total = 0
        total += self.search_rows()
        total += self.search_cols()
        total += self.search_diagonals()
        logging.info("Flip to search other diagonals")
        self.wordsearch = np.fliplr(self.wordsearch)
        logging.info(self.wordsearch)
        total += self.search_diagonals()
        self.wordsearch = np.fliplr(self.wordsearch)#undo the flip
        return total

    def find_pattern(self):
        width = len(self.pattern1[0])
        height = len(self.pattern1)
        found = 0
        for r in range(len(self.wordsearch)-height+1):
            for c in range(len(self.wordsearch[0])-width+1):
                subdata = self.wordsearch[r:r+height, c:c+width].copy()
                logging.info(f"Check subdata({r},{c}):\n{subdata}")
                for r2,c2 in self.dontcares:
                    subdata[r2,c2] = '.'
                logging.info(f"Becomes:\n{subdata}")
                if (subdata == self.pattern1).all():
                    logging.info("Found!")
                    found += 1
                elif (subdata == np.fliplr(self.pattern1)).all():
                    logging.info("Found!")
                    found += 1
                elif (subdata == self.pattern2).all():
                    logging.info("Found!")
                    found += 1
                elif (subdata == np.flipud(self.pattern2)).all():
                    logging.info("Found!")
                    found += 1
                else:
                    logging.info("Not found")
        return found

if __name__ == '__main__':
    parser = getBasicArgparser('day 4')

    args = parser.parse_args()

    day4 = Day4(args.i, args.v)

    print(f"Part 1: Found {day4.find_matches()}")
    print(f"Part 2: Found {day4.find_pattern()}")

#1784 too low