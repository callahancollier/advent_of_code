#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day3(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.data = ''
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.data += line

    def extract_valid_code(self):
        instructions = []
        chunk = ''
        remaining = self.data
        while len(remaining) >= 8:
            logging.info(f"Remaining: {remaining}")
            while not remaining.startswith('mul(') and not remaining.startswith('do') and not remaining.startswith("don't") and len(remaining) >= 8:
                remaining = remaining[1:]
            logging.info(f"Align to start of instr: {remaining}")
            if len(remaining) < 8:
                break
            if remaining.startswith("don't"):
                instructions.append("don't")
                remaining = remaining[5:]
                continue
            if remaining.startswith('do'):
                instructions.append('do')
                remaining = remaining[2:]
                continue
            chunk = remaining[:4]
            remaining = remaining[4:] # remove mul(
            comma_idx = remaining.find(',')
            logging.info(f"comma at {comma_idx}, remaining {remaining}")
            if comma_idx >= 1 and comma_idx <= 3 and remaining[:comma_idx].isnumeric():
                chunk += remaining[:comma_idx]
                remaining = remaining[comma_idx:]
            else:
                logging.info(f"chunk {chunk} not valid")
                continue
            bracket_idx = remaining.find(')')
            logging.info(f"bracket at {bracket_idx}, remaining {remaining}")
            if bracket_idx >= 2 and bracket_idx <= 4 and remaining[1:bracket_idx].isnumeric():
                chunk += remaining[:bracket_idx+1]
                remaining = remaining[bracket_idx+1:]
            else:
                logging.info(f"chunk {chunk} not valid")
                continue
            logging.info(f"Chunk: {chunk}")
            instructions.append(chunk)
            chunk = ''
        logging.info(f"found instructions: {instructions}")
        return instructions

    def run_mult_instructions(self, instructions, enable_dos):
        sum = 0
        do = True
        for instr in instructions:
            if instr == 'do':
                do = True
            elif instr == "don't":
                do = False
            else:
                comma_idx = instr.find(',')
                a = int(instr[4:comma_idx])
                b = int(instr[comma_idx+1:-1])
                if not enable_dos:
                    sum += a*b
                elif do:
                    sum += a*b
        return sum

    def run(self, enable_dos):
        instructions = self.extract_valid_code()
        return self.run_mult_instructions(instructions, enable_dos)


if __name__ == '__main__':
    parser = getBasicArgparser('day 3')

    args = parser.parse_args()

    day3 = Day3(args.i, args.v)
    print(f"Part 1: Valid code is {day3.run(False)}")
    print(f"Part 2: Valid code is {day3.run(True)}")
