#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, Directions
import numpy as np
from functools import cache
from tqdm import tqdm


START = 'S'
END = 'E'
WALL = '#'
PATH = '.'


class Day20(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.start = None
        self.end = None
        self.racetrack = None
        self.parse_file()

    def parse_file(self):
        racetrack = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if START in line:
                    self.start = Point(row=len(racetrack), col=line.index(START))
                elif END in line:
                    self.end = Point(row=len(racetrack), col=line.index(END))
                racetrack.append([x for x in line])
        self.racetrack = np.array(racetrack)

    def get_at(self, pos):
        return self.racetrack[pos.row, pos.col]

    def get_path_to_end(self):
        path = []
        current = self.start
        while current != self.end:
            found = False
            for dir in Directions:
                neighbour = current + dir
                if self.get_at(neighbour) != WALL and neighbour not in path:
                    path.append(current)
                    current = neighbour
                    found = True
            if not found:
                logging.error(f"Something bad happened! Path so far is {path}")
                sys.exit(1)
        path.append(current)
        return path

    def display_path(self, path):
        track = self.racetrack.copy()
        for pt in path:
            track[pt.row, pt.col] = 'x'
        for row in track:
            logging.info(''.join(row))

    def find_baseline(self):
        path = self.get_path_to_end()
        logging.info(path)
        self.display_path(path)
        return len(path)-1, path # Don't include the start point

    def is_in_bounds(self, pt):
        if 0 <= pt.row < len(self.racetrack):
            if 0 <= pt.col < len(self.racetrack[0]):
                return True
        return False

    def get_cheat_reachable_from(self, n_cheats, point, path):
        options = set()
        for r in range(n_cheats+1):
            for c in range(n_cheats+1-r):
                news = [point+Point(r,c), point+Point(-r,c), point+Point(-r,-c), point+Point(r,-c)]
                for new in news:
                    if new != point and new in path: # and self.is_in_bounds(new):
                        options.add((point, new))
        return options

    def DEPR_get_cheater_options(self, n_cheats, point):
        options = []
        if n_cheats == 2:
            for dir1 in Directions:
                next1 = point + dir1
                if self.is_in_bounds(next1):
                    if self.get_at(next1) == WALL:
                        for dir2 in Directions:
                            next2 = next1 + dir2
                            if self.is_in_bounds(next2):
                                if self.get_at(next2) != WALL:
                                    options.append([next1, next2])
        return options

    def get_cheater_options(self, n_cheats, point):
        self.options = []
        logging.info(f"OPTION {point}")
        self.get_cheater_options_recurse(n_cheats, point, [])
        return self.options

    def get_cheater_options_recurse(self, cheats_remaining, point, option):
        logging.info(f"Recurse with {cheats_remaining} cheats, point {point}, {option}")
        for dir in Directions:
            next = point + dir
            if not self.is_in_bounds(next):
                continue
            if cheats_remaining == 1:
                if self.get_at(next) != WALL:
                    new_option = option.copy()
                    new_option.append(next)
                    logging.info(f"Found option: {new_option}")
                    self.options.append(new_option)
            elif cheats_remaining > 1:
                if self.get_at(next) == WALL:
                    new_option = option.copy()
                    new_option.append(next)
                    self.get_cheater_options_recurse(cheats_remaining-1, next, new_option)

    def find_cheater_paths(self, min_save, n_cheats):
        n_paths = 0
        baseline, baseline_path = self.find_baseline()
        logging.info(f"Baseline {baseline}")
        for point in tqdm(baseline_path):
            options = self.get_cheat_reachable_from(n_cheats, point, baseline_path)
            for option in options:
                manhattan = abs(option[-1].row - point.row) + abs(option[-1].col - point.col)
                n_skip = baseline_path.index(option[-1]) - baseline_path.index(point) - manhattan
                logging.info(f"From {point}, cheating at {option} saves {n_skip}, from {baseline_path.index(point)} to {baseline_path.index(option[-1])}")
                if n_skip >= min_save:
                    logging.info(f"That's valid")
                    n_paths += 1
        return n_paths


if __name__ == '__main__':
    parser = getBasicArgparser('day 20')

    args = parser.parse_args()

    day20 = Day20(args.i, args.v)

    #print(f"Part 1: baseline path length is {day20.find_cheater_paths(10, 2)}")
    print(f"Part 2: baseline path length is {day20.find_cheater_paths(100, 20)}")
