#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

@dataclass
class Entry:
    precedes : list()
    follows : list()


class Day5(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.rules_follow = {}
        self.rules_precede = {}
        self.updates = []
        self.parse_file()

    def parse_file(self):
        stage = 'rules'
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    stage = 'update'
                    continue
                line = line[0]
                if stage == 'rules':
                    if '|' not in line:
                        stage = 'update'
                        continue
                    else:
                        line = line.split('|')
                        if len(line) != 2:
                            logging.error(f"Malformed input! {line}")
                            sys.exit(1)
                        first = int(line[0])
                        second = int(line[1])
                        if first not in self.rules_follow:
                            self.rules_follow[first] = set()
                        if second not in self.rules_precede:
                            self.rules_precede[second] = set()
                        self.rules_follow[first].add(second)
                        self.rules_precede[second].add(first)
                elif stage == 'update':
                    if ',' not in line:
                        logging.error(f"Malformed input: {line}")
                        sys.exit(1)
                    self.updates.append([int(x) for x in line.split(',')])

    def validate_line(self, line):
        logging.info(f"Validate line {line}")
        entries = {}
        for value in line:
            if value in entries:
                logging.error(f"{value} appears twice!")
                sys.exit(1)
            newEntry = Entry(precedes=[], follows=[])
            for existing in entries.keys():
                newEntry.follows.append(existing)
                entries[existing].precedes.append(value)
            entries[value] = newEntry

        for entry in entries.keys():
            if entry in self.rules_follow.keys():
                for must_follow in self.rules_follow[entry]:
                    if must_follow in entries[entry].follows:
                        logging.info(" Invalid!")
                        return False
            if entry in self.rules_precede.keys():
                for must_precede in self.rules_precede[entry]:
                    if must_precede in entries[entry].precedes:
                        logging.info(" Invalid!")
                        return False
        logging.info(" Valid!")
        return True


    def validate_line_and_try_to_fix(self, line):
        logging.info(f"Validate line {line}")
        entries = {}
        for value in line:
            if value in entries:
                logging.error(f"{value} appears twice!")
                sys.exit(1)
            newEntry = Entry(precedes=[], follows=[])
            for existing in entries.keys():
                newEntry.follows.append(existing)
                entries[existing].precedes.append(value)
            entries[value] = newEntry

        for entry in entries.keys():
            if entry in self.rules_follow.keys():
                for must_follow in self.rules_follow[entry]:
                    if must_follow in entries[entry].follows:
                        logging.info(f" Invalid! Try swapping {entry} and {must_follow}")
                        return entry,must_follow
            if entry in self.rules_precede.keys():
                for must_precede in self.rules_precede[entry]:
                    if must_precede in entries[entry].precedes:
                        logging.info(f" Invalid. Try swapping {entry} and {must_precede}")
                        return entry,must_precede
        logging.info(" Valid!")
        return None

    def get_middle_value(self, line):
        if len(line)%2 == 0:
            logging.error("Line must have even length to get middle point!")
            sys.exit(1)
        mid = len(line)//2
        logging.info(f"Middle of {line} (len {len(line)}) is index {mid}: {line[mid]}")
        return line[mid]

    def check_updates(self):
        mid_sum = 0
        for update in self.updates:
            if self.validate_line(update):
                mid_sum += self.get_middle_value(update)
        return mid_sum

    def fix_updates(self):
        mid_sum = 0
        for update in self.updates:
            needed_update = False
            while not self.validate_line(update):
                needed_update = True
                val1,val2 = self.validate_line_and_try_to_fix(update)
                idx1 = update.index(val1)
                idx2 = update.index(val2)

                update[idx1],update[idx2] = update[idx2], update[idx1]
            if needed_update:
                mid_sum += self.get_middle_value((update))
        return mid_sum


if __name__ == '__main__':
    parser = getBasicArgparser('day 5')

    args = parser.parse_args()

    day5 = Day5(args.i, args.v)

    print(f"Part 1: Middle points of correct updates sum to {day5.check_updates()}")
    print(f"Part 2: Middle points of corrected updates sum to {day5.fix_updates()}")
