#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from dataclasses import dataclass


@dataclass
class Wire:
    output: int

    def __str__(self):
        return f"<{self.output}>"
    __repr__ = __str__


@dataclass
class Gate(Wire):
    operation: str
    op_name: str
    in0: int = None
    in1 : int = None

    def __str__(self):
        return f"<{self.in0} {self.operation} {self.in1}->{self.output}>"

    __repr__ = __str__

    def set_inputs(self, in0, in1):
        self.in0 = in0
        self.in1 = in1
        self.do_op()

    def do_op(self):
        if self.in0 is None or self.in1 is None:
            raise ValueError
        if self.operation == 'AND':
            self.output = self.in0 and self.in1
        elif self.operation == 'OR':
            self.output = self.in0 or self.in1
        elif self.operation == 'XOR':
            self.output = self.in0 ^ self.in1


class Node:
    def __init__(self, output_name, operation):
        self.operation = operation
        self.output_name = output_name
        self.in0 = None
        self.in1 = None

    def set_inputs(self, in0, in1):
        self.in0 = in0
        self.in1 = in1

    def get_output(self):
        if self.in0 is None or (self.in1 is None and self.operation != 'WIRE'):
            raise ValueError
        if self.operation == 'AND':
            return self.in0.get_output() and self.in1.get_output()
        elif self.operation == 'OR':
            return self.in0.get_output() or self.in1.get_output()
        elif self.operation == 'XOR':
            return self.in0.get_output() ^ self.in1.get_output()
        elif self.operation == 'WIRE':
            return self.in0
        else:
            logging.error("Bad op {self.operation}")
            sys.exit(1)

    def get_network(self):
        if self.in0 is None or (self.in1 is None and self.operation != 'WIRE'):
            raise ValueError
        if self.operation == 'WIRE':
            return self.output_name

        inputs = [self.in0.get_network(), self.in1.get_network()]
        inputs.sort()
        #return self.operation.join([a,b])
        return f"({inputs[0]}{self.operation}{inputs[1]})"


class Day24(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.nodes = None
        self.parse_file()

    def parse_file(self):
        initial_values = {}
        gates = {}
        outputs = {}
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            init_step = True
            for line in reader:
                if len(line) == 0:
                    init_step = False
                    continue
                line = line[0]
                if init_step:
                    line = line.split(': ')
                    initial_values[line[0]] = int(line[1])
                else:
                    line = line.split(' ')
                    gate_output = line[4]
                    gate_inputs = [line[0], line[2]]
                    gate_inputs.sort()
                    operation = line[1]
                    gates[gate_output] = (gate_inputs, operation)
                    if gate_output.startswith('z'):
                        outputs[gate_output] = (gate_inputs, operation)
        self.nodes = self.transform_into_tree(initial_values, gates, outputs)

    def transform_into_tree(self, initial_values, gates, outputs):
        nodes = {}
        for outp in outputs.keys():
            nodes[outp] = self.get_next_node(outp, initial_values, gates)
        return nodes

    def get_next_node(self, current, initial_values, gates):
        inputs,op = gates[current]
        output = Node(output_name=current, operation=op)
        input_nodes = []
        for inp in inputs:
            if inp in initial_values:
                node = Node(output_name=inp, operation='WIRE')
                node.set_inputs(initial_values[inp], None)
                input_nodes.append(node)
            else:
                node = self.get_next_node(inp, initial_values, gates)
                input_nodes.append(node)
        output.set_inputs(input_nodes[0], input_nodes[1])
        return output


    def parse_file2(self):
        initial_values = {}
        gates = {}
        outputs = {}
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            init_step = True
            for line in reader:
                if len(line) == 0:
                    init_step = False
                    continue
                line = line[0]
                if init_step:
                    line = line.split(': ')
                    initial_values[line[0]] = int(line[1])
                else:
                    line = line.split(' ')
                    if line[0] in initial_values:
                        gates[line[0]] = Wire(output=initial_values[line[0]])
                    if line[2] in initial_values:
                        gates[line[2]] = Wire(output=initial_values[line[2]])
                    gate_inputs = [line[0], line[2]]
                    gate_inputs.sort()
                    gate = Gate(op_name=line[4], operation=line[1], output=None)
                    gates[(gate_inputs[0], gate_inputs[1])] = gate
                    outputs[gate.op_name] = gate
        self.gates = gates
        self.outputs = outputs
        logging.info(self.gates)

    def get_result(self):
        outputs = [x for x in self.nodes.keys()]
        outputs.sort()
        result = ''
        for outp in outputs:
            digit = self.nodes[outp].get_output()
            logging.info(f"Output {outp} gives {digit}")
            result += str(digit)
        result = result[::-1] #reverse string
        logging.info(f"Final result in binary is {result}")
        return int(result,2)

    def get_network(self):
        networks = {}
        outputs = [x for x in self.nodes.keys()]
        outputs.sort()
        for outp in outputs:
            networks[outp] = self.nodes[outp].get_network()
            logging.info(f"Output {outp}: {networks[outp]}")


if __name__ == '__main__':
    parser = getBasicArgparser('day 24')

    args = parser.parse_args()

    day24 = Day24(args.i, args.v)
    #print(f"Part 1: Result is {day24.get_result()}")
    print(f"Part 2: Result is {day24.get_network()}")
