#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point
import numpy as np
import scipy
from dataclasses import dataclass


TOP_LEFT = 'TOP_LEFT'
TOP_RIGHT = 'TOP_RIGHT'
BOT_LEFT = 'BOTTOM_LEFT'
BOT_RIGHT = 'BOTTOM_RIGHT'


@dataclass
class Robot:
    position: Point
    velocity: Point

    def get_position_after_moves(self, n_moves, height, width):
        #Convert negative velocities into positive ones so we can use wraparound math
        r = self.velocity.row
        c = self.velocity.col
        if r < 0:
            r += height
        if c < 0:
            c += width
        velocity = Point(r, c)
        new_row = (n_moves*velocity.row + self.position.row) % height
        new_col = (n_moves*velocity.col + self.position.col) % width
        new_pt = Point(new_row, new_col)
        #logging.info(f"{self.position} moves to {new_pt}")
        return new_pt


class Day14(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.robots = None
        self.area_height = None
        self.area_width = None
        self.parse_file()

    def parse_file(self):
        max_x = 0
        max_y = 0
        robots = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                p = None
                v = None
                line = line[0].split(' ')
                if line[0].startswith('p='):
                    tmp = line[0][2:].split(',')
                    p = Point(col=int(tmp[0]), row=int(tmp[1]))
                    max_x = max(max_x, p.col)
                    max_y = max(max_y, p.row)
                else:
                    logging.error(f"Malformed input {line[0]}")
                if line[1].startswith('v='):
                    tmp = line[1][2:].split(',')
                    v = Point(col=int(tmp[0]), row=int(tmp[1]))
                else:
                    logging.error(f"Malformed input {line[1]}")
                robots.append(Robot(p, v))
        self.robots = robots
        self.area_width = max_x+1
        self.area_height = max_y+1
        print(f"Area is {self.area_width}x{self.area_height}")
        self.display_area(self.robots)

    def get_area(self, robots):
        space = np.array(self.area_height*[self.area_width*[0]])
        for robot in robots:
            cur = space[robot.position.row][robot.position.col]
            if cur == 9:
                logging.info(f"Assumptions failed, displaying area will not work right")
            else:
                space[robot.position.row][robot.position.col] = int(cur)+1
        return space

    def display_area(self, robots):
        space = self.get_area(robots)
        self.log_area(space)

    def log_area(self, space):
        for r in space:
            #logging.info(''.join([str(x) for x in r if r != 0 else '.'])
            logging.info(''.join([str(x) if x != 0 else '.' for x in r]))

    def get_quadrant_of_point(self, position, height, width):
        top = False
        bottom = False
        left = False
        right = False

        if position.row < (height-1)/2:
            top = True
        if position.row > (height-1)/2:
            bottom = True
        if position.col < (width-1)/2:
            left = True
        if position.col > (width-1)/2:
            right = True
        if top and left:
            return TOP_LEFT
        if top and right:
            return TOP_RIGHT
        if bottom and left:
            return BOT_LEFT
        if bottom and right:
            return BOT_RIGHT
        return None

    def get_empty_quad_counts(self):
        return {
            TOP_LEFT : 0,
            TOP_RIGHT : 0,
            BOT_LEFT : 0,
            BOT_RIGHT : 0
        }

    def get_num_robots_per_quadrant(self, after_seconds):
        moved_robots = []
        quad_counts = self.get_empty_quad_counts()
        for robot in self.robots:
            moved_robots.append(Robot(robot.get_position_after_moves(after_seconds, self.area_height, self.area_width), Point(0,0)))
            quad = self.get_quadrant_of_point(moved_robots[-1].position, self.area_height, self.area_width)
            logging.info(f"Quadrant {quad}")
            if quad is not None:
                quad_counts[quad] += 1
        self.display_area(moved_robots)
        logging.info(quad_counts)
        mult = 1
        for x in quad_counts.values():
            mult = mult * x
        return mult

    def find_christmas_tree(self):
        elapsed = 0
        robots = self.robots.copy()
        quad_counts = self.get_empty_quad_counts()
        quad_counts[None] = 0
        self.avg = 0
        while not self.detect_christmas_tree(robots, quad_counts, elapsed):
            moved_robots = []
            for robot in robots:
                moved_robots.append(
                    Robot(robot.get_position_after_moves(1, self.area_height, self.area_width),
                          robot.velocity))
                quad = self.get_quadrant_of_point(moved_robots[-1].position, self.area_height, self.area_width)
                quad_counts[quad] += 1
            elapsed += 1
            robots = moved_robots
            #Might be able to optimize a bit my modifying robots in place?
        return elapsed

    def detect_christmas_tree_manual(self, robots, quadrants, iterations):
        if (iterations-88)%101 == 0: #quadrants[None] == self.area_width + 1:
            logging.info("Christmas tree?")
            self.display_area(robots)
            logging.info(f"Iteration {iterations}")
            input('enter')
#Iterations 88 189 290 391 492 start to show numbers congregating towards the center
# By observations, iteration 8168 has the christmas tree (using my eyes for image detection)

    def detect_christmas_tree(self, robots, quadrants, iterations):
        space = self.get_area(robots)
        #self.log_area(space)
        total_neighbours = self.get_number_of_neighbours(space)
        logging.info(f"{iterations}: neighbours {total_neighbours} {self.avg}")
        # When things line up, the number of averages should jump
        if self.avg > 0 and total_neighbours > 2*self.avg:
            self.log_area(space)
            return True
        self.avg = (self.avg * iterations + total_neighbours)/(iterations+1)

    def get_number_of_neighbours(self, space_array):
        # For each point in the array, count the number of neighbours
        # Using convolution: convolve with a 3x3 array of 1's, each point in the result is the
        # sum of it and its neighbouring values
        # See https://stackoverflow.com/questions/12612663/counting-of-adjacent-cells-in-a-numpy-array
        c = scipy.signal.convolve2d(space_array, np.ones((3,3)), mode='same')
        # Mask off zeroes from the original array, we don't care about their counts
        res = c * space_array
        #I"m not sure whether this quite works for the spaces with >1 correctly, but
        # for this application I only care about the statistics of it so I'm not bothering
        # to check whether it works
        return np.sum(res)


if __name__ == '__main__':
    parser = getBasicArgparser('day 14')

    args = parser.parse_args()

    day14 = Day14(args.i, args.v)

    print(f"Part 1: safety factor is {day14.get_num_robots_per_quadrant(100)}")
    print(f"Part 2: christmas tree happens at {day14.find_christmas_tree()}")
