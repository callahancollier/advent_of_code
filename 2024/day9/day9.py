#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from dataclasses import dataclass


EMPTY = '.'


@dataclass
class Entry:
    id : int
    empty : bool
    length : int


class Day9(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.disk_map = self.parse_file()

    def parse_file(self):
        disk_map = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                disk_map += [int(x) for x in line]
        return disk_map

    def parse_disk_map(self):
        block_sizes = []
        empty_space_sizes = []
        block = True
        for x in self.disk_map:
            if block:
                block_sizes.append(x)
            else:
                empty_space_sizes.append(x)
            block = not block
        if len(block_sizes) != len(empty_space_sizes)+1:
            logging.error("Bad assumption!")
            sys.exit(1)
        return block_sizes, empty_space_sizes

    def parse_disk_map_for_part_2(self):
        disk_map = []
        block_id = 0
        empty_id = 0
        empty = False
        for x in self.disk_map:
            if empty:
                disk_map.append(Entry(id=empty_id, empty=empty, length=x))
                empty_id += 1
            else:
                disk_map.append(Entry(id=block_id, empty=empty, length=x))
                block_id += 1
            empty = not empty
        return disk_map, block_id, empty_id

    def calculate_checksum_move_file_by_block(self):
        block_sizes, empty_space_sizes = self.parse_disk_map()
        left_idx = 0
        right_idx = len(block_sizes)-1
        position = 0
        defragged = ''
        checksum = 0
        while left_idx < right_idx:
            #file id is the index
            #position is the position
            #alternate between the left and right to simulate the block movement
            for i in range(block_sizes[left_idx]):
                logging.info(f"Checksum add {left_idx}*{position} (left),  sizes {block_sizes}")
                checksum += left_idx*position
                position += 1
                defragged += str(left_idx)
            if left_idx >= len(empty_space_sizes):
                break
            for i in range(empty_space_sizes[left_idx]):
                if left_idx != right_idx:
                    logging.info(f"Checksum add {right_idx}*{position} (right), sizes {block_sizes}")
                    checksum += right_idx*position
                    position += 1
                    defragged += str(right_idx)
                    block_sizes[right_idx] -= 1
                    if block_sizes[right_idx] == 0:
                        right_idx -= 1
            left_idx += 1
            logging.info(f"Done loop left={left_idx}, right={right_idx}")
        while block_sizes[right_idx] != 0 and left_idx <= right_idx:
            logging.info(f"Checksum add {right_idx}*{position} (right), sizes {block_sizes}!")
            checksum += right_idx*position
            position += 1
            defragged += str(right_idx)
            block_sizes[right_idx] -= 1

        logging.info(defragged)
        return checksum

    def print_entry_list(self, disk_map):
        if self.debug:
            result = ''
            for entry in disk_map:
                if entry.empty:
                    result += entry.length*EMPTY
                else:
                    if entry.id > 9:
                        logging.info("Warning: won't print quite right for multi-digit ids")
                    result += entry.length*str(entry.id)
            logging.info(result)

    def calculate_checksum_move_file_whole(self):
        disk_map, n_block, n_empty = self.parse_disk_map_for_part_2()
        logging.info(f"Disk map has {len(disk_map)} entries")
        first_empty = 1
        last_full = len(disk_map)-1
        self.print_entry_list(disk_map)
        while last_full > 0:
            found = False
            for i in range(first_empty, last_full+1):
                logging.info(f"Check {i}: {disk_map[i]} against {last_full}: {disk_map[last_full]}")
                if disk_map[i].empty and disk_map[i].length >= disk_map[last_full].length:
                    logging.info(f"Swap {last_full} into {i}")
                    disk_map[i].length -= disk_map[last_full].length #reduce the empty
                    disk_map[last_full].empty = True #remove from
                    disk_map.insert(i, Entry(id=disk_map[last_full].id, empty=False, length=disk_map[last_full].length))#insert to
                    while not disk_map[first_empty].empty:
                        first_empty += 1
                    while disk_map[last_full].empty:
                        last_full -= 1

                    self.print_entry_list(disk_map)
                    found = True
                    break
            logging.info(f"Was a candidate found? {found}")
            if not found:
                #Move on
                last_full -= 1
                while disk_map[last_full].empty:
                    last_full -= 1

        checksum = 0
        n = 0
        for i in range(len(disk_map)):
            if not disk_map[i].empty:
                for j in range(disk_map[i].length):
                    logging.info(f"Checksum add {disk_map[i].id} * {n}")
                    checksum += disk_map[i].id * n
                    n += 1
            else:
                n += disk_map[i].length
        return checksum


if __name__ == '__main__':
    parser = getBasicArgparser('day 9')

    args = parser.parse_args()

    day9 = Day9(args.i, args.v)

    print(f"Part 1: Checksum is {day9.calculate_checksum_move_file_by_block()}")
    print(f"Part 2: Checksum is {day9.calculate_checksum_move_file_whole()}")
