#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day21(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.possible_allergens = {}
        self.all_ingredients = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if "contains" in line:
                    line = line.split(' (')
                    allergen_list = line[1][:-1]  # Strip the ')'
                    allergen_list = allergen_list.removeprefix('contains ')
                    allergen_list = allergen_list.split(', ')
                    ingredients = line[0].split(' ')

                    self.debugPrint("{} contains {}".format(ingredients, allergen_list))

                    self.all_ingredients = self.all_ingredients + ingredients
                    for allergen in allergen_list:
                        if allergen not in self.possible_allergens:
                            self.debugPrint("Start {}".format(allergen))
                            self.possible_allergens[allergen] = []  # list of sets
                        self.debugPrint("Append to {}".format(allergen))
                        self.possible_allergens[allergen].append(set(ingredients))
                        self.debugPrint("{} is now {}".format(allergen, self.possible_allergens[allergen]))

    def get_filtered_allergens(self):
        filtered = {}
        for allergen in self.possible_allergens:
            self.debugPrint("Ingredients that might contain {}: {}".format(allergen, self.possible_allergens[allergen]))
            # Get the intersection -- entries that are in all the lists
            u = set.intersection(*self.possible_allergens[allergen])
            self.debugPrint("    intersection: {}".format(u))
            filtered[allergen] = u
        self.debugPrint("Filtered: {}".format(filtered))
        return filtered

    def get_definitely_safe_ingredients(self):
        allergen_possibilities = set()
        filtered = self.get_filtered_allergens()
        for filtered_allergen in filtered:
            allergen_possibilities.update(filtered[filtered_allergen])
        self.debugPrint("Possible allergens: {}".format(allergen_possibilities))
        # return all ingredients that are not in the potential allergens
        return set(self.all_ingredients) - allergen_possibilities

    def get_allergen_list(self):
        allergens = {}
        filtered = self.get_filtered_allergens()
        max_iter = 10
        while len(allergens) != len(filtered):
            max_iter = max_iter - 1
            if max_iter == 0:
                raise RuntimeError("Too many iterations")
            for allergen in filtered:
                if allergen in allergens:
                    continue
                self.debugPrint("Check {}: {}".format(allergen, filtered[allergen]))
                if len(filtered[allergen]) == 1:
                    self.debugPrint("   {} is done".format(allergen))
                    allergens[allergen] = list(filtered[allergen])[0]
                else:
                    new_set = set()
                    for ingredient in filtered[allergen]:
                        self.debugPrint("    Already done: {}".format(list(allergens.values())))
                        if ingredient not in list(allergens.values()):
                            self.debugPrint("        {} is not in done".format(ingredient))
                            new_set.add(ingredient)
                    filtered[allergen] = new_set
                    self.debugPrint("    new set: {}".format(new_set))
        allergen_list = list(allergens.keys())
        allergen_list.sort()
        dangerous = [allergens[allergen] for allergen in allergen_list]
        self.debugPrint("Final list of dangerous ingredients: {}".format(dangerous))
        return ','.join([str(x) for x in dangerous])

    def get_max_list_size(self, filtered_allergens):
        max_list_size = 0
        for key in filtered_allergens:
            if len(filtered_allergens[key]) > max_list_size:
                max_list_size = len(filtered_allergens[key])
        return max_list_size

    def count_safe_ingredients(self):
        safe = self.get_definitely_safe_ingredients()
        self.debugPrint("Definitely safe: {}".format(safe))
        count = 0
        for ingredient in safe:
            count = count + self.all_ingredients.count(ingredient)
        return count


if __name__ == '__main__':
    parser = getBasicArgparser('day 21')

    args = parser.parse_args()

    day21 = Day21(args.i, args.v)

    print("Definitely safe ingredients appear {} times".format(day21.count_safe_ingredients()))
    print("Canonical dangerous ingredient list: {}".format(day21.get_allergen_list()))
