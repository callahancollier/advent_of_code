#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class DayOne(BaseFunctions):
    def __init__(self, inputFile):
        super().__init__(inputFile, False)
        self.data = []

        self.readData()

    def readData(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                self.data.append(int(line[0]))

    def mult(self, values):
        mult = 1
        for val in values:
            mult = mult * val
        return mult

    def getTwoThatAddTo2020(self):
        answer = []
        for valA in self.data:
            for valB in self.data:
                testSum = valA + valB
                if testSum == 2020:
                    answer.append(valA)
                    answer.append(valB)
                    return answer

    def getThreeThatAddTo2020(self):
        answer = []
        for valA in self.data:
            for valB in self.data:
                for valC in self.data:
                    testSum = valA + valB + valC
                    if testSum == 2020:
                        answer.append(valA)
                        answer.append(valB)
                        answer.append(valC)
                        return answer


if __name__ == '__main__':
    parser = getBasicArgparser("day 1")

    args = parser.parse_args()

    dayOne = DayOne(args.i)

    answer = dayOne.getTwoThatAddTo2020()
    print('Part one: {}'.format(dayOne.mult(answer)))

    answer = dayOne.getThreeThatAddTo2020()
    print('Part two: {}'.format(dayOne.mult(answer)))

