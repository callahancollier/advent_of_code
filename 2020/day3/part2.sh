#!/bin/bash

slopeRights=(
1
3
5
7
1
)

slopeDowns=(
1
1
1
1
2
)

mult=1
for index in ${!slopeRights[*]}
do
  echo "python3 day3.py -r ${slopeRights[$index]} -d ${slopeDowns[$index]} -i input.txt"
  resp=$(python3 day3.py -r ${slopeRights[$index]} -d ${slopeDowns[$index]} -i input.txt)
  echo "$resp"
  result=$(echo "$resp" | sed -n 's/.*: \([0-9]\{1,3\}\) trees/\1/p')
  mult=$((mult*result))
done

echo "Final result: $mult"