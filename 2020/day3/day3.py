#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day3(BaseFunctions):
    def __init__(self, inputFileName, slopeR, slopeD, enableDebug):
        super().__init__(inputFileName, enableDebug)
        self.slopeR = slopeR
        self.slopeD = slopeD

        self.map = []
        self.parseFile()
        self.mapDepth = len(self.map)
        self.mapWidth = len(self.map[0])

        self.debugPrint("STARTING MAP:")
        self.printMap()

    def printMap(self):
        for row in self.map:
            self.debugPrint(row)

    def parseFile(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                self.map.append(list(line[0]))

    def traverseMapGetTrees(self):
        idx_x = 0
        idx_y = 0
        trees = 0
        while idx_y < self.mapDepth:
            self.debugPrint("x {} y {} depth {} width {}".format(idx_x, idx_y, self.mapDepth, self.mapWidth))
            if self.map[idx_y][idx_x] == '#':
                trees = trees + 1
                self.map[idx_y][idx_x] = 'X'
            else:
                self.map[idx_y][idx_x] = 'O'

            idx_x = idx_x + self.slopeR
            while idx_x >= self.mapWidth:
                idx_x = idx_x - self.mapWidth
            idx_y = idx_y + self.slopeD
        self.debugPrint("END MAP:")
        self.printMap()
        return trees


if __name__ == '__main__':
    parser = getBasicArgparser("day 3")
    parser.add_argument('-r', type=int, help='slope, right component', required=True)
    parser.add_argument('-d', type=int, help='slope, down component', required=True)

    args = parser.parse_args()

    day3 = Day3(args.i, args.r, args.d, args.v)

    print('Slope of {} right, {} down: {} trees'.format(args.r, args.d, day3.traverseMapGetTrees()))
