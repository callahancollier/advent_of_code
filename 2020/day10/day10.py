#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day10(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.adapters = []
        self.parse_file()

        self.groupings = []
        self.one_jolt_diffs = 0
        self.three_jolt_diffs = 0

        self.combo_list = {}

    def getOneJoltDiffs(self):
        return self.one_jolt_diffs

    def getThreeJoltDiffs(self):
        return self.three_jolt_diffs

    def parse_file(self):
        # Start with the outlet (0)
        self.adapters = [ 0 ]
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.adapters.append(int(line))
        # Built-in adapter always 3 greater than the max
        self.adapters.append(max(self.adapters) + 3)
        self.adapters.sort()

    def workOutAdapterChain(self):
        # Since they are sorted, they are already in order...
        for i in range(0, len(self.adapters)-1):
            if self.adapters[i+1] - self.adapters[i] == 1:
                self.one_jolt_diffs = self.one_jolt_diffs + 1
                self.groupings.append(1)
            elif self.adapters[i + 1] - self.adapters[i] == 3:
                self.three_jolt_diffs = self.three_jolt_diffs + 1
                self.groupings.append(3)
            else:
                print("ERROR: Difference between {} and {} is {}".format(
                    self.adapters[i+1], self.adapters[i],
                    self.adapters[i + 1] - self.adapters[i]
                ))

    def getCombos(self):
        array = [0] * (max(self.adapters)+1)
        for idx in self.adapters:
            array[idx] = 1
        # Theory:
        # Gather groupings of 1s and number of 1s in each
        groups = []
        num_1s = 0
        for interval in array:
            if interval == 1:
                num_1s = num_1s + 1
            else:
                if num_1s != 0:
                    groups.append(num_1s)
                num_1s = 0
        if num_1s != 0:
            groups.append(num_1s)
        self.debugPrint(self.adapters)
        self.debugPrint(array)
        self.debugPrint(groups)
        mult = 1
        for combo in groups:
            # combos are 2^(N-1)
            # multiply all the combos together
            #mult_by = (2**(combo-1))
            mult_by = 1
            if combo > 2:
                mult_by = self.get_num_combos(combo-2)
            self.debugPrint("Mult: {} (from {})".format(mult_by, combo))
            mult = mult * mult_by
        return mult

    def get_num_combos(self, number):
        if number in self.combo_list:
            return self.combo_list[number]
        invalid = 0
        for val in range(0, 2**number):
            binary = format(val, '0{}b'.format(number)) # bin(val)[2:]
            consecutive_zeroes = 0
            for bit in binary:
                if bit == '0':
                    consecutive_zeroes = consecutive_zeroes + 1
                elif bit == '1':
                    if consecutive_zeroes > 2:
                        invalid = invalid + 1
                    consecutive_zeroes = 0
            if consecutive_zeroes > 2:
                invalid = invalid + 1
            self.debugPrint("{}: {} consecutive zeroes".format(binary, consecutive_zeroes))
        self.combo_list[number] = 2**number - invalid
        self.debugPrint("For group of {}, {} invalid, {} combos".format(
            number, invalid, self.combo_list[number]))
        return self.combo_list[number]

    def runTest(self, numTests):
        self.get_num_combos(numTests)


if __name__ == '__main__':
    parser = getBasicArgparser('day 10')
    parser.add_argument('-t', type=int, help='Run test instead of actual solution', default=None)

    args = parser.parse_args()

    day10 = Day10(args.i, args.v)
    day10.workOutAdapterChain()

    if args.t is not None:
        day10.runTest(args.t)
        sys.exit(0)

    onejolts = day10.getOneJoltDiffs()
    threejolts = day10.getThreeJoltDiffs()

    print("There are {} 1-jolt diffs and {} 3-jolt diffs".format(
        onejolts, threejolts))

    print("1-jolts multiplied by 3-jolts: {}".format(onejolts * threejolts))
    print("There are {} combinations".format(day10.getCombos()))
