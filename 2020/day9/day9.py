#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day9(BaseFunctions):
    def __init__(self, input_file, enable_debug, window_size):
        super().__init__(input_file, enable_debug)
        self.window_len = window_size
        self.window = []
        self.data = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.data.append(int(line))

    def getEncryptionWeakness(self):
        first_invalid = self.findFirstNoncompliant()
        contiguous_sum = self.getContiguousSumTo(first_invalid)
        if contiguous_sum == None:
            print("ERROR: A contiguous sum was not found for {}".format(first_invalid))
            return 0
        self.debugPrint("{} sum to {}".format(contiguous_sum, first_invalid))
        min_val = min(contiguous_sum)
        max_val = max(contiguous_sum)
        return min_val + max_val

    def getContiguousSumTo(self, target):
        for start_idx in range(0, len(self.data)):
            contiguous = [self.data[start_idx]]
            next_idx = start_idx + 1
            self.debugPrint("Start: {}".format(contiguous))
            while self.getSum(contiguous) < target:
                contiguous.append(self.data[next_idx])
                next_idx = next_idx + 1
                self.debugPrint("     : {}".format(contiguous))
            if self.getSum(contiguous) == target:
                return contiguous
        return None

    def getSum(self, list):
        sum = 0
        for item in list:
            sum = sum + item
        return sum

    def findFirstNoncompliant(self):
        # Fetch the preamble
        window = self.data[0:self.window_len]
        self.debugPrint("Preamble: {}".format(window))
        for next_val in self.data[self.window_len:]:
            if not self.isValueSumOfTwoWindowValues(next_val, window):
                return next_val
            window = window[1:] + [next_val]

    def isValueSumOfTwoWindowValues(self, value, window):
        for val_a in window:
            for val_b in window:
                self.debugPrint("{} + {} = {}".format(val_a, val_b, val_a + val_b))
                if val_a + val_b == value:
                    return True
        return False


if __name__ == '__main__':
    parser = getBasicArgparser('day 9')
    parser.add_argument('-w', type=int, help='window size', default=25)

    args = parser.parse_args()

    day9 = Day9(args.i, args.v, args.w)

    print("First number that doesn't follow the rule: {}".format(day9.findFirstNoncompliant()))

    print("Encryption weakness: {}".format(day9.getEncryptionWeakness()))
