#!/usr/bin/env python3

import csv
import sys
import numpy as np

sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day17(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)

        self.pocket_dimension = None
        self.ACTIVE = 1
        self.INACTIVE = 0
        self.mapping = {
            '.': self.INACTIVE,
            '#': self.ACTIVE
        }

        self.parse_file()
        self.debugPrint("Start:\n{}".format(self.pocket_dimension))

        num_z = np.ma.size(self.pocket_dimension, 0)
        num_y = np.ma.size(self.pocket_dimension, 1)
        num_x = np.ma.size(self.pocket_dimension, 2)
        for z in range(0, num_z):
            for y in range(0, num_y):
                for x in range(0, num_x):
                    self.debugPrint("[{}][{}][{}] = {}".format(
                        z, y, x, self.pocket_dimension[z][y][x]))

    def map_char(self, char):
        return self.mapping[char]

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            line_num = 0
            for line in reader:
                line = line[0]
                if self.pocket_dimension is None:
                    # assume the initial state is square
                    self.pocket_dimension = np.zeros((1, len(line), len(line)))
                self.debugPrint("Add in {}".format(line))
                for i in range(0, len(line)):
                    self.pocket_dimension[0][line_num][i] = self.map_char(line[i])
                line_num = line_num + 1

    def run_cycles(self, num_cycles):
        for i in range(0, num_cycles):
            self.run_cycle()

    def run_cycle(self):
        # Extend the array in all directions by 2
        for axis in [0, 1, 2]:
            self.pocket_dimension = np.insert(self.pocket_dimension, 0, values=0, axis=axis)
            self.pocket_dimension = np.insert(self.pocket_dimension, np.ma.size(self.pocket_dimension, axis), values=0,
                                              axis=axis)
        self.debugPrint("Resized:\n{}".format(self.pocket_dimension))
        num_z = np.ma.size(self.pocket_dimension, 0)
        num_y = np.ma.size(self.pocket_dimension, 1)
        num_x = np.ma.size(self.pocket_dimension, 2)
        new_state = np.zeros((num_z, num_y, num_x))
        self.debugPrint("Dimensions: z {} y {} x {}".format(num_z, num_y, num_x))
        for z in range(0, num_z):
            for y in range(0, num_y):
                for x in range(0, num_x):
                    #self.debugPrint("[{}][{}][{}] = {}".format(
                    #    z, y, x, self.pocket_dimension[z][y][x]))
                    new_state[z][y][x] = self.get_next_state(x, y, z)
        self.debugPrint("New state:\n{}".format(new_state))
        self.pocket_dimension = new_state
        self.trim_zeros()
        self.debugPrint("Trimmed:\n{}".format(self.pocket_dimension))

    def get_next_state(self, x, y, z):
        current = np.copy(self.pocket_dimension[z][y][x])
        num_active_neighbours = 0
        for z_mod in [-1, 0, 1]:
            for y_mod in [-1, 0, 1]:
                for x_mod in [-1, 0, 1]:
                    if ((z + z_mod >= np.ma.size(self.pocket_dimension, 0)) or
                            (y + y_mod >= np.ma.size(self.pocket_dimension, 1)) or
                            (x + x_mod >= np.ma.size(self.pocket_dimension, 2))):
                        continue


                    if self.pocket_dimension[z + z_mod][y + y_mod][x + x_mod] == self.ACTIVE:
                        num_active_neighbours = num_active_neighbours + 1
        # the point itself was also counted, so account for that
        num_active_neighbours = num_active_neighbours - current
        self.debugPrint("{} {} {} has {} active neighbours, is currently {}".format(
            z,y,x,num_active_neighbours, current))
        if current == self.ACTIVE:
            if num_active_neighbours == 2 or num_active_neighbours == 3:
                pass
            else:
                self.debugPrint("clear {} {} {}".format(z, y, x))
                current = self.INACTIVE
        else:
            if num_active_neighbours == 3:
                self.debugPrint("set {} {} {}".format(z, y, x))
                current = self.ACTIVE
        return current

    def get_num_active_cubes(self):
        return np.count_nonzero(self.pocket_dimension)

    def trim_zeros(self):
        #remove any dimensions of the array that are all zeros
        z = 0
        y = 0
        x = 0
        while np.count_nonzero(self.pocket_dimension[z,:,:]) == 0:
            z = z + 1
        while np.count_nonzero(self.pocket_dimension[:,y,:]) == 0:
            y = y + 1
        while np.count_nonzero(self.pocket_dimension[:,:,x]) == 0:
            x = x + 1
        self.pocket_dimension = self.pocket_dimension[z:,y:,x:]
        # repeat from the other direction
        z = np.ma.size(self.pocket_dimension, 0) - 1
        y = np.ma.size(self.pocket_dimension, 1) - 1
        x = np.ma.size(self.pocket_dimension, 2) - 1
        while np.count_nonzero(self.pocket_dimension[z,:,:]) == 0:
            z = z - 1
        while np.count_nonzero(self.pocket_dimension[:,y,:]) == 0:
            y = y - 1
        while np.count_nonzero(self.pocket_dimension[:,:,x]) == 0:
            x = x - 1

if __name__ == '__main__':
    parser = getBasicArgparser('day 17')
    parser.add_argument('-n', type=int, help='Number of cycles', required=True)

    args = parser.parse_args()

    day17 = Day17(args.i, args.v)

    day17.run_cycles(args.n)

    print("After {} cycles, there are {} active cubes".format(args.n, day17.get_num_active_cubes()))
