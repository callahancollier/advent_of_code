#!/usr/bin/env python3

import csv
import sys
import math
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


@dataclass
class Instr:
    cmd: str
    val: int


@dataclass
class Direction:
    easterly: int
    northerly: int


@dataclass
class Position:
    east: int
    north: int
    direction: Direction


def rotateDirectionRight(direction):
    new_east = direction.northerly
    new_north = direction.easterly * -1
    return Direction(new_east, new_north)

def rotatePositionRight(position):
    new_east = position.north
    new_north = position.east* -1
    return Position(new_east, new_north, position.direction)


class Day12(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.instructions = []
        self.position = Position(0, 0, Direction(1, 0))
        self.waypoint = Position(10, 1, Direction(0, 0))

        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.instructions.append(Instr(line[0], int(line[1:])))
        self.debugPrint("Instructions: {}".format(self.instructions))

    def follow_part1_instructions(self):
        self.position = Position(0, 0, Direction(1, 0))
        self.debugPrint("Start position: {}".format(self.position))
        for instr in self.instructions:
            self.debugPrint("Instruction: {}".format(instr))
            if instr.cmd == "N":
                self.position.north = self.position.north + instr.val
            if instr.cmd == "S":
                self.position.north = self.position.north - instr.val
            if instr.cmd == "E":
                self.position.east = self.position.east + instr.val
            if instr.cmd == "W":
                self.position.east = self.position.east - instr.val
            if instr.cmd == "L":
                if instr.val % 90 != 0:
                    print("ERROR: Requested turn by {} degrees".format(instr.val))
                num_turns = int(instr.val/90)
                # one left turn is equal to 3 right turns
                self.rotateShipRight(num_turns*3)
            if instr.cmd == "R":
                if instr.val % 90 != 0:
                    print("ERROR: Requested turn by {} degrees".format(instr.val))
                num_turns = int(instr.val/90)
                self.rotateShipRight(num_turns)
            if instr.cmd == "F":
                self.position.north = self.position.north + instr.val * self.position.direction.northerly
                self.position.east = self.position.east + instr.val * self.position.direction.easterly
            self.debugPrint("Next position: {}".format(self.position))

    def rotateShipRight(self, num_turns):
        self.debugPrint("Turn {} times".format(num_turns))
        for i in range(0, num_turns):
            self.position.direction = rotateDirectionRight(self.position.direction)

    def getManhattanDistance(self):
        return abs(self.position.east) + abs(self.position.north)

    def follow_part2_instructions(self):
        self.position = Position(0, 0, Direction(1, 0))
        self.waypoint = Position(10, 1, Direction(0, 0)) # Is relative to the ship
        self.debugPrint("Start position: {}".format(self.position))
        self.debugPrint("  Start waypoint: {}".format(self.waypoint))
        for instr in self.instructions:
            self.debugPrint("Instruction: {}".format(instr))
            if instr.cmd == "N":
                self.waypoint.north = self.waypoint.north + instr.val
            if instr.cmd == "S":
                self.waypoint.north = self.waypoint.north - instr.val
            if instr.cmd == "E":
                self.waypoint.east = self.waypoint.east + instr.val
            if instr.cmd == "W":
                self.waypoint.east = self.waypoint.east - instr.val
            if instr.cmd == "L":
                if instr.val % 90 != 0:
                    print("ERROR: Requested turn by {} degrees".format(instr.val))
                num_turns = int(instr.val/90)
                # one left turn is equal to 3 right turns
                self.rotateWaypointRight(num_turns*3)
            if instr.cmd == "R":
                if instr.val % 90 != 0:
                    print("ERROR: Requested turn by {} degrees".format(instr.val))
                num_turns = int(instr.val/90)
                self.rotateWaypointRight(num_turns)
            if instr.cmd == "F":
                self.position.north = self.position.north + instr.val * self.waypoint.north
                self.position.east = self.position.east + instr.val * self.waypoint.east
            self.debugPrint("Next position: {}".format(self.position))
            self.debugPrint("  Next waypoint {}".format(self.waypoint))

    def rotateWaypointRight(self, num_turns):
        self.debugPrint("Turn {} times".format(num_turns))
        for i in range(0, num_turns):
            self.waypoint = rotatePositionRight(self.waypoint)


if __name__ == '__main__':
    parser = getBasicArgparser('day 12')

    args = parser.parse_args()

    day12 = Day12(args.i, args.v)

    day12.follow_part1_instructions()
    day12.debugPrint("***")
    print("Part 1: Manhattan distance: {}".format(day12.getManhattanDistance()))
    day12.debugPrint("-------------------------")

    day12.follow_part2_instructions()
    day12.debugPrint("***")
    print("Part 2: Manhattan distance: {}".format(day12.getManhattanDistance()))
