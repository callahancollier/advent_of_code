#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day18(BaseFunctions):
    def __init__(self, input_file, enable_debug, part):
        super().__init__(input_file, enable_debug)

        self.part = part
        if self.part > 2 or self.part < 1:
            raise ValueError("Unknown part {}".format(self.part))
        # division is not mentioned so we'll assume its the same as multiplication...
        # I don't think there's any actual division in the puzzle
        self.precedence = {'+': 2, '*': 1, '/': 1, '(': -1}

        self.expression_results = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if self.part == 1:
                    newline = ""
                    # This evaluates from left to right where normal rpn goes the other way,
                    # so reverse the order of the string (flip all the parens) to account for that
                    for i in range(len(line)-1, -1, -1):
                        if line[i] == '(':
                            newline = newline + ')'
                        elif line[i] == ')':
                            newline = newline + '('
                        else:
                            newline = newline + line[i]
                    line = newline
                self.expression_results.append(
                    self.evaluate_rpn(self.convert_to_rpn([x for x in line.replace(' ', '')])))
                self.debugPrint("-----------------------")
                self.debugPrint("{} = {}".format(line, self.expression_results[-1]))
                self.debugPrint("-----------------------")

    def convert_to_rpn(self, expression):
        # First, sanity check -- make sure parentheses are matched
        if expression.count('(') != expression.count(')'):
            raise ValueError("ERROR: Mismatched parentheses!")
        self.debugPrint("As infix: {}".format(' '.join(expression)))
        # A variation on the Shunting Yard algorithm
        operator_stack = []
        output = []
        for x in expression:
            if x.isnumeric():
                output.append(x)
            elif x == '(':
                operator_stack.append(x)
            elif x in ['+', '*', '/']:
                if self.part == 1:
                    operator_stack.append(x)
                else:
                    if len(operator_stack) == 0 or self.precedence[x] > self.precedence[operator_stack[-1]]:
                        operator_stack.append(x)
                    else:
                        output.append(operator_stack.pop())
                        operator_stack.append(x)
            elif x == ')':
                while operator_stack[-1] != '(':
                    output.append(operator_stack.pop())
                # Throw away the '('
                operator_stack.pop()
            else:
                raise ValueError("Unknown token {}".format(x))
        while len(operator_stack) != 0:
            if operator_stack[-1] == '(':
                operator_stack.pop()
            else:
                output.append(operator_stack.pop())
        return output

    def evaluate_rpn(self, rpn):
        stack = []
        self.debugPrint("RPN: {}".format(rpn))
        for token in rpn:
            self.debugPrint("Token: {}".format(token))
            # If it's a number, push onto the stack
            # If it's an operator, pop two numbers and apply the operator to them
            if token.isnumeric():
                stack.append(token)
            else:
                if len(stack) < 2:
                    raise RuntimeError(
                        "Not enough tokens to do operation {}".format(token))
                a = int(stack.pop())
                b = int(stack.pop())
                self.debugPrint("   {} {} {}".format(a, b, token))
                if token == '+':
                    result = str(a + b)
                elif token == "*":
                    result = str(a * b)
                elif token == "/":
                    result = str(a * b)
                else:
                    raise ValueError("Unknown operator {}".format(token))
                self.debugPrint("      result {}".format(result))
                stack.append(result)
        if len(stack) != 1:
            raise ValueError("End of tokens, but stack contains {}".format(stack))
        return int(stack[0])

    def get_sum_of_results(self):
        self.debugPrint("Results: {}".format(self.expression_results))
        resultSum = 0
        for val in self.expression_results:
            resultSum = resultSum + val
        return resultSum


if __name__ == '__main__':
    parser = getBasicArgparser('day 18')
    parser.add_argument('-p', type=int, help='Run part 1 or part 2', required=True)

    args = parser.parse_args()

    day18 = Day18(args.i, args.v, args.p)

    print("Sum of all results: {}".format(day18.get_sum_of_results()))
