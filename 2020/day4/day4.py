#!/usr/bin/env python3

import csv
import functools
import re
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day4(BaseFunctions):
    def __init__(self, inputFile, enableDebug):
        super().__init__(inputFile, enableDebug)
        self.passports = []

        self.required_fields = [ "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" ]
        self.optional_fields = [ "cid" ]

        self.validations = {
            "byr": functools.partial(self.validateWithRange, min=1920, max=2002, numDigits=4),
            "iyr": functools.partial(self.validateWithRange, min=2010, max=2020, numDigits=4),
            "eyr": functools.partial(self.validateWithRange, min=2020, max=2030, numDigits=4),
            "hgt": functools.partial(self.validateHeight, cm_min=150, cm_max=193, in_min=59, in_max=76),
            "hcl": self.validateColour,
            "ecl": self.validateEyeColour,
            "pid": functools.partial(self.validateWithRange, min=-1, max=-1, numDigits=9),
            "cid": self.validateAlwaysTrue
        }
        self.importPassportData()


    def debugPrint(self, text):
        if self.debug:
            print(text)

    def importPassportData(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            this_passport = {}
            for line in reader:
                if len(line) == 0:
                    self.passports.append(this_passport)
                    this_passport = {}
                    continue

                entries = line[0].split(' ')
                for entry in entries:
                    data = entry.split(':')
                    if len(data) != 2:
                        print("ERROR: expected one key value pair, got {} from {}".format(data, entry))
                    this_passport[data[0]] = data[1]
            if len(this_passport) != 0:
                self.passports.append(this_passport)
        print("Found {} passports".format(len(self.passports)))

    def getPassportsWithAllRequiredFields(self):
        validPassports = []
        for passport in self.passports:
            isValid = True
            for field in self.required_fields:
                if field not in passport:
                    isValid = False
            if isValid:
                validPassports.append(passport)
        return validPassports

    def getPassportsWithAllFieldsValid(self):
        passports = self.getPassportsWithAllRequiredFields()
        validPassports = []
        for passport in passports:
            self.debugPrint('-----------------------------------------')
            allFieldsValid = True
            for field in passport:
                self.debugPrint("Validate field {} value {}".format(field, passport[field]))
                if not self.validations[field](passport[field]):
                    self.debugPrint("Field {} is invalid".format(field))
                    allFieldsValid = False
            if allFieldsValid:
                validPassports.append(passport)
        return validPassports

    def validateAlwaysTrue(self, unused):
        return True

    def validateWithRange(self, val, min, max, numDigits):
        if min == -1 or int(val) >= min:
            if max == -1 or int(val) <= max:
                if len(str(val)) == numDigits:
                    return True
        return False

    def validateHeight(self, valAndUnit, cm_min, cm_max, in_min, in_max):
        tmp = re.findall(r'[A-Za-z]+|\d+', valAndUnit)
        if len(tmp) != 2:
            return False
        val = tmp[0]
        units = tmp[1]
        if units == "cm":
            return self.validateWithRange(val, cm_min, cm_max, len(str(val)))
        else:
            return self.validateWithRange(val, in_min, in_max, len(str(val)))

    def validateColour(self, colourStr):
        if len(re.findall(r'#[a-f0-9]{6}', colourStr)) != 1:
            return False
        return True

    def validateEyeColour(self, colourStr):
        if colourStr in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
            return True
        return False



if __name__ == '__main__':
    parser = getBasicArgparser("day 4")

    args = parser.parse_args()

    day4 = Day4(args.i, args.v)

    print("Passports with all fields present: {}".format(len(day4.getPassportsWithAllRequiredFields())))

    print("Passports with all fields present and valid: {}".format(len(day4.getPassportsWithAllFieldsValid())))
