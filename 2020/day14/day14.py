#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day14(BaseFunctions):
    def __init__(self, input_file, enable_debug, part):
        super().__init__(input_file, enable_debug)
        self.part = part

        self.LEN_MASK = 36
        self.mem = {}
        if self.part == 1:
            self.parse_file_part1()
        elif self.part == 2:
            self.parse_file_part2()
        else:
            print("ERROR: unknown part {}, only part 1 and 2 supported".format(self.part))

    def parse_file_part1(self):
        mask = ""
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                instr = line[0].split(' = ')
                if instr[0] == "mask":
                    mask = [char for char in instr[1]]
                elif instr[0].startswith("mem"):
                    addr = int(instr[0].removeprefix("mem[").removesuffix("]"))
                    val = self.convertToBinaryArray(instr[1])
                    while len(mask) != len(val):
                        val = ["0"] + val
                    self.debugPrint("addr    {}:".format(addr))
                    self.debugPrint("mask    {}".format(''.join(mask)))
                    self.debugPrint("val     {}".format(''.join(val)))
                    self.mem[addr] = self.apply_mask_to_value(mask, val)
                    self.debugPrint("result: {}".format(''.join(self.mem[addr])))
                else:
                    print("ERROR: unexpected input {}".format(line))

    def convertToBinaryArray(self, decim):
        return [char for char in bin(int(decim))[2:]]

    def apply_mask_to_value(self, mask, value):
        if len(mask) != self.LEN_MASK:
            print("ERROR: malformed mask {}".format(mask))
        if len(value) != self.LEN_MASK:
            print("ERROR: malformed value {}".format(value))
        masked = ["0"] * self.LEN_MASK
        for i in range(0, self.LEN_MASK):
            if mask[i] != "X":
                masked[i] = mask[i]
            else:
                masked[i] = value[i]
        return masked

    def getSumOfValues(self):
        total = 0
        for addr in self.mem:
            total = total + int(''.join(self.mem[addr]), 2)
        return total

    def parse_file_part2(self):
        mask = ""
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                instr = line[0].split(' = ')
                if instr[0] == "mask":
                    mask = [char for char in instr[1]]
                elif instr[0].startswith("mem"):
                    addr = self.apply_mask_to_addr(mask, int(instr[0].removeprefix("mem[").removesuffix("]")))
                    val = int(instr[1])
                    self.assign_with_floating_addr_bits(addr, val)
                else:
                    print("ERROR: unexpected input {}".format(line))

    def apply_mask_to_addr(self, mask, addr):
        addr = self.convertToBinaryArray(addr)
        self.debugPrint("addr:    {}".format(''.join(addr)))
        while len(addr) != len(mask):
            addr = ["0"] + addr
        if len(mask) != self.LEN_MASK:
            print("ERROR: malformed mask {}".format(mask))
        if len(addr) != self.LEN_MASK:
            print("ERROR: malformed addr {}".format(addr))
        masked = ["0"] * self.LEN_MASK
        for i in range(0, self.LEN_MASK):
            if mask[i] == "0":
                masked[i] = addr[i]
            else:
                masked[i] = mask[i]
        self.debugPrint("masked: {}".format(''.join(masked)))
        return masked

    def assign_with_floating_addr_bits(self, addr, value):
        addr_save = addr.copy()
        num_floats = addr.count("X")
        num_float_combos = 2**num_floats
        # Write every possible combo of X's
        for i in range(0, num_float_combos):
            addr = ''.join(addr_save)
            #binary = bin(i)[2:]
            binary = format(i, '0{}b'.format(num_floats))
            for bit in binary:
                addr = addr.replace("X", bit, 1)
            self.mem[addr] = self.convertToBinaryArray(value)
            self.debugPrint("   write {} val {}".format(addr, value))


if __name__ == '__main__':
    parser = getBasicArgparser('day 14')
    parser.add_argument('-p', type=int, help='Run part 1 or part 2', required=True)

    args = parser.parse_args()

    day14 = Day14(args.i, args.v, args.p)

    print("Sum of each value in memory: {}".format(day14.getSumOfValues()))
