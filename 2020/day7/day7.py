#!/usr/bin/env python3

import csv
import sys
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


@dataclass
class BagInfo:
    num_bags: int
    bag_name: str


class Day7(BaseFunctions):
    def __init__(self, input_file, enable_debug, my_bag):
        super().__init__(input_file, enable_debug)
        self.my_bag = my_bag

        self.bag_info = {}
        self.parse_bags()

    def parse_bags(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' contain ')
                self.debugPrint(line)
                source_bag = self.strip_bag_ending(line[0])
                other_bags = line[1].split(', ')
                contained_bags = []
                for bag in other_bags:
                    bag = self.strip_bag_ending(bag)
                    if "no other" not in bag:
                        split_bag = bag.split(' ')
                        contained_bags.append(BagInfo(int(split_bag[0]), ' '.join(split_bag[1:])))
                self.bag_info[source_bag] = contained_bags
            self.debugPrint(self.bag_info)
            self.debugPrint(self.bag_info[self.my_bag])

    def strip_bag_ending(self, bag):
        # strip the word 'bag' or 'bags'
        bag = bag.replace(' bags', '')
        bag = bag.replace(' bag', '')
        bag = bag.replace('.', '')
        return bag

    def get_num_bags_containing_yours(self):
        bag_list = set()
        bag_list = self.get_bags_containing_target(self.my_bag, bag_list)
        self.debugPrint("Final list: {}".format(bag_list))
        return len(bag_list)

    def get_bags_containing_target(self, target_bag, bag_list):
        self.debugPrint("Recurse! Current total is {}".format(bag_list))
        for bag in self.bag_info:
            for contained in self.bag_info[bag]:
                self.debugPrint("Search for {} -> {} contains {}".format(target_bag, bag, contained.bag_name))
                if contained.bag_name == target_bag:
                    self.debugPrint("--> {} contains {}".format(bag, target_bag))
                    if bag not in bag_list:
                        bag_list = self.get_bags_containing_target(bag, bag_list)
                    bag_list.add(bag)
                    self.debugPrint("Post-recurse: {}".format(bag_list))
        self.debugPrint("Return {}".format(bag_list))
        return bag_list

    def get_your_num_contained_bags(self):
        return self.get_target_contained_bags(self.my_bag, 0)

    def get_target_contained_bags(self, target_bag, current_total):
        for contained in self.bag_info[target_bag]:
            subcontained = self.get_target_contained_bags(contained.bag_name, 0)
            self.debugPrint("{} contains {} {} (and the {} within that)".format(target_bag, contained.num_bags, contained.bag_name, subcontained))
            current_total = current_total + contained.num_bags + contained.num_bags*subcontained
        return current_total


    def dep_get_target_contained_bags(self, target_bag, current_total):
        self.debugPrint("Recurse! Target {}, current total is {}".format(target_bag, current_total))
        sub_bags_sum = 0
        for contained in self.bag_info[target_bag]:
            self.debugPrint("{} contains {} {}".format(target_bag, contained.num_bags, contained.bag_name))
            additional = contained.num_bags * self.get_target_contained_bags(contained.bag_name, current_total)
            sub_bags_sum = sub_bags_sum + additional
            self.debugPrint("{} has {} additional bags".format(contained.bag_name, additional))
        if sub_bags_sum == 0:
            current_total = current_total + 1
        else:
            current_total = current_total + sub_bags_sum
        self.debugPrint("Return, {} total {}".format(target_bag, current_total))
        return current_total


if __name__ == '__main__':
    parser = getBasicArgparser('day 7')
    parser.add_argument('-p', type=int, help='Which part to run, 1 or 2', default=None)

    args = parser.parse_args()

    your_bag = "shiny gold"

    day7 = Day7(args.i, args.v, your_bag)

    if args.p == 1:
        print("{} bags can eventually contain at least one {} bag".format(day7.get_num_bags_containing_yours(), your_bag))
    elif args.p == 2:
        print("{} must contain {} other bags".format(your_bag, day7.get_your_num_contained_bags()))
    else:
        print("ERROR: invalid -p argument {}, valid args are 1 or 2".format(args.p))
