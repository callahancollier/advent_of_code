#!/usr/bin/env python3

import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day13(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.earliest_departure = 0
        self.busIDs = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if self.earliest_departure == 0:
                    self.earliest_departure = int(line)
                else:
                    self.parseBusesFromString(line)

    def parseBusesFromString(self, buses):
        self.busIDs = [self.getParsedBusID(n) for n in buses.split(',')]

    def getParsedBusID(self, val):
        if val == "x":
            return None
        else:
            return int(val)

    def getEarliestBusID(self):
        best_bus = 0
        min_depart_time = 999999999
        for bus in self.busIDs:
            if bus is None:
                continue
            closest_departure_time = (int(self.earliest_departure / bus) + 1) * bus
            if closest_departure_time < self.earliest_departure:
                print("Error: math didn't work out")
            if closest_departure_time < min_depart_time:
                min_depart_time = closest_departure_time
                best_bus = bus
        return (best_bus, (min_depart_time - self.earliest_departure))

    def getTimestampForOrganizedDeparture(self):
        # find such that
        # bus[0] departs at t
        # bus[1] departs at t+1
        # bus[N] departs at t+N

        # # Start with the first two, find where they line up the way we want
        # first_two_align = 1
        # while not self.verifyPart2(first_two_align, self.busIDs[0:2]):
        #     first_two_align = first_two_align + 1
        # # First two are aligned. They will continue to align every N, where N is the LCM
        # lcm = numpy.lcm.reduce(self.busIDs[0:2])
        # # Now add in the next one
        # first_three_align = first_two_align
        # while not self.verifyPart2(first_two_align, self.busIDs[0:3]):
        #     first_three_align = first_three_align + lcm
        # # etc etc

        # Now, generalize:
        test_time = 1
        lcm = 1
        for idx in range(2, len(self.busIDs)+1):
            while not self.verifyPart2(test_time, self.busIDs[0:idx]):
                test_time = test_time + lcm
            self.debugPrint("{} align at {}".format(self.busIDs[0:idx], test_time))
            xs_removed = [y for y in self.busIDs[0:idx] if y is not None]
            lcm = numpy.lcm.reduce(xs_removed)
        return test_time

    def getTimestampForOrganizedDepartureBruteForce(self):
        # Take 2: mild brute force
        max_bus = 0
        for bus in self.busIDs:
            if bus is not None:
                if bus > max_bus:
                    max_bus = bus
        max_bus_idx = self.busIDs.index(max_bus)

        # Test all the multiples until we find the right one
        mult = 2
        while not self.verifyPart2(mult*max_bus - max_bus_idx, self.busIDs):
            mult = mult + 1

        return mult*max_bus - max_bus_idx

    def verifyPart2(self, time, buses):
        result = True
        for i in range(0, len(buses)):
            if buses[i] is None:
                continue
            depart_time = (int((time + i) / buses[i])) * buses[i]
            self.debugPrint("{}: Bus {} departs at {}".format(i, buses[i], depart_time))
            if depart_time != (time + i):
                result = False
        return result


if __name__ == '__main__':
    parser = getBasicArgparser('day 13')
    parser.add_argument('-t', type=str, help='Test part 2 with a provided example list', default=None)

    args = parser.parse_args()

    day13 = Day13(args.i, args.v)

    if args.t is not None:
        day13.parseBusesFromString(args.t)

    busID, wait_time = day13.getEarliestBusID()
    print("Best bus: {}, wait time {}".format(busID, wait_time))
    print("ID multiplied by wait time: {}".format(busID * wait_time))

    timestamp = day13.getTimestampForOrganizedDeparture()
    print("Earliest timestamp so all buses depart at the right offsets: {}".format(timestamp))
