#!/usr/bin/env python3

import csv
import sys
sys.path.append('..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day19(BaseFunctions):
    def __init__(self, input_file, enable_debug, part):
        super().__init__(input_file, enable_debug)
        self.part = part
        self.rules = {}
        self.messages = []
        self.parse_file()

        self.debugPrint("Parsed rules: {}".format(self.rules))

    def parse_file(self):
        raw_rules = {}
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            getting_rules = True
            for line in reader:
                if len(line) == 0:
                    getting_rules = False
                    continue
                line = line[0]
                if getting_rules:
                    line = line.split(': ')
                    rule_num = int(line[0])
                    rule = line[1]
                    # strip the quotes to make it easier to parse
                    rule = rule.replace('"', '')
                    raw_rules[rule_num] = rule
                else:
                    self.messages.append(line)
        # if self.part == 2:
        #     raw_rules[8] = '42 | 42 8'
        #     raw_rules[11] = '42 31 | 42 11 31'
        self.parse_rules(raw_rules)

    def parse_rules(self, raw_rules):
        finished_rules = set()

        special_rules = {0: raw_rules[0],
                         8: raw_rules[8],
                         11: raw_rules[11]}

        del raw_rules[0]
        # del raw_rules[8]
        # del raw_rules[11]

        self.debugPrint("Raw rules: {}".format(raw_rules))

        for rule_num in raw_rules:
            self.rules[rule_num] = raw_rules[rule_num].split(' | ')

        while len(finished_rules) != len(raw_rules):
            for i in self.rules:
                self.debugPrint("Check rule {}: {}".format(i, self.rules[i]))
                done = True
                for rule in self.rules[i]:
                    for c in rule:
                        if c.isdigit():
                            done = False
                if done:
                    self.debugPrint("    Rule {} is done".format(i))
                    finished_rules.add(i)
                    continue

                rules_next = []
                for sub_rule in self.rules[i]:
                    substitution_found = False
                    self.debugPrint("    Sub rule {}".format(sub_rule))
                    sub_split = sub_rule.split(' ')
                    for c in range(0, len(sub_split)):
                        self.debugPrint("        {}".format(sub_split[c]))
                        if sub_split[c].isnumeric() and int(sub_split[c]) in finished_rules:
                            for result in self.rules[int(sub_split[c])]:
                                next = ' '.join(sub_split[0:c]) + ' ' + result + ' ' + ' '.join(sub_split[c+1:])
                                rules_next.append(next)
                                self.debugPrint("        Add {} to next: {}".format(result, next))
                                substitution_found = True
                            break # only change one digit at a time for simplicity
                    if not substitution_found:
                        self.debugPrint("        No substitution")
                        rules_next.append(' '.join(sub_split))
                self.debugPrint("    Next iteration: {}".format(rules_next))
                self.rules[i] = rules_next
        print("Done!")

    def part2(self):
        # special rules:
        #   0: 8 11
        #   8: 42 | 42 8
        #  11: 42 31 | 42 11 31
        # These basically combine up to any number of 42's, then any number of 31's
        print("42: {}".format([x.replace(' ', '') for x in [s for s in self.rules[42]]]))
        print("31: {}".format([x.replace(' ', '') for x in [s for s in self.rules[31]]]))
        matching = []
        for message in self.messages:
            message_save = message
            print("Message: {}".format(message))
            count_42 = 0
            count_31 = 0
            while self.starts_with_one_of(message, self.rules[42]):
                message = self.remove_any_of(message, self.rules[42])
                count_42 = count_42 + 1
            while self.starts_with_one_of(message, self.rules[31]):
                message = self.remove_any_of(message, self.rules[31])
                count_31 = count_31 + 1
            print("Remaining message: {}".format(message))
            if len(message) == 0 and count_42 >= 2 and count_31 >= 1 and count_42 > count_31:
                matching.append(message_save)
        print("Matches:")
        print('\n'.join(matching))
        return matching

    def starts_with_one_of(self, message, match_list):
        for entry in match_list:
            entry = entry.replace(' ', '')
            if message[0:len(entry)] == entry: # message.startswith(entry):
                print("    {} starts with {}".format(message, entry))
                return True
        return False

    def remove_any_of(self, message, match_list):
        for entry in match_list:
            entry = entry.replace(' ', '')
            if message.startswith(entry):
                print("    {} remove {}".format(message, entry))
                message = message[len(entry):]
                return message
        raise RuntimeError("Something has gone wrong...")

    def get_messages_that_match_rule0(self):
        if self.part == 1:
            return self.part1()
        elif self.part == 2:
            return self.part2()
        else:
            raise ValueError("Unknown part {}".format(self.part))

    def part1(self):
        matching = []
        rule_0 = [rule.replace(' ', '') for rule in self.rules[0]]
        for message in self.messages:
            if message in rule_0:
                matching.append(message)
        self.debugPrint("Matching messages:\n{}".format('\n'.join(matching)))
        return matching


if __name__ == '__main__':
    parser = getBasicArgparser('day 19')
    parser.add_argument('-p', type=int, help='Run part 1 or part 2', required=True)

    args = parser.parse_args()

    day19 = Day19(args.i, args.v, args.p)

    matching = day19.get_messages_that_match_rule0()
    print("{} matching messages".format(len(matching)))
