#!/usr/bin/env python3

import csv
import sys
import copy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day22(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.hands = [[], []]
        self.starting_hands = copy.deepcopy(self.hands)
        self.winner = -1

        self.parse_file()

    def reset_hands(self):
        self.winner = -1
        self.hands = copy.deepcopy(self.starting_hands)

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            player = -1
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if line.startswith('Player'):
                    player = player + 1
                else:
                    self.hands[player].append(int(line))
        self.debugPrint("Starting hands: ")
        self.format_hands(self.hands)
        self.starting_hands = copy.deepcopy(self.hands)

    def format_hands(self, hands):
        for player in range(0, len(hands)):
            self.debugPrint("  Player {}: {}".format(player, hands[player]))

    def wait_for_user(self):
        if self.debug:
            input("Press Enter to continue...")

    def get_winner_this_round(self, hands):
        winning_player = -1
        max_card = 0
        for player in range(0, len(hands)):
            if hands[player][0] > max_card:
                max_card = hands[player][0]
                winning_player = player
        return winning_player

    def give_winner_cards(self, winning_player, hands):
        # Order matters here, winner goes first
        for player in range(0, len(hands)):
            player = (player + winning_player) % len(hands)
            hands[winning_player].append(hands[player].pop(0))
        return hands

    def play_combat(self):
        self.reset_hands()
        round = 1
        while True:
            self.debugPrint("-- Round {} --".format(round))
            round = round + 1
            self.format_hands(self.hands)

            winning_player = self.get_winner_this_round(self.hands)

            self.hands = self.give_winner_cards(winning_player, self.hands)
            self.debugPrint("Player {} wins this round".format(winning_player + 1))

            if any([len(self.hands[n]) == 0 for n in range(0, len(self.hands))]):
                for player in range(0, len(self.hands)):
                    if len(self.hands[player]) != 0:
                        self.winner = player
                        self.debugPrint("Player {} wins the game!".format(self.winner+1))
                break
            #self.wait_for_user()

    def play_recursive_combat(self):
        self.reset_hands()
        self.winner = self.recursive_combat(self.hands, 1)

    def recursive_combat(self, hands, game):
        next_game = game + 1
        round = 1
        rounds = []
        winner = -1
        self.debugPrint("Start Game {} of Recursive Combat".format(game))
        self.wait_for_user()
        while True:
            self.debugPrint("-- Round {} (Game {}) --".format(round, game))
            self.format_hands(hands)

            if all([len(hands[x])-1 >= hands[x][0] for x in range(0, len(hands))]):
                recursive_hands = copy.deepcopy(hands)
                for i in range(0, len(recursive_hands)):
                    recursive_hands[i] = recursive_hands[i][1:recursive_hands[i][0]+1]
                winner = self.recursive_combat(recursive_hands, next_game)
                next_game = next_game + 1
            else:
                winner = self.get_winner_this_round(hands)

            hands = self.give_winner_cards(winner, hands)
            self.debugPrint("Player {} wins Round {} Game {}".format(winner + 1, round, game))

            if any([len(hands[n]) == 0 for n in range(0, len(hands))]):
                for player in range(0, len(hands)):
                    if len(hands[player]) != 0:
                        winner = player
                        self.debugPrint("Player {} wins Game {}!".format(winner+1, game))
                break

            if hands in rounds:
                self.debugPrint("This hand has been played before!")
                winner = 0
                break
            else:
                rounds.append(copy.deepcopy(hands))
            round = round + 1
            self.wait_for_user()
        return winner

    def get_winners_score(self):
        if self.winner < 0:
            raise RuntimeError("You need to play the game first!")
        winners_hand = self.hands[self.winner]
        score = 0
        for i in range(0, len(winners_hand)):
            score = score + (winners_hand[i] * (len(winners_hand) - i))
        return score


if __name__ == '__main__':
    parser = getBasicArgparser('day 22')

    args = parser.parse_args()

    day22 = Day22(args.i, args.v)

    day22.play_combat()
    print("Regular Combat: Winning player's score is {}".format(day22.get_winners_score()))

    day22.play_recursive_combat()
    print("Recursive Combat: Winning player's score is {}".format(day22.get_winners_score()))
