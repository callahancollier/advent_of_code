#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day2(BaseFunctions):
    def __init__(self, inputFile):
        super().__init__(inputFile, False)
        self.input_filename = inputFile

    def validatePasswordsSledShop(self):
        numValidPasswords = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                words = line[0].split(' ')
                min = int(words[0].split('-')[0])
                max = int(words[0].split('-')[1])
                target = words[1][:-1]

                password = words[2]

                count = 0
                for letter in password:
                    if letter == target:
                        count = count + 1

                if count >= min and count <= max:
                    numValidPasswords = numValidPasswords + 1
        return numValidPasswords

    def validatePasswordsTobogganShop(self):
        numValidPasswords = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                words = line[0].split(' ')
                posA = int(words[0].split('-')[0])
                posB = int(words[0].split('-')[1])
                target = words[1][:-1]

                password = words[2]

                posAfound = (password[posA-1] == target)
                posBfound = (password[posB-1] == target)

                if posAfound != posBfound:
                    numValidPasswords = numValidPasswords + 1
        return numValidPasswords


if __name__ == '__main__':
    parser = getBasicArgparser("day 2")

    args = parser.parse_args()


    day2 = Day2(args.i)
    print('Part one: {}'.format(day2.validatePasswordsSledShop()))

    print('Part two: {}'.format(day2.validatePasswordsTobogganShop()))
