#!/usr/bin/env python3

import csv
import sys

sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day23(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.cups = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.cups = [int(x) for x in line]
        if len(set(self.cups)) != len(self.cups):
            raise ValueError("Error: expected only one of each cup")
        next_cup = max(self.cups) + 1
        self.cups = self.cups + list(range(next_cup, next_cup+1000000))

    def do_moves(self, num):
        current_cup = self.cups[0]
        for i in range(0, num):
            self.debugPrint("-- move {} --".format(i+1))
            current_cup = self.do_move(current_cup)

    def get_cup_index(self, target_index):
        return target_index % len(self.cups)

    def do_move(self, current_cup):
        self.debugPrint("current cup: {}".format(current_cup))
        self.debugPrint("cups: {}".format(self.cups))
        # Pick three cups clockwise of current cup and remove them
        current_cup_index = self.cups.index(current_cup)
        left = (current_cup_index + 1) % len(self.cups)
        right = (current_cup_index + 4) % len(self.cups)
        removed_cups = self.cups[left:right] if left < right else (self.cups[left:] + self.cups[:right])
        self.debugPrint("pick up cups[{}:{}] = {}".format(left, right, removed_cups))
        for cup in removed_cups:
            self.cups.remove(cup)
        # Pick destination cup (label equal to current_cup-1, or next lowest with wrapping
        destination_cup = current_cup - 1
        while self.cups.count(destination_cup) == 0:
            destination_cup = destination_cup - 1
            if destination_cup < 0:
                destination_cup = max(self.cups)
        self.debugPrint("destination cup: {}".format(destination_cup))
        # Place removed cups immediately clockwise of destination cup
        destination_cup_index = self.cups.index(destination_cup)
        self.cups = self.cups[0:destination_cup_index+1] + removed_cups + self.cups[destination_cup_index+1:]
        # Pick new current cup: next cup clockwise of current cup
        current_cup = self.cups[(self.cups.index(current_cup) + 1) % len(self.cups)]
        return current_cup

    def get_cups(self):
        return self.cups

    def get_formatted_cups(self):
        index_of_1 = self.cups.index(1)
        return self.cups[index_of_1+1:] + self.cups[:index_of_1]


if __name__ == '__main__':
    parser = getBasicArgparser('day 23')
    parser.add_argument('-n', type=int, help='Number of moves to do', default=10000000)

    args = parser.parse_args()

    day23 = Day23(args.i, args.v)

    day23.do_moves(args.n)
    print("After {} moves: {}".format(args.n, ''.join([str(x) for x in day23.get_formatted_cups()])))
