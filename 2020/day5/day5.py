#!/usr/bin/env python3

import csv
import sys
from dataclasses import dataclass
sys.path.append('..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

@dataclass
class BoardingPassInfo:
    row: int
    col: int
    seatID: int

class Day5(BaseFunctions):
    def __init__(self, inputFile, enableDebug, boardingPassCode=None):
        super().__init__(inputFile, enableDebug)

        self.singleBoardingPass = boardingPassCode
        self.boardingPasses = []

        self.parseFile()

    def parseFile(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                self.boardingPasses.append(self.parseBoardingPass(line[0]))

    def getMaxSeatId(self):
        curMax = 0
        for boardingPass in self.boardingPasses:
            if boardingPass.seatID > curMax:
                curMax = boardingPass.seatID
        return curMax

    def getSingleBoardingPassInfo(self):
        return self.parseBoardingPass(self.singleBoardingPass)

    def parseBoardingPass(self, boardingPassCode):
        row = self.binaryParse(boardingPassCode[0:7], 127)
        col = self.binaryParse(boardingPassCode[7:], 7)
        if row == -1 or col == -1:
            print("ERROR: something went wrong parsing {}".format(boardingPassCode))
        seatID = row*8 + col
        return BoardingPassInfo(row, col, seatID)

    def binaryParse(self, code, startingUpperRange):
        remainingLower = 0
        remainingUpper = startingUpperRange
        self.debugPrint("Parse {}, start range {} to {}".format(code, remainingLower, remainingUpper))
        for char in code:
            remaining = remainingUpper - remainingLower
            if char == 'F' or char == 'L':
                #Lower half
                remainingUpper = remainingUpper - int(remaining/2) - 1
            else:
                #Upper half
                remainingLower = remainingLower + int(remaining/2) + 1
            self.debugPrint("{}: new range {} to {}".format(char, remainingLower, remainingUpper))
        if remainingLower != remainingUpper:
            return -1
        return remainingLower

    def findYourBoardingPass(self):
        seatIDs = []
        for boardingPass in self.boardingPasses:
            seatIDs.append(boardingPass.seatID)

        seatIDs.sort()
        self.debugPrint(seatIDs)

        prevId = seatIDs[0]
        for i in range(1,len(seatIDs)-1):
            if seatIDs[i] - prevId != 1:
                return seatIDs[i] - 1
            prevId = seatIDs[i]


if __name__ == '__main__':
    parser = getBasicArgparser('day 5')
    parser.add_argument('-t', type=str, help='Boarding pass code', default=None)

    args = parser.parse_args()

    day5 = Day5(args.i, args.v, args.t)

    if args.t:
        print("Test single boarding pass: {}".format(day5.getSingleBoardingPassInfo()))

    print("Max seat ID found in {}: {}".format(args.i, day5.getMaxSeatId()))

    print("Your seat ID: {}".format(day5.findYourBoardingPass()))