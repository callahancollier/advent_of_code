#!/usr/bin/env python3

import csv
import sys
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


@dataclass
class Range:
    min: int
    max: int


class Day16(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)

        self.fields = {}
        self.my_ticket = []
        self.nearby_tickets = []
        self.parse_file()

        self.debugPrint("Nearby tickets:")
        [self.debugPrint(val) for val in [ticket for ticket in self.nearby_tickets]]

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            stage = "fields"
            for line in reader:
                if len(line) != 0:
                    line = line[0]
                if stage == "fields":
                    if len(line) == 0:
                        stage = "yours"
                    else:
                        line = line.split(':')
                        field = line[0]
                        ranges = line[1].split(" or ")
                        range0 = ranges[0].split('-')
                        range1 = ranges[1].split('-')
                        self.fields[field] = [Range(int(range0[0]), int(range0[1])),
                                              Range(int(range1[0]), int(range1[1]))]
                elif stage == "yours":
                    if len(line) == 0 or line == "your ticket:":
                        continue
                    self.my_ticket = [int(x) for x in line.split(',')]
                    stage = "nearby"
                elif stage == "nearby":
                    if len(line) == 0 or line == "nearby tickets:":
                        continue
                    self.nearby_tickets.append([int(x) for x in line.split(',')])

    def get_ticket_scanning_error_rate(self):
        invalid_val_sum = 0
        for ticket in self.nearby_tickets:
            for value in ticket:
                if not self.is_value_valid(value):
                    self.debugPrint("{} is invalid".format(value))
                    invalid_val_sum = invalid_val_sum + value
        return invalid_val_sum

    def is_value_valid(self, value, fields_to_test=None):
        if fields_to_test == None:
            fields_to_test = self.fields.copy()
        for field in fields_to_test:
            for range in self.fields[field]:
                if value >= range.min and value <= range.max:
                    return True
        return False

    def get_valid_fields(self, values):
        valid_fields = []
        for field in self.fields:
            if all([self.is_value_valid(value, [field]) for value in values]):
                valid_fields.append(field)
        self.debugPrint("Valid fields for {}:{} (length {})".format(values, valid_fields, len(valid_fields)))
        if len(valid_fields) == 0:
            print("ERROR: found no valid fields")
        return valid_fields

    def remove_invalid_tickets(self):
        self.nearby_tickets = [nearby for nearby in self.nearby_tickets if all([self.is_value_valid(val) for val in nearby])]

    def get_my_ticket_fields(self):
        self.remove_invalid_tickets()
        iterations = 10000
        skip = []
        my_ticket_fields = {}
        while len(my_ticket_fields) != len(self.my_ticket):
            for i in range(0, len(self.nearby_tickets[0])):
                if i in skip:
                    continue
                values = [ticket[i] for ticket in self.nearby_tickets]
                valid_fields = self.get_valid_fields(values)
                self.debugPrint("return {} valid fields, index {}".format(len(valid_fields), i))
                if len(valid_fields) == 1:
                    self.debugPrint("Remove {} from dict".format(valid_fields[0]))
                    my_ticket_fields[valid_fields[0]] = self.my_ticket[i]
                    del self.fields[valid_fields[0]]
                    self.debugPrint("Remaining dict: {}".format(self.fields))
                    skip.append(i)
            iterations = iterations - 1
            if iterations == 0:
                self.debugPrint("ERROR: out of iterations")
                break

        self.debugPrint("My ticket fields: {}".format(my_ticket_fields))
        return my_ticket_fields

    def get_mult_of_departure_fields(self, expected_num_departure_fields):
        d = self.get_my_ticket_fields()
        count = 0
        mult = 1
        for field in d:
            if field.startswith("departure"):
                count = count + 1
                mult = mult * d[field]
        if count != expected_num_departure_fields:
            print("ERROR: expected {} fields, found {}".format(expected_num_departure_fields, count))
        return mult


if __name__ == '__main__':
    parser = getBasicArgparser('day 16')

    args = parser.parse_args()

    day16 = Day16(args.i, args.v)

    print("Ticket scanning error rate: {}".format(day16.get_ticket_scanning_error_rate()))

    print("Departure fields multiplied: {}".format(day16.get_mult_of_departure_fields(6)))
