#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day6(BaseFunctions):
    def __init__(self, input_file, enableDebug):
        super().__init__(input_file, enableDebug)
        self.parseFileForUniqueAnswers()

    def parseFileForUniqueAnswers(self):
        return self.parseFileWithFunction(self.sumUniqueAnswers)

    def parseFileForCommonAnswers(self):
        return self.parseFileWithFunction(self.sumCommonAnswers)

    def parseFileWithFunction(self, func):
        total = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            self.debugPrint("Opened file")
            this_group = []
            for line in reader:
                if len(line) == 0:
                    #Done this group
                    total = total + func(this_group)
                    this_group = []
                else:
                    this_group.append(line[0])
            if len(this_group) != 0:
                total = total + func(this_group)
        return total

    def sumUniqueAnswers(self, group):
        all_answers = []
        for individual in group:
            for letter in individual:
                all_answers.append(letter)
        self.debugPrint("This group answered: {}".format(all_answers))
        return len(set(all_answers))

    def sumCommonAnswers(self, group):
        self.debugPrint("This group: {}".format(group))
        common = set(group[0])
        for i in range(1, len(group)):
            self.debugPrint("Current set {}, next group {}".format(common, group[i]))
            common = common.intersection(group[i])
        self.debugPrint("Return {}".format(len(common)))
        return len(common)


if __name__ == '__main__':
    parser = getBasicArgparser('day 6')

    args = parser.parse_args()

    day6 = Day6(args.i, args.v)

    print("Total unique: {}".format(day6.parseFileForUniqueAnswers()))
    print("Total common: {}".format(day6.parseFileForCommonAnswers()))
