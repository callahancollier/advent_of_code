#!/usr/bin/env python3

import csv
import sys
from dataclasses import dataclass
sys.path.append('..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

@dataclass
class Entry:
    prev: int
    prev_prev: int


class Day15(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.start = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.parse_string(line)

    def parse_string(self, string):
        self.start = [int(x) for x in string.split(',')]

    def play_game_to(self, num_turns):
        spoken_words = []
        for turn in range(1, num_turns+1):
            self.debugPrint(spoken_words)
            if turn <= len(self.start):
                spoken_words.append(self.start[turn-1])
            else:
                if spoken_words[-1] not in spoken_words[:-1]:
                    spoken_words.append(0)
                else:
                    # diff btw to last two previously spoken times
                    idx = 0
                    self.debugPrint("        search for {}".format(spoken_words[-1]))
                    for i in range(0, len(spoken_words)-1):
                        word = spoken_words[i]
                        if word == spoken_words[-1]:
                            idx = i
                        self.debugPrint("        {} idx {}".format(word, idx))
                    self.debugPrint("    Last spoken at index {}, current len {}".format(idx, len(spoken_words)))
                    spoken_words.append(len(spoken_words) - (idx+1))
            self.debugPrint("Turn {}: {}".format(turn, spoken_words[-1]))
        return spoken_words[-1]

    def alternate_play_game_to(self, num_turns):
        spoken_words = {} # Key = word, value = last two spoken turns
        prev_word = None
        for turn in range(1, num_turns+1):
            if turn <= len(self.start):
                spoken_words = self.set(self.start[turn-1], turn, spoken_words)
                prev_word = self.start[turn-1]
            else:
                if prev_word not in spoken_words:
                    print("ERROR: You did something wrong.")
                if spoken_words[prev_word].prev_prev is None:
                    spoken_words = self.set(0, turn, spoken_words)
                    prev_word = 0
                else:
                    next_number = spoken_words[prev_word].prev - spoken_words[prev_word].prev_prev
                    self.set(next_number, turn, spoken_words)
                    prev_word = next_number
            self.debugPrint("Turn {}: {}".format(turn, prev_word))
        return prev_word

    def set(self, number, turn, spoken_words):
        if number not in spoken_words:
            spoken_words[number] = Entry(turn, None)
        else:
            spoken_words[number] = Entry(turn, spoken_words[number].prev)
        self.debugPrint("    set {} to {}".format(number, spoken_words[number]))
        return spoken_words

    def get_assign(self, number, turn, spoken_words):
        retval = None
        if number in spoken_words:
            retval = [turn] + spoken_words[number][1:]
        else:
            retval = [turn, None]
        self.debugPrint("    Set {} to {}".format(number, retval))
        return retval



if __name__ == '__main__':
    parser = getBasicArgparser('day 15')
    parser.add_argument('-n', type=int, help='Number of turns', required=True)
    parser.add_argument('-t', type=str, help='Override input file and use this example instead', default=None)

    args = parser.parse_args()

    day15 = Day15(args.i, args.v)

    if args.t is not None:
        day15.parse_string(args.t)

    print("Word number {}: {}".format(args.n, day15.alternate_play_game_to(args.n)))
