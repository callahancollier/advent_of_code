#!/usr/bin/env python3

import csv
import sys
from enum import Enum
sys.path.append('..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser



class Day11(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.layout = []

        self.floor = '.'
        self.empty = 'L'
        self.occupied = '#'

        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                row = []
                for char in line:
                    row.append(char)
                self.layout.append(row)

    def print_layout(self):
        self.debugPrint('----------')
        for row in self.layout:
            self.debugPrint(''.join(row))
        self.debugPrint('----------')

    def applyRulesUntilStopsChanging(self, part):
        if part == 1:
            while self.applyRulesPart1() != 0:
                pass
        elif part == 2:
            while self.applyRulesPart2() != 0:
                pass
        else:
            print("ERROR: Unknown part {}".format(part))

    def countOccupiedSeats(self):
        num_occupied = 0
        for row in self.layout:
            for col in row:
                if col == self.occupied:
                    num_occupied = num_occupied + 1
        return num_occupied

    def applyRulesPart1(self):
        return self.applyRules(4, self.getAdjacent)

    def applyRulesPart2(self):
        return self.applyRules(5, self.getEyelineAdjacent)

    def applyRules(self, count_to_change, getAdjacentFunction):
        # If seat is empty and no occupied seats adjacent, become occupied
        # If seat is occupied and four or more seats adjacent also occupied, become empty
        # Otherwise, no change
        new_layout = [x[:] for x in self.layout]
        row = 0
        col = 0
        num_changes = 0
        while row < len(self.layout) and col < len(self.layout[0]):
            seat = self.layout[row][col]
            adjacent = getAdjacentFunction(row, col)
            self.debugPrint("Seat {} {}: {}, Adjacent: {}".format(
                row, col, seat, adjacent))
            if seat == self.empty and self.occupied not in adjacent:
                self.debugPrint("  make {}".format(self.occupied))
                new_layout[row][col] = self.occupied
                num_changes = num_changes + 1
            elif seat == self.occupied and adjacent.count(self.occupied) >= count_to_change:
                self.debugPrint("  make {}".format(self.empty))
                new_layout[row][col] = self.empty
                num_changes = num_changes + 1
            col = col + 1
            if col == len(self.layout[0]):
                row = row + 1
                col = 0
        self.layout = new_layout
        return num_changes

    def getAdjacent(self, row, col):
        # Adjacent is one of eight, up, down, left, right, diagonal
        adjacent = []
        if row != 0:
            # up
            adjacent.append(self.layout[row-1][col])
        if row != len(self.layout)-1:
            # down
            adjacent.append(self.layout[row+1][col])
        if col != 0:
            # left
            adjacent.append(self.layout[row][col-1])
        if col != len(self.layout[0])-1:
            # right
            adjacent.append(self.layout[row][col+1])
        if row != 0 and col != 0:
            # diag, up and left
            adjacent.append(self.layout[row-1][col-1])
        if row != 0 and col != len(self.layout[0])-1:
            # diag, up and right
            adjacent.append(self.layout[row - 1][col + 1])
        if row != len(self.layout)-1 and col != 0:
            # diag, down and left
            adjacent.append(self.layout[row+1][col-1])
        if row != len(self.layout)-1 and col != len(self.layout[0])-1:
            # diag, down and right
            adjacent.append(self.layout[row + 1][col + 1])
        return adjacent

    def getEyelineAdjacent(self, row, col):
        adjacent = []
        for row_test in range(row-1, -1, -1):
            # up
            if self.layout[row_test][col] != self.floor:
                adjacent.append(self.layout[row_test][col])
                break
        for row_test in range(row+1, len(self.layout)):
            # down
            if self.layout[row_test][col] != self.floor:
                adjacent.append(self.layout[row_test][col])
                break
        for col_test in range(col-1, -1, -1):
            # left
            if self.layout[row][col_test] != self.floor:
                adjacent.append(self.layout[row][col_test])
                break
        for col_test in range(col+1, len(self.layout[0])):
            # right
            if self.layout[row][col_test] != self.floor:
                adjacent.append(self.layout[row][col_test])
                break
        for row_test, col_test in zip(range(row-1, -1, -1), range(col-1, -1, -1)):
            # diag, up and left
            if self.layout[row_test][col_test] != self.floor:
                adjacent.append(self.layout[row_test][col_test])
                break
        for row_test, col_test in zip(range(row - 1, -1, -1), range(col + 1, len(self.layout[0]))):
            # diag, up and right
            if self.layout[row_test][col_test] != self.floor:
                adjacent.append(self.layout[row_test][col_test])
                break
        for row_test, col_test in zip(range(row + 1, len(self.layout)), range(col - 1, -1, -1)):
            # diag, down and left
            if self.layout[row_test][col_test] != self.floor:
                adjacent.append(self.layout[row_test][col_test])
                break
        for row_test, col_test in zip(range(row + 1, len(self.layout)), range(col + 1, len(self.layout[0]))):
            # diag, down and right
            if self.layout[row_test][col_test] != self.floor:
                adjacent.append(self.layout[row_test][col_test])
                break
        return adjacent


if __name__ == '__main__':
    parser = getBasicArgparser('day 11')
    parser.add_argument('-p', type=int, help='Which part to run, 1 or 2', default=None)

    args = parser.parse_args()

    day11 = Day11(args.i, args.v)

    day11.applyRulesUntilStopsChanging(args.p)
    print("Part {}: After changes stop, there are {} occupied seats".format(args.p, day11.countOccupiedSeats()))

