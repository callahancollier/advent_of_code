#!/usr/bin/env python3

import csv
import sys
import numpy as np
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


@dataclass
class Edges:
    top: str
    left: str
    right: str
    bottom: str


class Day20(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)

        self.tile_size = 0
        self.tiles = {}  # map ids to tile data
        self.borders = {}

        self.parse_file()
        self.collect_border_info()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            current_id = 0
            this_tile = []
            for line in reader:
                if len(line) == 0:
                    self.tiles[current_id] = np.array(this_tile)
                    this_tile = []
                    continue
                line = line[0]
                if line.startswith('Tile'):
                    current_id = int(line.split(' ')[1][:-1])
                    continue
                else:
                    if self.tile_size == 0:
                        self.tile_size = len(line)
                    this_tile.append([x for x in line])
            if len(this_tile) != 0:
                self.tiles[current_id] = np.array(this_tile)
                this_tile = []
        self.print_tiles()

    def print_tiles(self):
        for id in self.tiles:
            self.debugPrint("Tile {}".format(id))
            self.debugPrint(self.tiles[id])

    def get_edges(self, tile):
        top = ''.join(tile[0, :])
        bottom = ''.join(tile[self.tile_size - 1, :])
        left = ''.join(tile[:, 0])
        right = ''.join(tile[:, self.tile_size - 1])
        return Edges(top, left, right, bottom)

    def collect_border_info(self):
        # Numpy indexing is [col, row]
        for tile in self.tiles:
            for reversed in [False, True]:  # index them both forward and reversed
                edges = self.get_edges(self.tiles[tile])

                if reversed:
                    edges.top = edges.top[::-1]
                    edges.bottom = edges.bottom[::-1]
                    edges.left = edges.left[::-1]
                    edges.right = edges.right[::-1]

                if not reversed:
                    self.debugPrint("Tile {}".format(tile))
                self.debugPrint("    top    {}".format(edges.top))
                self.debugPrint("    left   {}".format(edges.left))
                self.debugPrint("    right  {}".format(edges.right))
                self.debugPrint("    bottom {}".format(edges.bottom))

                if edges.top not in self.borders:
                    self.borders[edges.top] = []
                if edges.bottom not in self.borders:
                    self.borders[edges.bottom] = []
                if edges.left not in self.borders:
                    self.borders[edges.left] = []
                if edges.right not in self.borders:
                    self.borders[edges.right] = []

                self.borders[edges.top].append(tile)
                self.borders[edges.bottom].append(tile)
                self.borders[edges.left].append(tile)
                self.borders[edges.right].append(tile)

    def get_corner_ids(self):
        # No need to actually assemble the whole image... just find the four corner tiles
        corners = []
        for tile_id in self.tiles:
            edges = self.get_edges(self.tiles[tile_id])

            self.debugPrint("FIND CORNERS:")
            self.debugPrint("test key {}".format(tile_id))
            self.debugPrint("top[{}]:    {}".format(edges.top, self.borders[edges.top]))
            self.debugPrint("bottom[{}]: {}".format(edges.bottom, self.borders[edges.bottom]))
            self.debugPrint("left[{}]:   {}".format(edges.left, self.borders[edges.left]))
            self.debugPrint("right[{}]:  {}".format(edges.right, self.borders[edges.right]))

            if edges.top not in self.borders or edges.bottom not in self.borders or \
                    edges.left not in self.borders or edges.right not in self.borders:
                raise RuntimeError("Id {} not found in borders dictionaries!".format(tile_id))
            edge_vertical = self.is_border_edge(edges.left) or self.is_border_edge(edges.right)
            edge_horizonal = self.is_border_edge(edges.top) or self.is_border_edge(edges.bottom)
            if edge_vertical and edge_horizonal:
                self.debugPrint("--> {} is a corner".format(tile_id))
                corners.append(tile_id)
            else:
                self.debugPrint("--> {} not a corner".format(tile_id))
        if len(corners) != 4:
            self.debugPrint("Corners: {}".format(corners))
            raise ValueError("Found wrong number of corners: {}".format(len(corners)))
        return corners

    def is_border_edge(self, border_key):
        return len(self.borders[border_key]) < 2

    def has_edge(self, tile, edge):
        edges = self.get_edges(tile)
        if self.matches(edges.top, edge):
            return True
        if self.matches(edges.left, edge):
            return True
        if self.matches(edges.right, edge):
            return True
        if self.matches(edges.bottom, edge):
            return True
        return False

    def get_border_edge(self, tile):
        edges = self.get_edges(tile)
        if self.is_border_edge(edges.top):
            return edges.top
        if self.is_border_edge(edges.bottom):
            return edges.bottom
        if self.is_border_edge(edges.left):
            return edges.left
        if self.is_border_edge(edges.right):
            return edges.right
        return None

    def get_image(self):
        self.debugPrint("CONSTRUCT IMAGE")
        used_ids = []
        image = []
        row = []
        corners = self.get_corner_ids()

        # Put one of the corners in the top left (any corner)
        top_left = corners.pop()
        top_left_edges = self.get_edges(self.tiles[top_left])
        edges = [getattr(top_left_edges, x) for x in ['top', 'left', 'right', 'bottom'] if self.is_border_edge(getattr(top_left_edges, x))]
        if len(edges) != 2:
            raise ValueError("Something went wrong, there are {} edges for {}".format(len(edges), top_left))
        top_left_tile = self.orient(edges[0], "top", self.tiles[top_left])
        row.append(self.orient(edges[1], "left", top_left_tile))
        used_ids.append(top_left)

        self.debugPrint("Top left done:\n{}\n---".format(row))

        # Fill in the top row
        target_edge = self.get_edges(row[-1]).right
        self.debugPrint("Next target edge {}".format(target_edge))
        while not self.is_border_edge(target_edge):
            for potential in self.borders[target_edge]:
                self.debugPrint("Potential next id is {}".format(potential))
                if potential not in used_ids:
                    next = self.orient(target_edge, "left", self.tiles[potential])
                    self.debugPrint("Append {}:\n{}\n---".format(potential, next))
                    row.append(next)
                    used_ids.append(potential)
                    target_edge = self.get_edges(row[-1]).right
                    if potential in corners:
                        corners.remove(potential)
                    break
                else:
                    self.debugPrint("Already used")
            self.debugPrint("Next target edge {}".format(target_edge))
        if len(corners) != 2:
            raise RuntimeError("Something went wrong filling in top row, corners: {}".format(corners))
        image.append(row)
        row = []
        while len(used_ids) != len(self.tiles):
            # Fill in the rest of the rows
            # left edge
            target_edge = self.get_edges(image[-1][0]).bottom
            self.debugPrint("Next row target edge {}".format(target_edge))
            for potential in self.borders[target_edge]:
                if potential not in used_ids:
                    border_edge = self.get_border_edge(self.tiles[potential])
                    if border_edge is not None:
                        next_tile = self.orient(border_edge, "left", self.tiles[potential])
                        next_tile = self.orient(target_edge, "top", next_tile)
                        row.append(next_tile)
                        self.debugPrint("Left edge {}:\n{}\n---".format(potential, next_tile))
                        used_ids.append(potential)
                        if potential in corners:
                            corners.remove(potential)
            # rest of row
            target_edge = self.get_edges(row[-1]).right
            self.debugPrint("Next target edge {}".format(target_edge))
            while not self.is_border_edge(target_edge):
                for potential in self.borders[target_edge]:
                    self.debugPrint("Potential next id is {}".format(potential))
                    if potential not in used_ids:
                        next = self.orient(target_edge, "left", self.tiles[potential])
                        self.debugPrint("Append {}:\n{}\n---".format(potential, next))
                        row.append(next)
                        used_ids.append(potential)
                        target_edge = self.get_edges(row[-1]).right
                        if potential in corners:
                            corners.remove(potential)
                        break
                    else:
                        self.debugPrint("Already used")
                self.debugPrint("Next target edge {}".format(target_edge))
            image.append(row)
            row = []
        if len(corners) != 0:
            raise RuntimeError("Still some corners left: {}".format(corners))
        self.debugPrint("Image: {}".format(image))
        return image

    def build_image(self):
        image_list = self.get_image()
        # Remove borders
        for c in range(0, len(image_list)):
            for r in range(0, len(image_list[0])):
                image_list[c][r] = np.delete(image_list[c][r], self.tile_size-1, axis=0)
                image_list[c][r] = np.delete(image_list[c][r], 0, axis=0)
                image_list[c][r] = np.delete(image_list[c][r], self.tile_size-1, axis=1)
                image_list[c][r] = np.delete(image_list[c][r], 0, axis=1)

        # Combine into a single np array
        for c in range(0, len(image_list)):
            image_list[c] = np.concatenate(image_list[c], axis=1)
        image = np.concatenate(image_list, axis=0)
        self.debugPrint("Final image: {}".format(image))
        return image

    def get_water_roughness(self):
        image = self.build_image()
        monster_count = self.count_all_sea_monsters(image)
        hashes_per_mountain = 15
        hash_count = 0
        for row in image:
            for col in row:
                if col == '#':
                    hash_count = hash_count + 1
        return hash_count - hashes_per_mountain*monster_count

    def count_all_sea_monsters(self, image):
        count = 0
        stage = 'flip -'

        while count == 0:
            if stage == 'flip -':
                image = np.flip(image, 1)
                stage = 'rotate'
            elif stage == 'flip |':
                image = np.flip(image, 0)
                stage = 'flip -'
            elif stage == 'rotate':
                image = np.rot90(image)
                stage = 'flip |'
            count = self.count_sea_monsters(image)
        return count

    def count_sea_monsters(self, image):
        monsters = 0
        sea_monster = [
            [x for x in '                  # '],
            [x for x in '#    ##    ##    ###'],
            [x for x in ' #  #  #  #  #  #   ']
        ]
        sea_monster_reversed = [x[::-1] for x in sea_monster]
        for i in range(0, len(image)-3):
            for j in range(0, len(image[0])-20):
                test = image[i:i+3, j:j+20]
                self.debugPrint("i {} j {} Test this slice:\n{}\n---".format(i, j, test))
                found = True
                found_rev = True
                for k in range(0, len(sea_monster)):
                    for l in range(0, len(sea_monster[k])):
                        if sea_monster[k][l] == '#' and test[k][l] != '#':
                            found = False
                        if sea_monster_reversed[k][l] == '#' and test[k][l] != '#':
                            found_rev = False
                if found:
                    monsters = monsters + 1
                if found_rev:
                    monsters = monsters + 1
        self.debugPrint("Found {} sea monsters".format(monsters))
        return monsters

    def matches(self, str1, str2):
        if str1 == str2:
            return True
        if str1[::-1] == str2:
            return True
        return False

    def orient(self, edge, orientation, tile):
        edges = self.get_edges(tile)
        stage = 'flip -'
        num_tries = 10
        # if you flip horizontally, flip vertically, rotate, repeat, and check
        # after each move, you'll eventually find the right state (I think)
        while not getattr(edges, orientation) == edge:
            if stage == 'flip -':
                tile = np.flip(tile, 1)
                stage = 'rotate'
            elif stage == 'flip |':
                tile = np.flip(tile, 0)
                stage = 'flip -'
            elif stage == 'rotate':
                tile = np.rot90(tile)
                stage = 'flip |'
            edges = self.get_edges(tile)
            num_tries = num_tries - 1
            if num_tries == 0:
                raise RuntimeError("too many iterations")
        return tile


if __name__ == '__main__':
    parser = getBasicArgparser('day 20')

    args = parser.parse_args()

    day20 = Day20(args.i, args.v)

    corners = day20.get_corner_ids()
    print("Corner ids: {}".format(corners))
    mult = 1
    for corner in corners:
        mult = mult * corner
    print("Corner id's multiplied: {}".format(mult))

    print("Roughness: {}".format(day20.get_water_roughness()))
