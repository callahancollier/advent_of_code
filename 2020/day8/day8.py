#!/usr/bin/env python3

import csv
import sys
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


@dataclass
class Instr:
    cmd: str
    val: int
    more: str

class Day8(BaseFunctions):
    def __init__(self, input_file, enable_debug, max_loops):
        super().__init__(input_file, enable_debug)
        self.max_loops = max_loops
        self.acc = 0
        self.prev_acc = 0
        self.prog = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                self.prog.append(Instr(line[0], int(line[1]), ""))

    def returnAccBeforeLoop(self):
        if not self.runProg():
            return self.prev_acc
        else:
            print("ERROR: Enexpected notmal program termination!")
            return 0

    def printProg(self, mod_idx, header=""):
        self.debugPrint("------")
        for i in range(0, len(self.prog)):
            mod_str = "*" if i == mod_idx else ""
            self.debugPrint("{} {} {} {} {}".format(header, self.prog[i].cmd,
                                                    self.prog[i].val, mod_str,
                                                    self.prog[i].more))
        self.debugPrint("------")

    def fixProgReturnAccAfterFinish(self):
        runs = self.max_loops
        swapped_idx = -1
        #orig_prog = self.prog.copy()
        while self.runProg() == False:
            self.debugPrint("Orig prog:")
            self.printProg(-1, "...")
            swapped_idx = swapped_idx + 1
            self.debugPrint("Restore orig prog, try fix at idx {}".format(swapped_idx))
            self.prog.clear()
            #self.prog = orig_prog.copy() #TODO: Why isn't this working???
            self.parse_file()
            if swapped_idx >= len(self.prog):
                print("ERROR: Ran out of instructions to try to fix!")
                return 0
            # Find next possible command to change
            while self.prog[swapped_idx].cmd == "acc":
                swapped_idx = swapped_idx + 1
                if swapped_idx >= len(self.prog):
                    print("ERROR: Ran out of instructions to try to fix!")
                    return 0
            # Change it
            if self.prog[swapped_idx].cmd == "nop":
                self.prog[swapped_idx].cmd = "jmp"
            else:
                self.prog[swapped_idx].cmd = "nop"
            self.debugPrint("Modified prog at {}:".format(swapped_idx))
            self.printProg(swapped_idx)
            runs = runs - 1
            if runs <= 0:
                print("ERROR: too many loops")
                break
        return self.acc

    def clearMores(self):
        for i in range(0, len(self.prog)):
            self.prog[i].more = ""

    def runProg(self):
        self.clearMores()
        prev_instrs = []
        instr = 0
        count = 1
        self.acc = 0
        self.prev_acc = self.acc
        while instr < len(self.prog):
            self.prog[instr].more = " ".join([self.prog[instr].more, str(count)])
            count = count + 1
            if instr in prev_instrs:
                self.debugPrint("Infinite loop!")
                self.printProg(-1, "-->")
                return False
            self.prev_acc = self.acc
            self.debugPrint("Current instruction idx {} is {}".format(instr, self.prog[instr]))
            if self.prog[instr].cmd == "nop":
                prev_instrs.append(instr)
                instr = instr + 1
            elif self.prog[instr].cmd == "acc":
                self.acc = self.acc + self.prog[instr].val
                prev_instrs.append(instr)
                instr = instr + 1
            elif self.prog[instr].cmd == "jmp":
                prev_instrs.append(instr)
                instr = instr + self.prog[instr].val
        if instr == len(self.prog):
            self.printProg(-1, "-->")
            return True
        self.printProg(-1, "-->")
        return False


if __name__ == '__main__':
    parser = getBasicArgparser('day 8')
    parser.add_argument('-l', type=int, help='Maximum number of loops before causing an error', default=1000)

    args = parser.parse_args()

    day8 = Day8(args.i, args.v, args.l)

    print("Just before the infinite loop starts, acc is {}".format(day8.returnAccBeforeLoop()))

    print("When fixed program exits, acc is {}".format(day8.fixProgReturnAccAfterFinish()))