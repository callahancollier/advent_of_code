#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day12(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.caves = {}
        self.parse_file()

        self.paths = []

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                line = line.split('-')

                if line[0] not in self.caves.keys():
                    self.caves[line[0]] = []
                if line[1] not in self.caves.keys():
                    self.caves[line[1]] = []

                self.caves[line[0]].append(line[1])
                self.caves[line[1]].append(line[0])
        self.debugPrint('Cave system: {}'.format(self.caves))

    def count_paths(self, part):
        self.paths = []
        path = []
        if part == 1:
            self.traverse_path(True, 'start', path)
        else:
            self.traverse_path(False, 'start', path)
        self.debugPrint('Final list of paths:')
        for p in self.paths:
            self.debugPrint(','.join(p))
        return len(self.paths)

    def traverse_path(self, second_path_used, from_cave, current_path):
        path = current_path.copy()
        path.append(from_cave)
        if from_cave == 'end':
            self.debugPrint("Found path {}".format(path))
            self.paths.append(path)
            return
        for dest in self.caves[from_cave]:
            allow_second_path = False
            if dest == 'start' and 'start' in path:
                self.debugPrint('   {}: start already in path'.format(path))
                continue
            if dest.islower() and dest in path:
                if not second_path_used:
                    # Allowed to happen once, only once
                    self.debugPrint("   (Allowing second use of path)")
                    allow_second_path = True
                else:
                    self.debugPrint('   {}: {} already exists in path'.format(path, dest))
                    continue
            self.debugPrint("   {}".format(second_path_used))
            self.debugPrint('   {}: {} goes to {}'.format(path, from_cave, dest))
            self.traverse_path((second_path_used or allow_second_path), dest, path)


if __name__ == '__main__':
    parser = getBasicArgparser('day 12')
    parser.add_argument('-p', type=int, help='Which part to run', default=-1)

    args = parser.parse_args()

    day12 = Day12(args.i, args.v)

    if args.p == 1 or args.p < 0:
        print('Part 1: Cave system has {} paths'.format(day12.count_paths(1)))
    if args.p == 2 or args.p < 0:
        print('Part 2: Cave system has {} paths'.format(day12.count_paths(2)))
