#!/usr/bin/env python3

import csv
import sys
import numpy
import copy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day22(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.minidx = 999999999
        self.maxidx = 0
        self.instructions = []
        self.parse_file()

    def parse_file(self):
        self.minidx = 999999
        self.maxidx = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                line = line.split(' ')
                command = line[0]
                line = line[1].replace('=','').replace('x','').replace('y','').replace('z','').split(',')
                start = [0, 0, 0]
                end = [0, 0, 0]
                for i, coord_range in enumerate(line):
                    coord_range = coord_range.split('..')
                    start[i] = int(coord_range[0])
                    end[i] = int(coord_range[1])

                for idx in start:
                    self.minidx = min(self.minidx, idx)
                    self.maxidx = max(self.maxidx, idx)
                for idx in end:
                    self.minidx = min(self.minidx, idx)
                    self.maxidx = max(self.maxidx, idx)
                self.instructions.append((command, tuple(start), tuple(end)))


class Day22Part1(Day22):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.cubes = numpy.zeros((101, 101, 101))

    def shift(self, coord):
        cx, cy, cz = coord
        cx = cx + 50
        cy = cy + 50
        cz = cz + 50
        return cx, cy, cz

    def apply_steps(self):
        step = 0
        cubes_on = 0
        for command, start, end in self.instructions:
            setval = 0
            if command == 'on':
                setval = 1

            sx, sy, sz = self.shift(start)
            ex, ey, ez = self.shift(end)

            before = self.countOnCubes(self.cubes[sz:ez+1, sy:ey+1, sx:ex+1])
            self.cubes[sz:ez+1, sy:ey+1, sx:ex+1] = setval
            after = self.countOnCubes(self.cubes[sz:ez+1, sy:ey+1, sx:ex+1])
            self.debugPrint("After step {}: {} cubes".format(step, after-before))
            cubes_on += after - before
            self.debugPrint(" ({} cubes are on)".format(cubes_on))
            step += 1
        return cubes_on

    def countOnCubes(self, cubes):
        total = 0
        for z,z_row in enumerate(cubes):
            for y,y_row in enumerate(z_row):
                for x,x_row in enumerate(y_row):
                    if x_row == 1:
                        total += 1
        return total


class CubeSection:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def get_num_cubes(self):
        return (self.end[0] - self.start[0] + 1) * (self.end[1] - self.start[1] + 1) * (self.end[2] - self.start[2] + 1)

    def get_overlap(self, other_cube):
        osx, osy, osz = other_cube.start
        oex, oey, oez = other_cube.end
        sx, sy, sz = self.start
        ex, ey, ez = self.end

        # No overlap case
        if osx > ex or osy > ey or osz > ez:
            return None
        if sx > oex or sy > oey or sz > oez:
            return None

        nsx = max(sx, osx)
        nsy = max(sy, osy)
        nsz = max(sz, osz)
        nex = min(ex, oex)
        ney = min(ey, oey)
        nez = min(ez, oez)
        return CubeSection((nsx,nsy,nsz), (nex,ney,nez))

    def split(self, overlap):
        if overlap is None:
            return [self]
        osx, osy, osz = overlap.start
        oex, oey, oez = overlap.end
        sx, sy, sz = self.start
        ex, ey, ez = self.end

        # Split into sub cubes surrounding the overlap.
        new_cubes = []
        #TODO need to confirm whether to use > and < or >= and <=

        # slice down the x axis to the left of the overlap
        if osx > sx:
            new_cubes.append(CubeSection( (sx, sy, sz), (osx-1, ey, ez) ))
        # slice down the x axis to the right of the overlap
        if oex < ex:
            new_cubes.append(CubeSection( (oex+1, sy, sz), (ex, ey, ez) ))
        # slice down the y axis in front of the overlap, less what we've already chopped
        if osy > sy:
            new_cubes.append(CubeSection( (osx, sy, sz), (oex, osy-1, ez) ))
        # ditto for the y axis behind the overlap
        if oey < ey:
            new_cubes.append(CubeSection( (osx, oey+1, sz), (oex, ey, ez) ))
        # same deal for the z axis below overlap
        if osz > sz:
            new_cubes.append(CubeSection( (osx, osy, sz), (oex, oey, osz-1) ))
        # and the z axis above overlap
        if oez < ez:
            new_cubes.append(CubeSection( (osx, osy, oez+1), (oex, oey, ez) ))
        return new_cubes


class Day22Part2(Day22):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        #self.shift_all_indices()

    def shift_all_indices(self):
        shift_by = -1*self.minidx + 1
        # print("shift by {}".format(shift_by))
        # print("{}\n***".format(self.instructions))
        new_instructions = []
        for cmd, start, end in self.instructions:
            sx, sy, sz = start
            ex, ey, ez = end
            new_instructions.append((
                cmd,
                (sx+shift_by,sy+shift_by,sz+shift_by),
                (ex + shift_by, ey + shift_by, ez + shift_by)
            ))
        self.instructions = new_instructions
        # print("{}\n***".format(self.instructions))
        # for instr in self.instructions:
        #     print(instr)

    def apply_steps(self):
        on_cubes = []
        for cmd, start, end in self.instructions:
            print("--> Turn {} from {} to {}, {} cubes".format(cmd, start, end, len(on_cubes)))
            instr_cube = CubeSection(start, end)
            current_on_cubes = copy.deepcopy(on_cubes)
            if cmd == 'on':
                new_on_cubes = []
                for i, cube in enumerate(current_on_cubes):
                    overlap = cube.get_overlap(instr_cube)
                    if overlap is not None:
                        self.debugPrint("Overlap with {}: {} to {}".format(i, overlap.start, overlap.end))
                        self.debugPrint("   Split to:")
                        for c in instr_cube.split(overlap):
                            self.debugPrint("     {} to {}".format(c.start, c.end))
                    new_on_cubes = new_on_cubes + cube.split(overlap)
                on_cubes = new_on_cubes
                on_cubes.append(instr_cube)
            elif cmd == 'off':
                new_on_cubes = []
                for i, cube in enumerate(on_cubes):
                    overlap = cube.get_overlap(instr_cube)
                    if overlap is not None:
                        self.debugPrint("Overlap with {}: {} to {}".format(i, overlap.start, overlap.end))
                    if overlap is not None:
                        new_on_cubes = new_on_cubes + cube.split(overlap)
                    else:
                        new_on_cubes.append(cube)
                on_cubes = new_on_cubes
            else:
                print("ERROR: Unknown command {}".format(cmd))
                sys.exit(-1)
            self.debugPrint("Cubes:")
            for i,c in enumerate(on_cubes):
                self.debugPrint("  {}: {} to {}".format(i, c.start, c.end))
            if self.debug:
                self.debugPrint(" currently {} cubes on".format(self.sum_cubes(on_cubes)))
            #self.merge_cubes(on_cubes)

        return self.sum_cubes(on_cubes)

    def sum_cubes(self, on_cubes):
        total_cubes_on = 0
        for cube in on_cubes:
            total_cubes_on += cube.get_num_cubes()
        return total_cubes_on

    def merge_cubes(self, cubes):
        new_cubes = []
        merged = []
        for i in range(0, len(cubes)):
            isx, isy, isz = cubes[i].start
            iex, iey, iez = cubes[i].end
            for j in range(i+1, len(cubes)):
                jsx, jsy, jsz = cubes[j].start
                jex, jey, jez = cubes[j].end
                if isx == jsx and iex == jex and isy == jsy and iey == jey:
                    new_cubes.append(CubeSection((isx, isy, min(isz,jsz)),(iex,iey,max(iez,jez))))
                    self.debugPrint("Merge cubes {} and {}: {} to {}".format(i, j, new_cubes[-1].start, new_cubes[-1].end))
                    merged = merged + [i, j]
                    break
                elif isz == jsz and iez == jez and isy == jsy and iey == jey:
                    new_cubes.append(CubeSection((min(isx,jsx), isy, isz),(max(iex,jex),iey,iez)))
                    self.debugPrint("Merge cubes {} and {}: {} to {}".format(i, j, new_cubes[-1].start, new_cubes[-1].end))
                    merged = merged + [i, j]
                    break
                elif isx == jsx and iex == jex and isz == jsz and iez == jez:
                    new_cubes.append(CubeSection((isx, min(isy,jsy), isz),(iex,max(iey,jey),iez)))
                    self.debugPrint("Merge cubes {} and {}: {} to {}".format(i, j, new_cubes[-1].start, new_cubes[-1].end))
                    merged = merged + [i, j]
                    break
        for n, c in enumerate(cubes):
            if n not in merged:
                new_cubes.append(c)
        if len(merged) != 0:
            self.debugPrint("Merged cubes:")
            for i, c in enumerate(cubes):
                self.debugPrint("  {}: {} to {}".format(i, c.start, c.end))
        return new_cubes


if __name__ == '__main__':
    parser = getBasicArgparser('day 22')
    parser.add_argument('-p', type=int, help="Which part to run, default is run both", default=-1)

    args = parser.parse_args()

    if args.p != 2:
        day22p1 = Day22Part1(args.i, args.v)
        print("Part 1: after all steps, {} cubes are on".format(day22p1.apply_steps()))
    if args.p != 1:
        day22p2 = Day22Part2(args.i, args.v)
        print("Part 2: after all steps, {} cubes are on".format(day22p2.apply_steps()))
