#!/usr/bin/env python3

import csv
import sys
import ast
import math
import copy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day18(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.numbers = []
        self.parse_file()

    def conv_str_to_list(self, string) -> list:
        return ast.literal_eval(string)

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.numbers.append(self.conv_str_to_list(line))

    def get_largest_mag_of_any_two_sum(self):
        largest_magnitude = 0
        largest_a = []
        largest_b = []
        largest_result = []
        numbers = copy.deepcopy(self.numbers)
        self.debugPrint("Compare these options:")
        for i, num in enumerate(numbers):
            self.debugPrint("{}: {}".format(i, num))
        self.debugPrint("-----")
        for i, a in enumerate(numbers):
            for j, b in enumerate(numbers):
                if i == j:
                    continue
                sum = self.reduce(self.add_snailfish(a, b))
                mag = self.calculate_magnitude(sum)
                self.debugPrint("  {}".format(a))
                self.debugPrint(" +{}".format(b))
                self.debugPrint(" ={}".format(sum))
                self.debugPrint("-----")
                self.debugPrint("{}+{}: mag {}".format(i, j, mag))
                if mag > largest_magnitude:
                    largest_magnitude = mag
                    largest_a = a
                    largest_b = b
                    largest_result = sum
        return largest_magnitude

    def calculate_magnitude(self, snailfish):
        lhs, rhs = snailfish
        if type(lhs) == list:
            lhs = self.calculate_magnitude(lhs)
        if type(rhs) == list:
            rhs = self.calculate_magnitude(rhs)
        return 3*lhs + 2*rhs

    def add_snailfish_numbers(self):
        numbers = copy.deepcopy(self.numbers)
        result = numbers.pop(0)
        for next in numbers:
            result = self.add_snailfish(result, next)
            result = self.reduce(result)
        magnitude = self.calculate_magnitude(result)
        return result, magnitude

    def add_snailfish(self, lhs, rhs):
        return [copy.deepcopy(lhs), copy.deepcopy(rhs)]

    def reduce(self, snailfish):
        # If nested 4 times, explode
        # else, if regular number 10 or greater, split
        while(True):
            exploded, snailfish = self.explode(snailfish)
            if not exploded:
                got_split, snailfish = self.splitnum(snailfish)
            if not exploded and not got_split:
                break
        return snailfish

    def explode(self, snailfish):
        for i, a in enumerate(snailfish):
            if type(a) is list:
                if len(a) != 2:
                    print("Malformed list {}".format(a))
                for j, b in enumerate(a):
                    if type(b) is list:
                        if len(b) != 2:
                            print("Malformed list {}".format(b))
                        for k, c in enumerate(b):
                            if type(c) is list:
                                if len(c) != 2:
                                    print("Malformed list {}".format(c))
                                for l, d in enumerate(c):
                                    if type(d) is list:
                                        if len(d) != 2:
                                            print("Malformed list {}".format(d))
                                        # left number
                                        if l == 1:
                                            c[0] = self.add_dir(1, c[0], d[0])
                                        elif k == 1:
                                            b[0] = self.add_dir(1, b[0], d[0])
                                        elif j == 1:
                                            a[0] = self.add_dir(1, a[0], d[0])
                                        elif i == 1:
                                            snailfish[0] = self.add_dir(1, snailfish[0], d[0])
                                        # right number
                                        if l == 0:
                                            c[1] = self.add_dir(0, c[1], d[1])
                                        elif k == 0:
                                            b[1] = self.add_dir(0, b[1], d[1])
                                        elif j == 0:
                                            a[1] = self.add_dir(0, a[1], d[1])
                                        elif i == 0:
                                            snailfish[1] = self.add_dir(0, snailfish[1], d[1])
                                        c[l] = 0
                                        return True, snailfish
        return False, snailfish

    def add_dir(self, dir, lst, add):
        if type(lst) is list:
            if type(lst[dir]) is list:
                if type(lst[dir][dir]) is list:
                    if type(lst[dir][dir][dir]) is list:
                        if type(lst[dir][dir][dir][dir]) is list:
                            lst[dir][dir][dir][dir][dir] += add
                        else:
                            lst[dir][dir][dir][dir] += add
                    else:
                        lst[dir][dir][dir] += add
                else:
                    lst[dir][dir] += add
            else:
                lst[dir] += add
        else:
            lst += add
        return lst

    def splitnum(self, snailfish):
        for i, a in enumerate(snailfish):
            if type(a) is list:
                for j, b in enumerate(a):
                    if type(b) is list:
                        for k, c in enumerate(b):
                            if type(c) is list:
                                for l, d in enumerate(c):
                                    if type(d) is list:
                                        for m, e in enumerate(d):
                                            if type(e) is list:
                                                print("ERROR: Too much depth in list!")
                                                print(snailfish)
                                                sys.exit(1)
                                            else:
                                                if e >= 10:
                                                    d[m] = self.pairify(e)
                                                    return True, snailfish
                                    else:
                                        if d >= 10:
                                            c[l] = self.pairify(d)
                                            return True, snailfish
                            else:
                                if c >= 10:
                                    b[k] = self.pairify(c)
                                    return True, snailfish
                    else:
                        if b >= 10:
                            a[j] = self.pairify(b)
                            return True, snailfish
            else:
                if a >= 10:
                    snailfish[i] = self.pairify(a)
                    return True, snailfish
        return False, snailfish

    def pairify(self, val):
        return [math.floor(val/2), math.ceil(val/2)]


if __name__ == '__main__':
    parser = getBasicArgparser('day 18')
    parser.add_argument('-r', type=str, help='Test snailfish reduction. -i and -m options are ignored in this case.', default=None)
    parser.add_argument('-m', type=str, help='Test snailfish magnitude. -i option is ignored in this case.', default=None)

    args = parser.parse_args()

    day18 = Day18(args.i, args.v)

    if args.r is not None:
        snailfish_number = day18.conv_str_to_list(args.r)
        print("Snailfish number: {}".format(snailfish_number))
        print("Reduce: {}".format(day18.reduce(snailfish_number)))
    elif args.m is not None:
        snailfish_number = day18.conv_str_to_list(args.m)
        print("Snailfish number: {}".format(snailfish_number))
        print("Magnitude: {}".format(day18.calculate_magnitude(snailfish_number)))
    else:
        print("Adding all snailfish numbers:")
        sum, mag = day18.add_snailfish_numbers()
        print(" Final result: {}".format(sum))
        print(" Magnitude of result: {}".format(mag))
        print("--------")
        print("Finding largest magnitude of sum of any two numbers:")
        print("Largest magnitude: {}".format(day18.get_largest_mag_of_any_two_sum()))
        print("--------")
