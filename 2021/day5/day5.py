#!/usr/bin/env python3

import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day5(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.lines = []
        self.maxCoord = 0
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                points = []
                for coord in line.split(' -> '):
                    individual = [int(x) for x in coord.split(',')]
                    points.append(individual)
                    self.maxCoord = max(self.maxCoord, max(individual))
                self.lines.append(points)

    def is_diagonal(self, line):
        return line[0][0] != line[1][0] and line[0][1] != line[1][1]

    def is_horizontal(self, line):
        return line[0][1] == line[1][1]

    def is_vertical(self, line):
        return line[0][0] == line[1][0]

    def get_diagram(self, handle_diagonals):
        diagram = numpy.zeros((self.maxCoord+1, self.maxCoord+1))
        for line in self.lines:
            self.debugPrint("Draw line: {}".format(line))
            inc = 1
            if line[0][0] > line[1][0]:
                inc = -1
            xrange = list(range(line[0][0], line[1][0]+inc, inc))
            if line[0][1] > line[1][1]:
                inc = -1
            else:
                inc = 1
            yrange = list(range(line[0][1], line[1][1]+inc, inc))

            if len(xrange) == 1:
                xrange = len(yrange) * xrange
            if len(yrange) == 1:
                yrange = len(xrange) * yrange

            if self.is_horizontal(line):
                self.debugPrint("   (Horizontal)")
            elif self.is_vertical(line):
                self.debugPrint("   (Vertical)")
            else:
                if handle_diagonals:
                    self.debugPrint("   (Diagonal)")
                else:
                    self.debugPrint("   ...Skipping Diagonal")
                    continue

            if len(xrange) != len(yrange):
                print("ERROR: ranges do not match!")
            for i in range(0, len(xrange)):
                x = xrange[i]
                y = yrange[i]
                diagram[y][x] += 1
                self.debugPrint("   Add to x index {} y index {}: {}".format(x, y, diagram[y][x]))
            self.debugPrint(diagram)
        return diagram

    def count_dangerous(self, diagram, greater_or_equal):
        return numpy.count_nonzero(diagram >= greater_or_equal)


if __name__ == '__main__':
    parser = getBasicArgparser('day 5')
    parser.add_argument('-part1', action='store_true', default=False, help='Calculate result for part 1')
    parser.add_argument('-part2', action='store_true', default=False, help='Calculate result for part 2')

    args = parser.parse_args()

    day5 = Day5(args.i, args.v)

    doBoth = not args.part1 and not args.part2

    if args.part1 or doBoth:
        part1 = day5.get_diagram(False)
        print("Horizontal and Vertical only: Number of dangerous areas: {}".format(day5.count_dangerous(part1, 2)))

    if args.part2 or doBoth:
        part2 = day5.get_diagram(True)
        print("All lines: Number of dangerous areas: {}".format(day5.count_dangerous(part2, 2)))
