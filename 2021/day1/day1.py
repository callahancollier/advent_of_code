#!/usr/bin/env python3

import csv
import sys

class Day1:
    def __init__(self, input_file):
        self.input_file = input_file
        self.topo = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_file) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = int(line[0])
                self.topo.append(line)

    def get_number_of_depth_increases(self, window_size=1):
        num_incr = 0
        start = window_size
        for i in range(start, len(self.topo)):
            sum_prev = 0
            sum_curr = 0
            for j in range(0, window_size):
                sum_prev = sum_prev + self.topo[i-j-1]
                sum_curr = sum_curr + self.topo[i-j]
            if sum_curr - sum_prev > 0:
                num_incr = num_incr + 1
        return num_incr


if __name__ == '__main__':
    day1 = Day1('input.csv')
    print("Part 1: Number of depth increases: {}".format(day1.get_number_of_depth_increases()))
    print("Part 2: Number of depth increases: {}".format(day1.get_number_of_depth_increases(3)))
