#!/usr/bin/env python3

import csv
import sys
from collections import Counter
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day7(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.crabs = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.crabs = [int(x) for x in line.split(',')]

    def get_fuel_to_align_linear(self, alignment):
        fuel = 0
        for crab in self.crabs:
            fuel += abs(crab - alignment)
        return fuel

    def get_fuel_to_align_exp(self, alignment):
        #1+2+3+...+n = n(n+1)/2
        fuel = 0
        for crab in self.crabs:
            diff = abs(crab - alignment)
            fuel += diff * (diff + 1) / 2
        return fuel

    def get_best_alignment_brute_force(self, exponential_fuel):
        min_fuel = 1e9
        min_align = 0
        for align in range(0, max(self.crabs)):
            if not exponential_fuel:
                fuel = self.get_fuel_to_align_linear(align)
            else:
                fuel = self.get_fuel_to_align_exp(align)
            if fuel < min_fuel:
                min_fuel = fuel
                min_align = align
        print("Aligning at {} costs {} fuel".format(min_align, min_fuel))
        return min_fuel



if __name__ == '__main__':
    parser = getBasicArgparser('day 7')

    args = parser.parse_args()

    day7 = Day7(args.i, args.v)

    print("Part 1:")
    day7.get_best_alignment_brute_force(False)
    print('-----------------------------------')
    print("Part 2:")
    day7.get_best_alignment_brute_force(True)
