#!/usr/bin/env python3

import csv
import sys
from enum import Enum
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Direction:
    def __init__(self, enum: str, val: int):
        self.type = enum
        self.val = val


class Day2(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.directions = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                self.directions.append(Direction(line[0], int(line[1])))

    def calc_final_position_part_one(self):
        depth = 0
        horizontal_position = 0
        for direction in self.directions:
            self.debugPrint("Move {} {}".format(direction.type, direction.val))
            if direction.type == "forward":
                horizontal_position += direction.val
            elif direction.type == "up":
                depth -= direction.val
            elif direction.type == "down":
                depth += direction.val
            else:
                print("ERROR: unknown direction {}".format(direction.type))
        return depth,horizontal_position

    def calc_final_position_part_two(self):
        depth = 0
        horizontal_position = 0
        aim = 0
        for direction in self.directions:
            self.debugPrint("Move {} {}".format(direction.type, direction.val))
            if direction.type == "forward":
                horizontal_position += direction.val
                depth += direction.val * aim
            elif direction.type == "up":
                aim -= direction.val
            elif direction.type == "down":
                aim += direction.val
            else:
                print("ERROR: unknown direction {}".format(direction.type))
        return depth, horizontal_position, aim


if __name__ == '__main__':
    parser = getBasicArgparser('day 2')

    args = parser.parse_args()

    day2 = Day2(args.i, args.v)

    depth,horizontal_position = day2.calc_final_position_part_one()

    print("PART ONE:")
    print("Final depth:              {}".format(depth))
    print("Final horizonal position: {}".format(horizontal_position))
    print("Multiplied together:      {}".format(depth * horizontal_position))
    print("---------------------------------")
    depth,horizontal_position,aim = day2.calc_final_position_part_two()
    print("PART TWO:")
    print("Final depth:              {}".format(depth))
    print("Final horizonal position: {}".format(horizontal_position))
    print("Final aim:                {}".format(aim))
    print("Multiplied together:      {}".format(depth * horizontal_position))
