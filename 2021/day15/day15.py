#!/usr/bin/env python3

import csv
import sys
import numpy
import math
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, ShortestPathCardinalBase, Point


class CavernTraverse(ShortestPathCardinalBase):
    def __init__(self, debug, end, cavern):
        super().__init__(debug, Point(0, 0), end, cavern)

    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        return current_g_score + self.array[neighbour_node.row, neighbour_node.col]

    def tally_path(self, current, camefrom):
        pathtotal = self.array[current.row, current.col]
        while current in camefrom.keys():
            current = camefrom[current]
            pathtotal += self.array[current.row, current.col]
        # Don't count the start point of the path
        pathtotal -= self.array[0, 0]
        return pathtotal


class Day15(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.cavern = None
        self.parse_file()

    def parse_file(self):
        cavern = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                cavern.append([int(x) for x in line])
        self.cavern = numpy.array(cavern)

    def expand_cavern(self):
        og_cavern = self.cavern.copy()
        extra_cavern = self.cavern.copy()
        # Five times larger in both directions
        # To the right
        for i in range(0, 4):
            for x in range(0, len(extra_cavern[0])):
                for y in range(0, len(extra_cavern)):
                    extra_cavern[y, x] += 1
                    if extra_cavern[y, x] > 9:
                        extra_cavern[y, x] = 1
            self.cavern = numpy.c_[self.cavern, extra_cavern]
        # Now down
        extra_cavern = self.cavern.copy()
        for i in range(0, 4):
            for x in range(0, len(extra_cavern[0])):
                for y in range(0, len(extra_cavern)):
                    extra_cavern[y, x] += 1
                    if extra_cavern[y, x] > 9:
                        extra_cavern[y, x] = 1
            self.cavern = numpy.r_[self.cavern, extra_cavern]

    def manhattan(self, point):
        x, y = point
        return (len(self.cavern[0]) - x) + (len(self.cavern) - y)

    def get_shortest_path(self):
    #def new(self):
        goal = Point(len(self.cavern[0])-1, len(self.cavern)-1)
        calculator = CavernTraverse(self.debug, goal, self.cavern.copy())
        return calculator.get_shortest_path()

    def deprecated(self):
    #def get_shortest_path(self):

        # Using A* search
        openset = set()

        camefrom = {}
        gscore = {}
        fscore = {}

        start = (0,0)
        goal = (len(self.cavern[0])-1, len(self.cavern)-1)
        openset.add(start)
        gscore[start] = 0
        fscore[start] = self.manhattan(start)

        while len(openset) != 0:
            current = self.get_openset_node_with_lowest_score(openset, fscore)
            self.debugPrint("Consider {}".format(current))
            if current == goal:
                return self.tally_path(current, camefrom)

            openset.discard(current)
            for adj in [-1, 1]:
                neighbourA = (current[0]+adj, current[1])
                neighbourB = (current[0], current[1]+adj)
                for neighbour in [neighbourA, neighbourB]:
                    if neighbour[0] < 0 or neighbour[0] >= len(self.cavern[0]):
                        continue
                    if neighbour[1] < 0 or neighbour[1] >= len(self.cavern):
                        continue
                    self.debugPrint("Check neighbour {}".format(neighbour))
                    tentative_gscore = gscore.get(current, math.inf) + self.cavern[neighbour[0], neighbour[1]]
                    if tentative_gscore < gscore.get(neighbour, math.inf):
                        camefrom[neighbour] = current
                        gscore[neighbour] = tentative_gscore
                        fscore[neighbour] = tentative_gscore + self.manhattan(neighbour)
                        openset.add(neighbour)
                        self.debugPrint("Add {} to set".format(neighbour))
            self.debugPrint(openset)
        print("ERROR did not find any path")
        return 0

    def get_openset_node_with_lowest_score(self, openset, fscore):
        min_val = (-1,-1)
        for val in openset:
            self.debugPrint("Compare {} for {} with {} for {}".format(
                fscore.get(val, math.inf), val,
                fscore.get(min_val, math.inf), min_val,
            ))
            if fscore.get(val, math.inf) <= fscore.get(min_val, math.inf):
                self.debugPrint("New min {}".format(val))
                min_val = val
        self.debugPrint("min overall {}".format(min_val))
        return min_val

    def tally_path(self, current, camefrom):
        pathtotal = self.cavern[current[0],current[1]]
        while current in camefrom.keys():
            current = camefrom[current]
            pathtotal += self.cavern[current[0], current[1]]
        # Don't count the start point of the path
        pathtotal -= self.cavern[0,0]
        return pathtotal


if __name__ == '__main__':
    parser = getBasicArgparser('day 15')

    args = parser.parse_args()

    day15 = Day15(args.i, args.v)

    print("Shortest path: {}".format(day15.get_shortest_path()))

    day15.expand_cavern()
    print("Shortest path: {}".format(day15.get_shortest_path()))
