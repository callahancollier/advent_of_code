#!/usr/bin/env python3

import csv
import sys
import numpy
import math
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day13(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.paper = None
        self.instructions = []
        self.num_folds = 0
        self.parse_file()

    def parse_file(self):
        reading_dots = True
        max_x = 0
        max_y = 0
        coords = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    reading_dots = False
                    continue
                line = line[0]
                if reading_dots:
                    coords.append([int(x) for x in line.split(',')])
                    if max_x < coords[-1][0]:
                        max_x = coords[-1][0]
                    if max_y < coords[-1][1]:
                        max_y = coords[-1][1]
                else:
                    line = line.replace("fold along ", "")
                    line = line.split('=')
                    self.instructions.append((line[0],int(line[1])))
        self.paper = numpy.full((max_y+1, max_x+1), '.')
        for coord in coords:
            self.paper[coord[1],coord[0]] = '#'
        self.debugPrint("Initial paper ({}x{}):".format(max_x+1, max_y+1))
        self.printPaper()

    def printPaper(self, force=False):
        save_debug = self.debug
        if force:
            self.debug = True
        for row in self.paper:
            self.debugPrint(''.join(row))
        if force:
            self.debug = save_debug

    def fold(self, num_folds):
        # Assuming that the folds are always in the middle, or if they are not, we need to add one on the edge farthest
        # from (0,0). By observation of the input, this seems to hold true for this puzzle
        for f in range(0, num_folds):
            fold_dir, fold_place = self.instructions.pop(0)
            if fold_dir == 'y':
                if (len(self.paper)-1) - (fold_place+1) != fold_place - 1:
                    self.debugPrint("Uneven fold: fold {} at {}, dim {}x{}".format(fold_dir, fold_place, len(self.paper[0]), len(self.paper)))
                    self.paper = numpy.r_[self.paper, numpy.full((1,len(self.paper[0])), '.')]
                self.debugPrint("Fold horizontally at y={}".format(fold_place))
                for y in range(0, fold_place):
                    #self.debugPrint("Overlap {}:  {}".format(y, self.paper[y,:]))
                    #self.debugPrint("Overlap {}: {}".format(-1-y, self.paper[-1-y,:]))
                    for x in range(0, len(self.paper[y])):
                        if self.paper[-1-y, x] == '#':
                            self.paper[y, x] = '#'
                    #self.debugPrint("Result:     {}".format(self.paper[y, :]))
                    #self.debugPrint("----------")
                #self.debugPrint("Before resize:")
                #self.printPaper()
                self.paper = self.paper[0:fold_place, 0:len(self.paper[0])]
            elif fold_dir == 'x':
                if (len(self.paper[0])-1) - (fold_place+1) != fold_place - 1:
                    self.debugPrint("Uneven fold: fold {} at {}, dim {}x{}".format(fold_dir, fold_place, len(self.paper[0]), len(self.paper)))
                    self.paper = numpy.c_[self.paper, numpy.full((len(self.paper), 1), '.')]
                self.debugPrint("Fold vertically at x={}".format(fold_place))
                for x in range(0, fold_place):
                    #self.debugPrint("Overlap {}:  {}".format(x, self.paper[:,x]))
                    #self.debugPrint("Overlap {}: {}".format(-1-x, self.paper[:,-1-x]))
                    for y in range(0, len(self.paper)):
                        if self.paper[y, -1-x] == '#':
                            self.paper[y, x] = '#'
                    #self.debugPrint("Result:     {}".format(self.paper[:,x]))
                    #self.debugPrint("----------")
                #self.debugPrint("Before resize:")
                #self.printPaper()
                #self.debugPrint("resize({},{})".format(len(self.paper), fold_place))
                self.paper = self.paper[0:len(self.paper), 0:fold_place]
            else:
                print("ERROR: unknown index {}".format(fold_dir))
            self.num_folds += 1
            self.debugPrint("After fold {}:".format(self.num_folds))
            self.printPaper()

    def count_dots(self):
        return numpy.count_nonzero(self.paper == '#')

    def finish_folding(self):
        self.fold(len(self.instructions))


if __name__ == '__main__':
    parser = getBasicArgparser('day 13')
    parser.add_argument('-n', type=int, help='Number of folds to do', default=1)

    args = parser.parse_args()

    day13 = Day13(args.i, args.v)

    day13.fold(args.n)
    print("After {} fold: {} dots".format(args.n, day13.count_dots()))

    day13.finish_folding()
    print("After finishing all the folds:")
    day13.printPaper(True)
