#!/usr/bin/env python3

import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


def binary_char_list_to_decimal(charlist):
    return int("".join(charlist), 2)


class Day3(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.diagnostics = None
        self.parse_file()

    def parse_file(self):
        temp = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                temp.append([char for char in line])
        self.diagnostics = numpy.array(temp)
        self.debugPrint("Input: \n{}".format(self.diagnostics))

    def moreZeros(self, array):
        num0 = numpy.count_nonzero(array == '0')
        num1 = numpy.count_nonzero(array == '1')
        return num0 > num1

    def moreOnes(self, array):
        num0 = numpy.count_nonzero(array == '0')
        num1 = numpy.count_nonzero(array == '1')
        return num0 < num1

    def getMostCommon(self, array, default):
        if self.moreZeros(array):
            return '0'
        elif self.moreOnes(array):
            return '1'
        return default

    def getLeastCommon(self, array, default):
        if not self.moreZeros(array):
            return '0'
        elif not self.moreOnes(array):
            return '1'
        return default

    def calculate_part1(self):
        gamma = ""
        epsilon = ""
        for i in range(0, len(self.diagnostics[0])):
            gamma += self.getMostCommon(self.diagnostics[:, i], '0')
            epsilon += self.getLeastCommon(self.diagnostics[:, i], '0')
        self.debugPrint("gamma: {}, epsilon: {}".format(gamma, epsilon))
        return binary_char_list_to_decimal(gamma) * binary_char_list_to_decimal(epsilon)

    def get_oxygen_gen_rating(self):
        self.debugPrint("O2 generator rating:")
        return self.get_rating(self.getMostCommon, '1')

    def get_co2_scrubber_rating(self):
        self.debugPrint("CO2 scrubber rating:")
        return self.get_rating(self.getLeastCommon, '0')

    def get_rating(self, get_function, function_dflt):
        current = numpy.copy(self.diagnostics)
        i = 0
        while len(current[:, 0]) > 1:
            if i > len(current[0, :]) - 1:
                print("ERROR: ran out of bit positions")
                break
            self.debugPrint("Current options:\n{}".format(current))
            new_list = []
            target = get_function(current[:, i], function_dflt)
            self.debugPrint("Looking at position {}, keep entries with {}".format(i, target))
            for entry in current:
                self.debugPrint("       Consider entry {} Position {} is {}".format(entry, i, entry[i]))
                if entry[i] == target:
                    self.debugPrint(" --> Keep entry {}".format(entry))
                    new_list.append(entry)
            current = numpy.array(new_list)
            i += 1
        return current[0]

    def calculate_part2(self):
        o2_gen_rating = self.get_oxygen_gen_rating()
        co2_scrub_rating = self.get_co2_scrubber_rating()

        self.debugPrint("Oxygen generator: {}, CO2 scrubber: {}".format(o2_gen_rating, co2_scrub_rating))

        return binary_char_list_to_decimal(o2_gen_rating) * binary_char_list_to_decimal(co2_scrub_rating)


if __name__ == '__main__':
    parser = getBasicArgparser('day 3')

    args = parser.parse_args()

    day3 = Day3(args.i, args.v)

    print("Part one answer: {}".format(day3.calculate_part1()))
    print('-------------------------------------')

    print("Part two answer: {}".format(day3.calculate_part2()))
    print('-------------------------------------')
