#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day8(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.entries = []
        self.map = {}
        self.reverse_map = {}
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                line = line.split('|')
                signal_patterns = line[0].strip().split(' ')
                output_values = line[1].strip().split(' ')
                self.entries.append((signal_patterns, output_values))

    def count_unique_digits(self):
        unique_digits = 0
        for entry in self.entries:
            signal_patterns, output_values = entry
            for value in output_values:
                if len(value) in [2, 4, 3, 7]:
                    unique_digits += 1
        return unique_digits

    def is_intersection(self, candidate, check_list):
        found = True
        for check in check_list:
            self.debugPrint("  Check for {} ({}) in {}".format(check, self.reverse_map[check], candidate))
            comparator = self.reverse_map[check]
            checker = candidate
            if len(self.reverse_map[check]) < len(candidate):
                comparator = candidate
                checker = self.reverse_map[check]
            for seg in checker:
                if seg not in comparator:
                    self.debugPrint("    {} != {}".format(seg, comparator))
                    found = False
                else:
                    self.debugPrint("    {} == {}".format(seg, comparator))
        return found

    def alphabetize(self, input_string):
        return ''.join(sorted(input_string))

    def assign_to_map(self, value, seg_string):
        seg_string = self.alphabetize(seg_string)
        self.map[seg_string] = value
        self.reverse_map[value] = seg_string
        self.debugPrint("{} is {}".format(value, seg_string))

    def find_value(self, value, candidates, check_list):
        for digit in candidates:
            found = self.is_intersection(digit, check_list)
            if found:
                self.assign_to_map(value, digit)
                candidates.remove(digit)
                return candidates
        print("ERROR: Did not find {}".format(value))
        return candidates

    def map_digits(self, signal_patterns):
        self.map = {}
        self.reverse_map = {}
        six_length = [] # 0, 6, 9
        five_length = [] # 2, 3, 5
        # Get the easy ones first
        for pattern in signal_patterns:
            length = len(pattern)
            if length == 2:
                self.assign_to_map(1, pattern)
            elif length == 4:
                self.assign_to_map(4, pattern)
            elif length == 3:
                self.assign_to_map(7, pattern)
            elif length == 7:
                self.assign_to_map(8, pattern)
            elif length == 6:
                six_length.append(pattern)
            elif length == 5:
                five_length.append(pattern)
            else:
                print("ERROR: unknown pattern {}".format(pattern))

        self.debugPrint("Start out: {}".format(self.reverse_map))
        self.debugPrint(five_length)
        self.debugPrint(six_length)

        # 9: has all the segs from 4 and 7
        six_length = self.find_value(9, six_length, [4, 7])

        # 3: has all the segs from 9 and 7
        five_length = self.find_value(3, five_length, [9, 7])

        # 5: Has all the digits from 9 (now that 3 has been removed)
        five_length = self.find_value(5, five_length, [9])

        # 6: Has all digits from 5
        six_length = self.find_value(6, six_length, [5])

        # 0: is the last one left with length of six
        if len(six_length) != 1:
            print("ERROR: Something went wrong with the six-length-ers")
        self.assign_to_map(0, six_length.pop())

        # 2: is the last one left with length of five
        if len(five_length) != 1:
            print("ERROR: Something went wrong with the five-length-ers")
        self.assign_to_map(2, five_length.pop())

        self.debugPrint("Finish: {}".format(self.reverse_map))

    def get_outputs(self):
        outputs = []
        for i, entry in enumerate(self.entries):
            result = 0
            signal_patterns, output_values = entry
            self.map_digits(signal_patterns)
            self.debugPrint("=======================")
            for output in output_values:
                output = self.alphabetize(output)
                self.debugPrint(output)
                decoded = self.map[output]
                self.debugPrint("{} maps to {}".format(output, decoded))
                result = result * 10 + decoded
            self.debugPrint("Entry i is code {}".format(result))
            outputs.append(result)
            self.debugPrint("-----------------------")
        return outputs

    def get_sum_of_outputs(self):
        outputs = self.get_outputs()
        return sum(outputs)


if __name__ == '__main__':
    parser = getBasicArgparser('day 8')

    args = parser.parse_args()

    day8 = Day8(args.i, args.v)

    print("Unique digits in the output: {}".format(day8.count_unique_digits()))

    print("Sum of output codes: {}".format(day8.get_sum_of_outputs()))
