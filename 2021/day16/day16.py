#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Binary:
    def __init__(self, data):
        self.data = data

    def pop_bits(self, num_bits):
        bits = self.data[0:num_bits]
        self.data = self.data[num_bits:]
        return bits


class Packet:
    def __init__(self, type_id, value=None):
        self.type_id = type_id
        self.value = value
        self.sub_packets = []

    def get_value(self):
        if self.value is not None:
            if self.type_id != LITERAL_ID:
                print("ERROR: Malformed packet, id {} but value {}".format(self.type_id, self.value))
            if len(self.sub_packets) != 0:
                print("ERROR: Malformed packet, value {} sub {}".format(self.value, self.sub_packets))
            return self.value

        if self.type_id == SUM_ID:
            sum = self.sub_packets.pop(0).get_value()
            print("SUM")
            print("   {}".format(sum))
            for pkt in self.sub_packets:
                val = pkt.get_value()
                sum += val
                print("  +{}".format(val))
            print(" ={}".format(sum))
            return sum
        elif self.type_id == PRODUCT_ID:
            prod = self.sub_packets.pop(0).get_value()
            print("PROD")
            print("   {}".format(prod ))
            for pkt in self.sub_packets:
                val = pkt.get_value()
                prod *= val
                print("  *{}".format(prod))
            print(" ={}".format(prod))
            return prod
        elif self.type_id == MIN_ID:
            min = self.sub_packets.pop(0).get_value()
            print("MIN")
            print("   {}".format(min))
            for pkt in self.sub_packets:
                val = pkt.get_value()
                if val < min:
                    min = val
                print("   {}".format(min))
            print(" ={}".format(min))
            return min
        elif self.type_id == MAX_ID:
            max = self.sub_packets.pop(0).get_value()
            print("MAX")
            print("   {}".format(max))
            for pkt in self.sub_packets:
                val = pkt.get_value()
                if val > max:
                    max = val
                print("   {}".format(max))
            print(" ={}".format(max))
            return max
        elif self.type_id == GT_ID:
            if len(self.sub_packets) != 2:
                print("ERROR: GT packet has {} sub packets".format(len(self.sub_packets)))
            lhs = self.sub_packets[0].get_value()
            rhs = self.sub_packets[1].get_value()
            print("GT: {} > {}".format(lhs, rhs))
            if lhs > rhs:
                print(" =1")
                return 1
            print(" =0")
            return 0
        elif self.type_id == LT_ID:
            if len(self.sub_packets) != 2:
                print("ERROR: LT packet has {} sub packets".format(len(self.sub_packets)))
            lhs = self.sub_packets[0].get_value()
            rhs = self.sub_packets[1].get_value()
            print("LT: {} < {}".format(lhs, rhs))
            if lhs < rhs:
                print(" =1")
                return 1
            print(" =0")
            return 0
        elif self.type_id == EQ_ID:
            if len(self.sub_packets) != 2:
                print("ERROR: EQ packet has {} sub packets".format(len(self.sub_packets)))
            lhs = self.sub_packets[0].get_value()
            rhs = self.sub_packets[1].get_value()
            print("EQ: {} == {}".format(lhs, rhs))
            if lhs == rhs:
                print(" =1")
                return 1
            print(" =0")
            return 0

LITERAL_ID = 4
SUM_ID = 0
PRODUCT_ID = 1
MIN_ID = 2
MAX_ID = 3
GT_ID = 5
LT_ID = 6
EQ_ID = 7


class Day16(BaseFunctions):
    def __init__(self, input_file, enable_debug, example_data):
        super().__init__(input_file, enable_debug)
        self.binary = ""
        self.bin_len = 0
        self.versions = []
        self.packet_tree = None
        if example_data is None:
            self.parse_file()
        else:
            self.binary = bin(int(example_data, 16))[2:]
            self.bin_len = len(example_data)
        while len(self.binary) != self.bin_len*4:
            self.binary = '0' + self.binary
        self.debugPrint("Binary: {}".format(self.binary))

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.binary += bin(int(line, 16))[2:]
                self.bin_len = len(line)

    def get_header(self, data):
        version = data.pop_bits(3)
        type_id = data.pop_bits(3)
        self.versions.append(version)
        self.debugPrint("Packet version {}, type ID {}".format(int(version,2), int(type_id, 2)))
        return data, version, int(type_id,2)

    def parse_literal(self, data):
        value = ""
        while True:
            next_bits = data.pop_bits(5)
            value += next_bits[1:]
            if next_bits[0] == '0':
                break
        self.debugPrint(" Parse literal packet: value {} ({})".format(value, int(value,2)))
        packet = Packet(LITERAL_ID, int(value,2))
        return data, packet

    def parse_operator(self, data, type_id):
        packet = Packet(type_id)
        length_type_id = data.pop_bits(1)
        self.debugPrint(" Parse operator packet")
        if length_type_id == '0':
            total_length_bits = int(data.pop_bits(15), 2)
            self.debugPrint("  Length type ID {}, total length {}".format(length_type_id, total_length_bits))
            sub_data = Binary(data.pop_bits(total_length_bits))
            while len(sub_data.data) != 0:
                sub_data, sub_packet = self.parse_packet(sub_data)
                packet.sub_packets.append(sub_packet)
            return data, packet
        elif length_type_id == '1':
            number_of_sub_packets = int(data.pop_bits(11), 2)
            self.debugPrint("  Length type ID {}, number of sub packets {}".format(length_type_id, number_of_sub_packets))
            for i in range(0, number_of_sub_packets):
                data, sub_packet = self.parse_packet(data)
                packet.sub_packets.append(sub_packet)
            return data, packet
        else:
            print("ERROR: binary data is not 1 or 0: {}".format(length_type_id))

    def parse_data(self):
        data = Binary(self.binary)
        data, packet_tree = self.parse_packet(data)
        if '1' in data.data:
            print("WARNING: Expecting only zeroes left over, but have {}".format(data.data))
        self.debugPrint("Left over at the end: {}".format(data.data))
        self.packet_tree = packet_tree

    def parse_packet(self, data):
        data, version, type_id = self.get_header(data)
        if type_id == LITERAL_ID:
            data = self.parse_literal(data)
        else:
            data = self.parse_operator(data, type_id)
        return data

    def get_sum_of_versions(self):
        if len(self.versions) == 0:
            print("ERROR: didn't parse the packets first!")
        total = 0
        for version in self.versions:
            total += int(version, 2)
        return total

    def work_out_packet_solution(self):
        if self.packet_tree is None:
            print("ERROR: didn't parse the packets first!")
        return self.packet_tree.get_value()


if __name__ == '__main__':
    parser = getBasicArgparser('day 16')
    parser.add_argument('-e', type=str, help='Example data to parse. Causes the program to ignore the -i option', default=None)

    args = parser.parse_args()

    if args.e is not None:
        print("Ignoring -i input, using example data")

    day16 = Day16(args.i, args.v, args.e)

    day16.parse_data()
    print("Part 1: Sum of version numbers in all packets: {}".format(day16.get_sum_of_versions()))

    result = day16.work_out_packet_solution()
    print("Solution to transmission: {}".format(result))