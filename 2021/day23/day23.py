#!/usr/bin/env python3

import csv
import sys
import copy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Layout:
    def __init__(self, rooms, hallway):
        self.rooms = rooms
        self.hallway = hallway


class Day23(BaseFunctions):
    def __init__(self, input_file, enable_debug, part):
        super().__init__(input_file, enable_debug)
        self.starting_layout = None
        self.parse_file()
        if part == 1:
            pass
        elif part == 2:
            print("Doing part 2")
            self.make_it_part_2()
        else:
            print("ERROR: unknown part {}, should be 1 or 2".format(part))
            sys.exit(-1)

        self.STEP_COSTS = {'A': 1, 'B': 10, 'C': 100, 'D': 1000}
        self.ROOM_TO_HALLWAY = {0: 2, 1: 4, 2: 6, 3: 8}
        self.TARGET_ROOMS = {'A': 0, 'B': 1, 'C': 2, 'D': 3}
        self.energy_costs = []

        self.tried = []

    def make_it_part_2(self):
        addins = [ ['D', 'D'], ['C','B'], ['B','A'], ['A','C']]
        new_rooms = []
        for room in range(len(self.starting_layout.rooms)):
            new = [self.starting_layout.rooms[room][0]] + addins[room] + [self.starting_layout.rooms[room][1]]
            new_rooms.append(new)
        self.starting_layout.rooms = new_rooms

    def parse_file(self):
        rooms = [[] for _ in range(4)]
        hallway = ['.' for _ in range(11)]
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0][3:]
                if line[0] == '#' or line[0] == '.':
                    continue
                for i in range(0, 4):
                    rooms[i].append(line[0])
                    line = line[2:]
        self.starting_layout = Layout(rooms, hallway)

    def print_layout(self, layout, force=False):
        debug_save = self.debug
        if force:
            self.debug = True
        self.debugPrint(13*'#')
        self.debugPrint('#' + ''.join(layout.hallway) + '#')
        self.debugPrint('###{}#{}#{}#{}###'.format(
            layout.rooms[0][0], layout.rooms[1][0], layout.rooms[2][0], layout.rooms[3][0]
        ))
        for i in range(1, len(layout.rooms[0])):
            self.debugPrint('  #{}#{}#{}#{}#'.format(
                layout.rooms[0][i], layout.rooms[1][i], layout.rooms[2][i], layout.rooms[3][i]
            ))
        self.debugPrint('  #########')
        self.debug = debug_save

    def everyones_home(self, layout):
        for room, who in enumerate(['A', 'B', 'C', 'D']):
            for i in range(len(layout.rooms[room])):
                if layout.rooms[room][i] != who:
                    return False
        return True

    def can_reach_spot_from_room(self, room, roomplace, spot, layout):
        # if roomplace == 1 and layout.rooms[room][0] != '.':
        #     return False
        start = self.ROOM_TO_HALLWAY[room]
        for i in range(min(start, spot), max(start, spot)+1):
            if layout.hallway[i] != '.':
                return False
        return True

    def can_reach_room_from_hallway(self, room, spot, layout, who):
        if self.TARGET_ROOMS[who] != room:
            return False
        if layout.rooms[room][0] != '.':
            return False
        for i in range(1, len(layout.rooms[room])):
            if layout.rooms[room][i] != '.' and layout.rooms[room][i] != who:
                return False
        stop = self.ROOM_TO_HALLWAY[room]
        for i in range(min(spot, stop), max(spot, stop)+1):
            if i == spot:
                continue
            if layout.hallway[i] != '.':
                return False
        return True

    def can_reach_hallway_from_hallway(self, start, end, layout):
        for i in range(min(start, end), max(start, end)+1):
            if i == start:
                continue
            if layout.hallway[i] != '.':
                return False
        return True

    def initial_shuffle(self, new_layout, new_cost):
        # Anyone who can move directly through the hall into their target room does so
        # Anyone in their target room moves down if possible
        # Anyone not in their target moves up if possible
        # Keep doing it until no moves are made
        moves = -1
        while moves != 0:
            moves = 0
            for room in range(len(new_layout.rooms)):
                # Into room
                who = new_layout.rooms[room][0]
                if who != '.':
                    if self.can_reach_spot_from_room(room, 0, self.ROOM_TO_HALLWAY[self.TARGET_ROOMS[who]], new_layout):
                        if self.can_reach_room_from_hallway(self.TARGET_ROOMS[who],
                                                            self.ROOM_TO_HALLWAY[self.TARGET_ROOMS[who]], new_layout, who):
                            new_layout.rooms[self.TARGET_ROOMS[who]][0] = who
                            new_layout.rooms[room][0] = '.'
                            new_cost += self.STEP_COSTS[who] * (2 + abs(self.ROOM_TO_HALLWAY[room] - self.ROOM_TO_HALLWAY[
                                self.TARGET_ROOMS[who]]))  # Step into hall, across, into room
                            moves += 1

                # Down
                for i in range(len(new_layout.rooms[room])-2, -1, -1):
                    w = new_layout.rooms[room][i]
                    if w == '.':
                        continue
                    can_move_down = True
                    for t in ['A', 'B', 'C', 'D']:
                        if t == w:
                            continue
                        if t in new_layout.rooms[room][i+1:]:
                            can_move_down = False
                    if can_move_down:
                        if self.TARGET_ROOMS[w] == room:
                            if new_layout.rooms[room][i+1] == '.':
                                new_layout.rooms[room][i+1] = w
                                new_layout.rooms[room][i] = '.'
                                new_cost += self.STEP_COSTS[w]
                                moves += 1

                # for i in range(len(new_layout.rooms[room])-1, -1, -1):
                #     w = new_layout.rooms[room][i]
                #     if w == '.':
                #         continue
                #     if self.TARGET_ROOMS[w] == room:
                #         j = i + 1
                #         while new_layout.rooms[room][j] == '.' and j < len(new_layout.rooms[room])-2 and new_layout.rooms[room][j+1] == '.':
                #             j += 1
                #         if new_layout.rooms[room][j] == '.':
                #             new_layout.rooms[room][j] = w
                #             new_layout.rooms[room][i] = '.'
                #             new_cost += abs(j-i) * self.STEP_COSTS[w]

                # Up
                for i in range(1, len(new_layout.rooms[room])):
                    w = new_layout.rooms[room][i]
                    if w == '.':
                        continue
                    move_down = False
                    for t in ['A', 'B', 'C', 'D']:
                        if t == w:
                            continue
                        if t in new_layout.rooms[room][i:]:
                            move_down = True
                    if self.TARGET_ROOMS[w] != room or move_down:
                        if new_layout.rooms[room][i-1] == '.':
                            new_layout.rooms[room][i-1] = w
                            new_layout.rooms[room][i] = '.'
                            new_cost += self.STEP_COSTS[w]
                            moves += 1

                # for i in range(1, len(new_layout.rooms[room])):
                #     w = new_layout.rooms[room][i]
                #     if w == '.':
                #         continue
                #     if self.TARGET_ROOMS[w] != room:
                #         j = i - 1
                #         while new_layout.rooms[room][j] == '.' and j > 1 and new_layout.rooms[room][j-1] == '.':
                #             j -= 1
                #         if new_layout.rooms[room][j] == '.':
                #             new_layout.rooms[room][j] = w
                #             new_layout.rooms[room][i] = '.'
                #             new_cost += abs(j-1) * self.STEP_COSTS[w]

            # Anyone in the hallway moves into their room (top space) if possible
            for spot in range(len(new_layout.hallway)):
                who = new_layout.hallway[spot]
                if who == '.':
                    continue
                target_room = self.TARGET_ROOMS[who]
                if self.can_reach_room_from_hallway(target_room, spot, new_layout, new_layout.hallway[spot]):
                    new_layout.rooms[target_room][0] = who
                    new_layout.hallway[spot] = '.'
                    new_cost += self.STEP_COSTS[who] * (
                                1 + abs(self.ROOM_TO_HALLWAY[target_room] - spot))  # step across hall, step into room
                    moves += 1
        return new_layout, new_cost

    def get_energy_cost(self, layout, cost):
        if self.everyones_home(layout):
            print("Everyone's home! Total cost {}".format(cost))
            return cost

        self.debugPrint("Take a look at:")
        self.print_layout(layout)
        self.debugPrint("---")

        new_layout = copy.deepcopy(layout)
        new_cost = copy.deepcopy(cost)

        new_layout, new_cost = self.initial_shuffle(new_layout, new_cost)

        # Do the initial shuffling a second time
        #new_layout, new_cost = self.initial_shuffle(new_layout, new_cost)
        #new_layout, new_cost = self.initial_shuffle(new_layout, new_cost)
        #new_layout, new_cost = self.initial_shuffle(new_layout, new_cost)

        self.debugPrint("After initial shuffling: {}".format(new_cost))
        self.print_layout(new_layout)

        if self.everyones_home(new_layout):
            print("Everyone's home! Total cost {}".format(new_cost))
            return new_cost

        # Compute the costs for moving each top place that's not in its target room to any place in the hallway
        costs = []
        for room in range(len(new_layout.rooms)):
            who = new_layout.rooms[room][0]
            if who == '.':
                continue
            if self.TARGET_ROOMS[who] == room:
                if new_layout.rooms[room][1] == '.' or new_layout.rooms[room][1] == who:
                    continue

            for spot in range(len(new_layout.hallway)):
                if spot in self.ROOM_TO_HALLWAY.values():
                    continue
                if self.can_reach_spot_from_room(room, 0, spot, new_layout):
                    new_new_layout = copy.deepcopy(new_layout)
                    new_new_cost = copy.deepcopy(new_cost)
                    new_new_layout.hallway[spot] = who
                    new_new_layout.rooms[room][0] = '.'
                    new_new_cost += self.STEP_COSTS[who] * (1 + abs(self.ROOM_TO_HALLWAY[room] - spot)) # step into hall, step to spot
                    self.debugPrint("recurse with:")
                    self.print_layout(new_new_layout)
                    costs.append(self.get_energy_cost(new_new_layout, new_new_cost))
        if len(costs) == 0:
            self.debugPrint("WARNING: Did not find any solutions")
            return 99999
        return min(costs)

    def find_all_moves_return_least_cost(self):
        return self.get_energy_cost(self.starting_layout, 0)


if __name__ == '__main__':
    parser = getBasicArgparser('day 23')
    parser.add_argument('-p', type=int, help='Which part to run', required=True)

    args = parser.parse_args()

    day23 = Day23(args.i, args.v, args.p)

    print("Least cost to sort everyone: {}".format(day23.find_all_moves_return_least_cost()))
