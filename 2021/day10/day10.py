#!/usr/bin/env python3

import csv
import sys
from enum import Enum
import statistics
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Status(Enum):
    GOOD = 0,
    CORRUPTED = 1,
    INCOMPLETE = 2


class Day10(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.lines = []
        self.parse_file()

        self.opening_brackets = {
            ')': '(',
            ']': '[',
            '}': '{',
            '>': '<'
        }
        self.closing_brackets = {v: k for k, v in self.opening_brackets.items()}
        self.corrupted_scores = {
            ')': 3,
            ']': 57,
            '}': 1197,
            '>': 25137
        }

        self.incomplete_scores = {
            ')': 1,
            ']': 2,
            '}': 3,
            '>': 4
        }

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.lines.append(line)

    def process_line(self, line):
        stack = []
        self.debugPrint("Check line {}".format(line))
        for c in line:
            if c in self.opening_brackets.values():
                stack.append(c)
            elif c in self.opening_brackets.keys():
                if stack[-1] != self.opening_brackets[c]:
                    self.debugPrint("  Corrupted")
                    return Status.CORRUPTED, c
                stack.pop()
        if len(stack) != 0:
            self.debugPrint("  Incomplete")
            return Status.INCOMPLETE, self.get_completion(stack)
        return Status.GOOD, ''

    def get_completion(self, stack):
        missing_chars = ''
        while len(stack) != 0:
            missing_chars += self.closing_brackets[stack.pop()]
        self.debugPrint("    Need to add {}".format(missing_chars))
        return missing_chars

    def get_scores(self):
        corrupted_score = 0
        incomplete_score = []
        for line in self.lines:
            status, result = self.process_line(line)
            if status == Status.CORRUPTED:
                corrupted_score += self.corrupted_scores[result]
            elif status == Status.INCOMPLETE:
                incomplete_score.append(self.score_completion(result))
        return corrupted_score, statistics.median(incomplete_score)

    def score_completion(self, completion):
        total_score = 0
        for c in completion:
            total_score = total_score * 5 + self.incomplete_scores[c]
        self.debugPrint("Calculate score for {}: {}".format(completion, total_score))
        return total_score


if __name__ == '__main__':
    parser = getBasicArgparser('day 10')

    args = parser.parse_args()

    day10 = Day10(args.i, args.v)

    corrupted, incomplete= day10.get_scores()
    print("Score of corrupted lines: {}".format(corrupted))
    print("Score of incomplete lines: {}".format(incomplete))
