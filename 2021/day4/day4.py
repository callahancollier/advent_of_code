#!/usr/bin/env python3

import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day4(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.WIN_OFFSET = 100
        self.draws = []
        self.cards = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            linenumber = 0
            for line in reader:
                if linenumber == 0:
                    self.draws = [int(x) for x in line[0].split(',')]
                elif not line:
                    self.cards.append([])
                else:
                    self.cards[len(self.cards)-1].append([int(x) for x in line[0].split(' ') if x])
                linenumber += 1
        self.print_cards()

    def print_cards(self):
        self.debugPrint("Cards:")
        for card in self.cards:
            self.debugPrint("")
            for row in card:
                self.debugPrint(row)

    def is_winner(self, card):
        array = numpy.array(card)
        for r in range(0, len(array)):
            row = array[r, :]
            if numpy.alltrue(row >= self.WIN_OFFSET):
                return True
        for c in range(0, len(array[0])):
            col = array[:, c]
            if numpy.alltrue(col >= self.WIN_OFFSET):
                return True
        return False

    def calculate_score(self, card, winning_draw):
        score = 0
        for row in card:
            for num in row:
                if num < self.WIN_OFFSET:
                    score += num
        print("score is {} * {}".format(winning_draw, score))
        return score * winning_draw

    def bingo(self):
        winners = []
        winning_cards = []
        for draw in self.draws:
            self.debugPrint("--------------\nDRAW: {}".format(draw))
            for c, card in enumerate(self.cards):
                if c in winning_cards:
                    continue
                for r, row in enumerate(card):
                    for n, num in enumerate(row):
                        if num == draw:
                            self.debugPrint("Card {} has {} at row {} col {}".format(c, draw, r, n))
                            self.cards[c][r][n] = self.WIN_OFFSET + num
                if self.is_winner(card):
                    print("We have a winner! Card {} from draw {}".format(c, draw))
                    winning_cards.append(c)
                    winners.append([draw, c])
            if len(winners) == len(self.cards):
                break
            self.print_cards()
        return winners

    def calculate_first_winner_score(self, winners):
        return self.calculate_score(self.cards[winners[0][1]], winners[0][0])

    def calculate_last_winner_score(self, winners):
        return self.calculate_score(self.cards[winners[len(winners)-1][1]], winners[len(winners)-1][0])


if __name__ == '__main__':
    parser = getBasicArgparser('day 4')

    args = parser.parse_args()

    day4 = Day4(args.i, args.v)

    winners = day4.bingo()

    print("Score of first winning card: {}".format(day4.calculate_first_winner_score(winners)))
    print("Score of last winning card: {}".format(day4.calculate_last_winner_score(winners)))

