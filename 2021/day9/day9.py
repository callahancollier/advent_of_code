#!/usr/bin/env python3

import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day9(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.heightmap = None
        self.low_points = []
        self.y_max = 0
        self.x_max = 0
        self.parse_file()

    def parse_file(self):
        height_map = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                height_map.append([int(x) for x in line])
        self.heightmap = numpy.array(height_map)
        self.y_max = len(self.heightmap)
        self.x_max = len(self.heightmap[0])
        self.debugPrint(self.heightmap)

    def is_low_point(self, x, y):
        val = self.heightmap[y][x]
        self.debugPrint("Consider point {},{}:\n{}".format(x, y,
            self.heightmap[max(0,y-1):min(y+2,self.y_max+1),max(0,x-1):min(x+2,self.x_max+1)]))
        # self.debugPrint("[{}:{},{}:{}]".format(
        #     max(0, y - 1),min(y + 2, self.y_max + 1), max(0, x - 1),min(x + 2, self.x_max + 1)
        # ))
        if x > 0 and self.heightmap[y][x-1] <= val:
            return False
        if x < self.x_max-1 and self.heightmap[y][x+1] <= val:
            return False
        if y > 0 and self.heightmap[y-1][x] <= val:
            return False
        if y < self.y_max-1 and self.heightmap[y+1][x] <= val:
            return False
        self.debugPrint("---> {} ({},{}) is a low point".format(val, x, y))
        return True

    def get_risk_level(self):
        risk_levels = []
        for x in range(0, self.x_max):
            for y in range(0, self.y_max):
                if self.is_low_point(x, y):
                    risk_levels.append(self.heightmap[y][x] + 1)
                    self.low_points.append((x, y))
        return sum(risk_levels)

    def get_basins(self):
        basins = []
        if len(self.low_points) == 0:
            save_debug = self.debug
            self.debug = False
            self.get_risk_level()
            self.debug = save_debug

        for low_point_x, low_point_y in self.low_points:
            self.debugPrint("Size basin at {},{}".format(low_point_x, low_point_y))
            basin = self.get_basin(low_point_x, low_point_y, set())
            basins.append(len(basin))
            self.debugPrint("Basin at {},{} has size {}".format(low_point_x, low_point_y, len(basin)))
        return basins

    def get_basin(self, x, y, positions_checked):
        if x < 0 or y < 0 or x >= self.x_max or y >= self.y_max:
            return positions_checked

        if (x,y) in positions_checked:
            return positions_checked

        self.debugPrint("Check {},{}".format(x, y))
        if self.heightmap[y][x] == 9:
            self.debugPrint("9")
            return positions_checked

        positions_checked.add((x, y))
        # Right
        [positions_checked.add(p) for p in self.get_basin(x+1, y, positions_checked)]
        # Left
        [positions_checked.add(p) for p in self.get_basin(x-1, y, positions_checked)]
        # Up
        [positions_checked.add(p) for p in self.get_basin(x, y+1, positions_checked)]
        # Down
        [positions_checked.add(p) for p in self.get_basin(x, y-1, positions_checked)]

        return positions_checked

    def multiply_three_largest_basin_sizes(self):
        basins = sorted(self.get_basins())
        return basins[-1] * basins[-2] * basins[-3]


if __name__ == '__main__':
    parser = getBasicArgparser('day 9')
    parser.add_argument('-p', type=int, help='Which part to run', default=0)

    args = parser.parse_args()

    day9 = Day9(args.i, args.v)

    if args.p == 1 or args.p == 0:
        print("Risk level: {}".format(day9.get_risk_level()))

    if args.p == 2 or args.p == 0:
        print("Sizes of three largest basins, multiplied: {}".format(day9.multiply_three_largest_basin_sizes()))