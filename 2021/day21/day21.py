#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day21(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.positions = []
        self.WIN_SCORE = None
        self.parse_file()

    def parse_file(self):
        psn = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                psn = int(line.split(' ')[1])
                self.positions.append(int(line.split(':')[1]))
        self.debugPrint("Starting positions: {}".format(self.positions))

    def move_on_board(self, fromPosition, num):
        return (fromPosition + num - 1) % 10 + 1


class Day21Part1(Day21):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.scores = len(self.positions) * [0]
        self.WIN_SCORE = 1000

    def get_winner(self):
        if self.scores[0] >= self.WIN_SCORE:
            return 0
        elif self.scores[1] >= self.WIN_SCORE:
            return 1

    def someone_has_won(self):
        if self.scores[0] >= self.WIN_SCORE or self.scores[1] >= self.WIN_SCORE:
            return True
        return False

    def run_deterministic_dice_game(self):
        total_die_rolls = 0
        die = 0
        while not self.someone_has_won():
            for player in [0, 1]:
                for roll in range(0, 3):
                    total_die_rolls += 1
                    die = (die % 100) + 1
                    self.positions[player] = self.move_on_board(self.positions[player], die)
                self.scores[player] += self.positions[player]
                if self.scores[player] == self.WIN_SCORE:
                    break
            self.debugPrint("Die at {}, player 1 score {} posn {} player 2 score {} posn {}".format(
                die, self.scores[0], self.positions[0], self.scores[1], self.positions[1]
            ))
        winner = self.get_winner()
        loser = (winner + 1) % 2
        self.debugPrint("Player {} wins!".format(winner+1))
        self.debugPrint("Loser's score is {}, die has been rolled {} times".format(self.scores[loser], total_die_rolls))
        return self.scores[loser] * total_die_rolls


class Day21Part2(Day21):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.WIN_SCORE = 21

    def is_winner(self, board):
        for spot in board:
            for player in spot:
                if player >= self.WIN_SCORE:
                    return True
        return False

    def run_dirac_dice_game(self):
        player = 1
        boards = []
        boards.append([{} for i in range(10)])
        boards.append([{} for i in range(10)])
        boards[0][self.positions[0]-1][0] = 1 # Player 1's board has one pawn with score 0 at the starting position
        boards[1][self.positions[1]-1][0] = 1 # Player 2's board has one pawn with score 0 at the starting position
        active_universes = 1
        winning_universes = [0, 0]
        while active_universes != 0:
        #for i in range(2):
            player = (player + 1) % 2
            board = boards[player]
            new_active_universes = 0
            self.debugPrint("PLAYER {}:\n{}\n***".format(player, board))
            # for universe in range(active_universes):
            next_board_state = [{} for i in range(10)]
            for diceroll1 in [1, 2, 3]:
                for diceroll2 in [1, 2, 3]:
                    for diceroll3 in [1, 2, 3]:
                        moves = diceroll1 + diceroll2 + diceroll3
                        for position, current_scores in enumerate(board):
                            for current_score in current_scores.keys():
                                next_position = self.move_on_board(position+1, moves)
                                next_score = current_score+next_position

                                if next_score not in next_board_state[next_position-1].keys():
                                    next_board_state[next_position-1][next_score] = 0
                                next_board_state[next_position-1][next_score] += board[position][current_score]
            self.debugPrint("{}".format(next_board_state))
            winners, next_board_state = self.count_out_winners(next_board_state)
            new_active_universes = self.count_universes(next_board_state)
            self.debugPrint("{}: {} winners, remaining universes ({} total):\n{}\n*****".format(active_universes, winners, new_active_universes, next_board_state))
            winning_universes[player] += active_universes*winners
            boards[player] = next_board_state
            self.debugPrint("Player1: {}, player2: {}".format(winning_universes[0], winning_universes[1]))

            active_universes = new_active_universes
        print("Player 1 wins {} universes, player 2 wins {}".format(winning_universes[0], winning_universes[1]))
        return max(winning_universes)

    def count_universes(self, board):
        total = 0
        for position in board:
            for score in position.keys():
                total += position[score]
        return total

    def count_out_winners(self, board):
        winners = 0
        for position in board:
            winning_scores = [x for x in position.keys() if x >= self.WIN_SCORE]
            for winning_score in winning_scores:
                winners += position.pop(winning_score, 0)
        return winners, board


if __name__ == '__main__':
    parser = getBasicArgparser('day 21')
    parser.add_argument('-p', type=int, help='Which part to run', required=True)

    args = parser.parse_args()

    if args.p == 1:
        day21p1 = Day21Part1(args.i, args.v)
        print("Part 1 result: {}".format(day21p1.run_deterministic_dice_game()))
    elif args.p == 2:
        day21p2 = Day21Part2(args.i, args.v)
        print("Part 2 result: {}".format(day21p2.run_dirac_dice_game()))
    else:
        print("Must run either part 1 or 2")
