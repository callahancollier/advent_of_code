#!/usr/bin/env python3

import csv
import sys
import math
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day17(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.target_x = []
        self.target_y = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                line = line.replace('target area: x=', '')
                self.target_x = [int(x) for x in line.split(',')[0].split('..')]

                line = line.split('=')[1]
                self.target_y = [int(x) for x in line.split('..')]

    def simulate(self, x_vel, y_vel):
        x = 0
        y = 0

        max_y = 0

        while x <= max(self.target_x) and y >= min(self.target_y):
            x += x_vel
            y += y_vel

            self.debugPrint("  x,y at {},{}".format(x, y))

            max_y = max(max_y, y)

            if min(self.target_x) <= x <= max(self.target_x):
                if min(self.target_y) <= y <= max(self.target_y):
                    self.debugPrint("({},{}):T Target area: x from {} to {}, y from {} to {}".format(
                        x_vel, y_vel, self.target_x[0], self.target_x[1], self.target_y[0], self.target_y[1]))
                    return True, max_y

            if x_vel < 0:
                x_vel += 1
            elif x_vel > 0:
                x_vel -= 1
            y_vel -= 1

        self.debugPrint("({},{}):F Target area: x from {} to {}, y from {} to {}".format(
            x_vel, y_vel, self.target_x[0], self.target_x[1], self.target_y[0], self.target_y[1]))
        return False, max_y

    def hit_target_with_maximum_arc(self):
        max_y = 0
        velocities = []
        for x in range(-1, 4*max(self.target_x)):
            for y in range(-4 * max(self.target_x), 4 * max(self.target_x)):
                res, arc = self.simulate(x, y)
                if res:
                    max_y = max(max_y, arc)
                    velocities.append((x, y))
        for vel in velocities:
            self.debugPrint("{}, {}".format(vel[0], vel[1]))
        return max_y, len(velocities)


if __name__ == '__main__':
    parser = getBasicArgparser('day 17')
    parser.add_argument('-s', type=str, help='Simulate trajectory for the x,y', default='')

    args = parser.parse_args()

    day17 = Day17(args.i, args.v)

    if len(args.s) != 0:
        sim_x, sim_y = [int(x) for x in args.s.split(',')]

        result, arc_height = day17.simulate(sim_x, sim_y)
        if result:
            string = "does"
        else:
            string = "does NOT"
        print("Trajectory x={}, y={} {} fall in the target area. Arc got to {}".format(
            sim_x, sim_y, string, arc_height))
    else:
        arc, vel = day17.hit_target_with_maximum_arc()
        print("Best arc: {}, number of velocities: {}".format(arc, vel))
