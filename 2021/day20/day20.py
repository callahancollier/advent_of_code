#!/usr/bin/env python3

import csv
import sys
import numpy
import copy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day20(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.image = None
        self.algorithm = ''
        self.background = '.'
        self.parse_file()

    def parse_file(self):
        image = []
        with open(self.input_filename) as csvfile:
            reading_algorithm = True
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if reading_algorithm:
                    self.algorithm = line
                    reading_algorithm = False
                else:
                    image.append([x for x in line])
        self.image = numpy.array(image)
        if self.algorithm[0] == self.algorithm[-1]:
            print("ERROR: you have made bad assumptions about your input: 0->{}, end->{}".format(self.algorithm[0], self.algorithm[-1]))
            sys.exit(-1)

    def pixel_to_binary_str(self, pixel):
        if pixel == '.':
            return '0'
        elif pixel == '#':
            return '1'
        else:
            print("ERROR unknown image pixel {}".format(pixel))
            sys.exit(-1)

    def enhance_multiple(self, times):
        for i in range(0, times):
            result = self.enhance()
        return result

    def get_new_column(self, fill):
        return numpy.full((len(self.image), 1), fill)

    def get_new_row(self, fill):
        return numpy.full((1, len(self.image[0])), fill)

    def enhance(self):
        self.debugPrint("----------\nStarting image:\n{}\n----------".format(self.image))
        # self.debugPrint("{}".format(self.image[:,0:2]))
        while not numpy.all(self.image[:, 0:2] == self.background):
            self.image = numpy.c_[self.get_new_column(self.background), self.image]
        # self.debugPrint("{}".format(self.image[:,-1]))
        while not numpy.all(self.image[:, -2:] == self.background):
            self.image = numpy.c_[self.image, self.get_new_column(self.background)]
        # self.debugPrint("{}".format(self.image[0,:]))
        while not numpy.all(self.image[0:2, :] == self.background):
            self.image = numpy.r_[self.get_new_row(self.background), self.image]
        # self.debugPrint("{}".format(self.image[-1,:]))
        while not numpy.all(self.image[-2:, :] == self.background):
            self.image = numpy.r_[self.image, self.get_new_row(self.background)]
        self.debugPrint("----------\nAfter padding:\n{}\n----------".format(self.image))

        newimage = copy.deepcopy(self.image)

        for r in range(1, len(self.image[0])-1):
            for c in range(1, len(self.image)-1):
                consider = self.image[c-1:c+2, r-1:r+2]
                self.debugPrint("Consider pixel {},{}:\n{}".format(r,c,consider))
                value = int(''.join([self.pixel_to_binary_str(x) for x in consider.flatten().tolist()]), 2)
                self.debugPrint("Is value {}".format(value))
                newimage[c, r] = self.algorithm[value]
        if self.image[0,0] == '.':
            self.background = self.algorithm[0]
        else:
            self.background = self.algorithm[-1]
        self.image = newimage[1:-1, 1:-1]
        self.debugPrint("----------\nEnhanced image:\n{}\n----------".format(self.image))
        lit_pixels = numpy.count_nonzero(self.image == '#')
        return lit_pixels


if __name__ == '__main__':
    parser = getBasicArgparser('day 20')
    parser.add_argument('-p', type=int, help='Which part to run', required=True)

    args = parser.parse_args()

    day20 = Day20(args.i, args.v)

    if args.p == 1:
        num = 2
    elif args.p != 2:
        print("ERROR: Must run either part 1 or part 2")
    else:
        num = 50

    print("After {} enhancements, there are {} lit pixels".format(num, day20.enhance_multiple(num)))