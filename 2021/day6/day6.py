#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day6(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.FRESH_FISH = 8
        self.RESET_FISH = 6
        self.fishes = {}
        self.parse_file()

    def parse_file(self):
        for k in range(0, self.FRESH_FISH+1):
            self.fishes[k] = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                for fish in line.split(','):
                    fish = int(fish)
                    if fish not in self.fishes:
                        print("ERROR: unknown fish index {}".format(fish))
                    self.fishes[fish] += 1

    def increment_days(self, num_days):
        self.debugPrint("Initial state: {}".format(self.fishes))
        for day in range(0, num_days):
            carry_down = 0
            for n in range(self.FRESH_FISH, -1, -1):
                self.fishes[n], carry_down = carry_down, self.fishes[n]
            self.fishes[self.RESET_FISH] += carry_down
            self.fishes[self.FRESH_FISH] = carry_down
            self.debugPrint("After {} days: {}".format(day+1, self.fishes))
        total_fish = 0
        for num_fish in self.fishes.values():
            total_fish += num_fish
        return total_fish


if __name__ == '__main__':
    parser = getBasicArgparser('day 6')
    parser.add_argument('-d', type=int, help='number of days', required=True)

    args = parser.parse_args()

    day6 = Day6(args.i, args.v)

    print("After {} days there are {} fish".format(args.d, day6.increment_days(args.d)))
