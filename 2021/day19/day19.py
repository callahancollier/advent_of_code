#!/usr/bin/env python3

import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Scanner:
    def __init__(self, scanner_num):
        self.num = scanner_num
        self.scan_data = []
        self.scanner_location = (-1, -1, -1)

    def get_beacons_wrt(self, wrt):
        x, y, z = wrt
        beacons = []
        for (bx, by, bz) in self.scan_data:
            beacons.append((bx - x, by - y, bz - z))
        return beacons

    def rotate_beacon_90_degrees(self, axis):
        if self.scanner_location != (-1, -1, -1):
            print("ERROR: rotation is locked")
            return
        for i, data in enumerate(self.scan_data):
            x, y, z = data
            if axis == 'z':
                # rotate 90 about z, x becomes y, y becomes -x, z=z
                self.scan_data[i] = (-1*y, x, z)
            elif axis == 'x':
                # rotate 90 about x, y becomes z, z becomes -y, x=x
                self.scan_data[i] = (x, -1*z, y)
            elif axis == 'y':
                # rotate 90 about y, x becomes z, z becomes -x, y=y
                self.scan_data[i] = (-1*z, y, x)
            else:
                print("ERROR: unknown axis {}".format(axis))


class Day19(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.scanners = []
        self.beacons = set()
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            scanner = -1
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if line[0:3] == '---':
                    line = line.replace('--- scanner ', '')
                    line = line.replace(' ---', '')
                    scanner = int(line)
                    if scanner != len(self.scanners):
                        print("ERROR parsing input")
                        sys.exit(1)
                    self.scanners.append(Scanner(scanner))
                else:
                    x, y, z = [int(i) for i in line.split(',')]
                    self.scanners[scanner].scan_data.append((x, y, z))
        self.scanners[0].scanner_location = (0, 0, 0)

    def all_aligned(self):
        for scanner in self.scanners:
            if scanner.scanner_location == (-1, -1, -1):
                return False
        return True

    def rotate_axes(self):
        next = 0
        while True:
            next = self.align_scanners(self.scanners[next])
            if self.all_aligned():
                break
        for scanner in self.scanners:
            print("Scanner {} location: ({},{},{})".format(
                scanner.num,
                scanner.scanner_location[0],
                scanner.scanner_location[1],
                scanner.scanner_location[2]
            ))

    def align_scanners(self, wrt):
        if wrt.scanner_location != (-1, -1, -1):
            for beacon_ref in wrt.scan_data:
                self.debugPrint("Beacons for reference: {}".format(wrt.scan_data))
                beaconsA = wrt.get_beacons_wrt(beacon_ref)
                for scanner in self.scanners:
                    if scanner.scanner_location != (-1, -1, -1):
                        continue
                    if scanner.num == wrt.num:
                        continue
                    self.debugPrint("--- Scanner {} ---".format(scanner.num))
                    # x x x z z
                    for rotation in range(0, 3):
                        for axis in ['y', 'y', 'y', 'y', 'z', 'y', 'y', 'y', 'y', 'x']:
                            scanner.rotate_beacon_90_degrees(axis)
                            for b in scanner.scan_data:
                                beaconsB = scanner.get_beacons_wrt(b)
                                common = set(beaconsA).intersection(beaconsB)
                                if len(common) >= 12:
                                    # We have enough info to figure out the position of the sensor
                                    # b is the same beacon as the reference beacon
                                    bx, by, bz = b
                                    rx, ry, rz = beacon_ref
                                    wx, wy, wz = wrt.scanner_location
                                    self.debugPrint("Beacon {} corresponds to {}".format(
                                        b, beacon_ref
                                    ))

                                    scanner.scanner_location = (rx-bx+wx, ry-by+wy, rz-bz+wz)
                                    self.debugPrint("Scanner {} is at position ({},{},{})".format(scanner.num,
                                                                                              scanner.scanner_location[0],
                                                                                              scanner.scanner_location[1],
                                                                                              scanner.scanner_location[2]))
                                    return scanner.num
        print("ERROR: unable to determine any scanner locations")
        if wrt.num == 0:
            return len(self.scanners)-1
            #sys.exit(1)
        return wrt.num - 1

    def count_beacons(self):
        beacons = set()
        for scanner in self.scanners:
            sx, sy, sz = scanner.scanner_location
            for bcn in scanner.get_beacons_wrt((-sx, -sy, -sz)):
                beacons.add(bcn)
        return beacons

    def get_largest_manhattan_distance(self):
        largest = 0
        index_a = -1
        index_b = -1
        for scanner1 in self.scanners:
            for scanner2 in self.scanners:
                i = scanner1.num
                j = scanner2.num
                if i == j:
                    continue
                a, b, c = scanner1.scanner_location
                d, e, f = scanner2.scanner_location
                manhattan = abs(d - a) + abs(e - b) + abs(f - c)
                print("Manhattan of {} ({},{},{}) and {} ({},{},{}): {}".format(
                    i, a, b, c, j, d, e, f, manhattan))
                if manhattan > largest:
                    largest = manhattan
                    index_a = i
                    index_b = j
        print("Scanners {} and {} are farthest apart".format(index_a, index_b))
        return largest


if __name__ == '__main__':
    parser = getBasicArgparser('day 19')

    args = parser.parse_args()

    day19 = Day19(args.i, args.v)

    day19.rotate_axes()
    beacons = day19.count_beacons()
    print("Number of beacons: {}".format(len(beacons)))

    print("Largest Manhattan distance: {}".format(day19.get_largest_manhattan_distance()))
