#!/usr/bin/env python3
import copy
import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day25(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.cucumbers = None
        self.parse_file()

    def parse_file(self):
        cucumbers = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                cucumbers.append([x for x in line])
        self.cucumbers = numpy.array(cucumbers)

    def do_step(self):
        return self.move_east() + self.move_south()

    def move_east(self):
        moved = 0
        for r in range(len(self.cucumbers)):
            new_row = copy.deepcopy(self.cucumbers[r,:])
            self.debugPrint("Move east, row {}".format(new_row))
            for i in range(len(new_row)):
                if self.cucumbers[r, i] == '>' and self.cucumbers[r, (i+1)%len(new_row)] == '.':
                    new_row[(i+1)%len(new_row)] = '>'
                    new_row[i] = '.'
                    moved += 1
            self.cucumbers[r,:] = new_row
            self.debugPrint(" result {}".format(self.cucumbers[r,:]))
        return moved

    def move_south(self):
        moved = 0
        for c in range(len(self.cucumbers[0])):
            new_col = copy.deepcopy(self.cucumbers[:,c])
            self.debugPrint("Move south, row {}".format(new_col))
            for i in range(len(new_col)):
                if self.cucumbers[i, c] == 'v' and self.cucumbers[(i+1)%len(new_col), c] == '.':
                    new_col[(i+1)%len(new_col)] = 'v'
                    new_col[i] = '.'
                    moved += 1
            self.cucumbers[:,c] = new_col
            self.debugPrint(" result {}".format(self.cucumbers[:, c]))
        return moved

    def get_steps_til_stop_moving(self):
        moved = -1
        steps = 0
        self.debugPrint("Initial state:\n{}\n---".format(self.cucumbers))
        while moved != 0:
            moved = self.do_step()
            steps += 1
            self.debugPrint("After step {}:\n{}\n---".format(steps, self.cucumbers))
        return steps


if __name__ == '__main__':
    parser = getBasicArgparser('day 25')

    args = parser.parse_args()

    day25 = Day25(args.i, args.v)

    print("Cucumbers stop moving after step {}".format(day25.get_steps_til_stop_moving()))