#!/usr/bin/env python3

import csv
import sys
import collections
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day14(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.polymer = ""
        self.rules = {}
        self.parse_file()

    def parse_file(self):
        got_template = False
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if not got_template:
                    self.polymer = line
                    got_template = True
                else:
                    line = line.split(' -> ')
                    if len(line[0]) != 2:
                        print("Error: expected two chars but got {}".format(line[0]))
                    self.rules[line[0]] = line[1]
            self.debugPrint("Rules: {}".format(self.rules))

    def do_steps(self, num_steps):
        #self.debugPrint("Start: {}".format(self.polymer))
        for i in range(0, num_steps):
            insert = {}
            for j in range(1, len(self.polymer)):
                pair = self.polymer[j-1:j+1]
                #self.debugPrint("Compare: {}".format(pair))
                if pair in self.rules:
                    #self.debugPrint("Replace {} with {} at {}".format(pair, self.rules[pair], j))
                    insert[j] = self.rules[pair]
            offset = 0
            for idx in sorted(insert.keys()):
                self.polymer = self.polymer[:idx+offset] + insert[idx] + self.polymer[idx+offset:]
                #self.debugPrint("Insert {} at {}: {}".format(insert[idx], idx+offset, self.polymer))
                offset += 1
            #self.debugPrint("After step {}: {}".format(i+1, self.polymer))

    def do_steps_smart(self, num_steps):
        counts = {}
        for n in self.polymer:
            if n not in counts:
                counts[n] = 0
            counts[n] += 1

        new_pairs = {}
        for j in range(1, len(self.polymer)):
            pair = self.polymer[j-1:j+1]
            if pair in self.rules:
                replacement = self.rules[pair]
                if replacement not in counts:
                    counts[replacement] = 0
                counts[replacement] += 1
                # For each possible pair, track how many there are?
                if pair[0]+replacement not in new_pairs:
                    new_pairs[pair[0]+replacement] = 0
                if replacement+pair[1] not in new_pairs:
                    new_pairs[replacement+pair[1]] = 0
                new_pairs[pair[0] + replacement] += 1
                new_pairs[replacement + pair[1]] += 1
        self.debugPrint(new_pairs)
        self.debugPrint(counts)
        self.debugPrint('---')
        for i in range(1, num_steps):
            newer_pairs = {}
            for pair in new_pairs.keys():
                if pair in self.rules:
                    replacement = self.rules[pair]
                    if replacement not in counts:
                        counts[replacement] = 0
                    counts[replacement] += new_pairs[pair]
                    if pair[0] + replacement not in newer_pairs:
                        newer_pairs[pair[0] + replacement] = 0
                    if replacement + pair[1] not in newer_pairs:
                        newer_pairs[replacement + pair[1]] = 0
                    newer_pairs[pair[0] + replacement] += new_pairs[pair]
                    newer_pairs[replacement + pair[1]] += new_pairs[pair]
                    self.debugPrint("  {} gets {} * {}, now {} {} and {} {}".format(
                        pair, replacement, new_pairs[pair],
                        pair[0]+replacement, newer_pairs[pair[0]+replacement],
                        replacement+pair[1], newer_pairs[replacement+pair[1]]))
            new_pairs = newer_pairs.copy()
            self.debugPrint(new_pairs)
            self.debugPrint(counts)
            self.debugPrint('---')
        return max(counts.values()) - min(counts.values())


    def get_polymer_value(self):
        self.debugPrint("Polymer length is {}".format(len(self.polymer)))
        counts = collections.Counter(self.polymer).most_common()
        most = counts[0][1]
        least = counts[-1][1]
        return most - least


if __name__ == '__main__':
    parser = getBasicArgparser('day 14')
    parser.add_argument('-n', type=int, help='Number of steps to run', default=1)

    args = parser.parse_args()

    day14 = Day14(args.i, args.v)

    #day14.do_steps(args.n)
    #print("After {} steps, result is {}".format(args.n, day14.get_polymer_value()))

    print("After {} steps, result is {}".format(args.n, day14.do_steps_smart(args.n)))
