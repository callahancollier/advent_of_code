#!/usr/bin/env python3
import copy
import csv
import sys
import math

sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Instruction:
    def __init__(self, op, lhs, rhs=0):
        self.op = op
        self.lhs = lhs
        self.rhs = rhs


class Day24(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.variables = {'w': 0, 'x': 0, 'y': 0, 'z': 0}
        self.instructions = []
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                self.instructions.append(line)

    def is_variable(self, thing):
        if thing.lstrip('-').isdigit():
            return False
        return True

    def perform_instructions(self, input_string):
        inputs = [int(x) for x in input_string]
        instructions = copy.deepcopy(self.instructions)
        for i, instruction in enumerate(instructions):
            self.debugPrint("Line {}: {}".format(i+1, instruction))
            op = instruction.pop(0)
            lhs = instruction.pop(0)
            if not self.is_variable(lhs):
                print("Malformed program! {}".format(lhs))
                sys.exit(1)
            if op == 'inp':
                # No RHS
                input_read = inputs.pop(0)
                self.variables[lhs] = input_read
                self.debugPrint(self.variables)
                continue

            if lhs not in self.variables.keys():
                print("Malformed program, line {}: unknown LHS variable {}".format(i, lhs))
                sys.exit(-1)
            rhs = instruction.pop(0)
            if self.is_variable(rhs):
                if rhs not in self.variables.keys():
                    print("Undefined variable {}".format(rhs))
                    sys.exit(1)
                rhs = self.variables[rhs]
            else:
                rhs = int(rhs)
            if op == 'add':
                self.variables[lhs] = self.variables[lhs] + rhs
            elif op == 'mul':
                self.variables[lhs] = self.variables[lhs] * rhs
            elif op == 'div':
                self.variables[lhs] = math.trunc(self.variables[lhs] / rhs)
            elif op == 'mod':
                self.variables[lhs] = self.variables[lhs] % rhs
            elif op == 'eql':
                if self.variables[lhs] == rhs:
                    self.variables[lhs] = 1
                else:
                    self.variables[lhs] = 0
            self.debugPrint(self.variables)
        if len(inputs) != 0:
            print("WARNING: unused input string: {}".format(inputs))
        self.debugPrint(self.variables)

    def run_example(self, input_string):
        self.perform_instructions(input_string)
        print("Final state:")
        print(self.variables)

    def run_MONAD(self, input_string):
        z = self.compute_all_digits(input_string)
        if z == 0:
            print("{} is valid!".format(input_string))
            return True
        return False

    def run_MONAD_instr(self, input_string):
        self.perform_instructions(input_string)
        #self.compute_all_digits(input_string)
        if self.variables['z'] == 0:
            print("{} is valid!".format(input_string))
            return True
        return False

    def find_largest_valid_model_number_the_hard_way(self):
        modelnum = 99999999999999
        result = self.run_MONAD(str(modelnum))
        while result == False:
            modelnum -= 1
            modelstring = str(modelnum)
            if '0' in modelstring:
                continue
            result = self.run_MONAD(modelstring)
        return modelnum

    def find_largest_valid_model_number_with_some_solved(self):
        remaining_unknowns = 99999999
        result = self.run_MONAD(self.build_modelstring(remaining_unknowns))
        runs = 0
        while result == False:
            remaining_unknowns -= 1
            if '0' in str(remaining_unknowns):
                continue
            result = self.run_MONAD(self.build_modelstring(remaining_unknowns))
            if runs % 100000 == 0:
                print("Test {}".format(self.build_modelstring(remaining_unknowns)))
            runs += 1
        return self.build_modelstring(remaining_unknowns)

    def build_modelstring(self, unknowns):
        # These were solved by hand
        unknowns_str = str(unknowns)
        knowns = {5: '9', 4: '8', 6: '9', 7: '2', 9: '9', 10: '4'}

        if len(unknowns_str) + len(knowns.keys()) != 14:
            print("Run out of modelstrings... {}".format(unknowns_str))

        modelstring = ""
        string_idx = 0
        for i in range(13, -1, -1):
            if i in knowns.keys():
                modelstring += knowns[i]
            else:
                modelstring += unknowns_str[string_idx]
                string_idx += 1
        return modelstring

    # Observation of the input:
    # Start from the end (last input)
    # digit 14 into w
    # clear x
    # move z to x
    # x %26
    # z / 26
    # x - 9
    # x == w
    # x == 0
    # clear y
    # y = 25
    # y = y * x
    # y + 1
    # z = z * y
    # clear y
    # y = w
    # y + 7
    # y = y * x
    # z = z + y

    # w = digit_14
    # x = ((((prev_z % 26) - 9) == w) == 0)
    # y1 = 25 * x + 1
    # y2 = (w + 7) * x
    # z = (prev_z / 26) * y1 + y2

    # Okay, digit 13
    # clear x
    # move z to x
    # x % 26
    # z / 26       DIV
    # x - 2        ADD1
    # x == w
    # x == 0
    # clear y
    # y = 25
    # y = y * x
    # y + 1
    # z = z * y
    # clear y
    # y = w
    # y = y + 8    ADD2
    # y = y * x
    # z = z + y

    # w = digit_13
    # x = (((prev_z % 26) - 2) == w) == 0
    # y1 = 25 * x + 1
    # y2 = (w + 8)*x
    # z = (prev_z / 26) * y1 + y2
    # So same as next digit, but -2 and +8 instead of -9 and +7...

    # digit 12 next
    # same deal, but -14 and +8
    # Looks like they're all like this, but sometimes the z div is diff too
    # so...
    def compute_digit(self, digit, prev_z, div, add1, add2):
        w = digit
        x = (((prev_z % 26) + add1) == w) == 0
        y1 = (25 * x) + 1
        y2 = (w + add2) * x
        z = math.trunc(prev_z / div) * y1 + y2
        return z

    # x is either 0 or 1, so:
    def compute_digit_simplified(self, digit, prev_z, div, add1, add2):
        w = digit
        x = (((prev_z % 26) + add1) == w) == 0
        if (prev_z % 26 + add1) != w:
            # x = 1
            y1 = 26
            y2 = w + add2
            z = math.trunc(prev_z / div) * 26 + w + add2
        else:
            # x = 0
            y1 = 1
            y2 = 0
            z = math.trunc(prev_z / div)
        return z

    def compute_all_digits(self, modelstr):
        params = [
            [1,  11,  14],
            [1,  14,  6],
            [1,  15,  6],
            [1,  13,  13],
            [26, -12, 8],
            [26, 10,  8],
            [26, -15, 7],
            [1,  13,  10],
            [1,  10,  8],
            [26, -13, 12],
            [26, -13, 10],
            [26, -14, 8],
            [26, -2,  8],
            [26, -9,  7]
        ]
        z = 0
        for i, param in enumerate(params):
            div, add1, add2 = param
            digit = int(modelstr[i])
            z = self.compute_digit(digit, z, div, add1, add2)
            self.debugPrint("Digit {} gives z {}".format(digit, z))
        return z

    # Math:
    # - If add1 > 0 and add1 < 9, x = 1 always
    # - If possible, choose the x=0 branch to minimize z
    # - If it's between two digits, maximise the lower one if possible
    # x = (((prev_z % 26) + add1) == 0) == w)
    # if x = 0 => z = prev_z/div
    # if x = 1 => z = (prev_z/div)*26 + w + add2
    #
    # (x=1) z1 = w1 + 14
    # (x=1) z2 = 26z1 + w2 + 6
    # (x=1) z3 = 26z2 + w3 + 6
    # (x=1) z4 = 26z3 + w4 + 13
    # (x=0) z5 = 26(z4/26), z4/26 = z3 b/c w4 + 13 < 26 so goes to 0
    #    also z4 % 26 - 12 = w5 = w4 + 13 - 12 = w4 + 1
    #       => w5 = 9, w4 = 8
    # (x=1) z6 = 26*(z5/26) + w6 + 8
    # (x=0) z7 = 26*(z6/26)
    #    also z6 % 26 - 15 = w7 = w6 + 8 - 15 = w6 - 7
    #       => w6 = 9, w7 = 2
    # (...)
    # (x=0) z10 = 26*(z9/26)
    #    also z9 % 26 - 13 = w10 = w9 + 8 - 13 = w9 - 5
    #       => w9 = 9, w10 = 4


if __name__ == '__main__':
    parser = getBasicArgparser('day 24')
    parser.add_argument('-e', type=str, help='Run as example instead of MONAD', default=None)
    parser.add_argument('-t', type=str, help='Run specific test input instead of all possible inputs', default=None)

    args = parser.parse_args()

    day24 = Day24(args.i, args.v)

    print("WARNING! This was solved by hand, see jpegs. This code won't output the correct answer")

    if args.e is not None:
        day24.run_example(args.e)
    if args.t is not None:
        if len(args.t) != 14:
            print("ERROR: input must be 14 digits ({})".format(len(args.e)))
            sys.exit(-1)
        day24.run_MONAD(args.t)
    else:
        print("Largest valid model number: {}".format(day24.find_largest_valid_model_number_with_some_solved()))
