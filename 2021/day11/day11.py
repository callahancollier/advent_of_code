#!/usr/bin/env python3

import csv
import sys
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day11(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.energies = None
        self.x_max = 0
        self.y_max = 0
        self.num_flashes = 0
        self.steps_run = 0
        self.first_step_all_flash = -1
        self.parse_file()

    def parse_file(self):
        energies = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                energies.append([int(x) for x in line])
        self.energies = numpy.array(energies)
        self.x_max = len(self.energies[0])
        self.y_max = len(self.energies)
        self.debugPrint("Dimensions are {}x{}".format(self.x_max, self.y_max))

    def model_steps(self, num_steps):
        for step in range(0, num_steps):
            self.steps_run += 1
            previous_num_flashes = self.num_flashes
            self.energies += 1
            for x in range(0, self.x_max):
                for y in range(0, self.y_max):
                    if self.energies[y,x] > 9:
                        self.flash(x,y)
            self.debugPrint("After step {}:\n{}".format(step+1, self.energies))
            if self.num_flashes - previous_num_flashes == self.x_max*self.y_max:
                self.first_step_all_flash = self.steps_run
        return self.num_flashes

    def continue_until_all_flash(self):
        while self.first_step_all_flash < 0:
            self.model_steps(1)
        return self.first_step_all_flash

    def flash(self, x_point, y_point):
        if self.energies[y_point, x_point] == 0:
            # Already flashed
            return
        self.energies[y_point, x_point] = 0

        self.num_flashes += 1
        self.debugPrint("Point {},{} flashes ({} total)".format(x_point, y_point, self.num_flashes))

        for x in range(x_point-1,x_point+2):
            for y in range(y_point-1, y_point+2):
                if x < 0 or x >= self.x_max or y < 0 or y >= self.y_max:
                    continue
                if self.energies[y,x] == 0:
                    # Only allowed to flash once per step
                    continue
                self.energies[y,x] += 1
                if self.energies[y,x] > 9:
                    self.flash(x, y)


if __name__ == '__main__':
    parser = getBasicArgparser('day 11')
    parser.add_argument('-s', type=int, help='Number of steps to run', default=100)

    args = parser.parse_args()

    day11 = Day11(args.i, args.v)

    flashes = day11.model_steps(args.s)
    print("After {} steps, there have been {} flashes".format(args.s, flashes))

    print("First step where all flash together: {}".format(day11.continue_until_all_flash()))
