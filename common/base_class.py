#!/usr/bin/env python3

import argparse
import math
import logging
from dataclasses import dataclass
from enum import Enum


class DebugFunctions:
    def __init__(self, enable_debug):
        self.debug = enable_debug
        if self.debug:
            logging.basicConfig(level=logging.DEBUG, format="%(message)s")
        else:
            logging.basicConfig(level=logging.WARNING, format="%(levelname)s:%(message)s")

    def debugPrint(self, text):
        if self.debug:
            print(text)


class BaseFunctions(DebugFunctions):
    def __init__(self, input_filename, enable_debug):
        super().__init__(enable_debug)
        self.input_filename = input_filename


def getBasicArgparser(dayDesc, include_part_enum=False):
    parser = argparse.ArgumentParser(description='Advent of Code {}'.format(dayDesc))
    parser.add_argument('-i', type=str, help='input filename', default='input.txt')
    parser.add_argument('-v', action='store_true', help='enable debug')

    if include_part_enum:
        parser.add_argument('-p', type=int, default=-1, help='Choose which part to run')

    return parser


@dataclass(frozen=True)
class Point:
    row: int
    col: int

    def __str__(self):
        return "({},{})".format(self.row, self.col)
    __repr__ = __str__

    def __eq__(self, other):
        if not isinstance(other, Point):
            return NotImplemented
        return self.row == other.row and self.col == other.col

    def __add__(self, other):
        new_row = self.row + other.row
        new_col = self.col + other.col
        return Point(row=new_row, col=new_col)


class Directions(Point, Enum):
    # https://stackoverflow.com/questions/37678418/python-enums-with-complex-types
    # Use the enum values as though they are Point's
    UP = -1, 0
    RIGHT = 0,1
    DOWN = 1, 0
    LEFT = 0, -1


class ShortestPathBase(DebugFunctions):
    # See https://medium.com/@nicholas.w.swift/easy-a-star-pathfinding-7e6689c7f7b2
    def __init__(self, debug, start):
        super().__init__(debug)
        self.start = start
        # address arrays as array[x, y] or array[row, col]; x = row, y = col

    def manhattan(self, point):
        # manhattan distance between the start and this point
        raise NotImplementedError("Derived class needs to implement this function!")

    def get_neighbours(self, node, camefrom):
        raise NotImplementedError("Derived class needs to implement this function!")

    def are_at_end(self, point):
        raise NotImplementedError("Derived class needs to implement this function!")

    def get_shortest_path(self):
        # Using A* search
        openset = set()

        camefrom = {}
        gscore = {} # distance between the current node and the start
        #             h is the distance between the current node and the end node (estimated, here using manhattan dist)
        fscore = {} # total cost of the node f = g + h

        openset.add(self.start)
        gscore[self.start] = 0
        fscore[self.start] = self.manhattan(self.start)

        while len(openset) != 0:
            current = self.get_openset_node_with_lowest_score(openset, fscore)
            logging.debug("Consider %s", current)
            # self.debugPrint("Consider {}".format(current))
            if self.are_at_end(current):
                return self.tally_path(current, camefrom)

            openset.discard(current)
            for neighbour in self.get_neighbours(current, camefrom):
                logging.debug("  Check neighbour %s", neighbour)
                # self.debugPrint("Check neighbour {}".format(neighbour))
                tentative_gscore = self.get_next_g_score(current, gscore.get(current, math.inf), neighbour, camefrom)
                logging.debug("  Check neighbour %s", neighbour)
                logging.debug("   tentative g score: {} < {}?".format(tentative_gscore, gscore.get(neighbour, math.inf)))
                # self.debugPrint("tentative g score: {}".format(tentative_gscore))
                if tentative_gscore < gscore.get(neighbour, math.inf):
                    camefrom[neighbour] = current
                    gscore[neighbour] = tentative_gscore
                    fscore[neighbour] = tentative_gscore + self.manhattan(neighbour)
                    openset.add(neighbour)
                    logging.debug("    Add %s to set", neighbour)
                    # self.debugPrint("Add {} to set".format(neighbour))
            # logging.debug('openset: %s', openset)
            # self.debugPrint('openset: {}'.format(openset))
            # self.debugPrint('f_score: {}'.format(fscore))
            # self.debugPrint('g_score: {}'.format(gscore))
            # input("enter to continue")
        #print("ERROR did not find any path")
        raise RuntimeError("Did not find any path")

    def get_openset_node_with_lowest_score(self, openset, fscore):
        # self.debugPrint('--')
        min_val = (-1, -1)
        for val in openset:
            # self.debugPrint("Compare {} for {} with {} for {}".format(
            #     fscore.get(val, math.inf), val,
            #     fscore.get(min_val, math.inf), min_val,
            # ))
            if fscore.get(val, math.inf) <= fscore.get(min_val, math.inf):
                # self.debugPrint("New min {}".format(val))
                min_val = val
        # self.debugPrint("min overall {}".format(min_val))
        return min_val

    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        # Distance between the current node and the start, given the current score up to this node
        raise NotImplementedError("Derived class needs to implement this function!")
        # Example from 2021 day 15
        # return current_g_score + self.array[neighbour_node.row, neighbour_node.col]

    def tally_path(self, current, camefrom):
        raise NotImplementedError("Derived class needs to implement this")
        # Example from 2021 day 15
        # pathtotal = self.array[current[0], current[1]]
        # while current in camefrom.keys():
        #     current = camefrom[current]
        #     pathtotal += self.array[current[0], current[1]]
        # # Don't count the start point of the path
        # pathtotal -= self.array[0, 0]
        # return pathtotal


class ShortestPathCardinalBase(ShortestPathBase):
    def __init__(self, debug, start, end, array):
        super().__init__(debug, start)
        self.array = array
        self.end = end
        self.num_row = len(array)
        self.num_col = len(array[0])

    #Possibly if h(x) ie manhattan(pt) = 0, we get dijkstra?
    def manhattan(self, point):
        h_score = (self.start.row - point.row) + (self.start.col - point.col)
        # self.debugPrint("Manhattan of {} is {}".format(point, h_score))
        return h_score

    def get_neighbours(self, node, camefrom):
        neighbours = []
        for adj in [-1, 1]:
            neighbourA = Point(node.row + adj, node.col)
            neighbourB = Point(node.row, node.col + adj)
            for neighbour in [neighbourA, neighbourB]:
                if neighbour.row < 0 or neighbour.row >= self.num_row:
                    continue
                if neighbour.col < 0 or neighbour.col >= self.num_col:
                    continue
                neighbours.append(neighbour)
        return neighbours

    def are_at_end(self, point):
        return point == self.end
    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        raise NotImplementedError("Derived class needs to implement this function!")

    def tally_path(self, current, camefrom):
        raise NotImplementedError("Derived class needs to implement this")


def get_area_of_loop(loop_points: list[Point]):
    # Shoelace formula
    # Calculate area in the loop drawn by the cartesian points
    sum = 0
    for i in range(len(loop_points) - 1):
        sum += (loop_points[i].row * loop_points[i + 1].col -
                loop_points[i].col * loop_points[i + 1].row)
    sum += (loop_points[-1].row * loop_points[0].col -
            loop_points[-1].col * loop_points[0].row)
    return abs(sum / 2)


def get_num_points_enclosed_by_loop(loop_points):
    # Pick's Theorem
    # Area = n_interior_pts + (n_boundary_points)/2 - 1
    area = get_area_of_loop(loop_points)
    boundary_pts = len(loop_points)
    return area - (boundary_pts / 2) + 1


#TODO this is untested, wrote for 2024 day 12 but didn't end up using it
def is_point_enclosed_by(self, point, grid, loop_points):
    # Crossing Number Algorithm or Even-Odd Rule Algorithm
    inside_loop = False
    # If the point is on an edge, it's automatically outside the loop
    if point.row == 0 or point.row == len(grid[0])-1:
        return False
    if point.col == 0 or point.col == len(grid)-1:
        return False
    # Check in a row from the edge to the point, checking how many times
    # we cross into/out of the loop
    if Point(row=0, col=point.col) in loop_points:
        inside_loop = True
    start = inside_loop
    switches = 0
    for r in range(len(grid[0])):
        pt = Point(r, col=point.col)
        if pt in loop_points:
            if not inside_loop:
                switches += 1
            inside_loop = True
        else:
            if inside_loop:
                switches += 1
            inside_loop = False
    if switches % 2 == 0:
        return start
    else:
        return not start
