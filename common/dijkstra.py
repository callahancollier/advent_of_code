#!/usr/bin/env python3

import math
import logging


class DijkstraNode:
    def __init__(self, identifier):
        self.id = identifier
        self.distance = math.inf
        self.visited = False
        self.visited_by = None

    def get_neighbours(self):
        raise NotImplementedError("Derived class should implement this")

    def __lt__(self, other):
        return self.distance < other.distance


class DijkstraBase:
    def __init__(self, nodemap):
        self.nodes = nodemap # a dictionary to DijkstraNode subclass with identifier as key
        self.unvisited = self.get_initial_unvisited_list()

    def get_initial_unvisited_list(self):
        raise NotImplementedError("Derived class should implement this")

    def get_length_between(self, fromNode, toNode):
        raise NotImplementedError("Derived class should implement this")

    def is_destination(self, node):
        raise NotImplementedError("Derived class should implement this")

    def possibly_update_path(self, current, neighbour):
        score = current.distance + self.get_length_between(current, neighbour)
        update = neighbour.distance > score
        if update:
            neighbour.distance = score
            neighbour.visited_by = current.id
        return update

    def trace_back_and_return(self, dest_node):
        node = self.nodes[dest_node]
        result = 0
        while node.visited_by is not None:
            result += node.distance
            node = self.nodes[node.visited_by]
        return result

    def find_best_path(self, initial_node):
        self.nodes[initial_node].distance = 0
        current = self.nodes[initial_node]
        nodes_checked = 0
        while not self.is_destination(current):
            for n in current.get_neighbours():
                # calculate distance through the current node
                neighbour = self.nodes[n]
                self.possibly_update_path(current, neighbour)
            current.visited = True
            self.unvisited.remove(current)
            if len(self.unvisited) == 0:
                logging.warning("Ran out of unvisited nodes!")
                break
            current = self.get_smallest_tentative_distance()
            nodes_checked += 1
            if current.distance == math.inf:
                logging.warning("No nodes are connected")
                break
        destination = current
        if destination.visited_by is None:
            raise ValueError("Unable to trace to destination node")
        return self.trace_back_and_return(destination)

    def get_smallest_tentative_distance(self):
        self.unvisited.sort()
        return self.unvisited[0]

