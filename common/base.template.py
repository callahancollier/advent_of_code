#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class DayDAY_CODE(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]


if __name__ == '__main__':
    parser = getBasicArgparser('day DAY_CODE')

    args = parser.parse_args()

    dayDAY_CODE = DayDAY_CODE(args.i, args.v)
