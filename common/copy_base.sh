#!/bin/bash

print_usage()
{
  echo "Usage:   ./copy_base.sh -y <YEAR> -d <DAY_CODE>"
  echo "Example: ./copy_base.sh -y 2020 -d 1"
}

DAY=
YEAR=
while getopts "h?d:y:" opt
do
  case "$opt" in
  h|\?)
    print_usage
    exit 0
    ;;
  d)
    DAY=$OPTARG
    ;;
  y)
    YEAR=$OPTARG
    ;;
  esac
done

if [[ -z "$DAY" ]]
then
  echo "Option -d is required"
  print_usage
  exit 1
fi

if [[ -z "$YEAR" ]]
then
  echo "Option -y is required"
  print_usage
  exit 1
fi

DIR="../${YEAR}/day${DAY}"
FILE="${DIR}/day${DAY}.py"
if [[ -d $DIR ]]
then
  echo "ERROR: Directory $DIR already exists, not creating."
  exit 1
fi

mkdir -p $DIR

sed base.template.py -e "s:DAY_CODE:$DAY:g" > $FILE

echo "Copied python template for $YEAR day $DAY"
echo "cd $DIR"
