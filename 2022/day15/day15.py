#!/usr/bin/env python3

import csv
import sys
import logging
from enum import Enum
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point

LINE_START='Sensor at '
LINE_MID=': closest beacon is at '


class Corner(Enum):
    TOP_LEFT = 0
    TOP_RIGHT = 1
    BOTTOM_LEFT = 2
    BOTTOM_RIGHT = 3


def manhattan(pointa, pointb):
    return abs(pointb.row - pointa.row) + abs(pointb.col - pointa.col)


def expand_range(existing, new):
    combined = existing
    if existing[0] <= new[0] <= existing[1]:
        combined[1] = max(new[1], existing[1])
    elif existing[0] <= new[1] <= existing[1]:
        combined[0] = min(new[0], existing[0])
    elif existing[1]+1 == new[0]:
        combined = [existing[0], new[1]]
    elif new[1]+1 == existing[0]:
        combined = [new[0], existing[1]]
    else:
        return None
    return combined


class SensorInfo:
    def __init__(self, sensor, beacon):
        self.sensor = sensor
        self.closest_beacon = beacon
        self.manhattan = manhattan(sensor, beacon)

    def is_point_not_a_beacon(self, point):
        point_manhattan = manhattan(point, self.sensor)
        result = point_manhattan <= self.manhattan and point != self.closest_beacon or point == self.sensor
        logging.debug("Check point %s, sensor %s to beacon %s has manhattan %s, manhattan to point is %s, %s",
                      point, self.sensor, self.closest_beacon, self.manhattan, point_manhattan, result)
        return result

    def get_non_beacon_range(self, row, existing_ranges, max_coord):
        rem = self.manhattan - abs(row - self.sensor.row)
        if rem < 0:
            return existing_ranges
        this_range = [max(self.sensor.col - rem, 0), min(self.sensor.col + rem, max_coord)]
        #print("This range: {}, existing {}".format(this_range, existing_ranges))
        if len(existing_ranges) == 0:
            return [this_range]
        could_expand = True
        while could_expand:
            could_expand = False
            new_ranges = []
            for existing in existing_ranges:
                expanded = expand_range(existing, this_range)
                if expanded is not None:
                    this_range = expanded
                    could_expand = True
                else:
                    new_ranges.append(existing)
            existing_ranges = new_ranges
        existing_ranges.append(this_range)
        #print("   becomes {}".format(existing_ranges))
        return existing_ranges


class Day15(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.sensors = None
        self.corners = {}
        self.max_manhattan = 0
        self.known_beacons = []
        self.parse_file()

    def parse_file(self):
        self.sensors = []
        self.known_beacons = []
        self.corners = {
            Corner.TOP_LEFT: Point(99999, 99999),
            Corner.BOTTOM_RIGHT: Point(0, 0)
        }
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if not line.startswith(LINE_START):
                    logging.error("Malformed line: %s", line)
                line = line[len(LINE_START):].replace('x=','').replace('y=','').split(LINE_MID)
                sensor = [int(x) for x in line[0].split(', ')]
                beacon = [int(x) for x in line[1].split(', ')]
                info = SensorInfo(Point(sensor[0], sensor[1]),
                                  Point(beacon[0], beacon[1]))
                self.sensors.append(info)
                self.update_corners(info.sensor)
                self.update_corners(info.closest_beacon)
                self.max_manhattan = max(self.max_manhattan, info.manhattan)
                self.known_beacons.append(info.closest_beacon)
                logging.debug("Sensor at %s, closest beacon at %s",
                              info.sensor, info.closest_beacon)
        logging.debug("Corners are: %s", self.corners)
        logging.debug("Maximum manhattan distance is %s", self.max_manhattan)

    def update_corners(self, point):
        if point.row < self.corners[Corner.TOP_LEFT].row:
            self.corners[Corner.TOP_LEFT] = Point(point.row, self.corners[Corner.TOP_LEFT].col)
        if point.col < self.corners[Corner.TOP_LEFT].col:
            self.corners[Corner.TOP_LEFT] = Point(self.corners[Corner.TOP_LEFT].row, point.col)

        if point.row > self.corners[Corner.BOTTOM_RIGHT].row:
            self.corners[Corner.BOTTOM_RIGHT] = Point(point.row, self.corners[Corner.BOTTOM_RIGHT].col)
        if point.col > self.corners[Corner.BOTTOM_RIGHT].col:
            self.corners[Corner.BOTTOM_RIGHT] = Point(self.corners[Corner.BOTTOM_RIGHT].row, point.col)

    def print_field(self):
        row_start = self.corners[Corner.TOP_LEFT].row
        row_end = self.corners[Corner.BOTTOM_RIGHT].row
        col_start = self.corners[Corner.TOP_LEFT].col
        col_end = self.corners[Corner.BOTTOM_RIGHT].col
        print(self.corners[Corner.TOP_LEFT])
        for row in range(row_start, row_end+1):
            row_str = '{0:03}: '.format(row)
            for col in range(col_start, col_end+1):
                point = Point(row, col)
                if any([point == s.sensor for s in self.sensors]):
                    row_str += 'S'
                elif any([point == s.closest_beacon for s in self.sensors]):
                    row_str += 'B'
                else:
                    row_str += '.'
            print(row_str)

    def get_num_confirmed_beaconless(self, col, row_range=None):
        count = 0
        row_start = self.corners[Corner.TOP_LEFT].row - self.max_manhattan - 1
        row_end = self.corners[Corner.BOTTOM_RIGHT].row + self.max_manhattan + 1
        if row_range is not None:
            row_start = max(row_start, row_range[0])
            row_end = min(row_end, row_range[1])
        # col_start = self.corners[Corner.TOP_LEFT].col - self.max_manhattan - 1
        # col_end = self.corners[Corner.BOTTOM_RIGHT].col + self.max_manhattan + 1
        for row in range(row_start, row_end+1):
            point = Point(row, col)
            if any([s.is_point_not_a_beacon(point) for s in self.sensors]):
                count += 1
            if (point.row - row_start) % 10000 == 0:
                print("Check point row {}/{}".format(point.row, row_end))
        return count

    def get_tuning_freq(self, max_coord):
        for row in range(max_coord):
            ranges = []
            for s in self.sensors:
                ranges = s.get_non_beacon_range(row, ranges, max_coord)
            if row % (max_coord/100) == 0:
                print("Row {}, ranges {}".format(row, ranges))
            if [0, max_coord] in ranges:
                continue
            if len(ranges) > 2:
                print("ERROR: too many ranges")
                sys.exit(1)
            if len(ranges) == 2:
                ranges.sort()
                print("Found row {}! Iterating over {}".format(row, ranges))
                i = 0
                for r in ranges:
                    if i >= r[0]:
                        i = r[1]+1
                return self.calc_tuning_freq(row, i)

    def print_options(self, max_coord):
        row_start = max(0, self.corners[Corner.TOP_LEFT].row - self.max_manhattan - 1)
        row_end = min(max_coord, self.corners[Corner.BOTTOM_RIGHT].row + self.max_manhattan + 1)
        col_start = max(0, self.corners[Corner.TOP_LEFT].col - self.max_manhattan - 1)
        col_end = min(max_coord, self.corners[Corner.BOTTOM_RIGHT].col + self.max_manhattan + 1)
        print(Point(row_start, col_start))
        for row in range(row_start, row_end+1):
            dbg = '{0:03}: '.format(row)
            for col in range(col_start, col_end+1):
                point = Point(row, col)
                if any([point == s.sensor for s in self.sensors]):
                    dbg += 'S'
                elif any([point == s.closest_beacon for s in self.sensors]):
                    dbg += 'B'
                elif any([s.is_point_not_a_beacon(point) for s in self.sensors]):
                    dbg += '#'
                else:
                    dbg += '.'
            print(dbg)

    def calc_tuning_freq(self, row, col):
        return 4000000*row + col


if __name__ == '__main__':
    parser = getBasicArgparser('day 15')
    parser.add_argument('-y', type=int, help='"row" on which to run the test', default=2000000)
    parser.add_argument('-m', type=int, help='max value of coord for part 2', default=4000000)
    parser.add_argument('-t', action='store_true', help='only run part 2')

    args = parser.parse_args()

    # The AoC description has row and column flipped from us
    day15 = Day15(args.i, args.v)
    if not args.t:
        print('Part 1: Over "row" {}, there are {} spots confirmed to have no beacon'.format(
            args.y, day15.get_num_confirmed_beaconless(args.y)
        ))

    print("Part 2: Tuning frequency is {}".format(day15.get_tuning_freq(args.m)))
