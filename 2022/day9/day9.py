#!/usr/bin/env python3

import csv
import sys
import math
from enum import Enum

sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Direction(Enum):
    RIGHT = 'R'
    LEFT = 'L'
    UP = 'U'
    DOWN = 'D'


class Move:
    def __init__(self, dir, num):
        self.direction = dir
        self.num = int(num)


class Location:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "{},{}".format(self.x, self.y)

    def __eq__(self, other):
        if not isinstance(other, Location):
            return NotImplemented
        return self.x == other.x and self.y == other.y


class Day9(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.moves = self.parse_file()
        self.head = None
        self.tail = None
        self.visited = None

    def config(self, num_tails):
        self.head = Location(0, 0)
        self.tail = []
        for i in range(num_tails):
            self.tail.append(Location(0, 0))
        self.visited = [Location(0, 0)]

    def parse_file(self):
        moves = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                moves.append(Move(Direction(line[0]), line[1]))
        return moves

    def print_positions(self):
        self.debugPrint("Positions:")
        # self.debugPrint("  Head: {}".format(self.head))
        # for i in range(len(self.tail)):
        #     self.debugPrint("  Tail {}: {}".format(i, self.tail[0]))
        grid = []
        for y in range(min(self.head.y, min([t.y for t in self.tail]), min([v.y for v in self.visited])),
                       max(self.head.y, max([t.y for t in self.tail]), max([v.y for v in self.visited])) + 1):
            row = ''
            for x in range(min(self.head.x, min([t.x for t in self.tail]), min([v.x for v in self.visited])),
                           max(self.head.x, max([t.x for t in self.tail]), max([v.x for v in self.visited])) + 1):
                if self.head == Location(x, y):
                    row = row + 'H'
                elif Location(x, y) in self.tail:
                    for i in range(len(self.tail)):
                        if self.tail[i] == Location(x, y):
                            row = row + str(i + 1)
                            break
                elif Location(x, y) in self.visited:
                    for i in range(len(self.visited)):
                        if self.visited[i] == Location(x, y):
                            if x == 0 and y == 0:
                                row = row + 's'
                            else:
                                row = row + '#'
                            break
                else:
                    row = row + '.'
            grid.append(row)

        for line in grid[::-1]:
            self.debugPrint(line)

    def do_moves(self):
        for move in self.moves:
            for i in range(move.num):
                self.move_head(move.direction)
                self.debugPrint("Move head {} to {}".format(
                    move.direction, self.head
                ))
                for t in range(len(self.tail)):
                    if t == 0:
                        self.move_tail(self.head, self.tail[t])
                    else:
                        self.move_tail(self.tail[t - 1], self.tail[t])
                self.print_positions() # comment this out for speed
                if self.debug:
                    input()
                if self.tail[-1] not in self.visited:
                    self.visited.append(Location(self.tail[-1].x, self.tail[-1].y))
                    # self.debugPrint("  tail has visited {}".format(len(self.visited)))

    def move_head(self, direction):
        if direction == Direction.UP:
            self.head.y += 1
        elif direction == Direction.DOWN:
            self.head.y -= 1
        elif direction == Direction.LEFT:
            self.head.x -= 1
        elif direction == Direction.RIGHT:
            self.head.x += 1
        else:
            print("ERROR: Unknown direction")
            sys.exit(1)

    def same_column_or_row(self, head, tail):
        if head.y == tail.y:
            return True
        if head.x == tail.x:
            return True
        return False

    def move_tail(self, head, tail):
        same_col_or_row = self.same_column_or_row(head, tail)
        self.debugPrint("    {} {} same col/row: {}".format(head, tail, same_col_or_row))
        diffy = head.y - tail.y
        diffx = head.x - tail.x
        if diffy != 0:
            tail.y += int(diffy - math.copysign(1, diffy))
            if not same_col_or_row and abs(diffy) != 1:
                if abs(diffx) == 1:
                    tail.x += int(diffx)
                else:
                    tail.x += int(diffx - math.copysign(1, diffx))
        # diffy = head.y - tail.y
        diffx = head.x - tail.x
        if diffx != 0:
            tail.x += int(diffx - math.copysign(1, diffx))
            if not same_col_or_row and abs(diffx) != 1:
                if abs(diffy) == 1:
                    tail.y += int(diffy)
                else:
                    tail.y += int(head.y - tail.y)
        self.debugPrint("    y diff {}, x diff {}, tail to {}".format(
            diffy, diffx, tail))


if __name__ == '__main__':
    parser = getBasicArgparser('day 9')
    parser.add_argument('-t', action='store_true', help='only run part 2')

    args = parser.parse_args()

    day9 = Day9(args.i, args.v)
    if not args.t:
        day9.config(1)
        day9.do_moves()
        print("Part 1: Tail has gone over {} unique spaces".format(len(day9.visited)))
        print("================================")
        print("================================")

    day9.config(9)
    day9.do_moves()
    print("Part 2: Tails have gone over {} unique spaces".format(len(day9.visited)))
