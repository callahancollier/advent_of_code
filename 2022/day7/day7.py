#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

CD_CMD = '$ cd '
LS_CMD = '$ ls'
DIR_LISTING = 'dir '

DISK_SPACE=70000000
UPDATE_SPACE_REQUIRED=30000000


class DirectoryListing:
    def __init__(self, name, level, updir):
        self.name = name
        self.level = level
        self.subdirectories = []
        self.files = []
        self.updir = updir

    def get_subdirectory(self, sub_name):
        for sub in self.subdirectories:
            if sub.name == sub_name:
                return sub
        return None

    def __str__(self):
        spacer = self.level*' '
        result = spacer + '- ' + self.name + ' (dir)\n'
        for file in self.files:
            result += spacer + ' - ' + file.name + ' (file, size=' + str(file.size) + ')\n'
        for dir in self.subdirectories:
            result += dir.__str__()
        return result

    def get_total_size(self):
        size = 0
        for file in self.files:
            size += file.size
        for dir in self.subdirectories:
            size += dir.get_total_size()
        return size

    def find_sum_of_sizes_under(self, max_size):
        size = 0
        if self.get_total_size() < max_size:
            size += self.get_total_size()
        for dir in self.subdirectories:
            size += dir.find_sum_of_sizes_under(max_size)
        return size

    def get_dir_sizes(self):
        sizes = {self.name: self.get_total_size()}
        for dir in self.subdirectories:
            sizes.update(dir.get_dir_sizes())
        return sizes


class FileListing:
    def __init__(self, name, size):
        self.name = name
        self.size = int(size)


class Day7(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.root = DirectoryListing('/', 0, None)
        self.parse_file()
        self.debugPrint(self.root)
        self.debugPrint('Total size of /: {}'.format(self.root.get_total_size()))

    def parse_file(self):
        current_directory = self.root
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                self.debugPrint('Parse input {}'.format(line))

                if line.startswith(CD_CMD):
                    dest = line.split(CD_CMD)[1]
                    current_directory = self.change_directory(current_directory, dest)
                    self.debugPrint("--> cd to {}".format(current_directory.name))
                elif line.startswith(LS_CMD):
                    continue
                elif line.startswith(DIR_LISTING):
                    continue
                else:
                    line = line.split(' ')
                    current_directory.files.append(FileListing(line[1], line[0]))

    def change_directory(self, current, dest):
        if dest == '..':
            return current.updir
        if dest == '/':
            return self.root
        subdir = current.get_subdirectory(dest)
        if subdir is None:
            newdir = DirectoryListing(dest, current.level+1, current)
            current.subdirectories.append(newdir)
            return newdir
        else:
            return subdir

    def find_sum_of_sizes_under(self, max_size):
        return self.root.find_sum_of_sizes_under(max_size)

    def choose_which_to_delete(self):
        unused_space = DISK_SPACE - self.root.get_total_size()
        space_to_free = UPDATE_SPACE_REQUIRED - unused_space
        self.debugPrint("Need to free {}".format(space_to_free))
        delete_candidate = ('/', self.root.get_total_size())
        size_map = self.root.get_dir_sizes()
        for dir in size_map:
            if size_map[dir] >= space_to_free:
                if size_map[dir] < delete_candidate[1]:
                    delete_candidate = (dir, size_map[dir])
        self.debugPrint("Choose {} to delete".format(delete_candidate[0]))
        return delete_candidate[1]


if __name__ == '__main__':
    parser = getBasicArgparser('day 7')

    args = parser.parse_args()

    day7 = Day7(args.i, args.v)

    PART1_LIM = 100000
    print("Part 1: sum of dirs under {} is: {}".format(PART1_LIM, day7.find_sum_of_sizes_under(PART1_LIM)))
    print("Part 2: Choose directory to delete: frees up {}".format(day7.choose_which_to_delete()))