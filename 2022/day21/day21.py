#!/usr/bin/env python3

import csv
import sys
import logging
import operator

sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

OPERATIONS = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.floordiv
}
OP_STRINGS = {v: k for k, v in OPERATIONS.items()}
OP_STRINGS[None] = 'None'

INVERSE_OPS = {
    operator.add: operator.sub,
    operator.sub: operator.add,
    operator.mul: operator.floordiv,
    operator.floordiv: operator.mul
}


class Monkey:
    def __init__(self, name, value=None, operation=None, left=None, right=None):
        self.name = name
        self.value = value
        self.operation = operation
        self.left = left
        self.right = right

    def get_value(self, all_monkeys):
        if self.value is not None:
            logging.debug("%s yells: %s", self.name, self.value)
            return self.value
        else:
            logging.debug(" recurse %s %s %s", self.name, self.left, self.right)
            left_result = all_monkeys[self.left].get_value(all_monkeys)
            right_result = all_monkeys[self.right].get_value(all_monkeys)
            logging.debug("%s: calc %s %s %s", self.name, self.left, OP_STRINGS[self.operation], self.right)
            return self.operation(left_result, right_result)

    def is_in_path(self, all_monkeys, target):
        if self.name == target:
            return True
        if self.value is not None:
            return False
        if self.left == target or self.right == target:
            return True
        return all_monkeys[self.left].is_in_path(all_monkeys, target) or \
               all_monkeys[self.right].is_in_path(all_monkeys, target)

    def for_result_get_field(self, all_monkeys, target, you):
        logging.debug("Recurse target %s %s: val %s left %s right %s op %s",
                      target, self.name, self.value, self.left, self.right, OP_STRINGS[self.operation])
        if all_monkeys[self.left].is_in_path(all_monkeys, you):
            right_side = all_monkeys[self.right].get_value(all_monkeys)
            new_result = INVERSE_OPS[self.operation](target, right_side)
            if self.left == you:
                return new_result
            return all_monkeys[self.left].for_result_get_field(all_monkeys, new_result, you)
        elif all_monkeys[self.right].is_in_path(all_monkeys, you):
            left_side = all_monkeys[self.left].get_value(all_monkeys)
            if self.operation == operator.sub:
                new_result = operator.sub(left_side, target)
            elif self.operation == operator.floordiv:
                new_result = operator.floordiv(left_side, target)
            else:
                new_result = INVERSE_OPS[self.operation](
                    target, left_side
                )
            if self.right == you:
                return new_result
            return all_monkeys[self.right].for_result_get_field(all_monkeys, new_result, you)
        else:
            logging.error("An assumption is wrong! %s: val %s left %s right %s",
                          self.name, self.value, self.left, self.right)
            sys.exit(1)


class Day21(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.monkeys = {}
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(": ")
                if line[1].isnumeric():
                    self.monkeys[line[0]] = Monkey(line[0], value=int(line[1]))
                else:
                    ops = line[1].split(' ')
                    self.monkeys[line[0]] = Monkey(line[0],
                                                   left=ops[0],
                                                   operation=OPERATIONS[ops[1]],
                                                   right=ops[2])

    def get_roots_value(self):
        return self.monkeys['root'].get_value(self.monkeys)

    def get_humns_value(self):
        root = self.monkeys['root']
        if self.monkeys[root.left].is_in_path(self.monkeys, 'humn'):
            result = self.monkeys[root.right].get_value(self.monkeys)
            return self.monkeys[root.left].for_result_get_field(self.monkeys, result, 'humn')
        else:
            result = self.monkeys[root.left].get_value(self.monkeys)
            return self.monkeys[root.right].for_result_get_field(self.monkeys, result, 'humn')


if __name__ == '__main__':
    parser = getBasicArgparser('day 21')

    args = parser.parse_args()

    day21 = Day21(args.i, args.v)

    print("Part 1: root monkey yells {}".format(day21.get_roots_value()))

    print("Part 2: you should yell {}".format(day21.get_humns_value()))
