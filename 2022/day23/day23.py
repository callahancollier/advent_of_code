#!/usr/bin/env python3

import csv
import sys
import logging
from tqdm import tqdm
from enum import Enum
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point


ELF = '#'
EMPTY = '.'


class Dir(Enum):
    NORTH = 'N',
    EAST = 'E',
    SOUTH = 'S',
    WEST = 'W'


class Elf:
    def __init__(self, position, index):
        self.id = index
        self.position = position

    def no_adjacent(self, grove, offset_row, offset_col):
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                if i == j == 0:
                    continue
                if grove[self.position.row-offset_row+i][self.position.col-offset_col+j] == ELF:
                    return False
        return True

    def propose_move(self, directions, grove, offset_row, offset_col):
        #logging.debug("Elf %s %s".format(self.id, self.position))
        if self.no_adjacent(grove, offset_row, offset_col):
            return None
        for direction in directions:
            if direction == Dir.NORTH:
                #logging.debug("NORTH")
                #logging.debug([Point(self.position.row-offset_row-1,x) for x in range(self.position.col-offset_col-1, self.position.col-offset_col+2)])
                #logging.debug([grove[self.position.row-offset_row-1][x] for x in range(self.position.col-offset_col-1, self.position.col-offset_col+2)])
                if all([grove[self.position.row-offset_row-1][x] == EMPTY for x in range(self.position.col-offset_col-1, self.position.col-offset_col+2)]):
                    return Point(self.position.row-1, self.position.col)
            elif direction == Dir.SOUTH:
                #logging.debug("SOUTH")
                #logging.debug([Point(self.position.row-offset_row+1,x) for x in range(self.position.col-offset_col-1, self.position.col-offset_col+2)])
                #logging.debug([grove[self.position.row-offset_row+1][x] for x in range(self.position.col-offset_col-1, self.position.col-offset_col+2)])
                if all([grove[self.position.row-offset_row+1][x] == EMPTY for x in range(self.position.col-offset_col-1, self.position.col-offset_col+2)]):
                    return Point(self.position.row+1, self.position.col)
            elif direction == Dir.WEST:
                #logging.debug("WEST")
                #logging.debug([Point(x,self.position.col-offset_col-1) for x in range(self.position.row-offset_row-1, self.position.row-offset_row+2)])
                #logging.debug([grove[x][self.position.col-offset_col-1] for x in range(self.position.row-offset_row-1, self.position.row-offset_row+2)])
                if all([grove[x][self.position.col-offset_col-1] == EMPTY for x in range(self.position.row-offset_row-1, self.position.row-offset_row+2)]):
                    return Point(self.position.row, self.position.col - 1)
            elif direction == Dir.EAST:
                #logging.debug("EAST")
                #logging.debug([Point(x,self.position.col-offset_col+1) for x in range(self.position.row-offset_row-1, self.position.row-offset_row+2)])
                #logging.debug([grove[x][self.position.col-offset_col+1] for x in range(self.position.row-offset_row-1, self.position.row-offset_row+2)])
                if all([grove[x][self.position.col-offset_col+1] == EMPTY for x in range(self.position.row-offset_row-1, self.position.row-offset_row+2)]):
                    return Point(self.position.row, self.position.col+1)
        return None



class Day23(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.elves = None
        self.min_row = None
        self.max_row = None
        self.min_col = None
        self.max_col = None
        self.parse_file()
        self.directions = [Dir.NORTH, Dir.SOUTH, Dir.WEST, Dir.EAST]

    def reset(self):
        self.elves = None
        self.min_row = None
        self.max_row = None
        self.min_col = None
        self.max_col = None
        self.parse_file()
        self.directions = [Dir.NORTH, Dir.SOUTH, Dir.WEST, Dir.EAST]

    def parse_file(self):
        grove = []
        self.elves = []
        self.min_row = 99999
        self.max_row = 0
        self.min_col = 99999
        self.max_col = 0
        elf_idx = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if len(grove) == 0 and ELF not in line:
                    continue
                grove.append(line)
        # Trim leading empty columns
        while all([grove[x][0] == EMPTY for x in range(len(grove))]):
            for i in range(len(grove)):
                grove[i] = grove[i][1:]

        for i,row in enumerate(grove):
            for j,char in enumerate(row):
                if char == ELF:
                    point = Point(i, j)
                    self.elves.append(Elf(point, elf_idx))
                    elf_idx += 1
        self.adjust_maxmin()

    def adjust_maxmin(self):
        self.min_row = min([elf.position.row for elf in self.elves])
        self.max_row = max([elf.position.row for elf in self.elves])
        self.min_col = min([elf.position.col for elf in self.elves])
        self.max_col = max([elf.position.col for elf in self.elves])

    def draw_grove(self):
        elf_points = [e.position for e in self.elves]
        grove = []
        for row in range(self.min_row-1, self.max_row+2):
            grove_row = ''
            for col in range(self.min_col-1, self.max_col+2):
                if Point(row, col) in elf_points:
                    grove_row += ELF
                else:
                    grove_row += EMPTY
            grove.append(grove_row)
        return grove

    def log_grove(self):
        if self.debug:
            grove = self.draw_grove()
            for grove_row in grove:
                logging.debug(grove_row)

    def move_elves(self):
        proposed_moves = {}
        grove = self.draw_grove()
        for elf in self.elves:
            move = elf.propose_move(self.directions, grove, self.min_row-1, self.min_col-1)
            if move is not None:
                if move not in proposed_moves:
                    proposed_moves[move] = [elf]
                else:
                    proposed_moves[move].append(elf)
                #logging.debug("Elf %s proposes move from %s to %s", elf.id, elf.position, move)
            #else:
                #logging.debug("Elf %s doesn't move", elf.id)

        num_moved = 0
        for move,elves in proposed_moves.items():
            if len(elves) == 1:
                elves[0].position = move
                num_moved += 1
        self.adjust_maxmin()
        self.directions = self.directions[1:] + [self.directions[0]]
        return num_moved

    def get_empty_ground_tiles(self, num_rounds):
        logging.debug("At start: rows %s-%s cols %s-%s", self.min_row, self.max_row, self.min_col, self.max_col)
        self.log_grove()
        for round in tqdm(range(num_rounds)):
            moved = self.move_elves()
            if moved == 0:
                break
            logging.debug("===== After round %s:", round)
            self.log_grove()
            logging.debug("rows %s - %s cols %s - %s", self.min_row, self.max_row, self.min_col, self.max_col)
            if self.debug:
                input('--Enter to continue--')
        empty_ground_tiles = (1 + self.max_col - self.min_col) * (1 + self.max_row - self.min_row) - len(self.elves)
        return empty_ground_tiles

    def get_first_round_no_moves(self):
        logging.debug("At start: rows %s-%s cols %s-%s", self.min_row, self.max_row, self.min_col, self.max_col)
        self.log_grove()
        moved = -1
        rounds = 0
        while moved != 0:
            moved = self.move_elves()
            logging.debug("===== After round %s:", round)
            self.log_grove()
            logging.debug("rows %s - %s cols %s - %s", self.min_row, self.max_row, self.min_col, self.max_col)
            if self.debug:
                input('--Enter to continue--')
            rounds += 1
            if rounds % 100 == 0:
                logging.warning("Ran %s rounds", rounds)
        return rounds


if __name__ == '__main__':
    parser = getBasicArgparser('day 23')

    args = parser.parse_args()

    day23 = Day23(args.i, args.v)

    print("Part 1: After 10 rounds, there are {} empty ground tiles".format(day23.get_empty_ground_tiles(10)))

    day23.reset()
    print("Part 2: Elves stop moving after {} rounds".format(day23.get_first_round_no_moves()))
