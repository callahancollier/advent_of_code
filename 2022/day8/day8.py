#!/usr/bin/env python3

import csv
import sys
import numpy as np
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day8(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.trees = self.parse_file()
        self.num_row = len(self.trees[0])
        self.num_col = len(self.trees[:,0])

    def parse_file(self):
        trees = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                row = [int(x) for x in line]
                trees.append(row)
        return np.array(trees)

    def get_num_visible_trees(self):
        visible = 2*self.num_row + 2*(self.num_col-2)
        self.debugPrint("{} visible from the edges".format(visible))
        for row in range(1,self.num_row-1):
            for col in range(1,self.num_col-1):
                tree = self.trees[row,col]
                self.debugPrint("Check tree {},{}: {}".format(row,col,tree))
                visible_above = True
                for i in range(0,row):
                    if self.trees[i,col] >= tree:
                        visible_above = False
                        break
                visible_below = True
                for i in range(row+1,self.num_row):
                    if self.trees[i,col] >= tree:
                        visible_below = False
                        break
                visible_left = True
                for i in range(0,col):
                    if self.trees[row,i] >= tree:
                        visible_left = False
                        break
                visible_right = True
                for i in range(col+1,self.num_col):
                    if self.trees[row,i] >= tree:
                        visible_right = False
                        break
                if visible_above or visible_below or visible_right or visible_left:
                    self.debugPrint("--> visible!")
                    visible += 1
        return visible

    def calculate_tree_score(self, row, col):
        tree = self.trees[row,col]
        above = 0
        for i in range(row-1, -1, -1):
            above += 1
            if self.trees[i,col] >= tree:
                break
        below = 0
        for i in range(row+1, self.num_row):
            below += 1
            if self.trees[i,col] >= tree:
                break
        left = 0
        for i in range(col-1, -1, -1):
            left += 1
            if self.trees[row,i] >= tree:
                break
        right = 0
        for i in range(col + 1, self.num_col):
            right += 1
            if self.trees[row,i] >= tree:
                break
        return above*below*left*right

    def get_best_tree_score(self):
        best = 0
        for row in range(0,self.num_row):
            for col in range(0, self.num_col):
                score = self.calculate_tree_score(row, col)
                self.debugPrint("Tree {},{}:{} scores{}".format(
                    row, col, self.trees[row,col], score
                ))
                if score > best:
                    self.debugPrint("--> new best")
                    best = score
        return best


if __name__ == '__main__':
    parser = getBasicArgparser('day 8')

    args = parser.parse_args()

    day8 = Day8(args.i, args.v)
    print("Part 1: {} trees are visible".format(day8.get_num_visible_trees()))
    print("Part 2: best tree has a score of {}".format(day8.get_best_tree_score()))