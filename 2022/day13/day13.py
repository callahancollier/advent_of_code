#!/usr/bin/env python3

import csv
import sys
import ast
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

class Pair:
    def __init__(self, comp_a, comp_b):
        self.left = comp_a
        self.right = comp_b

    def compare(self):
        start_type_a = type(self.left)
        start_type_b = type(self.right)
        logging.debug(" Compare %s vs %s", self.left, self.right)
        if start_type_a == int and start_type_b == int:
            if self.left == self.right:
                logging.debug("   return None, int comp")
                return None
            else:
                logging.debug("   return self.left < self.right")
                return self.left < self.right

        if start_type_a == int:
            self.left = [self.left]
        if start_type_b == int:
            self.right = [self.right]

        assert(type(self.left) == list and type(self.right) == list)

        for i in range(len(self.left)):
            if i > len(self.right)-1:
                # left is longer than right
                logging.debug("   return False, left is longer")
                return False
            next = Pair(self.left[i], self.right[i]).compare()
            if next is not None:
                logging.debug("   return next")
                return next
        if len(self.right) > len(self.left):
            logging.debug("   return True")
            return True
        # same length, keep going
        logging.debug("   return None, same len")
        return None


class Item:
    def __init__(self, item):
        self.item = item

    def __lt__(self, other):
        return Pair(self.item, other.item).compare()

    def __str__(self):
        return "{}".format(self.item)


DIV_1 = Item([[2]])
DIV_2 = Item([[6]])


class Day13(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.pairs = self.parse_file()
        logging.debug("Parsed pairs:")
        for p in self.pairs:
            logging.debug("  %s", p.left)
            logging.debug("  %s", p.right)
            logging.debug("--")

    def parse_file(self):
        pairs = []
        first_item = None
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if first_item is None:
                    first_item = self.parse_into_list(line)
                else:
                    assert (first_item is not None)
                    pairs.append(Pair(first_item, self.parse_into_list(line)))
                    first_item = None
        return pairs

    def parse_into_list(self, line):
        # https://www.reddit.com/r/adventofcode/comments/zknx3n/2022_day_13_hello_eval_my_old_friend/
        # literal_eval is also not that bad
        return ast.literal_eval(line)

    def get_pairs_in_right_order(self):
        correct_pairs = []
        for i, pair in enumerate(self.pairs):
            logging.debug("=== Pair %d ===", i+1)
            logging.debug("Compare %s vs %s", pair.left, pair.right)
            if pair.compare():
                logging.debug("In right order!")
                correct_pairs.append(i+1) # elves index from 1
            else:
                logging.debug("In wrong order!")
        logging.debug("Correctly ordered pairs: %s", correct_pairs)
        return correct_pairs

    def sort_everything(self):
        full_list = [DIV_1, DIV_2]
        for pair in self.pairs:
            full_list.append(Item(pair.left))
            full_list.append(Item(pair.right))
        full_list.sort()
        logging.debug("Fully sorted list:")
        for x in full_list:
            logging.debug("   %s", x)
        return full_list

    def get_decoder_key(self):
        full = self.sort_everything()
        div1 = 0
        div2 = 0
        for i, item in enumerate(full):
            if item == DIV_1:
                div1 = i+1
            elif item == DIV_2:
                div2 = i+1
        return div1 * div2


if __name__ == '__main__':
    parser = getBasicArgparser('day 13')

    args = parser.parse_args()

    day13 = Day13(args.i, args.v)
    print("Part 1: sum of indices is {}".format(sum(day13.get_pairs_in_right_order())))

    print("Part 2: decoder key is {}".format(day13.get_decoder_key()))
