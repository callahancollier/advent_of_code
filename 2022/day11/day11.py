#!/usr/bin/env python3

import csv
import sys
import operator
import math
from collections import deque
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import DebugFunctions, BaseFunctions, getBasicArgparser

OPERATIONS = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.truediv
}

MONKEY_HDR = "Monkey "
START_HDR = "  Starting items: "
OPERATION_HDR = "  Operation: new = old "
TEST_HDR = "  Test: divisible by"
IF_TRUE = "    If true: throw to monkey "
IF_FALSE = "    If false: throw to monkey "


class Monkey(DebugFunctions):
    def __init__(self, debug, monkey_num, starting_items, operation, op_amt, test_div_by, true_throw, false_throw):
        super().__init__(debug)
        self.num = monkey_num
        self.items = deque(starting_items)
        self.op = OPERATIONS[operation]
        self.op_amt = op_amt
        self.test = test_div_by
        self.true_throw = true_throw
        self.false_throw = false_throw

        self.num_inspected = 0

    def inspect_and_throw(self, relief_val, common_denominator):
        thrown_items = []
        self.debugPrint("Monkey {}:".format(self.num))
        while len(self.items) != 0:
            item = self.items.popleft()
            val = item
            if self.op_amt != 'old':
                val = int(self.op_amt)
            inspected_and_bored = (self.op(item, val) // relief_val) % common_denominator
            self.debugPrint("  Inspect item {}, result {}".format(item, inspected_and_bored))
            if inspected_and_bored % self.test == 0:
                self.debugPrint("  Level {} is divisible by {}".format(inspected_and_bored, self.test))
                self.debugPrint("  Throw to monkey {}".format(self.true_throw))
                thrown_items.append((self.true_throw, inspected_and_bored))
            else:
                self.debugPrint("  Level {} is not divisible by {}".format(inspected_and_bored, self.test))
                self.debugPrint("  Throw to monkey {}".format(self.false_throw))
                thrown_items.append((self.false_throw, inspected_and_bored))
            self.num_inspected += 1
        return thrown_items

    def accept_throw(self, item):
        self.items.append(item)


class Day11(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.monkeys = None
        self.relief_val = 0
        self.common_denominator = 0

    def parse_file(self):
        monkeys = []
        current_monkey = 0
        items = deque()
        op_str = '*'
        op_val = 0
        test_div = 1
        true_throw = 0
        false_throw = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if line.startswith(MONKEY_HDR):
                    current_monkey = int(line.split(MONKEY_HDR)[1].split(':')[0])
                    if len(monkeys) != current_monkey:
                        print("Error: Monkeys are in unexpected order! {}".format(line))
                        sys.exit(1)
                elif line.startswith(START_HDR):
                    items = deque([int(x) for x in line.split(START_HDR)[1].split(', ')])
                elif line.startswith(OPERATION_HDR):
                    line = line.split(OPERATION_HDR)[1].split(' ')
                    op_str = line[0]
                    op_val = line[1]
                elif line.startswith(TEST_HDR):
                    test_div = int(line.split(TEST_HDR)[1])
                elif line.startswith(IF_TRUE):
                    true_throw = int(line.split(IF_TRUE)[1])
                elif line.startswith(IF_FALSE):
                    false_throw = int(line.split(IF_FALSE)[1])
                    monkeys.append(Monkey(self.debug, current_monkey, items, op_str, op_val, test_div, true_throw, false_throw))
                else:
                    print("Error: Unexpected line: {}".format(line))
                    sys.exit(1)
        return monkeys

    def get_test_mod(self):
        result = 1
        for monkey in self.monkeys:
            result = result * monkey.test
        return result

    def setup(self, relief_value):
        self.monkeys = self.parse_file()
        self.relief_val = relief_value
        self.common_denominator = self.get_test_mod()

    def get_monkey_business(self, num_rounds):
        for i in range(num_rounds):
            self.debugPrint("=== Round {} ===".format(i))
            self.do_round()
        inspected = []
        for m in range(len(self.monkeys)):
            monkey = self.monkeys[m]
            self.debugPrint("Monkey {} inspected {} times".format(m, monkey.num_inspected))
            inspected.append(monkey.num_inspected)
        inspected.sort()
        return inspected[-1] * inspected[-2]

    def do_round(self):
        for m in range(len(self.monkeys)):
            thrown = self.monkeys[m].inspect_and_throw(self.relief_val, self.common_denominator)
            for monkey, item in thrown:
                self.monkeys[monkey].accept_throw(item)


if __name__ == '__main__':
    parser = getBasicArgparser('day 11')
    parser.add_argument('-t', action='store_true', help='only run part 2')

    args = parser.parse_args()

    day11 = Day11(args.i, args.v)
    if not args.t:
        day11.setup(3)
        print("Part 1: monkey business is {}".format(day11.get_monkey_business(20)))

    day11.setup(1)
    print("Part 2: monkey business is {}".format(day11.get_monkey_business(10000)))
