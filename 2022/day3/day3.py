#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

GROUP_SIZE = 3

class Rucksack:
    def __init__(self, contents):
        if len(contents) % 2 != 0:
            raise ValueError("Length of contents must be even!")
        if len(contents) == 0:
            raise ValueError("Empty contents!")
        self.compartments = [contents[:int(len(contents)/2)],
                             contents[int(len(contents)/2):]]

    def get_common_items(self):
        common = set(self.compartments[0]).intersection(set(self.compartments[1]))
        return ''.join(common)

    def get_items(self):
        return self.compartments[0] + self.compartments[1]


class Group:
    def __init__(self, group_rucksacks):
        if len(group_rucksacks) != GROUP_SIZE:
            raise ValueError("There should be {} in each group".format(GROUP_SIZE))
        self.rucksacks = group_rucksacks

    def get_common_item(self):
        common = set(self.rucksacks[0].get_items())
        for other in self.rucksacks[1:]:
            common = common & set(other.get_items())
        return ''.join(common)


class Day3(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.rucksacks = self.parse_file()
        self.priority_list = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self.elf_groups = self.group_elves(self.rucksacks)

    def parse_file(self):
        sacks = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                sacks.append(Rucksack(line))
        return sacks

    def group_elves(self, rucksacks):
        if len(rucksacks) % GROUP_SIZE != 0:
            raise ValueError("Elves do not sort into groups of {}".format(GROUP_SIZE))
        groups = []
        for i in range(0, int(len(rucksacks)/GROUP_SIZE)):
            group = rucksacks[i*GROUP_SIZE:i*GROUP_SIZE+GROUP_SIZE]
            if len(group) != GROUP_SIZE:
                raise ValueError("Callahan can't do math, {}".format(group))
            groups.append(Group(group))
        return groups

    def get_priorities_both_compartments(self):
        items = []
        for rucksack in self.rucksacks:
            items.append(rucksack.get_common_items())
        self.debugPrint("Compartment common items: {}".format(items))
        return self.get_sum_of_priorities(items)

    def get_sum_of_priorities(self, items):
        priority_sum = 0
        for item in items:
            self.debugPrint("Item {}".format(item))
            priority_sum += self.priority_list.index(item)+1
        return priority_sum

    def get_priorities_of_groups(self):
        items = []
        for group in self.elf_groups:
            items.append(group.get_common_item())
        self.debugPrint("Group items: {}".format(items))
        return self.get_sum_of_priorities(items)


if __name__ == '__main__':
    parser = getBasicArgparser('day 3')

    args = parser.parse_args()

    day3 = Day3(args.i, args.v)

    print("Part 1: Sum of priorities of items in both compartments is {}".format(day3.get_priorities_both_compartments()))

    print("Part 2: Sum of priorities of each group item is {}".format(day3.get_priorities_of_groups()))