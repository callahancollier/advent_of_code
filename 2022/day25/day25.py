#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day25(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.snafu_strings = self.parse_file()

    def parse_file(self):
        snafus = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                snafus.append(line)
        return snafus

    def snafu_to_decimal(self, snafu):
        place = 1
        decimal = 0
        for c in reversed(snafu):
            if c == '-':
                decimal = decimal + (-1*place)
            elif c == '=':
                decimal = decimal + (-2*place)
            elif c.isnumeric() and int(c) < 3:
                decimal = decimal + (int(c)*place)
            else:
                logging.error("Invalid digit %s", c)
                sys.exit(1)
            place *= 5
        return decimal

    def decimal_to_snafu(self, decimal):
        snafu = ''
        logging.debug("Convert %s", decimal)
        while decimal != 0:
            remainder = decimal % 5
            decimal = decimal // 5
            if remainder == 4:
                snafu = '-' + snafu
                decimal += 1
            elif remainder == 3:
                snafu = '=' + snafu
                decimal += 1
            else:
                snafu = str(remainder) + snafu
            logging.debug("dec %s, rem %s, snafu %s", decimal, remainder, snafu)
        return snafu

    def snafu_sum(self):
        full_sum = sum([self.snafu_to_decimal(x) for x in self.snafu_strings])
        return self.decimal_to_snafu(full_sum)


if __name__ == '__main__':
    parser = getBasicArgparser('day 25')
    parser.add_argument('-s', type=str, help='convert one snafu to decimal', default=None)
    parser.add_argument('-d', type=int, help='convert one decimal to snafu', default=None)

    args = parser.parse_args()

    day25 = Day25(args.i, args.v)

    if args.s is not None:
        print("{} in snafu is {} in decimal".format(args.s, day25.snafu_to_decimal(args.s)))
    if args.d is not None:
        print("{} in decimal is {} in snafu".format(args.d, day25.decimal_to_snafu(args.d)))

    print("Part 1: SNAFU number for Bob's console is {}".format(day25.snafu_sum()))
