#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

SOP_LEN = 4
SOM_LEN = 14


class Day6(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.databuffer = self.parse_file()

    def parse_file(self):
        data = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                data.append(line)
        return data

    def get_index_of_sop(self, num_unique):
        results = []
        for data in self.databuffer:
            self.debugPrint("Data: {}".format(data))
            for i in range(0, len(data)):
                if self.all_chars_different(i, data[i:i+num_unique]):
                    self.debugPrint("SOP: {} at {}".format(data[i:i+num_unique], i))
                    results.append(i+num_unique)
                    break
        return results

    def all_chars_different(self, index, chars):
        self.debugPrint("   Test {}: {}".format(index, chars))
        self.debugPrint('     {} vs {}'.format(''.join(sorted(set(chars))), ''.join(sorted(chars))))
        if ''.join(sorted(set(chars))) == ''.join(sorted(chars)):
            return True
        return False


if __name__ == '__main__':
    parser = getBasicArgparser('day 6')

    args = parser.parse_args()

    day6 = Day6(args.i, args.v)

    for result in day6.get_index_of_sop(SOP_LEN):
        print("Part 1: first marker after character {}".format(result))
    print('----')

    for result in day6.get_index_of_sop(SOM_LEN):
        print("Part 2: first marker after character {}".format(result))
