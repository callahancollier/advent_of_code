#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day1(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.elves = None

        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            self.elves = []
            self.elves.append([])
            for line in reader:
                if len(line) == 0:
                    self.elves.append([])
                else:
                    line = line[0]
                    self.elves[-1].append(int(line))

    def get_calories_of_elf_with_most(self):
        max = 0
        for elf in self.elves:
            elfs_sum = 0
            for cal in elf:
                elfs_sum += cal
            if elfs_sum > max:
                max = elfs_sum
        return max

    def get_elf_calories(self):
        cals = []
        for elf in self.elves:
            sum = 0
            for cal in elf:
                sum += cal
            cals.append(sum)
        cals.sort()
        return cals


if __name__ == '__main__':
    parser = getBasicArgparser('day 1')

    args = parser.parse_args()

    day1 = Day1(args.i, args.v)

    print("Part 1: Elf with most calories has {} total".format(day1.get_calories_of_elf_with_most()))

    part2 = day1.get_elf_calories()
    print("Part 2: {} + {} + {} = {} total carried by the top three elves".format(
        part2[-1], part2[-2], part2[-3],
        part2[-1] + part2[-2] + part2[-3]
    ))