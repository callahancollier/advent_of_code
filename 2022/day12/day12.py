#!/usr/bin/env python3

import csv
import sys
import math
import numpy as np
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, ShortestPathCardinalBase


START = 'S'
START_VAL = 'a'
END = 'E'
END_VAL = 'z'


class HillTraverse(ShortestPathCardinalBase):
    def __init__(self, debug, start, end, hill):
        super().__init__(debug, start, end, hill)
        self.ranking = 'abcdefghijklmnopqrstuvwxyz'
        self.path = np.full_like(hill, '.')

    def get_rank(self, char):
        try:
            return self.ranking.index(char)
        except ValueError:
            print("Error: Can't find substring {}".format(char))
            sys.exit(1)

    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        neighbour_score = self.get_rank(self.array[neighbour_node.row, neighbour_node.col])
        current_rank = self.get_rank(self.array[current.row, current.col])
        if neighbour_score <= current_rank + 1:
            return current_g_score + 1
        return math.inf

    def tally_path(self, current, camefrom):
        total_length = 0
        while current in camefrom.keys():
            total_length += 1
            prev = camefrom[current]
            if prev.row < current.row:
                self.path[prev.row, prev.col] = 'v'
            elif prev.row > current.row:
                self.path[prev.row, prev.col] = '^'
            elif prev.col < current.col:
                self.path[prev.row, prev.col] = '>'
            elif prev.col > current.col:
                self.path[prev.row, prev.col] = '<'
            else:
                print("Error: Callahan made a mistake")
                sys.exit(1)
            current = camefrom[current]
        # self.path[self.start.row, self.start.col] = START
        # self.path[self.end.row, self.end.col] = END
        return total_length


class Day12(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.hill = None
        self.start = None
        self.end = None
        self.path = None
        self.parse_file()

    def parse_file(self):
        total = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                row = [c for c in line]
                if self.start is None:
                    try:
                        self.start = Point(len(total), row.index(START))
                        # self.debugPrint("START: {}: replace {} ({})".format(row, self.start, row[self.start.col]))
                        row[self.start.col] = START_VAL
                    except ValueError:
                        pass
                if self.end is None:
                    try:
                        self.end = Point(len(total), row.index(END))
                        # self.debugPrint("END: {}: replace {} ({})".format(row, self.end, row[self.end.col]))
                        row[self.end.col] = END_VAL
                    except ValueError:
                        pass
                total.append(row)
            self.hill = np.array(total)
            self.path = np.full_like(self.hill, '.')
            # self.debugPrint(self.hill)

    def get_shortest_path(self):
        traverser = HillTraverse(self.debug, self.start, self.end, self.hill)
        path_len = traverser.get_shortest_path()
        # self.debugPrint("Path\n{}".format(traverser.path))
        return path_len

    def get_shortest_from_any_start(self):
        possible_start_points = []
        shortest_path = math.inf
        shortest_start = Point(0, 0)
        for row in range(len(self.hill)):
            for col in range(len(self.hill[row])):
                if self.hill[row, col] == 'a':
                    possible_start_points.append(Point(row, col))
        print("Checking {} possible start points".format(len(possible_start_points)))
        for i in range(len(possible_start_points)):
            start = possible_start_points[i]
            if i % 10 == 0:
                print("Checked {} start points, current shortest {} from {}".format(i, shortest_path, shortest_start))
            traverser = HillTraverse(self.debug, start, self.end, self.hill)
            try:
                path_len = traverser.get_shortest_path()
            except RuntimeError:
                continue
            if path_len < shortest_path:
                shortest_path = path_len
                shortest_start = start
        return shortest_path


if __name__ == '__main__':
    parser = getBasicArgparser('day 12')

    args = parser.parse_args()

    day12 = Day12(args.i, args.v)
    print("Part 1: Shortest path is {} long".format(day12.get_shortest_path()))

    print("Part 2: Shortest path from any 'a' start point is: {}".format(day12.get_shortest_from_any_start()))
