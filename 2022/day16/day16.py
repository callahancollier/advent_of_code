#!/usr/bin/env python3

import csv
import sys
import logging
import tqdm
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, ShortestPathBase
from common.dijkstra import DijkstraBase, DijkstraNode


class Room(DijkstraNode):
    def __init__(self, name, tunnels, valve, opened=False):
        super().__init__(name)
        self.tunnels = tunnels
        self.valve = valve
        self.opened = opened

    def __str__(self):
        return "Valve {} has rate {}, tunnels leading to {}".format(
            self.id, self.valve, self.tunnels
        )
    __repr__ = __str__

    def __eq__(self, other):
        #TODO is checking just id enough?
        return self.id == other.id

    def get_neighbours(self):
        return self.tunnels


class DijkstraCave(DijkstraBase):
    def __init__(self, rooms):
        super().__init__(rooms)
        self.steps_remaining = 30

    def get_initial_unvisited_list(self):
        unvisited = []
        for n in self.nodes.keys():
            unvisited.append(self.nodes[n])
        return unvisited

    def get_length_between(self, fromNode, toNode):
        if not toNode.opened:
            return toNode.valve
        return 0

    def is_destination(self, node):
        if self.steps_remaining == 0:
            return True

    def possibly_update_path(self, current, neighbour):
        updated = super().possibly_update_path(current, neighbour)
        if updated:
            self.steps_remaining -= 1
        return updated




class AStarNode:
    def __init__(self, room, pressure, remaining_time):
        self.room = room
        self.room.opened = True
        self.pressure = pressure # this is negative so 'shortest' path becomes most pressure
        self.remaining_time = remaining_time

    def __str__(self):
        return "Node: room {}, pressure {}, {} remaining".format(
            self.room.name, self.pressure, self.remaining_time
        )
    __repr__ = __str__


class AStarCave(ShortestPathBase):
    def __init__(self, debug, start, tunnelmap, avg_pressure_release):
        super().__init__(debug, start)
        self.tunnelmap = tunnelmap
        self.avg_pressure_release = avg_pressure_release

    def manhattan(self, point):
        return point.pressure - self.avg_pressure_release*point.remaining_time

    def get_neighbours(self, node):
        neighbours = []
        for t in node.room.tunnels:
            room = self.tunnelmap[t]
            cost = 2
            pressure = room.valve
            if room.opened:
                cost -= 1
                pressure = 0
            neighbours.append(Node(room, node.pressure-pressure, node.remaining_time-cost))
        return neighbours

    def are_at_end(self, point):
        return point.remaining_time == 0

    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        logging.debug("   Get next g score from %s, current %s, neightbour %s pressure %s",
                      current.room.name, current_g_score, neighbour_node.room.name, neighbour_node.pressure)
        pressure = current.room.valve
        if neighbour_node.room.opened:
            pressure = 0
        return current_g_score - pressure

    def tally_path(self, current, camefrom):
        result = current.pressure
        while current in camefrom.keys():
            logging.error("%s came from %s", current.room.name, camefrom[current].room.name)
            current = camefrom[current]
        return result


class Day16(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.avg_pressure_release = 0
        self.tunnels = {}
        self.parse_file()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            pressure_sum = 0
            for line in reader:
                line = line[0].strip('Valve ')
                name = line.split(' ')[0]
                rate = int(line.split('has flow rate=')[1].split(';')[0])
                line = line.split('to valv')[1]
                tunnels = ''.join(line.split(' ')[1:])
                tunnels = tunnels.split(',')
                room = Room(name, tunnels, rate)
                self.tunnels[name] = room
                pressure_sum += rate
        self.avg_pressure_release = pressure_sum/len(self.tunnels.keys())
        for room in self.tunnels.items():
            logging.debug(room)

    def release_pressure(self):
        cave = DijkstraCave(self.tunnels)
        result = cave.find_best_path('AA')
        return result


if __name__ == '__main__':
    parser = getBasicArgparser('day 16')

    args = parser.parse_args()

    day16 = Day16(args.i, args.v)

    print("Part 1: Most pressure released is {}".format(day16.release_pressure()))
