#!/usr/bin/env python3

import csv
import sys
from enum import Enum
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

class OpponentMove(Enum):
    ROCK = 'A'
    PAPER = 'B'
    SCISSORS = 'C'

class YourMove(Enum):
    ROCK = 'X'
    PAPER = 'Y'
    SCISSORS = 'Z'

class Goal(Enum):
    LOSE = 'X'
    DRAW = 'Y'
    WIN = 'Z'

class Day2(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.part1moves = self.parse_file(1)
        self.part2moves = self.parse_file(2)
        self.shapeScores = {YourMove.ROCK:1, YourMove.PAPER:2, YourMove.SCISSORS:3}
        self.loses_to = {'ROCK':'SCISSORS',
                      'PAPER':'ROCK',
                      'SCISSORS':'PAPER'}
        self.wins_over = dict((v, k) for k, v in self.loses_to.items())

    def parse_file(self, part):
        moves = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                self.debugPrint("Read line: {}".format(line))
                if part == 1:
                    moves.append([OpponentMove(line[0]), YourMove(line[1])])
                elif part == 2:
                    moves.append([OpponentMove(line[0]), Goal(line[1])])
        return moves

    def getScore(self, you, them):
        score = self.shapeScores[you]
        if self.loses_to[you.name] == them.name:
            score += 6
        elif you.name == them.name:
            score += 3
        self.debugPrint("{} vs {}: {}".format(you, them, score))
        return score

    def calculate_part1_score(self):
        score = 0
        for round in self.part1moves:
            you = round[1]
            them = round[0]
            score += self.getScore(you, them)
        return score

    def calculate_part2_score(self):
        score = 0
        for round in self.part2moves:
            them = round[0]
            goal = round[1]
            you = None
            if goal == Goal.LOSE:
                you = YourMove[self.loses_to[them.name]]
            elif goal == Goal.WIN:
                you = YourMove[self.wins_over[them.name]]
            else:
                you = YourMove[them.name]
            score += self.getScore(you, them)
        return score


if __name__ == '__main__':
    parser = getBasicArgparser('day 2')

    args = parser.parse_args()

    day2 = Day2(args.i, args.v)
    print("Part 1: Your score will be {}".format(day2.calculate_part1_score()))
    print("Part 2: Your score will be {}".format(day2.calculate_part2_score()))