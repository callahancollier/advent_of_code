#!/usr/bin/env python3

import csv
import sys
import logging
from tqdm import tqdm
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Point3D:
    def __init__(self, coord_list):
        self.x = coord_list[0]
        self.y = coord_list[1]
        self.z = coord_list[2]
        if self.x < 0 or self.y < 0 or self.z < 0:
            logging.error("Negative indices are not supported")
            sys.exit(1)
        self.sides_open = 6

    def __str__(self):
        return '({},{},{})'.format(self.x, self.y, self.z)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_z(self):
        return self.z

    def reset(self):
        self.sides_open = 6


class Day18(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.cubes = None
        self.max_x = None
        self.max_y = None
        self.max_z = None
        self.parse_file()

    def parse_file(self):
        self.cubes = []
        self.max_x = 0
        self.max_y = 0
        self.max_z = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                cube = Point3D([int(p) for p in line.split(',')])
                self.cubes.append(cube)
                self.max_x = max(self.max_x, cube.x)
                self.max_y = max(self.max_y, cube.y)
                self.max_z = max(self.max_z, cube.z)

    def reset_cubes(self):
        for c in self.cubes:
            c.reset()

    def same_yz(self, cube, othercube):
        return othercube.y == cube.y and othercube.z == cube.z

    def same_xz(self, cube, othercube):
        return othercube.x == cube.x and othercube.z == cube.z

    def same_yx(self, cube, othercube):
        return othercube.y == cube.y and othercube.x == cube.x

    def compare_x(self, cube, othercube):
        if (othercube.x == cube.x + 1 or othercube.x == cube.x - 1) and self.same_yz(cube, othercube):
            logging.debug("   X match")
            return True

    def compare_y(self, cube, othercube):
        if (othercube.y == cube.y + 1 or othercube.y == cube.y - 1) and self.same_xz(cube, othercube):
            logging.debug("   Y match")
            return True

    def compare_z(self, cube, othercube):
        if (othercube.z == cube.z + 1 or othercube.z == cube.z - 1) and self.same_yx(cube, othercube):
            logging.debug("   Z match")
            return True

    def get_surface_area(self, cubes):
        surface_area = 0
        for cube in tqdm(cubes, desc="Surface area"):
            logging.debug("Check cube %s", cube)
            for othercube in cubes:
                logging.debug("  Against cube %s", othercube)
                if cube == othercube:
                    logging.debug("   Cubes are the same")
                    continue
                # cubes are touching if that face differs by 1 and the others are the same?
                if (othercube.x == cube.x + 1 or othercube.x == cube.x - 1) and self.same_yz(cube, othercube):
                    logging.debug("   X match")
                    cube.sides_open -= 1
                    continue
                if (othercube.y == cube.y + 1 or othercube.y == cube.y - 1) and self.same_xz(cube, othercube):
                    logging.debug("   Y match")
                    cube.sides_open -= 1
                    continue
                if (othercube.z == cube.z + 1 or othercube.z == cube.z - 1) and self.same_yx(cube, othercube):
                    logging.debug("   Z match")
                    cube.sides_open -= 1
                    continue
            surface_area += cube.sides_open

        return surface_area

    def get_surface_only_surface_area(self):
        air_cubes = []
        for x in tqdm(range(self.max_x+1), desc='x', position=0):
            for y in tqdm(range(self.max_y+1), desc='y', position=1, leave=False):
                for z in tqdm(range(self.max_z+1), desc='z', position=2, leave=False):
                    possible_cube = Point3D([x,y,z])
                    if possible_cube in self.cubes:
                        logging.debug("%s is a non-air cube", possible_cube)
                        continue
                    x_adjacent = [c.x for c in self.cubes if self.same_yz(possible_cube, c)]
                    x_adjacent.append(x)
                    x_adjacent.sort()
                    y_adjacent = [c.y for c in self.cubes if self.same_xz(possible_cube, c)]
                    y_adjacent.append(y)
                    y_adjacent.sort()
                    z_adjacent = [c.z for c in self.cubes if self.same_yx(possible_cube, c)]
                    z_adjacent.append(z)
                    z_adjacent.sort()
                    if x_adjacent[0] != x and x_adjacent[-1] != x:
                        if y_adjacent[0] != y and y_adjacent[-1] != y:
                            if z_adjacent[0] != z and z_adjacent[-1] != z:
                                logging.debug("Air cube %s", possible_cube)
                                air_cubes.append(possible_cube)
                                continue
                    logging.debug("%s is not an air cube", possible_cube)
        return self.get_surface_area(self.cubes) - self.get_surface_area(air_cubes)


if __name__ == '__main__':
    parser = getBasicArgparser('day 18')

    args = parser.parse_args()

    day18 = Day18(args.i, args.v)

    print("Part 1: Surface area of droplet is {}".format(day18.get_surface_area(day18.cubes)))

    day18.reset_cubes()

    print("Part 2: Surface are excluding enclosed of droplet is {}".format(day18.get_surface_only_surface_area()))