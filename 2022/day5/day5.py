#!/usr/bin/env python3

import csv
import sys
import numpy as np
from collections import deque
from copy import deepcopy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Move:
    def __init__(self, num, source, dest):
        self.num = int(num)
        self.source = int(source) - 1
        self.dest = int(dest) - 1


class Day5(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.crates = None
        self.moves = None
        self.parse_file()
        self.original_crates = deepcopy(self.crates)

    def parse_file(self):
        crates = None
        self.moves = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            stage = 'crates'
            for line in reader:
                if len(line) != 0:
                    line = line[0]
                if stage == 'crates':
                    if not line[1].isalpha() and line[1] != ' ':
                        stage = 'crate numbers'
                        continue
                    row = []
                    while len(line) != 0:
                        if line[0] == ' ':
                            line = line[1:]
                        row.append(line[1])
                        line = line[3:]
                    if crates is None:
                        crates = np.array(row)
                    else:
                        crates = np.vstack((crates, np.array(row)))
                elif stage == 'crate numbers':
                    stage = 'moves'
                elif stage == 'moves':
                    line = line.split(' ')
                    if line[0] != 'move' or line[2] != 'from' or line[4] != 'to':
                        print("ERROR: Malformed move line {}".format(line))
                        sys.exit(1)
                    self.moves.append(Move(line[1], line[3], line[5]))
        self.debugPrint("Read input")
        self.crates = []
        for i in range(0, len(crates[0])):
            self.crates.append(deque())
            data = ''.join(crates[:, i]).strip(' ')[::-1]
            for char in data:
                self.crates[-1].append(char)
        self.print_crates()

    def reset_crates(self):
        self.crates = deepcopy(self.original_crates)

    def print_crates(self, forcePrint=False):
        save = self.debug
        if forcePrint:
            self.debug = True
        self.debugPrint('crates:')
        for i in range(0, len(self.crates)):
            self.debugPrint('  {}:{}'.format(i+1, self.crates[i]))
        self.debug = save

    def move_crates_CrateMover_9000(self):
        for move in self.moves:
            self.debugPrint('Move {} from {} to {}'.format(
                move.num, move.source+1, move.dest+1
            ))
            for i in range(0, move.num):
                self.debugPrint('n={}:'.format(i))
                crate = self.remove_crate(move.source)
                self.place_crate(crate, move.dest)
                self.print_crates()

    def move_crates_CrateMover_9001(self):
        self.print_crates()
        for move in self.moves:
            self.debugPrint('Move {} from {} to {}'.format(
                move.num, move.source+1, move.dest+1
            ))
            crates = ''
            for i in range(0, move.num):
                self.debugPrint('n={}:'.format(i))
                crates += self.remove_crate(move.source)
            for crate in crates[::-1]:
                self.place_crate(crate, move.dest)
            self.print_crates()

    def remove_crate(self, source):
        return self.crates[source].pop()

    def place_crate(self, crate, dest):
        self.crates[dest].append(crate)

    def get_top_crates(self):
        result = ''
        for stack in self.crates:
            if len(stack) == 0:
                continue
            result += stack.pop()
        return result


if __name__ == '__main__':
    parser = getBasicArgparser('day 5')

    args = parser.parse_args()

    day5 = Day5(args.i, args.v)
    day5.move_crates_CrateMover_9000()
    print("Part 1: After rearrangement, top crates of the stacks are: {}".format(day5.get_top_crates()))

    day5.reset_crates()
    day5.move_crates_CrateMover_9001()
    print("Part 1: After rearrangement, top crates of the stacks are: {}".format(day5.get_top_crates()))
