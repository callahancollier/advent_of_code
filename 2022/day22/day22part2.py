#!/usr/bin/env python3

import csv
import sys
from enum import IntEnum
import numpy as np
from tqdm import tqdm
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point


class Dir(IntEnum):
    RIGHT = 0
    DOWN = 1
    LEFT = 2
    UP = 3

NUM_DIRECTIONS = 4

DIR_CHARS = {
    Dir.RIGHT: '>',
    Dir.LEFT: '<',
    Dir.UP: '^',
    Dir.DOWN: 'V'
}


class Row:
    def __init__(self, offset, tiles):
        self.offset = offset
        self.tiles = tiles


class Room:
    def __init__(self, rows, position, direction):
        self.rows = rows
        self.position = position
        self.direction = direction

    def turn_clockwise(self):
        self.direction = Dir((int(self.direction) + 1) % NUM_DIRECTIONS)

    def turn_counterclockwise(self):
        self.direction = Dir((int(self.direction) - 1) % NUM_DIRECTIONS)

    def move_forward(self, num_times):
        if self.direction == Dir.RIGHT:
            row = ''.join(self.rows[self.position.row])
            left_spaces = len(row) - len(row.lstrip(' '))
            row = row.strip(' ')
            idx = self.position.col - left_spaces
            for n in range(num_times):
                logging.debug("at %s move right: %s", idx, row[(idx+1)%len(row)])
                if row[(idx+1)%len(row)] != '#':
                    idx = (idx + 1) % len(row)
                else:
                    break
            self.position = Point(self.position.row, idx + left_spaces)
        elif self.direction == Dir.LEFT:
            row = ''.join(self.rows[self.position.row])
            left_spaces = len(row) - len(row.lstrip(' '))
            row = row.strip(' ')
            idx = self.position.col - left_spaces
            for n in range(num_times):
                logging.debug("at %s move left: %s", idx, row[(idx-1)%len(row)])
                if row[(idx-1)%len(row)] != '#':
                    idx = (idx - 1) % len(row)
                else:
                    break
            self.position = Point(self.position.row, idx + left_spaces)
        elif self.direction == Dir.DOWN:
            col = ''.join(self.rows[:, self.position.col])
            up_spaces = len(col) - len(col.lstrip(' '))
            col = col.strip(' ')
            logging.debug("spaces %s, col %s", up_spaces, col)
            idx = self.position.row - up_spaces
            for n in range(num_times):
                logging.debug("at %s move down: %s", idx, col[(idx + 1) % len(col)])
                if col[(idx+1)%len(col)] != '#':
                    idx = (idx + 1) % len(col)
                else:
                    break
            self.position = Point(idx + up_spaces, self.position.col)
        elif self.direction == Dir.UP:
            col = ''.join(self.rows[:, self.position.col])
            up_spaces = len(col) - len(col.lstrip(' '))
            col = col.strip(' ')
            idx = self.position.row - up_spaces
            for n in range(num_times):
                logging.debug("at %s move up: %s", idx, col[(idx - 1) % len(col)])
                if col[(idx-1)%len(col)] != '#':
                    idx = (idx - 1) % len(col)
                else:
                    break
            self.position = Point(idx + up_spaces, self.position.col)

    def get_final_password(self):
        row = self.position.row + 1
        col = self.position.col + 1
        facing = int(self.direction)
        return 1000*row + 4*col + facing

    def __str__(self):
        result = 'At {}'.format(self.position)
        dir_char = DIR_CHARS[self.direction]
        for r,row in enumerate(self.rows):
            row = ''.join(row)
            if r == self.position.row:
                row = row[:self.position.col] + dir_char + row[self.position.col+1:]
            if len(result) == 0:
                result = row
            else:
                result = result + '\n' + row
        return result


class Day22(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.room = None
        self.directions = None
        self.parse_file()

    def parse_file(self):
        rows = []
        max_len_line = 0
        phase = 'room'
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    phase = 'directions'
                    continue
                line = line[0]
                if phase == 'room':
                    max_len_line = max(max_len_line, len(line))
                    rows.append(list(line))
                else:
                    self.directions = self.split_directions(line)

        top_left = len(''.join(rows[0])) - len(''.join(rows[0]).lstrip(' '))
        while rows[0][top_left] == '#':
            top_left += 1

        for i in range(len(rows)):
            while len(rows[i]) < max_len_line:
                rows[i].append(' ')

        self.room = Room(np.array(rows), Point(0, top_left), Dir.RIGHT)

    def split_directions(self, dir_str):
        directions = []
        num = ''
        while len(dir_str) != 0:
            if dir_str[0].isnumeric():
                num += dir_str[0]
            else:
                if len(num) != 0:
                    directions.append(int(num))
                    num = ''

                if dir_str[0] == 'R':
                    directions.append('R')
                elif dir_str[0] == 'L':
                    directions.append('L')
                else:
                    logging.error("Unknown direction %s", dir_str[0])
                    sys.exit(1)
            dir_str = dir_str[1:]
        if len(num) != 0:
            directions.append(int(num))
        return directions

    def do_moves(self):
        logging.debug("At start:\n%s", self.room)
        for move in tqdm(self.directions):
            if move == 'R':
                self.room.turn_clockwise()
            elif move == 'L':
                self.room.turn_counterclockwise()
            else:
                self.room.move_forward(move)
            logging.debug("Move %s:\n%s", move, self.room)
        return self.room.get_final_password()


if __name__ == '__main__':
    parser = getBasicArgparser('day 22')

    args = parser.parse_args()

    day22 = Day22(args.i, args.v)

    print("Part 1: Final password is {}".format(day22.do_moves()))

# 162070 too low