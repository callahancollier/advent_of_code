#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point


ROCKS = [
    [
        '@@@@'
    ],
    [
        '.@.',
        '@@@',
        '.@.'
    ],
    [
        '..@',
        '..@',
        '@@@'
    ],
    [
        '@',
        '@',
        '@',
        '@'
    ],
    [
        '@@',
        '@@'
    ]
]
EMPTY = '.'
STOPPED = '#'
FLOOR = '-'
FALLING = '@'
WALL = '|'

ROOM_WIDTH = 7
EMPTY_ROW = WALL + ''.join(ROOM_WIDTH * [EMPTY]) + WALL


class Room:
    def __init__(self):
        self.room = [WALL + ''.join(ROOM_WIDTH * [FLOOR]) + WALL]
        for i in range(3):
            self.room.append(EMPTY_ROW)
        self.repeat_search = None
        self.repeat_period = None
        self.height_repeats_at = None

    def get(self, row, col):
        # The actual room is upside down to facilitate adding rows at the top, but always accessed as though it's not
        # logging.debug("Get row %s col %s: %s", row, col, self.room[-1-row][col])
        return self.room[-1-row][col]

    def set(self, row, col, val):
        # logging.debug("set row %s col %s to %s", row, col, val)
        old = self.room[-1-row]
        new = old[:col] + val + old[col+1:]
        self.room[-1-row] = new

    def get_height_of_tower(self):
        height = 0
        for r in self.room[1:]:
            if r.find(STOPPED) != -1:
                height += 1
            else:
                break
        logging.debug("Tower is %s tall", height)
        return height

    def get_tower_top_idx(self):
        start = len(self.room)-1
        while self.room[start].find(STOPPED) == -1:
            start -= 1
            if start == 0:
                break
        return start

    def get_top_of_tower(self, num_rows):
        start = self.get_tower_top_idx()
        return [self.room[start-x] for x in range(num_rows)]

    def add_height_to_room(self, rock):
        while self.room[-1] != EMPTY_ROW and self.room[-2] != EMPTY_ROW and self.room[-3] != EMPTY_ROW:
            self.room.append(EMPTY_ROW)
        while self.room[-4] == EMPTY_ROW:
            self.room = self.room[:-1]
        for r in rock:
            self.room.append(EMPTY_ROW)

    def __str__(self):
        #return '\n'.join(reversed(self.room))
        string = ''
        for i,line in enumerate(reversed(self.room)):
            string += line + str(len(self.room)-i) + '\n'
            # if i == 15:
            #     break
        return string
    __repr__ = __str__

    def detect_repeat(self, check_length):
        top = self.get_tower_top_idx()
        compare = self.room[top-check_length:top+1]
        repeat_a = 0
        repeat_search = None
        for i in range(top-check_length, 0, -1):
            if repeat_a == 0 and self.room[i-check_length:i+1] == compare:
                print("Detected repeat, i {}".format(i))
                repeat_a = i
                repeat_search = self.room[i-2*check_length:i-check_length+1]
                if len(repeat_search) != len(compare):
                    logging.error("Something has gone wrong!\nrepeat_end\n%s\ncompare\n%s",
                                  repeat_search, compare)
                    sys.exit(1)
            elif self.room[i-check_length:i+1] == compare:
                print("Detected repeat 2, i {}, a {}".format(i, repeat_a))
                print("Detected end of repeat at {}, tower height is {}".format(i, top))
                self.repeat_search = repeat_search
                self.repeat_period = repeat_a - i
                self.height_repeats_at = i - check_length
                return repeat_a-i, i
        return -1, -1


class FallingRock:
    def __init__(self, rocktype, rock_start_index):
        self.rock = ROCKS[rocktype]
        self.top_left = rock_start_index
        self.rock_width = len(self.rock[0])
        self.rock_height = len(self.rock)

    def push_rock(self, room, direction):
        if direction == '>':
            d = +1
        elif direction == '<':
            d = -1
        else:
            raise ValueError("Unknown push dir {}".format(direction))
        self.erase_rock(room)
        if all([self.can_push(room, r, direction) for r in range(self.rock_height)]):
            self.top_left = Point(self.top_left.row, self.top_left.col + d)
        self.draw_rock(room)

    def can_push(self, room, row, direction):
        if direction == '>':
            logging.debug("rock: %s", self.rock)
            logging.debug("rock[row][-1]: %s", self.rock[row][-1])
            logging.debug("room to right of edge: %s", room.get(self.top_left.row+row, self.top_left.col+self.rock_width))
            logging.debug("room to right of rightmost: %s", room.get(self.top_left.row+row, self.top_left.col + self.rightmost_rock_col(row)))
            if self.rock[row][-1] != EMPTY and room.get(self.top_left.row+row, self.top_left.col+self.rock_width) != EMPTY:
                logging.debug("Can't push right, row %s rock at edge", row)
                return False
            if room.get(self.top_left.row+row, self.top_left.col + self.rightmost_rock_col(row)+1) != EMPTY:
                logging.debug("Can't push right, row %s rock at %s", row, self.rightmost_rock_col(row))
                return False
        elif direction == '<':
            logging.debug("rock: %s", self.rock)
            logging.debug("rock[row][0]: %s", self.rock[row][0])
            logging.debug("room to left of edge: %s", room.get(self.top_left.row+row, self.top_left.col-1))
            logging.debug("room to left of leftmost: %s", room.get(self.top_left.row+row, self.top_left.col + self.leftmost_rock_col(row)))
            if self.rock[row][0] != EMPTY and room.get(self.top_left.row+row, self.top_left.col-1) != EMPTY:
                logging.debug("Can't push left, row %s rock at edge", row)
                return False
            if room.get(self.top_left.row+row, self.top_left.col + self.leftmost_rock_col(row)-1) != EMPTY:
                logging.debug("Can't push left, row %s rock at %s", row, self.leftmost_rock_col(row))
                return False
        else:
            raise ValueError('Unknown dir {}'.format(direction))
        return True

    def leftmost_rock_col(self, row):
        col = 0
        while self.rock[row][col] == EMPTY:
            col += 1
        logging.debug("Leftmost for row %s: %s", row, col)
        return col

    def rightmost_rock_col(self, row):
        col = self.rock_width-1
        while self.rock[row][col] == EMPTY:
            col -= 1
        return col

    def erase_rock(self, room):
        for r in range(self.rock_height):
            for c in range(self.rock_width):
                if self.rock[r][c] == FALLING:
                    room.set(self.top_left.row + r, self.top_left.col + c, EMPTY)

    def draw_rock(self, room, falling=True):
        if falling:
            draw = FALLING
        else:
            draw = STOPPED
        for r in range(self.rock_height):
            for c in range(self.rock_width):
                if self.rock[r][c] != EMPTY:
                    room.set(self.top_left.row + r, self.top_left.col + c, draw)

    def fall_rock(self, room):
        fell = False
        if all([self.can_drop(room, c) for c in range(self.rock_width)]):
            fell = True
            self.erase_rock(room)
            self.top_left = Point(self.top_left.row + 1, self.top_left.col)
        self.draw_rock(room, fell)
        return fell

    def bottommost_rock_row(self, col):
        row = self.rock_height-1
        while self.rock[row][col] == EMPTY:
            row -= 1
        return row

    def can_drop(self, room, col):
        if self.rock[-1][col] != EMPTY and room.get(self.top_left.row+self.rock_height, self.top_left.col + col) != EMPTY:
            return False
        if room.get(self.top_left.row+self.bottommost_rock_row(col)+1, self.top_left.col+col) != EMPTY:
            return False
        return True


class Day17(BaseFunctions):
    def __init__(self, input_file, enable_debug, enable_pause):
        super().__init__(input_file, enable_debug)
        self.enable_pause = enable_pause
        self.jets = self.parse_file()
        self.room = Room()
        self.jet_idx = 0
        self.rock_idx = 0
        self.heights = []
        self.height_to_index = {}
        self.index_to_height = {}

    def parse_file(self):
        jets = ''
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if len(jets) != 0:
                    logging.error("Unexpected number of lines found!")
                jets = line
        return jets

    def inc_rock_idx(self):
        self.rock_idx = (self.rock_idx + 1) % len(ROCKS)

    def inc_jet_idx(self):
        self.jet_idx = (self.jet_idx + 1) % len(self.jets)

    def start_new_rock(self):
        rock = FallingRock(self.rock_idx, Point(0, 3))
        new_rock = ROCKS[self.rock_idx]
        self.inc_rock_idx()
        self.room.add_height_to_room(new_rock)
        rock.draw_rock(self.room)
        return rock

    def build_tower(self, num_rocks, repeat_length):
        og_num_rocks = num_rocks
        for i in range(num_rocks):
            ht = -1
            if self.debug:
                ht = self.room.get_height_of_tower()
            rock = self.start_new_rock()

            dropping = True
            logging.debug("Dropping new rock %s, tower is %s room:\n%s", i, ht, self.room)
            if self.enable_pause or (self.debug and i == num_rocks - 1):
                input()
            while dropping:
                d = self.jets[self.jet_idx]
                rock.push_rock(self.room, d)
                logging.debug("Push rock %s %s:\n%s", i, d, self.room)
                if self.debug and i == num_rocks - 1:
                    input()
                self.inc_jet_idx()
                dropping = rock.fall_rock(self.room)
                logging.debug("Drop rock %s:\n%s", i, self.room)
                if self.enable_pause or (self.debug and i == num_rocks - 1):
                    input()
            if i % 10000 == 0:
                print("Done row {} ({}%)".format(i, (i/num_rocks)*100))
            self.index_to_height[i] = self.room.get_height_of_tower()
            self.height_to_index[self.index_to_height[i]] = i
            self.heights.append(self.room.get_height_of_tower())

            repeat, height_starts_repeat_at = self.room.detect_repeat(repeat_length)
            if repeat != -1:
                logging.warning("Returned %s,%s after %s blocks", repeat, height_starts_repeat_at, i+1)
                dropped_at_start_repeat = self.height_to_index[height_starts_repeat_at]+1 # i starts at zero
                blocks_repeat = (i+1 - dropped_at_start_repeat)/2
                logging.warning("Blocks repeat %s", blocks_repeat)
                blocks_repeat = int(blocks_repeat)
                num_repeats = (num_rocks - dropped_at_start_repeat) // blocks_repeat
                remainder = num_rocks - dropped_at_start_repeat - num_repeats*blocks_repeat
                logging.warning("starts repeat at %s (%s blocks), repeat %s (%s blocks), %s repeats rem %s",
                                height_starts_repeat_at, dropped_at_start_repeat,
                                repeat, blocks_repeat,
                                num_repeats, remainder)
                extra = self.heights[dropped_at_start_repeat+remainder] - self.heights[dropped_at_start_repeat]
                # extra = self.heights[dropped_at_start_repeat+remainder+1] - self.heights[dropped_at_start_repeat+1]
                height_after_repeats = height_starts_repeat_at + num_repeats*repeat + extra
                # print("{}".format(self.room))
                return height_after_repeats
        #print("{}".format(self.room))
        return self.room.get_height_of_tower() * og_num_rocks/num_rocks


if __name__ == '__main__':
    parser = getBasicArgparser('day 17')
    parser.add_argument('-l', type=int, help='number of rocks to drop', default=2022)
    parser.add_argument('-r', type=int, help='length to use to detect repeats', default=100)
    parser.add_argument('-p', action='store_true', help='enable debug pauses')

    args = parser.parse_args()

    day17 = Day17(args.i, args.v, args.p)

    # For the example, use repeat length 10 to 30, for input, use 100
    # Part 1, drop 2022 rocks, part 2, drop 1000000000000
    # TODO: the example has an off-by-one error for part 1, but part 2 and the actual input works (rounding?)
    print("After {} rocks, the tower is {} high".format(args.l, day17.build_tower(args.l, args.r)))

# 3100 is too low
# 3159 is too high
# 3113 is too low
# 3136 is wrong

# part 2:
# 1542941176479 too low