#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, ShortestPathCardinalBase

WALL = '#'
EMPTY = '.'


class Blizzard:
    def __init__(self, direction, position):
        self.direction = direction
        self.position = position

    def move(self, wrap_row, wrap_col):
        if self.direction == '<':
            self.position = Point(((self.position.row-1-1)%wrap_row)+1, self.position.col)
        elif self.direction == '>':
            self.position = Point(((self.position.row-1+1)%wrap_row)+1, self.position.col)
        elif self.direction == '^':
            self.position = Point(self.position.row, ((self.position.col-1-1)%wrap_col)+1)
        elif self.direction == 'v':
            self.position = Point(self.position.row, ((self.position.col-1+1)%wrap_col)+1)


class PathPoint(Point):
    time: int

    def __str__(self):
        return "({}},{})@{}".format(self.row, self.col, self.time)

    def __eq__(self, other):
        if not isinstance(other, Point):
            return NotImplemented
        return self.row == other.row and self.col == other.col and self.time == other.time


class Pathfinder(ShortestPathCardinalBase):
    def __init__(self, debug, start, end, valley, blizzards):
        super().__init__(debug, start, end, valley[0])
        self.valley = valley
        self.blizzards = blizzards

    def get_valley_at_time(self, time):
        while len(self.valley) < time+1:
            self.valley.append(self.get_next_valley())
        return self.valley[time]

    def get_next_valley(self):
        points = {}
        for storm in self.blizzards:
            storm.move(self.num_row-2, self.num_col-2)
            points[storm.position] = storm.direction

        prev = self.valley[-1]
        next = []
        for row in range(len(prev)):
            next_row = ''
            for col in range(len(prev[row])):
                try:
                    next_row += points[Point(row, col)]
                except KeyError:
                    if prev[row][col] == WALL:
                        next_row += WALL
                    else:
                        next_row += EMPTY
            next.append(next_row)
        self.valley.append(next)

    def get_neighbours(self, node):
        valley = self.get_valley_at_time(node.time)
        neighbours = []
        for adj in [-1, 1]:
            neighbourA = PathPoint(node.row + adj, node.col, node.time+1)
            neighbourB = PathPoint(node.row, node.col + adj, node.time+1)
            for neighbour in [neighbourA, neighbourB]:
                if neighbour.row < 0 or neighbour.row >= self.num_row:
                    continue
                if neighbour.col < 0 or neighbour.col >= self.num_col:
                    continue
                neighbours.append(neighbour)
        return neighbours


class Day24(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.parse_file()

    def parse_file(self):
        valley = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                valley.append(line)
        return valley


if __name__ == '__main__':
    parser = getBasicArgparser('day 24')

    args = parser.parse_args()

    day24 = Day24(args.i, args.v)
