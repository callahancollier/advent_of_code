#!/usr/bin/env python3

import csv
import sys
import logging
from tqdm import tqdm
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class DoublyLinkedListNode:
    def __init__(self, value, prev, next):
        self.value = value
        self.prev = prev
        self.next = next

    def __str__(self):
        if self.value != 0:
            logging.error("Printing starting with a non-zero node!")
            sys.exit(1)
        result = str(self.value)
        curr = self.next
        while curr.value != 0:
            result += ', ' + str(curr.value)
            curr = curr.next
        return result



class Day20(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.nodes = None
        self.zero_node = None
        self.parse_file()

    def parse_file(self):
        self.nodes = []
        self.zero_node = None
        head = None
        curr = None
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = int(line[0])
                if head is None:
                    head = DoublyLinkedListNode(line, None, None)
                    self.nodes.append(head)
                    curr = head
                else:
                    if curr.next is not None:
                        logging.error("Something has gone wrong with creating the list")
                        sys.exit(1)
                    node = DoublyLinkedListNode(line, curr, None)
                    self.nodes.append(node)
                    curr.next = node
                    curr = node
                    if curr.value == 0:
                        self.zero_node = curr
        set_len = head
        # Close the circle
        curr.next = head
        head.prev = curr
        if self.zero_node is None:
            logging.error("Did not find the 0 item!")
            sys.exit(1)

    def mix_file(self):
        logging.debug("Initial arrangement:\n%s", self.zero_node)
        for node in tqdm(self.nodes):
            if node.value == 0:
                continue
            logging.debug("\n%s moves:", node.value)
            logging.debug("  Before:    %s", self.zero_node)
            before = self.remove_node(node)
            logging.debug("  Remove %s: %s", node.value, self.zero_node)
            if node.value > 0:
                for i in range((node.value) % (len(self.nodes)-1)):
                    before = before.next
            else:
                for i in range(abs(node.value) % (len(self.nodes)-1)):
                    before = before.prev
            self.insert_node(node, before)
            logging.debug("  Insert %s: %s", node.value % (len(self.nodes)-1), self.zero_node)
            logging.debug(self.zero_node)

    def apply_decryption_key(self, key):
        curr = self.zero_node.next
        while curr.value != 0:
            curr.value = curr.value * key
            curr = curr.next

    def remove_node(self, node):
        before = node.prev
        after = node.next
        before.next = after
        after.prev = before
        return before

    def insert_node(self, node, after_this):
        after_node = after_this.next

        after_this.next = node
        node.prev = after_this
        after_node.prev = node
        node.next = after_node

    def get_grove_coord(self, num_loops=1):
        for i in range(num_loops):
            self.mix_file()
        coords = []
        ptr = self.zero_node
        for i in range(3):
            for j in range(1000):
                ptr = ptr.next
            logging.debug("%sth after zero is %s", (i+1)*1000, ptr.value)
            coords.append(ptr.value)
        return sum(coords)


if __name__ == '__main__':
    parser = getBasicArgparser('day 20')

    args = parser.parse_args()

    day20 = Day20(args.i, args.v)

    print("Part 1: Grove coordinates summed are {}".format(day20.get_grove_coord()))

    # Reset
    day20.parse_file()
    day20.apply_decryption_key(811589153)
    print("Part 2: Grove coordinates summed are {}".format(day20.get_grove_coord(10)))
