#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

NOOP = "noop"
ADDX = "addx "
CRT_WIDTH = 40
LIT = '#'
DARK = '.'


class Day10(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.x_register = 1
        self.cycles = self.parse_file()

    def parse_file(self):
        cycles = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if line.startswith(NOOP):
                    # 1 cycle no change
                    cycles.append(self.x_register)
                elif line.startswith(ADDX):
                    # 2 cycles, add value after
                    cycles.append(self.x_register)
                    cycles.append(self.x_register)
                    line = line.split(' ')
                    self.x_register += int(line[1])
                else:
                    print("ERROR: unknown instruction {}".format(line))
                    sys.exit(1)
            cycles.append(self.x_register)
        self.debugPrint(cycles)
        return cycles

    def signal_strength(self, cycle_num, reg_val):
        return cycle_num * reg_val

    def get_signal_strengths_at_every(self, start_cycle, every_n_cycles):
        cycle_to_check = start_cycle
        sum_of_sigs = 0
        while cycle_to_check < len(self.cycles):
            new_strength = self.signal_strength(cycle_to_check, self.cycles[cycle_to_check-1])
            self.debugPrint("Cycle {}: signal strength is {}".format(cycle_to_check,new_strength))
            sum_of_sigs += new_strength
            cycle_to_check += every_n_cycles
        return sum_of_sigs

    def draw_crt(self):
        row = ''
        for i in range(len(self.cycles)):
            crt_pos = i % CRT_WIDTH
            if crt_pos == 0:
                print(row)
                row = ''
            reg = self.cycles[i]
            sprite_pos = [reg-1, reg, reg+1]
            if crt_pos in sprite_pos:
                row += LIT
            else:
                row += DARK
            self.debugPrint("Cycle {}: CRT draws pixel {}, sprite is at {}".format(
                i+1, crt_pos, sprite_pos
            ))


if __name__ == '__main__':
    parser = getBasicArgparser('day 10')

    args = parser.parse_args()

    day10 = Day10(args.i, args.v)

    check_cycles = []
    print("Part 1: Sum of signal strengths is {}".format(day10.get_signal_strengths_at_every(20, 40)))
    print("Part 2:")
    day10.draw_crt()