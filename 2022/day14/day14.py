#!/usr/bin/env python3

import csv
import sys
import math
import logging
import numpy as np
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point

# row is 'y' and col is 'x'
SAND_SOURCE = Point(0, 500)
PATH_DIV = ' -> '


class Day14(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        paths, row_range, col_range = self.parse_file()
        self.offset = Point(row_range[0], col_range[0])
        self.lowest_row = row_range[1]
        self.floor_is_infinite = False
        self.cave = self.create_cave(paths, row_range, col_range)

    def parse_file(self):
        paths = []
        row_range = [math.inf, 0]
        col_range = [math.inf, 0]
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                line = line.split(PATH_DIV)
                path = []
                for part in line:
                    part = [int(x) for x in part.split(',')]
                    row_range[0] = min(row_range[0], part[1], SAND_SOURCE.row)
                    row_range[1] = max(row_range[1], part[1], SAND_SOURCE.row)
                    col_range[0] = min(col_range[0], part[0], SAND_SOURCE.col)
                    col_range[1] = max(col_range[1], part[0], SAND_SOURCE.col)
                    path.append(Point(part[1], part[0]))
                paths.append(path)
        logging.debug("Cave row from %s to %s", row_range[0], row_range[1])
        logging.debug("Cave col from %s to %s", col_range[0], col_range[1])
        return paths, row_range, col_range

    def create_cave(self, paths, row_range, col_range):
        cave = np.full((row_range[1]-row_range[0]+1, col_range[1]-col_range[0]+1), '.')
        for path in paths:
            for i in range(len(path)-1):
                start = path[i]
                end = path[i+1]
                logging.debug("%s: draw from %s to %s", i, start, end)
                if start.row == end.row:
                    if start.col > end.col:
                        start, end = end, start
                    for col in range(start.col, end.col+1):
                        cave = self.set_cave(cave, Point(start.row, col), '#')
                elif start.col == end.col:
                    if start.row > end.row:
                        start, end = end, start
                    for row in range(start.row, end.row+1):
                        cave = self.set_cave(cave, Point(row, start.col), '#')
                else:
                    logging.error("Unsupported path: from %s to %s", start, end)
                    sys.exit(1)
        cave = self.set_cave(cave, SAND_SOURCE, '+')
        logging.debug("Cave at start:")
        logging.debug(cave)
        return cave

    def set_cave(self, cave, point, val):
        logging.debug("Set %s to %s", point, val)
        cave = self.expand_cave(cave, point)
        if point.row >= self.lowest_row and self.floor_is_infinite:
            return
        cave[point.row - self.offset.row, point.col - self.offset.col] = val
        return cave

    def expand_cave(self, cave, target_point):
        if target_point.col < self.offset.col:
            logging.debug("Expand cave left, before:")
            self.print_cave(cave)
            cave = np.hstack([np.full_like(cave[:,0], '.').reshape(-1,1), cave])
            if self.floor_is_infinite:
                cave[-1,0] = '#'
            logging.debug("After:")
            self.print_cave(cave)
            self.offset = Point(self.offset.row, self.offset.col-1)
        if target_point.col > self.offset.col + len(cave[0])-1:
            logging.debug("Expand cave right, before:")
            self.print_cave(cave)
            cave = np.hstack([cave, np.full_like(cave[:,0].reshape(-1,1), '.')])
            if self.floor_is_infinite:
                cave[-1,-1] = '#'
            logging.debug("After:")
            self.print_cave(cave)
        return cave

    def get_cave(self, cave, point):
        try:
            return cave[point.row - self.offset.row, point.col - self.offset.col]
        except IndexError:
            logging.debug("Tried to access %s which is outside range of cave", point)
            if point.row >= self.lowest_row and self.floor_is_infinite:
                return '#'
            return '.'

    def clear_cave(self):
        for row in range(len(self.cave)):
            for col in range(len(self.cave[row])):
                if self.cave[row, col] == 'o':
                    self.cave[row, col] = '.'

    def add_cave_floor(self):
        # self.cave = np.hstack([self.cave, np.full_like(self.cave[:,0].reshape(-1,1), '.')])
        # self.cave = np.hstack([np.full_like(self.cave[:,0], '.').reshape(-1,1), self.cave])
        self.cave = np.vstack([self.cave, np.full_like(self.cave[0], '.')])
        self.cave = np.vstack([self.cave, np.full_like(self.cave[0], '#')])
        self.lowest_row += 2
        self.floor_is_infinite = True

    def drip_sand(self):
        num_drips = 0
        while not self.do_drip_sand():
            num_drips += 1
            logging.debug("Dripped sand %s", num_drips)
            if num_drips % 1000 == 0:
                print(num_drips)
            if num_drips == 12689:
                for row in self.cave:
                    print(''.join(row))
        #TODO there's a bug somewhere at the left-hand corner, causing the answer to be off by two
        for row in self.cave:
            print(''.join(row))
        return num_drips

    def do_drip_sand(self):
        sand = SAND_SOURCE
        logging.debug("About to drip sand")
        self.print_cave(self.cave)
        if self.get_cave(self.cave, SAND_SOURCE) == 'o':
            return True
        while self.offset.row <= sand.row <= self.lowest_row:
            if self.get_cave(self.cave, Point(sand.row+1, sand.col)) == '.':
                sand = Point(sand.row+1, sand.col)
            elif self.get_cave(self.cave, Point(sand.row+1, sand.col-1)) == '.':
                sand = Point(sand.row+1, sand.col-1)
            elif self.get_cave(self.cave, Point(sand.row+1, sand.col+1)) == '.':
                sand = Point(sand.row+1, sand.col+1)
            else:
                self.cave = self.set_cave(self.cave, sand, 'o')
                return False
        return True

    def print_cave(self, cave):
        for row in cave:
            logging.debug(''.join(row))


if __name__ == '__main__':
    parser = getBasicArgparser('day 14')

    args = parser.parse_args()

    day14 = Day14(args.i, args.v)
    print("Part 1: {} units of sand drop before it starts to fall into the abyss".format(day14.drip_sand()))

    day14.clear_cave()
    day14.add_cave_floor()
    print("Part 2: {} units of sand fall until it plugs the orifice".format(day14.drip_sand()))
