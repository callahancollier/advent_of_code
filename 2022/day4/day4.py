#!/usr/bin/env python3

import csv
import sys
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Assignment:
    def __init__(self, rangeList):
        self.lower = int(rangeList[0])
        self.upper = int(rangeList[1])

    def __str__(self):
        return "{}-{}".format(self.lower, self.upper)

    def get_list(self):
        full_list = [*range(self.lower, self.upper+1)]
        full_list.sort()
        return full_list

    def get_set(self):
        return set(self.get_list())


class AssignmentPair:
    def __init__(self, elf1, elf2):
        self.elf1 = elf1
        self.elf2 = elf2

    def __str__(self):
        return "elf1: {}, elf2: {}".format(self.elf1, self.elf2)


class Day4(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.assignments = self.parse_file()

    def parse_file(self):
        assignments = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(',')
                elf1 = line[0].split('-')
                elf2 = line[1].split('-')
                assignments.append(AssignmentPair(
                    Assignment(elf1), Assignment(elf2)
                ))
        return assignments

    def get_num_assignments_fully_contained(self):
        num = 0
        for assignment in self.assignments:
            both = list(assignment.elf1.get_set() & assignment.elf2.get_set())
            both.sort()
            self.debugPrint("{}: both is {}".format(
                assignment, both
            ))
            if both == assignment.elf1.get_list() or both == assignment.elf2.get_list():
                self.debugPrint("  Bingo!")
                num = num + 1
        return num

    def get_num_assignments_that_overlap_at_all(self):
        num = 0
        for assignment in self.assignments:
            both = list(assignment.elf1.get_set() & assignment.elf2.get_set())
            if len(both) != 0:
                num += 1
        return num


if __name__ == '__main__':
    parser = getBasicArgparser('day 4')

    args = parser.parse_args()

    day4 = Day4(args.i, args.v)
    print("Part 1: There are {} pairs where one fully contains the other".format(
        day4.get_num_assignments_fully_contained()
    ))

    print("Part 2: There are {} pairs that overlap at all".format(
        day4.get_num_assignments_that_overlap_at_all()
    ))