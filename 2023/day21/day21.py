#!/usr/bin/env python3

import csv
import sys
import logging
from copy import deepcopy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point


START = 'S'
GARDEN = '.'
ROCKS = '#'


class Day21(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.garden_plots = None
        self.start = None
        self.parse_file()
        self.visited = set()

    def parse_file(self):
        self.garden_plots = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                if START in line:
                    self.start = Point(len(self.garden_plots), line.index(START))
                self.garden_plots.append(list(line))
        logging.info(self.start)

    def walk(self, pos, path, remaining_steps):
        path.append(pos)
        if remaining_steps == 0:
            logging.info('Could walk {}'.format(path))
            self.visited.add(pos)
            return
        for next_incr in [Point(-1, 0), Point(1, 0), Point(0, -1), Point(0,1)]:
            next_pos = pos + next_incr
            if self.garden_plots[next_pos.row][next_pos.col] == GARDEN:
                if next_pos not in path:
                    logging.info("From {} to {}".format(path, next_pos))
                    self.walk(deepcopy(next_pos), deepcopy(path), remaining_steps-1)

    def max_plots_visited(self, n_steps):
        self.walk(self.start, [], n_steps)
        print(self.visited)
        return len(self.visited)


if __name__ == '__main__':
    parser = getBasicArgparser('day 21')
    parser.add_argument('-n', type=int, default=64, help='number of goal steps')

    args = parser.parse_args()

    day21 = Day21(args.i, args.v)

    print("Part 1: maximum plots visited is {}".format(day21.max_plots_visited(args.n)))