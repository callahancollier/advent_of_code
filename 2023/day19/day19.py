#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
import asteval
from copy import deepcopy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

aeval = asteval.Interpreter()


ACCEPTED = 'A'
REJECTED = 'R'
START_WORKFLOW = 'in'


END_POINTS = [ ACCEPTED, REJECTED ]


@dataclass
class MinMax:
    min: int
    max: int


@dataclass
class Part:
    rating: dict[str, int]

    def tally(self):
        return sum([x for x in self.rating.values()])


@dataclass
class WorkflowEntry:
    evaluator: str
    key_if_true: str


class Day19(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.workflow = None
        self.parts = None
        self.parse_file()
        logging.info(self.parts)
        logging.info(self.workflow)

    def parse_file(self):
        self.workflow = {}
        self.parts = []
        doing_workflow = True
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    doing_workflow = False
                    continue
                line = line[0]
                if doing_workflow:
                    line = line.split('{')
                    key = line[0]
                    data = line[1][:-1].split(',')
                    flows = []
                    for item in data:
                        if ':' not in item:
                            flows.append(WorkflowEntry('', item))
                        else:
                            tmp = item.split(':')
                            flows.append(WorkflowEntry(tmp[0], tmp[1]))
                    self.workflow[key] = flows
                else:
                    line = line[1:-1].split(',')
                    x = int(line[0].split('=')[1])
                    m = int(line[1].split('=')[1])
                    a = int(line[2].split('=')[1])
                    s = int(line[3].split('=')[1])
                    self.parts.append(Part({'x': x, 'm': m, 'a': a, 's': s}))

    def get_next_workflow(self, part, wf):
        workflow = self.workflow[wf]
        for step in workflow:
            if len(step.evaluator) == 0:
                return step.key_if_true
            target = step.evaluator[0]
            eval_string = "{} = {}; {}".format(target, part.rating[target], step.evaluator)
            if aeval(eval_string):
                return step.key_if_true
        logging.error("Did not find a valid matcher for {} in {}".format(part, wf))
        sys.exit(1)

    def tally_accepted_parts(self):
        total = 0
        for part in self.parts:
            wf = START_WORKFLOW
            while wf not in END_POINTS:
                wf = self.get_next_workflow(part, wf)
            if wf == ACCEPTED:
                total += part.tally()
        return total

    def get_final_tally(self, ranges):
        result = (
                (ranges['x'].max - ranges['x'].min + 1) *
                (ranges['m'].max - ranges['m'].min + 1) *
                (ranges['a'].max - ranges['a'].min + 1) *
                (ranges['s'].max - ranges['s'].min + 1)
        )
        logging.info("Ranges {}, result {}".format(ranges, result))
        return result

    def tally_ranges_for_workflow(self, wf, current_ranges):
        total = 0
        if wf == ACCEPTED:
            logging.info("ACCEPTED")
            return self.get_final_tally(current_ranges)
        elif wf == REJECTED:
            logging.info("REJECTED")
            return 0

        workflow = self.workflow[wf]
        logging.info("Workflow {}".format(wf))
        logging.info(current_ranges)
        for step in workflow:
            logging.info("{} Step {}   {}".format(wf, step, current_ranges))
            if len(step.evaluator) == 0:
                if step.key_if_true == ACCEPTED:
                    return total + self.get_final_tally(current_ranges)
                elif step.key_if_true == REJECTED:
                    return total
                else:
                    return total + self.tally_ranges_for_workflow(step.key_if_true, deepcopy(current_ranges))

            if '<' in step.evaluator:
                if current_ranges[step.evaluator[0]].min < int(step.evaluator.split('<')[-1]):
                    new_ranges = deepcopy(current_ranges)
                    new_ranges[step.evaluator[0]].max = min(
                        new_ranges[step.evaluator[0]].max,
                        int(step.evaluator.split('<')[-1])-1
                    )
                    total += self.tally_ranges_for_workflow(step.key_if_true, new_ranges)

                current_ranges[step.evaluator[0]].min = max(
                    current_ranges[step.evaluator[0]].min,
                    int(step.evaluator.split('<')[-1])
                )
                continue

            if '>' in step.evaluator:
                if current_ranges[step.evaluator[0]].max > int(step.evaluator.split('>')[-1]):
                    new_ranges = deepcopy(current_ranges)
                    new_ranges[step.evaluator[0]].min = max(
                        new_ranges[step.evaluator[0]].min,
                        int(step.evaluator.split('>')[-1])+1
                    )
                    total += self.tally_ranges_for_workflow(step.key_if_true, new_ranges)

                current_ranges[step.evaluator[0]].max = min(
                    current_ranges[step.evaluator[0]].max,
                    int(step.evaluator.split('>')[-1])
                )
                continue

            logging.error("Something unexpected happened, evaluator is {}".format(step.evaluator))
        return total

    def tally_all_accepted_parts(self):
        initial_ranges = {
            'x': MinMax(1, 4000),
            'm': MinMax(1, 4000),
            'a': MinMax(1, 4000),
            's': MinMax(1, 4000),
        }
        wf = START_WORKFLOW
        total = self.tally_ranges_for_workflow(wf, initial_ranges)
        return total


if __name__ == '__main__':
    parser = getBasicArgparser('day 19')

    args = parser.parse_args()

    day19 = Day19(args.i, args.v)

    print("Part 1: Sum of accepted parts is {}".format(day19.tally_accepted_parts()))
    print("Part 2: Sum of all accepted parts is {}".format(day19.tally_all_accepted_parts()))
