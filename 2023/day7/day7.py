#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
from enum import Enum
import pprint
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from functools import total_ordering


@total_ordering
class HandType(Enum):
    # Value matters
    FIVE_OF_A_KIND = 7
    FOUR_OF_A_KIND = 6
    FULL_HOUSE = 5
    THREE_OF_A_KIND = 4
    TWO_PAIR = 3
    ONE_PAIR = 2
    HIGH_CARD = 1

    def __lt__(self, other):
        if type(other) == type(self):
            return self.value < other.value
        raise ValueError('Cannot compare enums of different types!')

    def __eq__(self, other):
        if type(other) == type(self):
            return self.value == other.value
        raise ValueError('Cannot compare enums of different types!')


CardValue = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 11,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2
}
CardStr = {v: k for k, v in CardValue.items()}

CardValueJokers = dict(CardValue) # shallow copy
del CardValueJokers['J']
CardValueJokers['J'] = 1
CardStrJokers = {v: k for k, v in CardValueJokers.items()}


@dataclass
@total_ordering
class Hand:
    cards: list[int]
    bid: int
    has_jokers: bool

    def __str__(self):
        if self.has_jokers:
            return "Hand(cards={}({}/{}), bid={})".format(
                ''.join([CardStrJokers[x] for x in self.cards]), self.get_hand_type(), self.get_hand_type_with_jokers(), self.bid
            )
        else:
            return "Hand(cards={}({}), bid={})".format(
                ''.join([CardStr[x] for x in self.cards]), self.get_hand_type(), self.bid
            )
    __repr__ = __str__

    def get_card_counts(self):
        counts = {}
        for c in self.cards:
            if c not in counts:
                counts[c] = 0
            counts[c] += 1
        return counts

    def get_hand_type(self):
        if self.has_jokers:
            return self.get_hand_type_with_jokers()
        else:
            return self.get_hand_type_without_jokers()

    def get_hand_type_without_jokers(self):
        counts = self.get_card_counts()
        unique_cards = list(counts.keys())
        card_nums = list(counts.values())
        if len(unique_cards) == 1:
            return HandType.FIVE_OF_A_KIND
        if len(unique_cards) == 2:
            if counts[unique_cards[0]] == 4 or counts[unique_cards[-1]] == 4:
                return HandType.FOUR_OF_A_KIND
            if counts[unique_cards[0]] + counts[unique_cards[-1]] == 5:
                return HandType.FULL_HOUSE
        if len(unique_cards) == 3:
            for c in unique_cards:
                if counts[c] == 3:
                    return HandType.THREE_OF_A_KIND
            if card_nums.count(2) == 2:
                return HandType.TWO_PAIR
        if 2 in card_nums:
            return HandType.ONE_PAIR
        return HandType.HIGH_CARD

    def get_hand_type_with_jokers(self):
        counts = self.get_card_counts()
        num_jokers = 0
        if CardValueJokers['J'] in counts:
            num_jokers = counts[CardValueJokers['J']]
            del counts[CardValueJokers['J']]
        unique_cards = list(counts.keys())
        card_nums = list(counts.values())
        if len(unique_cards) == 1 or len(unique_cards) == 0:
            return HandType.FIVE_OF_A_KIND
        if len(unique_cards) == 2:
            if counts[unique_cards[0]]+num_jokers == 4 or counts[unique_cards[-1]]+num_jokers == 4:
                return HandType.FOUR_OF_A_KIND
            if counts[unique_cards[0]]+counts[unique_cards[-1]]+num_jokers == 5:
                return HandType.FULL_HOUSE
        if len(unique_cards) == 3:
            for c in unique_cards:
                if counts[c]+num_jokers == 3:
                    return HandType.THREE_OF_A_KIND
        pairs = self.get_num_pairs_with_jokers(counts, num_jokers)
        if pairs > 2:
            logging.error("Something has gone wrong!")
            return sys.exit(1)
        if pairs == 2:
            return HandType.TWO_PAIR
        if pairs == 1:
            return HandType.ONE_PAIR
        return HandType.HIGH_CARD

    def get_num_pairs_with_jokers(self, counts, num_jokers):
        pairs = 0
        for card in counts.keys():
            if counts[card] == 2:
                pairs += 1
            elif counts[card] == 1 and num_jokers > 0:
                pairs += 1
                num_jokers -= 1
        return pairs

    def __lt__(self, other):
        logging.info("Compare: {} lt {}".format(self, other))
        this_hand_type = self.get_hand_type()
        other_hand_type = other.get_hand_type()
        logging.info("{} lt {}".format(this_hand_type, other_hand_type))
        if this_hand_type == other_hand_type:
            for i in range(len(self.cards)):
                logging.info("i={}: {} lt {}".format(i, self.cards[i], other.cards[i]))
                if self.cards[i] < other.cards[i]:
                    logging.info(True)
                    return True
                elif self.cards[i] > other.cards[i]:
                    logging.info(False)
                    return False
            logging.error(
                "Comparison error: cards are the same!? {} ({}) {} ({})".format(
                    self, this_hand_type, other, other_hand_type))
        retval = this_hand_type < other_hand_type
        logging.info(retval)
        return retval

    def __eq__(self, other):
        return self.bid == other.bid and self.cards == other.cards


class Day7(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.hands = self.parse_file()

    def parse_file(self):
        hands = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                hands.append(Hand([CardValue[x] for x in line[0]], int(line[1]), False))
        logging.info(hands)
        return hands

    def get_total_winnings(self, hands):
        # put the cards in order
        logging.info("Before sorting:\n{}".format(pprint.pformat(hands)))
        hands.sort()
        logging.info("After sorting:\n{}".format(pprint.pformat(hands)))
        total = 0
        for i in range(len(hands)):
            total += hands[i].bid * (i+1)
        return total

    def get_joker_hand(self):
        new_hand = []
        for h in self.hands:
            new_hand.append(Hand([CardValueJokers[CardStr[x]] for x in h.cards], h.bid, True))
        return new_hand

    def get_total_winnings_without_jokers(self):
        return self.get_total_winnings(self.hands)

    def get_total_winnings_with_jokers(self):
        joker_hand = self.get_joker_hand()
        return self.get_total_winnings(joker_hand)


if __name__ == '__main__':
    parser = getBasicArgparser('day 7')

    args = parser.parse_args()

    day7 = Day7(args.i, args.v)

    print("Part 1: total winnings is {}".format(day7.get_total_winnings_without_jokers()))
    print("Part 2: total winnings is {}".format(day7.get_total_winnings_with_jokers()))
