#!/usr/bin/env python3

import csv
import sys
import logging
from enum import StrEnum
import numpy as np
from alive_progress import alive_bar
from copy import deepcopy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point


ENERGIZED = '#'


class Dir(StrEnum):
    NORTH = '^'
    EAST = '>'
    SOUTH = 'v'
    WEST = '<'


Directions = set(i.value for i in Dir)


MOVE = {
    Dir.NORTH: Point(-1, 0),
    Dir.EAST: Point(0, 1),
    Dir.SOUTH: Point(1, 0),
    Dir.WEST: Point(0, -1)
}


class Obj(StrEnum):
    EMPTY = '.'
    MIRROR_A = '/'
    MIRROR_B = '\\'
    SPLITTER_V = '|'
    SPLITTER_H = '-'


BEHAVIOR_FROM_HEADING = {
    Dir.NORTH: {
        Obj.EMPTY: [Dir.NORTH],
        Obj.MIRROR_A: [Dir.EAST],
        Obj.MIRROR_B: [Dir.WEST],
        Obj.SPLITTER_V: [Dir.NORTH],
        Obj.SPLITTER_H: [Dir.WEST, Dir.EAST]
    },
    Dir.EAST: {
        Obj.EMPTY: [Dir.EAST],
        Obj.MIRROR_A: [Dir.NORTH],
        Obj.MIRROR_B: [Dir.SOUTH],
        Obj.SPLITTER_V: [Dir.NORTH, Dir.SOUTH],
        Obj.SPLITTER_H: [Dir.EAST],
    },
    Dir.SOUTH: {
        Obj.EMPTY: [Dir.SOUTH],
        Obj.MIRROR_A: [Dir.WEST],
        Obj.MIRROR_B: [Dir.EAST],
        Obj.SPLITTER_V: [Dir.SOUTH],
        Obj.SPLITTER_H: [Dir.WEST, Dir.EAST],
    },
    Dir.WEST: {
        Obj.EMPTY: [Dir.WEST],
        Obj.MIRROR_A: [Dir.SOUTH],
        Obj.MIRROR_B: [Dir.NORTH],
        Obj.SPLITTER_V: [Dir.NORTH, Dir.SOUTH],
        Obj.SPLITTER_H: [Dir.WEST],
    }
}


class Day16(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.area = self.parse_file()
        self.area_save = deepcopy(self.area)
        self.energized = np.full(self.area.shape, '.')
        self.records = {}

    def parse_file(self):
        area = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = list(line[0])
                area.append(line)
        return np.array(area)

    def reset_area(self):
        # Not sure if we really need deepcopy here, but it doesn't hurt
        self.area = deepcopy(self.area_save)
        self.energized = np.full(self.area.shape, '.')
        self.records = {}

    def count_energized(self):
        return (self.energized == ENERGIZED).sum()

    def is_in_bounds(self, point: Point):
        if 0 <= point.row < len(self.area) and 0 <= point.col < len(self.area[0]):
            return True
        logging.info("{} not in bounds".format(point))
        return False

    def traverse(self, curr: Point, heading: Dir):
        while self.is_in_bounds(curr):
            if curr in self.records.keys() and heading in self.records[curr]:
                # Loop detection
                return
            if curr not in self.records.keys():
                self.records[curr] = [heading]
            else:
                self.records[curr].append(heading)
            logging.info("Step to {}, going {}".format(curr, heading))
            self.energized[curr.row, curr.col] = ENERGIZED
            current_space = self.area[curr.row, curr.col]
            if current_space in Directions:
                current_space = Obj.EMPTY
            next_steps = BEHAVIOR_FROM_HEADING[heading][current_space]
            if current_space == Obj.EMPTY:
                self.area[curr.row, curr.col] = heading.value
            save = self.area[curr.row, curr.col]
            self.area[curr.row, curr.col] = 'X'
            logging.info(self.area)
            self.area[curr.row, curr.col] = save
            while len(next_steps) > 0 and not self.is_in_bounds(curr + MOVE[next_steps[0]]):
                next_steps = next_steps[1:]
            if len(next_steps) == 0:
                return
            if len(next_steps) > 1:
                for step in next_steps[1:]:
                    moveto = curr + MOVE[step]
                    if self.is_in_bounds(moveto):
                        logging.info("Recurse: go {} to {}".format(step, moveto))
                        self.traverse(curr + MOVE[step], step)
            curr = curr + MOVE[next_steps[0]]
            heading = next_steps[0]

    def energize_from_top_left(self):
        curr = Point(0, 0)
        heading = Dir.EAST
        self.traverse(curr, heading)
        logging.info(self.energized)
        return self.count_energized()

    def energize_from_any_edge(self):
        total_iterations = 2*len(self.area) + 2*len(self.area[0])
        max_energized = 0
        with alive_bar(total_iterations) as bar:
            for i in range(len(self.area)):
                self.reset_area()
                self.traverse(Point(i, 0), Dir.EAST)
                max_energized = max(self.count_energized(), max_energized)
                bar()

                self.reset_area()
                self.traverse(Point(i, len(self.area)-1), Dir.WEST)
                max_energized = max(self.count_energized(), max_energized)
                bar()

            for i in range(len(self.area[0])):
                self.reset_area()
                self.traverse(Point(0, i), Dir.SOUTH)
                max_energized = max(self.count_energized(), max_energized)
                bar()

                self.reset_area()
                self.traverse(Point(len(self.area[0])-1,i), Dir.NORTH)
                max_energized = max(self.count_energized(), max_energized)
                bar()
        return max_energized


if __name__ == '__main__':
    parser = getBasicArgparser('day 16')

    args = parser.parse_args()

    day16 = Day16(args.i, args.v)

    print("Part 1: Number of energized spaces: {}".format(day16.energize_from_top_left()))
    print("Part 2: Maximum number of energized spaces: {}".format(day16.energize_from_any_edge()))