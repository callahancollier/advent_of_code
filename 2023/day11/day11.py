#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
from alive_progress import alive_bar
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point


GALAXY = '#'
EMPTY = '.'


class Day11(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.space = self.parse_file()
        self.galaxies = self.get_galaxy_list()
        logging.info("Expanded space:\n{}".format(self.space))
        logging.info("Galaxies: {}".format(self.galaxies))

    def parse_file(self):
        # parse file and expand space vertically as we go
        vert = []
        num_empty_rows = 0
        num_empty_cols = 0
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                vert.append(list(line))
                if GALAXY not in line:
                    num_empty_rows += 1
                    vert.append(list(line))
        tmp = np.array(vert)
        space = []
        for col in tmp.T:
            space.append(col.tolist())
            if GALAXY not in col:
                num_empty_cols += 1
                space.append(col.tolist())
        return np.array(space).T

    def get_galaxy_list(self):
        galaxies = []
        for r in range(len(self.space)):
            for c in range(len(self.space[0])):
                if self.space[r, c] == GALAXY:
                    galaxies.append(Point(r, c))
        return galaxies

    def tally_path_lengths_narrow(self):
        total = 0
        for a in self.galaxies:
            for b in self.galaxies:
                if a == b:
                    continue
                vert_dist = abs(a.row - b.row)
                horiz_dist = abs(a.col - b.col)
                path_len = vert_dist + horiz_dist
                logging.info("{}-{}: v {} h {}, distance {}".format(
                    a, b, vert_dist, horiz_dist, path_len
                ))
                total += path_len
        # Need to divide by 2 because we counted a -> b and b -> a
        # (running double the loops doesn't seem to affect the runtime so it's fine)
        return int(total/2)

    def tally_path_lengths_wide(self, empty_space_multiplier):
        # since space is already double-expanded, each empty row/col is 1/2 a million
        empty_size = empty_space_multiplier/2
        total = 0
        total_paths = len(self.galaxies)**2
        with alive_bar(total_paths) as bar:
            for a in self.galaxies:
                for b in self.galaxies:
                    bar()
                    empty_row = 0
                    empty_col = 0
                    if a == b:
                        continue
                    vert_dist = 0
                    for i in range(min(a.row,b.row)+1, max(a.row,b.row)+1):
                        if GALAXY not in self.space[i,:]:
                            empty_row += 1
                            vert_dist += empty_size
                        else:
                            vert_dist += 1

                    horiz_dist = 0
                    for i in range(min(a.col,b.col)+1, max(a.col,b.col)+1):
                        if GALAXY not in self.space[:,i]:
                            empty_col += 1
                            horiz_dist += empty_size
                        else:
                            horiz_dist += 1

                    path_len = vert_dist + horiz_dist
                    logging.info("{}-{}: v {} ({} empty) h {} ({} empty), distance {}".format(
                        a, b, vert_dist, empty_row, horiz_dist, empty_col, path_len
                    ))
                    total += path_len
        # Need to divide by 2 because we counted a -> b and b -> a
        return int(total/2)


if __name__ == '__main__':
    parser = getBasicArgparser('day 11')
    parser.add_argument('-m', type=int, default=1e6, help="empty space multiplier for part 2")

    args = parser.parse_args()

    day11 = Day11(args.i, args.v)

    print("Part 1: Sum of shortest paths is {}".format(day11.tally_path_lengths_narrow()))
    print("Part 2: Sum of shortest paths is {}".format(day11.tally_path_lengths_wide(args.m)))
