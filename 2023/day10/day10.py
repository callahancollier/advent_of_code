#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
np.set_printoptions(threshold=sys.maxsize)
sys.path.append('../..')  # TODO how to do this right?
from dataclasses import dataclass
from common.base_class import BaseFunctions, getBasicArgparser, Point, get_area_of_loop, get_num_points_enclosed_by_loop


NORTH = Point(-1, 0)
EAST = Point(0, 1)
SOUTH = Point(1, 0)
WEST = Point(0, -1)

REVERSE_DIR = {
    NORTH: SOUTH,
    EAST: WEST,
    WEST: EAST,
    SOUTH: NORTH
}

STR_DIR = {
    NORTH: 'N',
    EAST: 'E',
    WEST: 'W',
    SOUTH: 'S'
}


@dataclass
class Pipe:
    connections: list[Point]


PIPES = {
    '|': Pipe([NORTH, SOUTH]),
    '-': Pipe([EAST, WEST]),
    'L': Pipe([NORTH, EAST]),
    'J': Pipe([NORTH, WEST]),
    '7': Pipe([SOUTH, WEST]),
    'F': Pipe([SOUTH, EAST]),
    '.': Pipe([]),
    'S': Pipe([NORTH, EAST, SOUTH, WEST])
}
ANIMAL = 'S'


class Day10(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.start_pos = None
        self.landscape = None
        self.parse_file()

        logging.info("Start: animal at {}, landscape\n{}".format(
            self.start_pos, self.landscape))

    def parse_file(self):
        landscape = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = list(line[0])
                try:
                    idx = line.index(ANIMAL)
                    self.start_pos = Point(len(landscape), idx)
                except ValueError:
                    pass
                landscape.append(line)
        self.landscape = np.array(landscape)

    def traverse(self, landscape, current_pos, pipe):
        logging.info("Traverse from {}".format(current_pos))
        current_pipe = PIPES[landscape[current_pos.row, current_pos.col]]
        for look_dir in current_pipe.connections:
            look_point = current_pos + look_dir
            look_pipe = landscape[look_point.row, look_point.col]
            logging.info("Look {} to {}: {}".format(STR_DIR[look_dir],
                                                    look_point,
                                                    look_pipe))
            if REVERSE_DIR[look_dir] in PIPES[look_pipe].connections:
                if look_point not in pipe:
                    logging.info("This is the way to go!")
                    return look_point
        raise ValueError("Nowhere to go from {}:\n{}\n---".format(current_pos, landscape))

    def find_pipe_loop_steps_to_farthest(self):
        loop = np.full((len(self.landscape),len(self.landscape[0])), '.')
        pos = self.traverse(self.landscape, self.start_pos, [])
        pipe = [self.start_pos, pos]
        index = 1
        while self.landscape[pos.row, pos.col] != ANIMAL:
            loop[pos.row, pos.col] = str(index)
            pos = self.traverse(self.landscape, pos, pipe)
            pipe.append(pos)
            index += 1
            if index >= 3:  # don't need to keep the whole pipe, speeds it up a lot
                pipe = pipe[1:]
        loop[pos.row, pos.col] = ANIMAL
        logging.info("Loop traversed:\n{}\n---".format(loop))
        return int((len(pipe))/2)

    def get_pipe(self):
        pos = self.traverse(self.landscape, self.start_pos, [])
        pipe = [self.start_pos, pos]
        index = 1
        while self.landscape[pos.row, pos.col] != ANIMAL:
            pos = self.traverse(self.landscape, pos, pipe)
            pipe.append(pos)
            index += 1
            if index == 3:
                pipe = pipe[1:]
        return pipe

    def get_num_enclosed_points(self):
        pipe = self.get_pipe()
        logging.info(pipe)
        return int(get_num_points_enclosed_by_loop(pipe))


if __name__ == '__main__':
    parser = getBasicArgparser('day 10')

    args = parser.parse_args()

    day10 = Day10(args.i, args.v)

    print("Part 1: takes this many steps to get to the farthest point along the pipe: {}".format(day10.find_pipe_loop_steps_to_farthest()))
    print("Part 2: number of points enclosed by the loop: {}".format(day10.get_num_enclosed_points()))