#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from dataclasses import dataclass


@dataclass
class RGB:
    red: int
    green: int
    blue: int

    def get_power(self):
        return self.red * self.green * self.blue

class GameRecord:
    def __init__(self, game_id, rgb_list):
        self.id = game_id
        self.rgb_list = rgb_list

    def __str__(self):
        return "Game {}: {}".format(self.id, self.rgb_list)

    def isPossible(self, totals: RGB):
        for rgb in self.rgb_list:
            logging.info("Game {}: test {} against {}".format(self.id, rgb, totals))
            if rgb.red > totals.red or rgb.green > totals.green or rgb.blue > totals.blue:
                logging.info("  nope")
                return False
        return True

    def getMinCubesToMakePossible(self):
        min_possible = RGB(0, 0, 0)
        for rgb in self.rgb_list:
            if rgb.red > min_possible.red:
                min_possible.red = rgb.red
            if rgb.green > min_possible.green:
                min_possible.green = rgb.green
            if rgb.blue > min_possible.blue:
                min_possible.blue = rgb.blue
        return min_possible


class Day2(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.games = self.parse_file()

    def parse_file(self):
        games = []
        logging.info("Read in data:")
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                line = line.split(':')
                if not line[0].startswith('Game '):
                    logging.error("Error: malformed line: {}".format(line))
                game_number = int(line[0].split(' ')[-1])
                rgbs = []
                for entry in line[1].split(';'):
                    red = 0
                    blue = 0
                    green = 0
                    for cubeset in entry.split(','):
                        tmp = cubeset.strip(' ').split(' ')
                        print(tmp)
                        if tmp[1] == 'red':
                            red += int(tmp[0])
                        if tmp[1] == 'blue':
                            blue += int(tmp[0])
                        if tmp[1] == 'green':
                            green += int(tmp[0])
                    print(RGB(red,green,blue))
                    rgbs.append(RGB(red, green, blue))
                games.append(GameRecord(game_number, rgbs))
                logging.info(games[-1])
        logging.info('---')
        return games

    def check_if_games_possible(self, total_cubes):
        id_sum = 0
        for game in self.games:
            if game.isPossible(total_cubes):
                logging.info("Game {} is possible".format(game.id))
                id_sum += game.id
            else:
                logging.info("Game {} is not possible".format(game.id))
        return id_sum

    def get_sum_power_min_set_of_cubes(self):
        sum_of_powers = 0
        for game in self.games:
            min_possible = game.getMinCubesToMakePossible()
            sum_of_powers += min_possible.get_power()
            logging.info("Game {}: min cube set {}, power {}".format(
                game.id, min_possible, min_possible.get_power()
            ))
        return sum_of_powers


if __name__ == '__main__':
    parser = getBasicArgparser('day 2')

    args = parser.parse_args()

    day2 = Day2(args.i, args.v)

    print("Part 1: Sum of IDs of possible games: {}".format(day2.check_if_games_possible(RGB(12, 13, 14))))
    print("Part 2: Sum of power of minimum set of cubes present: {}".format(day2.get_sum_power_min_set_of_cubes()))