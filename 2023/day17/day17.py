#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, ShortestPathCardinalBase


class CrucibleTraverse(ShortestPathCardinalBase):
    def __init__(self, city):
        super().__init__(False, Point(0,0), Point(len(city)-1,len(city[0])-1), city)

    def get_neighbours(self, node, camefrom):
        neighbours = []
        for candidate in super().get_neighbours(node, camefrom):
            if self.is_valid_neighbour(node, candidate, camefrom):
                neighbours.append(candidate)
        logging.info("Valid neighbours: {}".format(neighbours))
        return neighbours

    def is_valid_neighbour(self, node, candidate, camefrom):
        prev = []
        curr = node
        while curr in camefrom.keys() and len(prev) < 4:
            prev.append(curr)
            curr = camefrom[curr]
        logging.info("Check potential path {} from {} to {} valid?".format(prev, node, candidate))
        # Can't go more than 3 in a straight line
        if len(prev) < 4:
            logging.info("  -> Valid!")
            return True
        # Straight line means either all the rows are the same
        if len([x.row for x in prev if x.row == candidate.row]) == len(prev):
            logging.info("  -> Not valid")
            return False
        # or all the cols are the same
        if len([x.col for x in prev if x.col == candidate.col]) == len(prev):
            logging.info("  -> Not valid")
            return False
        logging.info("  -> Valid!")
        return True

    def get_next_g_score(self, current, current_g_score, neighbour_node, camefrom):
        return current_g_score + self.array[neighbour_node.row, neighbour_node.col]

    def tally_path(self, current, camefrom):
        debug_array = self.array.copy()
        pathtotal = self.array[current.row, current.col]
        while current in camefrom.keys():
            current = camefrom[current]
            pathtotal += self.array[current.row, current.col]
            debug_array[current.row, current.col] = 0
        # Don't count the start point of the path
        pathtotal -= self.array[0, 0]
        logging.info(camefrom)
        logging.info(debug_array)
        return pathtotal


class Day17(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.city = self.parse_file()
        logging.info(self.city)

    def parse_file(self):
        city = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                city.append([int(x) for x in line])
        return np.array(city)

    def get_path_of_least_heat_loss(self):
        calculator = CrucibleTraverse(self.city.copy())
        return calculator.get_shortest_path()


if __name__ == '__main__':
    parser = getBasicArgparser('day 17')

    args = parser.parse_args()

    day17 = Day17(args.i, args.v)

    print("Part 1: Path with least heat loss has loss of {}".format(day17.get_path_of_least_heat_loss()))
