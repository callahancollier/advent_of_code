#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

ROCK = 'O'
CUBE = '#'
EMPTY = '.'


class Day14(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.platform = self.parse_file()
        logging.info(self.platform)

    def parse_file(self):
        platform = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = list(line[0])
                platform.append(line)
        return np.array(platform)

    def tilt_north(self, platform):
        for c in range(len(platform[0])):
            platform[:,c] = self.roll_towards_zero(platform[:,c])
        return platform

    def tilt_south(self, platform):
        return np.flipud(self.tilt_north(np.flipud(platform)))

    def tilt_west(self, platform):
        for r in range(len(platform)):
            platform[r] = self.roll_towards_zero(platform[r])
        return platform

    def tilt_east(self, platform):
        return np.fliplr(self.tilt_west(np.fliplr(platform)))

    def roll_towards_zero(self, array_1d):
        #logging.info("Roll towards zero: {}".format(array_1d))
        for i in range(len(array_1d)):
            if array_1d[i] == ROCK:
                idx = i
                while idx >= 1 and array_1d[idx-1] == EMPTY:
                    idx -= 1
                array_1d[i] = EMPTY
                array_1d[idx] = ROCK
        #logging.info("Rolled: {}".format(array_1d))
        return array_1d

    def calc_total_load(self):
        total = 0
        for r in range(len(self.platform)):
            row = self.platform[r]
            #logging.info("tally row {} : {} ({})".format(row, (row == ROCK), (row==ROCK).sum()))
            total += (len(self.platform) - r) * (row == ROCK).sum()
        return total

    def part1(self):
        self.platform = self.tilt_north(self.platform)
        logging.info(self.platform)
        return self.calc_total_load()

    def spin_cycle(self):
        self.platform = self.tilt_north(self.platform)
        self.platform = self.tilt_west(self.platform)
        self.platform = self.tilt_south(self.platform)
        self.platform = self.tilt_east(self.platform)

    def part2(self, rotations=1000000000):
        record = []
        for i in range(rotations):
            self.spin_cycle()
            record.append(self.calc_total_load())
            logging.info("Iteration {}".format(i))
            logging.info(self.platform)
            logging.info("Record of:       {}".format(record))

            # look for a repeating pattern to start
            repeat_sequence = []
            check_length = 100
            for j in range(3, check_length):
                if len(record) > check_length:
                    logging.info("compare\n{}\n{}".format(record[-j:], record[-2*j:-j]))
                    if record[-j:] == record[-2*j:-j]:
                        logging.info("Repeat length {}".format(j))
                        repeat_sequence = record[-j:]
                        break

            if len(repeat_sequence) > 0:
                remaining = rotations - (i+1)
                ans_idx = (remaining-1)%len(repeat_sequence)
                logging.info("{} {} left: repeat seq {} index {}".format(i, remaining, repeat_sequence, ans_idx))
                return repeat_sequence[ans_idx]


if __name__ == '__main__':
    parser = getBasicArgparser('day 14')
    parser.add_argument('-n', type=int, default=1000000000, help="Number of spin cycles for part 2")

    args = parser.parse_args()

    day14 = Day14(args.i, args.v)

    print("Part 1: total load after tilting north is {}".format(day14.part1()))
    print("Part 2: total load {} spin cycles is {}".format(args.n, day14.part2(args.n)))
