#!/usr/bin/env python3

import csv
import sys
import string
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


class Day1(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.string_digits = {
            'one' : 1,
            'two' : 2,
            'three' : 3,
            'four' : 4,
            'five' : 5,
            'six' : 6,
            'seven' : 7,
            'eight' : 8,
            'nine' : 9
        }
        self.data = self.parse_file()

    def parse_file(self):
        data = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                data.append(line)
        return data

    def dumb_transform_to_two_digit_cal(self, data_str):
        trimmed = data_str.strip(string.ascii_letters)
        if len(trimmed) == 0:
            logging.error("Something went wrong, can't trim {}".format(data_str))
            return 0
        if not trimmed[0].isdigit() or not trimmed[-1].isdigit():
            logging.error("Something went wrong, trimmed string is {}".format(trimmed))
            return 0
        return int(trimmed[0])*10 + int(trimmed[-1])

    def get_start_digit(self, data_str):
        valid_digits = list(string.digits)
        valid_digits = valid_digits + list(self.string_digits)
        for digit in valid_digits:
            logging.info("Check if {} starts with {}".format(data_str, digit))
            if data_str.startswith(digit):
                logging.info("yep")
                if digit in self.string_digits:
                    return self.string_digits[digit]
                return int(data_str[0])
        logging.info("{} doesn't start with anything".format(data_str))
        return -1

    def get_end_digit(self, data_str):
        valid_digits = list(string.digits)
        valid_digits = valid_digits + list(self.string_digits)
        for digit in valid_digits:
            logging.info("Check if {} ends with {}".format(data_str, digit))
            if data_str.endswith(digit):
                logging.info("yep")
                if digit in self.string_digits:
                    return self.string_digits[digit]
                return int(data_str[-1])
        logging.info("{} doesn't end with anything".format(data_str))
        return -1

    def smart_transform_to_two_digit_cal(self, data_str):
        result = 0
        # trim beginning
        while len(data_str) > 0:
            beginning = self.get_start_digit(data_str)
            if beginning < 0:
                data_str = data_str[1:]
            else:
                break
        if len(data_str) == 0:
            logging.error("Didn't find starting digit")
        while len(data_str) > 0:
            ending = self.get_end_digit(data_str)
            if ending < 0:
                data_str = data_str[:-1]
            else:
                break
        if len(data_str) == 0:
            logging.error("Didn't find ending digit")
            return -1
        result = 10*beginning
        result += ending
        return result

    def get_sum_of_cal_values(self, part):
        cal_sum = 0
        for line in self.data:
            if part == 1:
                cal_sum += self.dumb_transform_to_two_digit_cal(line)
            else:
                cal_sum += self.smart_transform_to_two_digit_cal(line)
        return cal_sum


if __name__ == '__main__':
    parser = getBasicArgparser('day 1')

    args = parser.parse_args()

    day1 = Day1(args.i, args.v)

    print("Part 1: sum of cal values is {}".format(day1.get_sum_of_cal_values(1)))
    print("Part 2: sum of cal values is {}".format(day1.get_sum_of_cal_values(2)))
