#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
import pprint
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from math import lcm

START_POS = 'AAA'
END_POS = 'ZZZ'
RIGHT = 'R'
LEFT = 'L'


@dataclass
class Node:
    id: str
    left: str
    right: str


class Day8(BaseFunctions):
    def __init__(self, input_file, enable_debug, part):
        super().__init__(input_file, enable_debug)
        self.part = part
        self.nodes = {}
        self.directions = None
        self.parse_file()

        logging.info("Directions: {}".format(pprint.pformat(self.directions)))
        logging.info("Nodes:\n{}".format(pprint.pformat(self.nodes)))

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    continue
                line = line[0]
                if self.directions is None:
                    self.directions = [x for x in line]
                else:
                    line = line.split(' = ')
                    nodeinfo = line[1].split(', ')
                    self.nodes[line[0]] = Node(line[0], nodeinfo[0][1:], nodeinfo[-1][:-1])


    def get_steps_to_traverse(self):
        if self.part == 1:
            return self.get_steps_to_traverse_like_a_human()
        elif self.part == 2:
            return self.get_steps_to_traverse_like_a_ghost()
        else:
            logging.error("unknown part {}".format(self.part))

    def is_end_part_1(self, pos):
        return pos == END_POS

    def is_end_part_2(self, pos):
        return pos.endswith('Z')

    def get_steps_to_traverse_like_a_human(self):
        return self.calc_steps_to_traverse(START_POS, self.is_end_part_1)

    def calc_steps_to_traverse(self, start_position, end_pos_func):
        current_position = start_position
        num_steps = 0
        while not end_pos_func(current_position):
            for dir in self.directions:
                if dir == RIGHT:
                    current_position = self.nodes[current_position].right
                elif dir == LEFT:
                    current_position = self.nodes[current_position].left
                else:
                    logging.error('Unknown direction {}'.format(dir))
                    sys.exit(1)
                num_steps += 1
                if end_pos_func(current_position):
                    break
        return num_steps

    def get_steps_to_traverse_like_a_ghost(self):
        start_nodes = [n.id for n in self.nodes.values() if n.id.endswith('A')]
        steps_to_loop = {}
        for node in start_nodes:
            steps_to_loop[node] = self.calc_steps_to_traverse(node, self.is_end_part_2)
        # This does assume that the time from A -> Z is the same as the time from Z -> Z, but that appears to be true
        return lcm(*steps_to_loop.values())


if __name__ == '__main__':
    parser = getBasicArgparser('day 8', include_part_enum=True)

    args = parser.parse_args()

    day8 = Day8(args.i, args.v, args.p)

    print("Part {}: Number of steps to traverse is {}".format(day8.part, day8.get_steps_to_traverse()))
