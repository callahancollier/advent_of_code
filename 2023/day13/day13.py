#!/usr/bin/env python3

import csv
import sys
import logging
import numpy as np
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


ASH = '.'
ROCKS = '#'


class Day13(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.valley = self.parse_file()

    def parse_file(self):
        valley = []
        pattern = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                if len(line) == 0:
                    valley.append(np.array(pattern))
                    pattern = []
                    continue
                line = line[0]
                pattern.append(list(line))
        valley.append(np.array(pattern))
        return valley

    def find_reflection_horizontal(self, pattern, num_allowed_false=0):
        # rows
        for r in range(1, len(pattern)):
            upper = np.flipud(pattern[:r])
            lower = pattern[r:]
            logging.info("Compare rows {} false:\n{}\nand\n{}".format(num_allowed_false, upper, lower))
            while len(upper) > len(lower):
                upper = upper[:-1]
            while len(lower) > len(upper):
                lower = lower[:-1]
            logging.info("Trimmed\n{}\nand\n{}".format(upper, lower))
            logging.info(upper == lower)
            logging.info((~(upper == lower)).sum())
            #if(upper == lower).all():
            if (~(upper == lower)).sum() == num_allowed_false:
                logging.info("Match at row {}! *********************".format(r))
                return r
        return None

    def find_reflection_vertical(self, pattern, num_allowed_false=0):
        # columns
        for c in range(1, len(pattern[0])):
            left = np.fliplr(pattern[:, :c])
            right = pattern[:, c:]
            logging.info("Compare cols {} false:\n{}\nand\n{}".format(num_allowed_false, left, right))
            while len(left[0]) > len(right[0]):
                left = left[:, :-1]
            while len(right[0]) > len(left[0]):
                right = right[:, :-1]
            logging.info("Trimmed\n{}\nand\n{}".format(left, right))
            logging.info(left == right)
            logging.info(~(left == right))
            logging.info((~(left == right)).sum())
            #if(left == right).all():
            if (~(left == right)).sum() == num_allowed_false:
                logging.info("Match at col {}! *********************".format(c))
                return c

    def find_reflection(self, pattern, num_false=0):
        num_rows_above = self.find_reflection_horizontal(pattern, num_false)
        if num_rows_above is not None:
            return num_rows_above*100

        num_cols_left = self.find_reflection_vertical(pattern, num_false)
        if num_cols_left is not None:
            return num_cols_left

        logging.error("Did not find a reflection for this pattern!\n{}".format(pattern))
        sys.exit(1)

    def summarize_notes(self, part):
        total = 0
        for pattern in self.valley:
            if part == 1:
                total += self.find_reflection(pattern)
            else:
                total += self.find_reflection(pattern, 1)
            logging.info("-----------")
        return total


if __name__ == '__main__':
    parser = getBasicArgparser('day 13')

    args = parser.parse_args()

    day13 = Day13(args.i, args.v)

    print("Part 1: after summarizing notes: {}".format(day13.summarize_notes(1)))
    print("Part 2: after summarizing notes: {}".format(day13.summarize_notes(2)))
