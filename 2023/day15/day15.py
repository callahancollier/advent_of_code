#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


@dataclass
class Lens:
    label: str
    focal_len: int


@dataclass
class Box:
    id: int
    lenses: list[Lens]

    def __str__(self):
        result = 'Box {}: '.format(self.id)
        for l in self.lenses:
            result += '[{} {}]'.format(l.label, l.focal_len)
        return result
    __repr__ = __str__


class Day15(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.init_seq = self.parse_file()
        self.boxes = {}

    def parse_file(self):
        data = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                data += line.split(',')
        return data

    def run_hash_algorithm(self, input_str):
        current = 0
        for c in input_str:
            ascii = ord(c)
            current += ascii
            current = current * 17
            current = current % 256
        return current

    def sum_hash_of_init_sequence(self):
        total = 0
        for seq in self.init_seq:
            total += self.run_hash_algorithm(seq)
        return total

    def populate_boxes(self):
        for seq in self.init_seq:
            if '-' in seq:
                label = seq.split('-')[0]
                box_num = self.run_hash_algorithm(label)
                if box_num not in self.boxes:
                    self.boxes[box_num] = Box(box_num, [])
                self.boxes[box_num].lenses = [x for x in self.boxes[box_num].lenses if x.label != label]
            elif '=' in seq:
                seq = seq.split('=')
                if len(seq) != 2:
                    logging.error("Malformed sequence {}".format(seq))
                    sys.exit(1)
                label = seq[0]
                focal_len = int(seq[1])
                box_num = self.run_hash_algorithm(label)
                if box_num not in self.boxes:
                    self.boxes[box_num] = Box(box_num, [])
                found = False
                for i in range(len(self.boxes[box_num].lenses)):
                    if self.boxes[box_num].lenses[i].label == label:
                        self.boxes[box_num].lenses[i].focal_len = focal_len
                        found = True
                        break
                if not found:
                    lens = Lens(label=label, focal_len=focal_len)
                    self.boxes[box_num].lenses.append(lens)
            else:
                logging.error("Operation character not found in {}".format(seq))
                sys.exit(1)
            logging.info("After {}:\n{}\n".format(seq, self.boxes))

    def get_focusing_power_of_lens_config(self):
        self.populate_boxes()
        total = 0
        for b in self.boxes.keys():
            for l in range(len(self.boxes[b].lenses)):
                x = 1 + b
                y = 1 + l
                z = self.boxes[b].lenses[l].focal_len
                focusing_power = x * y * z
                logging.info("Lens {} power {}*{}*{}={}".format(
                    self.boxes[b].lenses[l].label,
                    x,y,z, focusing_power
                ))

                total += focusing_power
        return total


if __name__ == '__main__':
    parser = getBasicArgparser('day 15')

    args = parser.parse_args()

    day15 = Day15(args.i, args.v)

    print("Part 1: Sum of hash algorithm on input sequence is {}".format(day15.sum_hash_of_init_sequence()))
    print("Part 2: Focusing power of lens configuration is {}".format(day15.get_focusing_power_of_lens_config()))
