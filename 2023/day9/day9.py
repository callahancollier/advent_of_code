#!/usr/bin/env python3

import csv
import sys
import logging
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from collections import deque


class Iterations:
    def __init__(self, history):
        longest = 0
        iterations = [deque(history)]
        for h in history:
            if len(str(h)) > longest:
                longest = len(str(h))
        while not Day9.is_all_zeroes(iterations[-1]):
            next = deque()
            for i in range(1, len(iterations[-1])):
                next.append(iterations[-1][i] - iterations[-1][i-1])
                if len(str(next[-1])) > longest:
                    longest = len(str(next[-1]))
            iterations.append(next)
        self.iterations = iterations
        self.longest = longest  # for printing purposes

    def __str__(self):
        num_its = len(self.iterations)
        strings = []
        for i in range(num_its):
            string = i*' '
            string += (num_its*' ').join([str(x).rjust(self.longest, ' ') for x in self.iterations[i]])
            strings.append(string)
        return '\n'.join(strings)
    __repr__ = __str__

    def extrapolate(self):
        # start by adding a zero to the end of the list of zeroes
        self.iterations[-1].append(0)
        # now start filling in placeholders (ie, appending to the rest of the lists)
        for i in range(len(self.iterations)-2, -1, -1):
            self.iterations[i].append(
                self.iterations[i][-1] + self.iterations[i+1][-1])
        logging.info("Extrapolated:\n{}".format(self))
        return self.iterations[0][-1]

    def extrapolate_backwards(self):
        # basically the same as extrapolate(), but on the left instead of the right
        self.iterations[-1].appendleft(0)
        for i in range(len(self.iterations)-2, -1, -1):
            self.iterations[i].appendleft(
                self.iterations[i][0] - self.iterations[i+1][0])
        logging.info("Extrapolated backwards:\n{}".format(self))
        return self.iterations[0][0]


class Day9(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.histories = self.parse_file()

    def parse_file(self):
        results = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0]
                results.append([int(x) for x in line.split(' ')])
        return results

    @staticmethod
    def is_all_zeroes(numlist):
        for item in numlist:
            if item != 0:
                return False
        return True

    def predict_next(self, history):
        iterations = [history]
        while not self.is_all_zeroes(iterations[-1]):
            next = []
            for i in range(1, len(iterations[-1])):
                next.append(iterations[-1][i] - iterations[-1][i-1])
            iterations.append(next)

    def sum_next_values(self):
        total = 0
        for history in self.histories:
            iter = Iterations(history)
            logging.info('Examine history: {}'.format(history))
            next_value = iter.extrapolate()
            logging.info('Next value: {}'.format(next_value))
            total += next_value
        return total

    def sum_prev_values(self):
        total = 0
        for history in self.histories:
            iter = Iterations(history)
            logging.info('Examine history: {}'.format(history))
            prev_value = iter.extrapolate_backwards()
            logging.info('Prev value: {}'.format(prev_value))
            total += prev_value
        return total


if __name__ == '__main__':
    parser = getBasicArgparser('day 9')

    args = parser.parse_args()

    day9 = Day9(args.i, args.v)

    print("Part 1: sum of next values for all histories: {}".format(day9.sum_next_values()))
    print("Part 2: sum of prev values for all histories: {}".format(day9.sum_prev_values()))
