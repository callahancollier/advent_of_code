#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
from enum import StrEnum
import pprint
import tqdm
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser, Point, get_area_of_loop, get_num_points_enclosed_by_loop


class Dir(StrEnum):
    LEFT = 'L'
    RIGHT = 'R'
    UP = 'U'
    DOWN = 'D'


MOVE = {
    Dir.LEFT: Point(0, -1),
    Dir.RIGHT: Point(0, 1),
    Dir.UP: Point(-1, 0),
    Dir.DOWN: Point(1, 0)
}

HEX_TO_DIR = {
    '0': Dir.RIGHT,
    '1': Dir.DOWN,
    '2': Dir.LEFT,
    '3': Dir.UP
}


@dataclass
class DigPlanData:
    direction: Dir
    meters: int
    hex_code: str


class Day18(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.dig_plan = self.parse_file()

    def parse_file(self):
        directions = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                d = line[0]
                if d not in [a.value for a in Dir]:
                    logging.error("Unknown direction {}".format(line[0]))
                m = int(line[1])
                h = line[2][2:-1]
                directions.append(DigPlanData(Dir(d), m, h))
        logging.info(pprint.pformat(directions))
        return directions

    def get_true_instructions(self):
        true_directions = []
        for s in self.dig_plan:
            code = s.hex_code
            d = code[-1]
            dist = int(code[:-1], 16)
            true_directions.append(DigPlanData(HEX_TO_DIR[d], dist, ''))
        return true_directions

    def get_loop(self, dig_plan):
        start = Point(0, 0)
        loop = []
        curr = start
        for step in dig_plan:
            for i in range(step.meters):
                curr = curr + MOVE[step.direction]
                loop.append(curr)
        if curr != start:
            logging.warning("Directions didn't make a loop!")
        logging.info("Loop of {}".format(len(loop)))
        return loop

    def get_area_of_lagoon(self):
        loop = self.get_loop(self.dig_plan)
        logging.info(pprint.pformat(loop))
        enclosed = get_num_points_enclosed_by_loop(loop)
        return enclosed + len(loop)

    def get_area_of_hex_lagoon(self):
        dig_plan = self.get_true_instructions()
        # Pick's Theorem and Shoelace formula, but calculate as we go
        shoelace_formula = 0
        curr = Point(0,0)
        n_points = 0
        for step in dig_plan:
            for i in range(step.meters):
                next = curr + MOVE[step.direction]
                shoelace_formula += (curr.row * next.col -
                                     curr.col * next.row)
                curr = next
                n_points += 1
        shoelace_formula = abs(shoelace_formula/2)

        picks_theorem = shoelace_formula - (n_points/2) + 1

        return picks_theorem + n_points


if __name__ == '__main__':
    parser = getBasicArgparser('day 18')

    args = parser.parse_args()

    day18 = Day18(args.i, args.v)

    print("Part 1: Lagoon holds this many cubic meters of lava: {}".format(day18.get_area_of_lagoon()))
    print("Part 2: Lagoon holds this many cubic meters of lava: {}".format(day18.get_area_of_hex_lagoon()))
