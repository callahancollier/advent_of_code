#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


@dataclass
class Card:
    id: int
    winning_numbers: list[int]
    numbers: list[int]

    def get_points(self):
        winners = self.get_num_winning_numbers()
        if winners <= 0:
            return 0
        return 2**(winners - 1)

    def get_num_winning_numbers(self):
        total = 0
        for num in self.numbers:
            if num in self.winning_numbers:
                total += 1
        return total


class Day4(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.cards = self.parse_file()
        self.instances = {}

    def parse_file(self):
        cards = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(':')
                card_info = [x for x in line[0].split(' ') if x] # split and remove empty
                if card_info[0] != 'Card':
                    logging.error(
                        "Malformed input! Expected Card, got {}".format(card_info[0]))
                id = int(card_info[1])
                line = line[1].split('|')
                winners = [x for x in line[0].split(' ') if x]
                numbers = [x for x in line[1].split(' ') if x]
                cards.append(Card(id=id, winning_numbers=winners, numbers=numbers))
                logging.info(cards[-1])
        return cards

    def tally_score_part_1(self):
        total = 0
        for card in self.cards:
            total += card.get_points()
        return total

    def tally_score_part_2(self):
        self.instances = {}
        card_scores = {}
        for card in self.cards:
            card_scores[card.id] = card.get_num_winning_numbers()
            self.instances[card.id] = 0

        for card_id in card_scores:
            self.get_recursive_score(card_scores, card_id)
        logging.info(self.instances)

        total = 0
        for id in self.instances:
            total += self.instances[id]
        return total

    def get_recursive_score(self, card_map, id, recurse_level=0):
        self.instances[id] += 1
        tabs = ' ' * recurse_level
        logging.info(tabs + "-Card {}: {} wins".format(id, card_map[id]))
        for i in range(card_map[id]):
            next = id+1+i
            logging.info(tabs + ' copy {}'.format(next))
            self.get_recursive_score(card_map, next, recurse_level+1)


if __name__ == '__main__':
    parser = getBasicArgparser('day 4')

    args = parser.parse_args()

    day4 = Day4(args.i, args.v)

    print("Part 1: scratchcards are worth {} points".format(day4.tally_score_part_1()))
    print("Part 2: scratchcards are worth {} points".format(day4.tally_score_part_2()))