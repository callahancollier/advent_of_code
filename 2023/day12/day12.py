#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
from itertools import groupby
from copy import deepcopy
from alive_progress import alive_bar
from tqdm import tqdm
from multiprocessing import Pool
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser


OPERATIONAL = '.'
DAMAGED = '#'
UNKNOWN = '?'


@dataclass
class Chunk:
    length: int
    data: str

    def __str__(self):
        return self.length*self.data
    __repr__ = __str__


class Day12(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.spring_info = None
        self.spring_info_chunked = None
        self.records = None
        self.spring_info2 = None
        self.spring_info_chunked2 = None
        self.records2 = None
        self.parse_file(1)

    def parse_file(self, part):
        self.spring_info = []
        self.spring_info_chunked = []
        self.records = []
        self.spring_info2 = []
        self.spring_info_chunked2 = []
        self.records2 = []
        mult = 2
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(' ')
                self.spring_info.append(list(line[0]))
                p2line0 = '?'.join(mult*[line[0]])
                self.spring_info2.append(list(p2line0))
                rec = [int(x) for x in line[1].split(',')]
                self.records.append(rec)
                rec = mult * rec
                self.records2.append(rec)
                chunks = []
                for chunk in [''.join(g) for _,g in groupby(line[0])]:
                    if chunk[0] == OPERATIONAL:
                        # collapse groups of working springs into one
                        chunks.append(Chunk(length=1, data=chunk[0]))
                    else:
                        chunks.append(Chunk(length=len(chunk), data=chunk[0]))
                self.spring_info_chunked.append(chunks)
                chunks2 = []
                for chunk in [''.join(g) for _,g in groupby(line[0])]:
                    if chunk[0] == OPERATIONAL:
                        # collapse groups of working springs into one
                        chunks2.append(Chunk(length=1, data=chunk[0]))
                    else:
                        chunks2.append(Chunk(length=len(chunk), data=chunk[0]))
                self.spring_info_chunked2.append(chunks2)
        for s in self.spring_info_chunked:
            logging.info(s)
        logging.info(self.records)
        logging.info("-------")
        if len(self.spring_info) != len(self.records):
            logging.error("Mismatch between springs and records!")
            sys.exit(1)

    def get_num_arrangements(self, spring_info, record):
        logging.info("Compare {} into {}".format(record, spring_info))
        min_len = sum(record) + len(record)-1

        if len(record) == 0:
            return 1  # not sure if this should be 1 or 0

        if spring_info[0].length < 0 or spring_info[-1].length < 0:
            logging.error("Something went wrong")
            sys.exit(1)

        while spring_info[0].length == 0 or spring_info[0].data == OPERATIONAL or\
                (spring_info[0].data == UNKNOWN and spring_info[0].length < record[0] and spring_info[1].data == OPERATIONAL):
            spring_info = spring_info[1:]
        while spring_info[-1].length == 0 or spring_info[-1].data == OPERATIONAL or\
                (spring_info[-1].data == UNKNOWN and spring_info[-1].length < record[-1] and spring_info[-2].data == OPERATIONAL):
            spring_info = spring_info[:-1]

        logging.info("  trim to {}".format(spring_info))

        # easiest case, only one option
        if sum([x.length for x in spring_info]) == min_len:
            return 1

        # special case
        if len(spring_info) == 3 and len(record) == 1 and spring_info[-1].data == UNKNOWN and spring_info[0].data == UNKNOWN:
            spring_info = spring_info[1:] + [spring_info[0]]

        # take care of the cases where it has to be somewhere first
        if spring_info[0].data == DAMAGED:
            logging.info("Anchor Left")
            if spring_info[0].length > record[0]:
                logging.info("Something bad happened!")
                sys.exit(1)
            else:
                if spring_info[0].length < record[0]:
                    if spring_info[0].length + spring_info[1].length > record[0]:
                        spring_info[1].length -= record[0] - spring_info[0].length
                        spring_info[0].length = record[0]
                    else:
                        spring_info[0].length += spring_info[1].length
                        spring_info = [spring_info[0]] + spring_info[2:]
                        return self.get_num_arrangements(deepcopy(spring_info), record)
                elif spring_info[0].length == record[0] and spring_info[1].data == UNKNOWN:
                    spring_info[1].length -= 1
                return self.get_num_arrangements(deepcopy(spring_info[1:]),
                                                 record[1:])

        if spring_info[-1].data == DAMAGED:
            logging.info("Anchor Right")
            if spring_info[-1].length > record[-1]:
                logging.info("Something bad happened!")
                sys.exit(1)
            else:
                if spring_info[-1].length < record[-1]:
                    if spring_info[-1].length + spring_info[-2].length > record[-1]:
                        spring_info[-2].length -= record[-1] - spring_info[-1].length
                        spring_info[-1].length = record[-1]
                    else:
                        spring_info[-1].length += spring_info[-2].length
                        spring_info = spring_info[:-2] + [spring_info[-1]]
                        return self.get_num_arrangements(deepcopy(spring_info), record)
                elif spring_info[-1].length == record[-1] and spring_info[-2].data == UNKNOWN:
                    spring_info[-2].length -= 1
                return self.get_num_arrangements(deepcopy(spring_info[:-1]),
                                                 record[:-1])

        if len(spring_info) == 1 and len(record) > 1:
            if min_len == spring_info[0].length:
                return 1
            tot = 0
            for i in range(spring_info[0].length-sum(record[1:])):
                new_len = spring_info[0].length - (record[0] + 1 + i)
                logging.info("for loop new len {}".format(new_len))
                if new_len == 0:
                    break
                new_spring_info = [Chunk(length=record[0], data=DAMAGED), Chunk(length=1, data=OPERATIONAL), Chunk(length=new_len, data=UNKNOWN)]
                tot += self.get_num_arrangements(new_spring_info, record)
                logging.info(" new total {}".format(tot))
            return tot

        if len(spring_info) == 1 or spring_info[1].data == OPERATIONAL:
            if record[0] <= spring_info[0].length:
                shifts = spring_info[0].length - record[0] + 1
                spring_info[0] = Chunk(length=record[0], data=DAMAGED)
                logging.info("shifty shift options {}".format(shifts))
                return shifts * self.get_num_arrangements(deepcopy(spring_info), record)
        elif spring_info[1].data == DAMAGED:
            if spring_info[0].length < record[0] and spring_info[1].length < record[0]:
                logging.info("place")
                spring_info[1].length += spring_info[0].length
                return self.get_num_arrangements(deepcopy(spring_info[1:]), record)
            elif spring_info[0].length > record[0]:
                logging.info("Combine and add spacer")
                spring_info = Chunk(length=spring_info[0].length-1, data=spring_info[0].data) +\
                    Chunk(length=1, data=OPERATIONAL) + spring_info[1:]
                return self.get_num_arrangements(deepcopy(spring_info), record)
            else:
                logging.info("Set beginning operational and recurse")
                spring_info[0].data = OPERATIONAL
                return self.get_num_arrangements(deepcopy(spring_info), record)
        else:
            logging.error("Uh oh")
            sys.exit(1)

    def is_valid_oneliner(self, string, record):
        # too slow
        str_record = [len(''.join(g)) for a,g in groupby(''.join(string)) if a=='#']
        return str_record == record

    def is_valid(self, string, record):
        curr = ''
        test = []
        for char in string+[' ']:
            if curr == '':
                curr = char
            else:
                if char != curr[-1]:
                    if curr[-1] == DAMAGED:
                        if len(test) >= len(record) or record[len(test)] != len(curr):
                            return False
                        test.append(len(curr))
                        #logging.info("    append: {}".format(test))
                    curr = char
                else:
                    curr = curr + char
            #logging.info("char {} curr {}".format(char, curr))
        #logging.info("       cmp {} {} for {}".format(test, record, string))
        return test == record

    def is_dead_end(self, string, record):
        curr = ''
        test = []
        for char in string+[' ']:
            if curr == '':
                curr = char
            else:
                if char == UNKNOWN:
                    return False
                if char != curr[-1]:
                    if curr[-1] == DAMAGED:
                        if len(test) >= len(record) or record[len(test)] != len(curr):
                            #print("curr {} test {} record {}".format(curr, test, record))
                            #print("Dead end: {} {}".format(string, record))
                            return True
                        test.append(len(curr))
                    curr = char
                else:
                    curr = curr + char
        return False

    def brute_force(self, spring_string, record):
        arrangements = 0
        n_unknown = len([x for x in spring_string if x == UNKNOWN])
        logging.info("{} unknowns for {}".format(n_unknown, spring_string))
        #for i in tqdm(range(2**n_unknown), leave=False):
        for i in range(2**n_unknown):
            test = bin(i)[2:].zfill(n_unknown)
            new_str = deepcopy(spring_string)
            logging.info("   {} for {}".format(test, ''.join(new_str)))
            is_dead_end = False
            for c in range(len(new_str)):
                if new_str[c] == UNKNOWN:
                    if test[0] == '1':
                        new_str[c] = OPERATIONAL
                    elif test[0] == '0':
                        new_str[c] = DAMAGED
                    else:
                        logging.error("Something has gone wrong")
                        sys.exit(1)
                    test = test[1:]
                    is_dead_end = self.is_dead_end(new_str, record)
                    if is_dead_end:
                        break
            if is_dead_end:
                continue
            if len(test) != 0:
                logging.error("Something didn't line up right: {}".format(test))
                sys.exit(1)
            logging.info("    test {}".format(''.join(new_str)))
            if self.is_valid(new_str, record):
                logging.info("    yup")
                arrangements += 1
        return arrangements

    def tally_num_arrangements(self):
        total = 0
        for i in range(len(self.records)):
            logging.info('---')
            logging.info('Row {}'.format(i+1))
            num = self.get_num_arrangements(deepcopy(self.spring_info_chunked[i]), self.records[i])
            logging.info(" {} arrangements".format(num))
            total += num
        return total

    def brute_force_part_1(self):
        total = 0
        with alive_bar(len(self.spring_info)) as bar:
            for s in range(len(self.spring_info)):
                total += self.brute_force(self.spring_info[s], self.records[s])
                bar()
        return total

    def fancy_math(self, s):
        single_sum = self.brute_force(self.spring_info[s], self.records[s])
        doubled_sum = self.brute_force(self.spring_info2[s], self.records2[s])
        ratio = doubled_sum / single_sum
        # print("{}: single {} double {}".format(s, single_sum, doubled_sum))
        # By observation, this is the relationship
        print("Done {}".format(s))
        return single_sum * (ratio ** 4)  # I assume the 4 is because it's multipled by 5 (N-1)?

    def part_2(self, n_processes):
        with Pool(n_processes) as p:
            total = sum(p.imap(self.fancy_math, range(len(self.spring_info))))
            #total = sum(tqdm(p.imap(self.fancy_math, range(len(self.spring_info))), total=len(self.spring_info)))
        return total

    def part_2_deprecated(self):
        total = 0
        for s in tqdm(range(len(self.spring_info)), desc="Lines"):
            single_sum = self.brute_force(self.spring_info[s], self.records[s])
            doubled_sum = self.brute_force(self.spring_info2[s], self.records2[s])
            ratio = doubled_sum / single_sum
            #print("{}: single {} double {}".format(s, single_sum, doubled_sum))
            # By observation, this is the relationship
            total += single_sum * (ratio**4) # I assume the 4 is because it's multipled by 5 (N-1)
        return total


if __name__ == '__main__':
    parser = getBasicArgparser('day 12')
    parser.add_argument('-n', type=int, default=None, help='Number of cores to run on in parallel for part 2')

    args = parser.parse_args()

    day12 = Day12(args.i, args.v)

    #print("Part 1: Total arrangements is {}".format(day12.brute_force_part_1()))
    day12.parse_file(2)
    print("Part 2: Total arrangements is {}".format(day12.part_2(args.n)))

    # After ~10 hours, on 33/1000