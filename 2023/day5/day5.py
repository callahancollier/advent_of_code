#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
from enum import StrEnum
import pprint
#from tqdm import tqdm
from alive_progress import alive_bar
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser

class Type(StrEnum):
    SEED = 'seed'
    SOIL = 'soil'
    FERTILIZER = 'fertilizer'
    WATER = 'water'
    LIGHT = 'light'
    TEMPERATURE = 'temperature'
    HUMIDITY = 'humidity'
    LOCATION = 'location'


@dataclass
class Range:
    start: int
    num: int


@dataclass
class Map:
    src : int
    dst : int
    rng : int
    def is_in_range(self, value: int):
        if value < self.src:
            return False
        idx = value - self.src
        return idx < self.rng
    def convert(self, value: int):
        idx = value - self.src
        if idx < self.rng:
            result = self.dst + idx
            logging.info("    idx {} from src {} -> dst {} = {}".format(idx, self.src, self.dst, result))
            return result
        raise ValueError

@dataclass
class AlmanacMap:
    src : str
    dst : str
    map : list[Map]


class Day5(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.seeds = []
        self.range_seeds = []
        self.num_ranged_seeds = 0
        self.maps = {}
        self.parse_file()
        self.interpret_seeds_as_ranges()

    def parse_file(self):
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            reading_heading = True
            mapinfo = None
            maplist = None
            linenum = 0
            for line in reader:
                linenum += 1
                if len(line) == 0:
                    reading_heading = True
                    if mapinfo is None or maplist is None:
                        continue
                    # finalize map
                    self.maps[mapinfo[0]] = AlmanacMap(src=mapinfo[0], dst=mapinfo[2], map=maplist)
                    mapinfo = None
                    maplist = None
                    continue
                line = line[0]
                if reading_heading:
                    line = line.split(':')
                    if len(self.seeds) == 0:
                        if line[0] != 'seeds':
                            logging.error("Malformed input file! {}".format(line))
                            sys.exit(1)
                        self.seeds = [int(x) for x in line[1].split(' ') if x]
                        logging.info("got seeds {}".format(self.seeds))
                    else:
                        if not line[0].endswith('map'):
                            logging.error("Malformed input file: {}".format(line))
                            sys.exit(1)
                        mapinfo = line[0].split(' ')[0].split('-')
                        if len(mapinfo) != 3:
                            logging.error("malformed mapinfo {}".format(mapinfo))
                            sys.exit(1)
                        if mapinfo[1] != 'to':
                            logging.error("malformed mapinfo: {}".format(mapinfo))
                            sys.exit(1)
                    reading_heading = False
                else:
                    data = line.split(' ')
                    if len(data) != 3:
                        logging.error("Malformed input data {}".format(data))
                        sys.exit(1)
                    if maplist is None:
                        maplist = []
                    maplist.append(Map(src=int(data[1]), dst=int(data[0]), rng=int(data[2])))
                    logging.info("Map {}: {}".format(data, maplist))
        if mapinfo is not None and maplist is not None:
            self.maps[mapinfo[0]] = AlmanacMap(src=mapinfo[0], dst=mapinfo[2], map=maplist)
        logging.info(pprint.pformat(self.maps))

    def interpret_seeds_as_ranges(self):
        if len(self.seeds) % 2 != 0:
            logging.error("Malformed input, there are {} seeds".format(len(self.seeds)))
        self.num_ranged_seeds = 0
        idx = 0
        self.range_seeds = []
        while idx < len(self.seeds):
            self.range_seeds.append(Range(start=self.seeds[idx], num=self.seeds[idx+1]))
            self.num_ranged_seeds += self.seeds[idx+1]
            idx += 2
        logging.info("{} ranged seeds: {}".format(self.num_ranged_seeds, self.range_seeds))

    def get_location(self, seed):
        id = Type.SEED
        current_val = seed
        logging.info("Seed {}".format(seed))
        while id != Type.LOCATION:
            map = self.maps[id]
            for m in map.map:
                if m.is_in_range(current_val):
                    current_val = m.convert(current_val)
                    break
            #unmapped values stay the same
            id = map.dst
            logging.info("  {} {}".format(id, current_val))
        logging.info('===')
        return current_val

    def get_lowest_location_number_part_1(self):
        lowest = None
        for seed in self.seeds:
            location = self.get_location(seed)
            logging.info("Seed {}: location {}".format(seed, location))
            if lowest is None or location < lowest:
                lowest = location
        return lowest

    def get_lowest_location_number_part_2(self):
        lowest = None
        with alive_bar(self.num_ranged_seeds) as bar:
            for rng in self.range_seeds:
                for seed in range(rng.start, rng.start+rng.num):
                    location = self.get_location(seed)
                    logging.info("Seed {}: location {}".format(seed, location))
                    if lowest is None or location < lowest:
                        lowest = location
                    bar()
        return lowest



if __name__ == '__main__':
    parser = getBasicArgparser('day 5')
    parser.add_argument('-p', type=int, help="Which part to run", default=0)

    args = parser.parse_args()

    day5 = Day5(args.i, args.v)

    if args.p == 1 or args.p == 0:
        print("Part 1: Lowest location number is {}".format(day5.get_lowest_location_number_part_1()))
    if args.p == 2 or args.p == 0:
        print("Part 2: Lowest location number is {}".format(day5.get_lowest_location_number_part_2()))
