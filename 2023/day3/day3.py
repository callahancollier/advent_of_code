#!/usr/bin/env python3

import csv
import sys
import logging
import numpy
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from dataclasses import dataclass

@dataclass(frozen=True)
class Coordinate:
    row : int
    col : int


@dataclass(frozen=True)
class NumberEntry:
    value : int
    coordinates : list[Coordinate]


class Day3(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.schematic = self.parse_file()
        self.log_schematic()
        self.numbers, self.coord_map = self.get_number_coordinates()

    def log_schematic(self):
        for row in self.schematic:
            logging.info(''.join(row))

    def parse_file(self):
        schematic = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                row = list(line[0])
                schematic.append(row)
        return schematic

    def isSymbol(self, character: str):
        return not character.isdigit() and not (character == '.')

    def isGear(self, character: str):
        return character == '*'

    def get_number_coordinates(self):
        numbers = []
        coord_map = {}
        for row_idx in range(len(self.schematic)):
            row = self.schematic[row_idx]
            col_idx = 0
            while col_idx < len(row):
                if row[col_idx].isdigit():
                    coordinates = [Coordinate(row_idx, col_idx)]
                    value = int(row[col_idx])
                    col_idx += 1
                    while col_idx < len(row) and row[col_idx].isdigit():
                        coordinates.append(Coordinate(row_idx, col_idx))
                        value = (value * 10) + int(row[col_idx])
                        col_idx += 1
                    number = NumberEntry(value=value, coordinates=coordinates)
                    numbers.append(number)
                    for coord in coordinates:
                        coord_map[coord] = number
                col_idx += 1
        logging.info("Found numbers:")
        for num in numbers:
            logging.info("  {} at {}".format(num.value, num.coordinates))
        return numbers, coord_map

    def find_part_numbers(self):
        part_numbers = []
        for row in range(len(self.schematic)):
            for col in range(len(self.schematic[0])):
                if self.isSymbol(self.schematic[row][col]):
                    for adj in self.get_adjacent_part_numbers(row, col):
                        if adj not in part_numbers:
                            part_numbers.append(adj)
        return part_numbers

    def get_adjacent_part_numbers(self, row, col):
        adjacent = []
        for r in range(row - 1, row + 2):
            for c in range(col - 1, col + 2):
                coord = Coordinate(r, c)
                if coord in self.coord_map:
                    pn = self.coord_map[coord]
                    if pn not in adjacent:
                        adjacent.append(pn)
        return adjacent

    def sum_pn_values(self):
        pns = self.find_part_numbers()
        total = 0
        for pn in pns:
            total += pn.value
        return total

    def find_gear_ratio_sums(self):
        total = 0
        for row in range(len(self.schematic)):
            for col in range(len(self.schematic[0])):
                if self.isGear(self.schematic[row][col]):
                    logging.info("Potential gear at {}".format(Coordinate(row,col)))
                    adj = self.get_adjacent_part_numbers(row, col)
                    logging.info("   {} Adjacent part numbers: {}".format(len(adj), adj))
                    if len(adj) == 2:
                        gear_ratio = adj[0].value * adj[1].value
                        logging.info(" Gear ratio: {}".format(gear_ratio))
                        total += gear_ratio
        return total

if __name__ == '__main__':
    parser = getBasicArgparser('day 3')

    args = parser.parse_args()

    day3 = Day3(args.i, args.v)
    print("Part 1: Sum of all part numbers is {}".format(day3.sum_pn_values()))
    print("Part 2: Sum of gear ratios is {}".format(day3.find_gear_ratio_sums()))
