#!/usr/bin/env python3

import csv
import sys
import logging
from dataclasses import dataclass
sys.path.append('../..')  # TODO how to do this right?
from common.base_class import BaseFunctions, getBasicArgparser
from tqdm import tqdm
import math


@dataclass
class Race:
    time: int
    distance: int


class Day6(BaseFunctions):
    def __init__(self, input_file, enable_debug):
        super().__init__(input_file, enable_debug)
        self.races = None
        self.part_2_race = None
        self.parse_file()

    def parse_file(self):
        times = []
        distances = []
        with open(self.input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter='\n')
            for line in reader:
                line = line[0].split(':')
                if line[0] == 'Time':
                    times = [int(x) for x in line[1].split(' ') if x]
                elif line[0] == 'Distance':
                    distances = [int(x) for x in line[1].split(' ') if x]
                else:
                    logging.error("Malformed input: {}".format(line))
        if len(times) != len(distances):
            logging.error("Uneven input!")
        races = []
        for i in range(len(times)):
            races.append(Race(time=times[i], distance=distances[i]))
        logging.info(races)

        comb_times = int(''.join([str(x) for x in times]))
        comb_distances = int(''.join([str(x) for x in distances]))

        self.races = races
        self.part_2_race = Race(time=comb_times, distance=comb_distances)

    def brute_force_ways_to_win(self, race):
        ways_to_win = 0
        logging.info(race)
        for hold_time in tqdm(range(1, race.time)):
            total_distance = hold_time * (race.time - hold_time)
            logging.info('hold for {} ms: go {} mm'.format(hold_time, total_distance))
            if total_distance > race.distance:
                ways_to_win += 1
        logging.info('-- {} ways to win'.format(ways_to_win))
        return ways_to_win

    # distance_gone = hold_time * (race_time - hold_time)
    # hold_time=h, race_time=t, distance_gone=d
    # d = h * (t - h)
    # d = ht - h^2
    # solve for distance_gone > record
    # ht - h^2 > record
    # 0 > h^2 - ht + record
    # solve for the two points where the equation crosses zero
    # quadratic formula: x = (-b +/- sqrt(b^2 - 4ac)) / 2a for ax^2 + bx + c = 0
    # a=1, b=-t, c=record
    # => ans_1 = (t + sqrt( t^2 - 4*record)) / 2
    # => ans_2 = (t - sqrt( t^2 - 4*record)) / 2
    # Then find the number of possible values of h between those two points (inclusive)
    def quad_form_ways_to_win(self, race):
        point_a = (race.time + math.sqrt(race.time**2 - (4*race.distance))) / 2
        point_a = math.ceil(point_a) - 1  # Do some tricks because you need to beat the record, not just match it
        point_b = (race.time - math.sqrt(race.time**2 - (4*race.distance))) / 2
        point_b = math.floor(point_b) + 1  # Do some tricks because you need to beat the record, not just match it
        num_possible_wins = abs(point_a - point_b) + 1  # inclusive so add 1
        logging.info(race)
        logging.info("  wins from {} to {}: {} total".format(point_b, point_a, num_possible_wins))
        return num_possible_wins

    def part1(self, brute_force=False):
        multiplied_ways_to_win = 1
        for race in tqdm(self.races):
            if brute_force:
                ways_to_win = self.brute_force_ways_to_win(race)
            else:
                ways_to_win = self.quad_form_ways_to_win(race)
            multiplied_ways_to_win *= ways_to_win
        return multiplied_ways_to_win

    def part2(self, brute_force=False):
        multiplied_ways_to_win = 1
        if brute_force:
            ways_to_win = self.brute_force_ways_to_win(self.part_2_race)
        else:
            ways_to_win = self.quad_form_ways_to_win(self.part_2_race)
        multiplied_ways_to_win *= ways_to_win
        return multiplied_ways_to_win


if __name__ == '__main__':
    parser = getBasicArgparser('day 6')
    parser.add_argument('-b', action='store_true', help='Do the brute force solution')

    args = parser.parse_args()

    day6 = Day6(args.i, args.v)

    print('Part 1: Number of ways to win, multiplied: {}'.format(day6.part1(args.b)))
    print('Part 2: Number of ways to win, multiplied: {}'.format(day6.part2(args.b)))
